package org.codeling.utils.errorcontainers;

public enum Level {
	ERROR, WARNING, INFO
}
