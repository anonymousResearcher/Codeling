package org.codeling.utils;

import java.io.File;
import java.nio.file.Path;

import org.eclipse.core.resources.ResourcesPlugin;

public class CodelingConfiguration {

	public static String TEMPORARY_MODEL_PROJECT_NAME = "architecture-carrying-code-temp";
	public static String TEMPORARY_MODEL_DEBUG_DIRECTORY_NAME = "debug";
	public static String TEMPORARY_MODELDIR_PATH = TEMPORARY_MODEL_PROJECT_NAME + File.separator;
	public static String DEBUG_MODELDIR_PATH = TEMPORARY_MODEL_PROJECT_NAME + File.separator
			+ TEMPORARY_MODEL_DEBUG_DIRECTORY_NAME + File.separator;

	public static Path getTemporaryModelProjectPath() {
		return ResourcesPlugin.getWorkspace().getRoot().getProject(TEMPORARY_MODELDIR_PATH).getLocation().toFile()
				.toPath();
	}
}
