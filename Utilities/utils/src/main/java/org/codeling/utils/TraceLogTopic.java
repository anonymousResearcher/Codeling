package org.codeling.utils;

/**
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public enum TraceLogTopic {

	TASK_EXECUTION("org.codeling.transformationmanager/debug/execution"),
	ID_MAPPING("org.codeling.transformationmanager/debug/id_mapping"),
	M2C_EXECUTED_TRANSFORMATIONS("org.codeling.lang.base.java/debug/m2c_executed_transformations"),
	FIND_TRANSLATED_ELEMENT_QUERY("org.codeling.lang.base.java/debug/find_translated_element_query"),
	FIND_TRANSLATED_ELEMENT_FOUND("org.codeling.lang.base.java/debug/find_translated_element_found"),
	FIND_TRANSLATED_ELEMENT_EVENT_CREATED("jorg.codeling.lang.base.java/debug/find_translated_element_created_event"),
	FIND_TRANSLATED_ELEMENT_FINISHED("org.codeling.lang.base.java/debug/find_translated_element_finished");

	String topic;

	private TraceLogTopic(String topic) {
		this.topic = topic;
	}

	public String getValue() {
		return topic;
	}
}
