package org.codeling.examples.resource_demand;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.codeling.examples.case_studies.ExtendedRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(ExtendedRunner.class)
public class BackupResourceDemandTestFiles {
	private final static Path BASE_PATH = Paths.get("/Users/mmueller/git/codeling/Examples/Resource_Demand/");
	private Path resultsPath = BASE_PATH.resolve("results");
	private Path telemetryFile = BASE_PATH.resolve("Memory_telemetry.csv");
	private Path mergedResultsFile = BASE_PATH.resolve("resource_demand_test_results_with_memory.csv");

	@Test
	public void backupAndMergeTelemetryData() throws IOException {
		Files.list(resultsPath).filter(p -> p.toString().endsWith("resource_demand_test_results.csv"))
				.forEach(csvFile -> {
					String fileName = csvFile.getFileName().toString();
					String[] fileNameFragments = fileName.split("-");
					String timestamp = fileNameFragments[0];
					String size = fileNameFragments[1];
					String run = fileNameFragments[2];
					String prefix = timestamp + "-" + size + "-" + run + "-";

					if (csvFile.toFile().exists() && telemetryFile.toFile().exists())
						try {
							// Create merged file
							addMaxMemoryToResultCVS(csvFile);

							// Create target directory
							Path targetDirectory = Files
									.createDirectory(resultsPath.resolve("size-" + size + "-run-" + run));

							// Move csv file
							if (csvFile.toFile().exists())
								Files.move(csvFile, targetDirectory.resolve(csvFile.getFileName()));
							// Move merged File
							Files.move(mergedResultsFile,
									targetDirectory.resolve(prefix + mergedResultsFile.getFileName()));
							// Copy telemetry file as backup
							Files.copy(telemetryFile, targetDirectory.resolve(prefix + telemetryFile.getFileName()));
							// Move result models
							Path resultModelsPath = resultsPath.resolve(prefix + "result-models");
							if (resultModelsPath.toFile().exists())
								Files.move(resultModelsPath, targetDirectory.resolve(resultModelsPath.getFileName()));
						} catch (IOException e) {
							e.printStackTrace();
							fail();
						}
				});
	}

	private void addMaxMemoryToResultCVS(Path csvFile) throws IOException {
		List<String> results = null;
		try (Stream<String> resultStream = Files.lines(csvFile)) {
			results = resultStream.map(resultLine -> {
				if (resultLine.startsWith("Size"))
					return resultLine + ",Max Memory"; // header
				String[] cols = resultLine.split(",");
				long uptimeAtStart = Long.parseLong(cols[4]);
				long uptimeAtEnd = Long.parseLong(cols[5]);
				long maxMemory = findMaxMemory(uptimeAtStart, uptimeAtEnd);
				return new String(resultLine + "," + maxMemory);
			}).collect(Collectors.toList());
		}

		Files.write(mergedResultsFile, results);
	}

	private long findMaxMemory(long uptimeAtStart, long uptimeAtEnd) {
		try (Stream<String> telemetryStream = Files.lines(telemetryFile)) {
			String lineWithMaxMemory = telemetryStream.filter(line -> {
				if (line.startsWith("\"Time"))
					return false; // Ignore header
				String[] cols = line.split(",");
				long time = (long) (Float.parseFloat(cols[0]) * 1000); // they are stored as seconds, we need millis.
				return time >= uptimeAtStart && time <= uptimeAtEnd; // All entries within the given time interval
			}).max(Comparator.comparing(line -> {
				return Long.parseLong(line.split(",")[3]);
			})).orElse(",,,-1");
			return Long.parseLong(lineWithMaxMemory.split(",")[3]);
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
	}
}
