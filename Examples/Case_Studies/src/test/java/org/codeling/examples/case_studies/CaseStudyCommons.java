package org.codeling.examples.case_studies;

import static org.junit.Assert.fail;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;

public class CaseStudyCommons {
	public IProject importProject(String projectFilePath) throws CoreException {
		if (projectFilePath == null)
			fail("projectFilePath is not set. Please provide paths to the test projects via the arguments \"-cocome-path=...\" or \"-jack-path=...\".");

		IProjectDescription description = ResourcesPlugin.getWorkspace()
				.loadProjectDescription(new Path(projectFilePath));
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(description.getName());
		if (!project.exists())
			project.create(description, null);
		if (!project.isOpen())
			project.open(null);

		return project;
	}

	public IProject copyImportProject(String projectFilePath) throws CoreException {
		IProjectDescription description = ResourcesPlugin.getWorkspace()
				.loadProjectDescription(new Path(projectFilePath));
		String originalProjectname = description.getName();
		String tmpProjectName = originalProjectname + "-temp";
		description.setName(tmpProjectName);
		IProject baseProject = ResourcesPlugin.getWorkspace().getRoot().getProject(tmpProjectName);
		if (!baseProject.exists())
			baseProject.create(description, null);
		if (!baseProject.isOpen())
			baseProject.open(null);

		// Copy the project, so that changes in the code will not affect the original
		IPath newPath = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(originalProjectname);
		baseProject.copy(newPath.removeFirstSegments(newPath.segmentCount() - 1), true, null);
		baseProject.delete(false, true, null);

		return ResourcesPlugin.getWorkspace().getRoot().getProject(originalProjectname);
	}

	public String getCoCoMEPath() {
		return getValue("-cocome-path");
	}

	public String getJACKPath() {
		return getValue("-jack-path");
	}

	private String getValue(String key) {
		for (String arg : Platform.getApplicationArgs())
			if (arg.startsWith(key + "=")) {
				String value = arg.substring(key.length() + 1);
				if (value.startsWith("\""))
					value = value.substring(1);
				if (value.endsWith("\""))
					value = value.substring(0, value.length() - 2);
				return value;
			}
		return null;
	}
}
