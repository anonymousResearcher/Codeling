package org.codeling.examples.case_studies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.codeling.lang.cocome.CoCoMELanguageDefinition;
import org.codeling.lang.uml.UMLLanguageDefinition;
import org.codeling.transformationmanager.ITransformationManager;
import org.codeling.utils.CodelingException;
import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageableElement;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CoCoME_UML_CaseStudy extends MayHaveIssues {

	private static List<EObject> umlModel;

	@Test
	public void _1_translateToUMLModel() throws CoreException, CodelingException, IOException {
		assert (false) : "Checking java assertion must not be switched on during these tests, because HenshinTGG will not work in this mode. Please do not use -ea as VM parameters";

		CaseStudyCommons c = new CaseStudyCommons();
		IJavaProject mainProject = JavaCore.create(c.copyImportProject(c.getCoCoMEPath()));
		LinkedList<IJavaProject> projects = new LinkedList<>();
		projects.addAll(Arrays.asList(mainProject));

		// Translate code to PCM model
		CoCoMELanguageDefinition implementationLanguage = new CoCoMELanguageDefinition();
		UMLLanguageDefinition specificationLanguage = new UMLLanguageDefinition();

		long startToModel = System.nanoTime();
		umlModel = ITransformationManager.createInstance().extractModelFromCode(implementationLanguage,
				specificationLanguage, null, projects);
		long endToModel = System.nanoTime();
		System.out.println("Required time for translation to model: " + (endToModel - startToModel) + " nano seconds.");
	}

	@Test
	public void _2_verifyModel() {
		Model model = getModel(umlModel);
		EList<PackageableElement> packaedElements = model.getPackagedElements();
		assertEquals(15, packaedElements.size());
		assertEquals(1, model.getOwnedComments().size());
	}

	@Test
	public void _4_verifyComponents() {
		Model model = getModel(umlModel);
		EList<PackageableElement> components = model.getPackagedElements();
		for (PackageableElement subcomponent : components) {
			if(!(subcomponent instanceof Component))
				continue;
			switch (subcomponent.getName()) {
			case "ProductDispatcherServer":
				verifyComponentProductDispatcher(subcomponent);
				break;
			case "ReportingServer":
				verifyComponentReportingServer(subcomponent);
				break;
			case "StoreServer":
				verifyComponentStoreServer(subcomponent);
				break;
			case "ReportingConsole":
				verifyComponentReportingConsole(subcomponent);
				break;
			case "StoreConsole":
				verifyComponentStoreConsole(subcomponent);
				break;
			case "TrivialBankServer":
				verifyComponentTrivialBank(subcomponent);
				break;
			case "CashBoxModel":
				verifyComponentCashBox(subcomponent);
				break;
			case "ExpressLightModel":
				verifyComponentExpressLight(subcomponent);
				break;
			case "cashdeskline":
				verifyComponentCashdeskline(subcomponent);
				break;
			case "BarcodeScannerModel":
				verifyComponentBarcodeScanner(subcomponent);
				break;
			case "CardReaderModel":
				verifyComponentCardReader(subcomponent);
				break;
			case "PrinterModel":
				verifyComponentPrinter(subcomponent);
				break;
			case "UserDisplayModel":
				verifyComponentUserDisplay(subcomponent);
				break;
			case "CoordinatorModel":
				verifyComponentCoordinator(subcomponent);
				break;
			case "CashDeskModel":
				verifyComponentCashDesk(subcomponent);
				break;
			case "CashDeskModel_Internal":
				verifyComponentCashDeskModel_Internal(subcomponent);
				break;
			case "console":
				verifyComponentConsole(subcomponent);
				break;
			case "application":
				verifyComponentApplication(subcomponent);
				break;
			case "inventory":
				verifyComponentInventory(subcomponent);
				break;
			}
		}
	}

	private void verifyIsComponent(PackageableElement packagableElement) {
		assertTrue("PackagableElement " + packagableElement.getName() + " must be a Component, but is not.",
				packagableElement instanceof PackageableElement);
	}

	private void verifyComponentInventory(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;

		verifyHasSubcomponents(component, "console", "application");
		verifyRequiresNone(component);
	}

	private void verifyComponentApplication(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;

		verifyHasSubcomponents(component, "StoreServer", "ReportingServer", "ProductDispatcherServer");
		verifyRequiresNone(component);
	}

	private void verifyComponentConsole(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;

		verifyHasSubcomponents(component, "StoreConsole", "ReportingConsole");
		verifyProvidesNone(component);
	}

	private void verifyComponentCashDesk(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;

		verifyHasSubcomponents(component, "ExpressLightModel", "CashBoxModel", "BarcodeScannerModel", "CardReaderModel",
				"PrinterModel", "UserDisplayModel", "CashDeskModel_Internal");
	}

	private void verifyComponentCoordinator(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesNone(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentUserDisplay(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentPrinter(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentCardReader(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentBarcodeScanner(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesOnlyStandardInterface(component);
	}

	private void verifyComponentCashDeskModel_Internal(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesNone(component);
		verifyRequires(component, "IBank", "IStoreInventory");
	}

	private void verifyComponentCashdeskline(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;

		verifyHasSubcomponents(component, "CashDeskModel", "CoordinatorModel");
	}

	private void verifyComponentExpressLight(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentCashBox(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentTrivialBank(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvides(component, "IBank");
		verifyRequiresNone(component);
	}

	private void verifyComponentStoreServer(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvides(component, "IStoreInventoryManager", "IStoreInventory");
		verifyRequires(component, "IProductDispatcher");
	}

	private void verifyComponentStoreConsole(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesNone(component);
		verifyRequires(component, "IStoreInventoryManager");
	}

	private void verifyComponentReportingServer(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentReportingConsole(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesNone(component);
		verifyRequires(component, "IReporting");
	}

	private void verifyComponentProductDispatcher(PackageableElement packagableElement) {
		verifyIsComponent(packagableElement);
		Component component = (Component) packagableElement;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyProvidesOnlyStandardInterface(Component component) {
		String interfaceName = "I";
		if (component.getName().endsWith("Model")) {
			interfaceName += component.getName().substring(0, component.getName().length() - "Model".length());
		} else if (component.getName().endsWith("Server")) {
			interfaceName += component.getName().substring(0, component.getName().length() - "Server".length());
		} else if (component.getName().endsWith("Console")) {
			interfaceName += component.getName().substring(0, component.getName().length() - "Console".length());
		}
		verifyProvides(component, interfaceName);
	}

	private boolean verifyProvidesNone(Component component) {
		return component.getInterfaceRealizations().isEmpty();
	}

	private boolean verifyRequiresNone(Component component) {
		return component.getUsedInterfaces().isEmpty();
	}

	private void verifyProvides(Component component, String... interfaceName) {
		LinkedList<String> missing = new LinkedList<>();
		for (String iface : interfaceName) {
			boolean found = false;
			for (Interface i : component.allRealizedInterfaces()) {
				if (i.getName().equals(iface)) {
					found = true;
					break;
				}
			}
			if (!found)
				missing.add(iface);
		}

		assertEquals("The following interfaces are not provided by the component " + component.getName() + ": "
				+ missing.stream().collect(Collectors.joining(",")), 0, missing.size());

		LinkedList<String> tooMany = new LinkedList<>();
		for (Interface i : component.allRealizedInterfaces()) {
			boolean found = false;
			for (String iface : interfaceName) {
				if (i.getName().equals(iface)) {
					found = true;
					break;
				}
			}
			if (!found)
				tooMany.add(i.getName());
		}
		assertEquals("The following interfaces should not be provided by the component " + component.getName() + ": "
				+ tooMany.stream().collect(Collectors.joining(", ")), 0, tooMany.size());
	}

	private void verifyRequires(Component component, String... interfaceName) {
		LinkedList<String> missing = new LinkedList<>();
		for (String iface : interfaceName) {
			boolean found = false;
			for (Interface i : component.allUsedInterfaces()) {
				if (i.getName().equals(iface)) {
					found = true;
					break;
				}
			}
			if (!found)
				missing.add(iface);
		}

		assertEquals("The following interfaces are not provided by the component " + component.getName() + ": "
				+ missing.stream().collect(Collectors.joining(",")), 0, missing.size());

		LinkedList<String> tooMany = new LinkedList<>();
		for (Interface i : component.allUsedInterfaces()) {
			boolean found = false;
			for (String iface : interfaceName) {
				if (i.getName().equals(iface)) {
					found = true;
					break;
				}
			}
			if (!found)
				tooMany.add(i.getName());
		}
		assertEquals("The following interfaces should not be provided by the component " + component.getName() + ": "
				+ tooMany.stream().collect(Collectors.joining(", ")), 0, tooMany.size());
	}

	private void verifyHasSubcomponents(Component component, String... expectedSubcomponents) {
		List<String> remainingComponents = new LinkedList<>();
		remainingComponents.addAll(Arrays.asList(expectedSubcomponents));
		List<Component> actualSubcomponents = new LinkedList<>();
		for (PackageableElement packageableElement : component.getPackagedElements()) {
			if (!(packageableElement instanceof Component))
				continue;
			actualSubcomponents.add((Component) packageableElement);

			String subcomponentFound = null;
			for (String expectedContext : remainingComponents) {
				if (expectedContext.equals(packageableElement.getName())) {
					subcomponentFound = expectedContext;
					break;
				}
			}
			if (subcomponentFound != null) {
				remainingComponents.remove(subcomponentFound);
			}

		}

		assertTrue("The following subcomponents do not exist in the component " + component.getName() + ": "
				+ remainingComponents.stream().collect(Collectors.joining(",")), remainingComponents.isEmpty());
		assertEquals("The component " + component + " must contain " + expectedSubcomponents.length + " subcomponents.",
				expectedSubcomponents.length, actualSubcomponents.size());
	}

	@Test
	public void _5_verifyInterfaces() {
		Model model = getModel(umlModel);
		List<PackageableElement> interfaces = model.getPackagedElements().stream().filter(p -> p instanceof Interface)
				.collect(Collectors.toList());
		for (PackageableElement p : interfaces) {
			Interface iface = (Interface) p;
			switch (iface.getName()) {
			case "IProductDispatcher":
				verifyInterfaceIProductDispatcher(iface);
				break;
			case "IReporting":
				verifyInterfaceIReporting(iface);
				break;
			case "IStoreInventoryManager":
				verifyInterfaceIStoreInventoryManager(iface);
				break;
			case "IStoreInventory":
				verifyInterfaceIStoreInventory(iface);
				break;
			case "IBarcodeScanner":
				verifyInterfaceIBarcodeScanner(iface);
				break;
			case "ICardReader":
				verifyInterfaceICardReader(iface);
				break;
			case "ICashBox":
				verifyInterfaceICashBox(iface);
				break;
			case "IExpressLight":
				verifyInterfaceIExpressLight(iface);
				break;
			case "IPrinter":
				verifyInterfaceIPrinter(iface);
				break;
			case "IUserDisplay":
				verifyInterfaceIUserDisplay(iface);
				break;
			case "IBank":
				verifyInterfaceIBank(iface);
				break;
			default:
				fail("Found an unexpected interface " + iface.getName());
				break;
			}
		}
	}

	private void verifyOperationsExist(Interface opIface, String... operationNames) {
		List<String> remainingNames = new LinkedList<>();
		remainingNames.addAll(Arrays.asList(operationNames));
		for (Operation operation : opIface.getOperations()) {
			String nameFound = null;
			for (String expectedName : remainingNames) {
				if (expectedName.equals(operation.getName())) {
					nameFound = expectedName;
					break;
				}
			}
			if (nameFound != null)
				remainingNames.remove(nameFound);
		}
		assertEquals("The following operations do not exist in the interface " + opIface.getName() + ": "
				+ remainingNames.stream().collect(Collectors.joining(", ")), 0, remainingNames.size());
		assertEquals(operationNames.length, opIface.getOperations().size());
	}

	private void verifyInterfaceIBank(Interface iface) {
		verifyOperationsExist(iface, "validateCard", "debitCard");
	}

	private void verifyInterfaceIUserDisplay(Interface iface) {
		verifyOperationsExist(iface, "setContent", "getMessage", "getMessageKind");
	}

	private void verifyInterfaceIPrinter(Interface iface) {
		verifyOperationsExist(iface, "tearOffPrintout", "printText", "getCurrentPrintout");
	}

	private void verifyInterfaceIExpressLight(Interface iface) {
		verifyOperationsExist(iface, "turnExpressLightOn", "turnExpressLightOff", "isOn");
	}

	private void verifyInterfaceICashBox(Interface iface) {
		verifyOperationsExist(iface, "open", "close", "isOpen", "pressControlKey", "pressNumpadKey");
	}

	private void verifyInterfaceIReporting(Interface iface) {
		verifyOperationsExist(iface, "getStoreStockReport", "getEnterpriseStockReport", "getEnterpriseDeliveryReport");
	}

	private void verifyInterfaceICardReader(Interface iface) {
		verifyOperationsExist(iface, "sendCreditCardInfo", "sendCreditCardPin");
	}

	private void verifyInterfaceIBarcodeScanner(Interface iface) {
		verifyOperationsExist(iface, "sendProductBarcode");
	}

	private void verifyInterfaceIStoreInventoryManager(Interface iface) {
		verifyOperationsExist(iface, "getStore", "getProductsWithLowStock", "getAllProducts",
				"getProductsWithStockItems", "orderProducts", "getOrder", "getOutstandingOrders", "rollInReceivedOrder",
				"changePrice", "markProductsUnavailableInStock", "getStockItems");
	}

	private void verifyInterfaceIStoreInventory(Interface iface) {
		verifyOperationsExist(iface, "getProductWithStockItem");
	}

	private void verifyInterfaceIProductDispatcher(Interface iface) {
		verifyOperationsExist(iface, "dispatchProductsFromOtherStores");
	}

	private Model getModel(List<EObject> eObjects) {
		for (EObject eObject : eObjects)
			if (eObject instanceof Model)
				return (Model) eObject;
		fail("No model exists in the UML model");
		return null;
	}

}
