package org.codeling.examples.case_studies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.cocome.CoCoMELanguageDefinition;
import org.codeling.lang.jee7.JEE7LanguageDefinition;
import org.codeling.transformationmanager.ITransformationManager;
import org.codeling.utils.CodelingException;
import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.junit.BeforeClass;
import org.junit.Test;

public class CoCoME_JEE_Migration_CaseStudy extends MayHaveIssues {

	private static IJavaProject originalProject;
	private static IJavaProject migratedProject;
	private static IPackageFragment newBeansPackage;

	@BeforeClass
	public static void migrate()
			throws CoreException, CodelingException, IOException, InterruptedException {
		assert (false) : "Checking java assertion must not be switched on during these tests, because HenshinTGG will not work in this mode. Please do not use -ea as VM parameters";

		CaseStudyCommons c = new CaseStudyCommons();
		originalProject = JavaCore.create(c.importProject(c.getCoCoMEPath()));
		LinkedList<IJavaProject> projects = new LinkedList<>();
		projects.addAll(Arrays.asList(originalProject));

		// Translate code to PCM model
		CoCoMELanguageDefinition implementationLanguage = new CoCoMELanguageDefinition();
		JEE7LanguageDefinition specificationLanguage = new JEE7LanguageDefinition();

		long startToModel = System.nanoTime();
		ITransformationManager.createInstance().migrateImplementation(implementationLanguage, specificationLanguage,
				new ConsoleProgressMonitor(CoCoME_JEE_Migration_CaseStudy.class), projects);
		long endToModel = System.nanoTime();
		System.out.println("Required time for translation migration: " + (endToModel - startToModel) + " nano seconds.");

		// "-in-null" should be "-in-jee", but the language definition is not
		// initialized in the test, and therefore the symbolic name is not read from the
		// extension
		migratedProject = JavaCore.create(
				ResourcesPlugin.getWorkspace().getRoot().getProject(originalProject.getElementName() + "-in-null"));
		assertTrue(migratedProject.exists());

		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		assertNotNull("The package new_architecture_elements must not be null.", newBeansPackage);
		assertTrue("The package new_architecture_elements must exist.", newBeansPackage.exists());
		assertEquals("The number of compilation units in the new_architecture_elements is not as expected.", 35,
				newBeansPackage.getCompilationUnits().length); // 17 Entities, 18 Component Beans
	}

	@Test
	public void verifyBeanApplication() throws NoSuchElementException, JavaModelException {
		String beanName = "Application";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.ApplicationScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.ApplicationScoped"));

		verifyChildComponentField(bean, "storeServer", "QStoreServer;");
		verifyChildComponentField(bean, "reportingServer", "QReportingServer;");
		verifyChildComponentField(bean, "productDispatcherServer", "QProductDispatcherServer;");
		verifyDelegatedOperation(bean, "getProductWithStockItem", "storeServer");
		verifyDelegatedOperation(bean, "getStockItems", "storeServer");
		verifyDelegatedOperation(bean, "markProductsUnavailableInStock", "storeServer");
		verifyDelegatedOperation(bean, "changePrice", "storeServer");
		verifyDelegatedOperation(bean, "rollInReceivedOrder", "storeServer");
		verifyDelegatedOperation(bean, "getOutstandingOrders", "storeServer");
		verifyDelegatedOperation(bean, "getOrder", "storeServer");
		verifyDelegatedOperation(bean, "orderProducts", "storeServer");
		verifyDelegatedOperation(bean, "getProductsWithStockItems", "storeServer");
		verifyDelegatedOperation(bean, "getAllProducts", "storeServer");
		verifyDelegatedOperation(bean, "getProductsWithLowStock", "storeServer");
		verifyDelegatedOperation(bean, "getStore", "storeServer");
		verifyDelegatedOperation(bean, "getEnterpriseDeliveryReport", "reportingServer");
		verifyDelegatedOperation(bean, "getEnterpriseStockReport", "reportingServer");
		verifyDelegatedOperation(bean, "getStoreStockReport", "reportingServer");
		verifyDelegatedOperation(bean, "dispatchProductsFromOtherStores", "productDispatcherServer");
		assertEquals(3, bean.getFields().length);
		assertEquals(16, bean.getMethods().length);
	}

	@Test
	public void verifyBeanBarcodeScannerModel() throws NoSuchElementException, JavaModelException {
		String beanName = "BarcodeScannerModel";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.RequestScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.RequestScoped"));

		verifyOperation(bean, "sendProductBarcode");
		assertEquals(0, bean.getFields().length);
		assertEquals(1, bean.getMethods().length);
	}

	@Test
	public void verifyBeanCardReaderModel() throws NoSuchElementException, JavaModelException {
		String beanName = "CardReaderModel";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.RequestScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.RequestScoped"));

		verifyOperation(bean, "sendCreditCardInfo");
		verifyOperation(bean, "sendCreditCardPin");
		assertEquals(0, bean.getFields().length);
		assertEquals(2, bean.getMethods().length);
	}

	@Test
	public void verifyBeanCashBoxModel() throws NoSuchElementException, JavaModelException {
		String beanName = "CashBoxModel";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.RequestScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.RequestScoped"));

		verifyOperation(bean, "open");
		verifyOperation(bean, "close");
		verifyOperation(bean, "isOpen");
		verifyOperation(bean, "pressControlKey");
		verifyOperation(bean, "pressNumpadKey");
		assertEquals(0, bean.getFields().length);
		assertEquals(5, bean.getMethods().length);
	}

	@Test
	public void verifyBeanCashdeskline() throws NoSuchElementException, JavaModelException {
		String beanName = "Cashdeskline";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.ApplicationScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.ApplicationScoped"));

		verifyChildComponentField(bean, "coordinatorModel", "QCoordinatorModel;");
		verifyChildComponentField(bean, "cashDeskModel", "QCashDeskModel;");
		verifyDelegatedOperation(bean, "getMessageKind", "cashDeskModel");
		verifyDelegatedOperation(bean, "getMessage", "cashDeskModel");
		verifyDelegatedOperation(bean, "setContent", "cashDeskModel");
		verifyDelegatedOperation(bean, "getCurrentPrintout", "cashDeskModel");
		verifyDelegatedOperation(bean, "printText", "cashDeskModel");
		verifyDelegatedOperation(bean, "tearOffPrintout", "cashDeskModel");
		verifyDelegatedOperation(bean, "sendProductBarcode", "cashDeskModel");
		verifyDelegatedOperation(bean, "sendCreditCardPin", "cashDeskModel");
		verifyDelegatedOperation(bean, "isOn", "cashDeskModel");
		verifyDelegatedOperation(bean, "turnExpressLightOff", "cashDeskModel");
		verifyDelegatedOperation(bean, "sendCreditCardInfo", "cashDeskModel");
		verifyDelegatedOperation(bean, "turnExpressLightOn", "cashDeskModel");
		verifyDelegatedOperation(bean, "pressNumpadKey", "cashDeskModel");
		verifyDelegatedOperation(bean, "pressControlKey", "cashDeskModel");
		verifyDelegatedOperation(bean, "isOpen", "cashDeskModel");
		verifyDelegatedOperation(bean, "close", "cashDeskModel");
		verifyDelegatedOperation(bean, "open", "cashDeskModel");
		assertEquals(2, bean.getFields().length);
		assertEquals(17, bean.getMethods().length);
	}

	@Test
	public void verifyBeanCashDeskModel() throws NoSuchElementException, JavaModelException {
		String beanName = "CashDeskModel";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.RequestScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.RequestScoped"));
		verifyChildComponentField(bean, "userDisplayModel", "QUserDisplayModel;");
		verifyChildComponentField(bean, "printerModel", "QPrinterModel;");
		verifyChildComponentField(bean, "expressLightModel", "QExpressLightModel;");
		verifyChildComponentField(bean, "cashBoxModel", "QCashBoxModel;");
		verifyChildComponentField(bean, "cardReaderModel", "QCardReaderModel;");
		verifyChildComponentField(bean, "barcodeScannerModel", "QBarcodeScannerModel;");
		verifyInjectedField(bean, "trivialBankServer", "QTrivialBankServer;");
		verifyInjectedField(bean, "storeServer", "QStoreServer;");
		verifyDelegatedOperation(bean, "getMessageKind", "userDisplayModel");
		verifyDelegatedOperation(bean, "getMessage", "userDisplayModel");
		verifyDelegatedOperation(bean, "setContent", "userDisplayModel");
		verifyDelegatedOperation(bean, "getCurrentPrintout", "printerModel");
		verifyDelegatedOperation(bean, "printText", "printerModel");
		verifyDelegatedOperation(bean, "tearOffPrintout", "printerModel");
		verifyDelegatedOperation(bean, "sendProductBarcode", "barcodeScannerModel");
		verifyDelegatedOperation(bean, "isOn", "expressLightModel");
		verifyDelegatedOperation(bean, "pressNumpadKey", "cashBoxModel");
		verifyDelegatedOperation(bean, "sendCreditCardPin", "cardReaderModel");
		verifyDelegatedOperation(bean, "turnExpressLightOff", "expressLightModel");
		verifyDelegatedOperation(bean, "turnExpressLightOn", "expressLightModel");
		verifyDelegatedOperation(bean, "pressControlKey", "cashBoxModel");
		verifyDelegatedOperation(bean, "isOpen", "cashBoxModel");
		verifyDelegatedOperation(bean, "sendCreditCardInfo", "cardReaderModel");
		verifyDelegatedOperation(bean, "close", "cashBoxModel");
		verifyDelegatedOperation(bean, "open", "cashBoxModel");
		assertEquals(8, bean.getFields().length);
		assertEquals(17, bean.getMethods().length);
	}

	@Test
	public void verifyBeanConsole() throws NoSuchElementException, JavaModelException {
		String beanName = "Console";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.ApplicationScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.ApplicationScoped"));

		verifyChildComponentField(bean, "storeConsole", "QStoreConsole;");
		verifyChildComponentField(bean, "reportingConsole", "QReportingConsole;");
		assertEquals(2, bean.getFields().length);
		assertEquals(0, bean.getMethods().length);
	}

	@Test
	public void verifyBeanCoordinatorModel() throws NoSuchElementException, JavaModelException {
		String beanName = "CoordinatorModel";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.RequestScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.RequestScoped"));

		assertEquals(0, bean.getFields().length);
		assertEquals(0, bean.getMethods().length);
	}

	@Test
	public void verifyBeanExpressLightModel() throws NoSuchElementException, JavaModelException {
		String beanName = "ExpressLightModel";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.RequestScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.RequestScoped"));

		verifyOperation(bean, "turnExpressLightOn");
		verifyOperation(bean, "turnExpressLightOff");
		verifyOperation(bean, "isOn");

		assertEquals(0, bean.getFields().length);
		assertEquals(3, bean.getMethods().length);
	}

	@Test
	public void verifyBeanInventory() throws NoSuchElementException, JavaModelException {
		String beanName = "Inventory";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.ApplicationScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.ApplicationScoped"));

		verifyChildComponentField(bean, "console", "QConsole;");
		verifyChildComponentField(bean, "application", "QApplication;");
		verifyDelegatedOperation(bean, "getProductWithStockItem", "application");
		verifyDelegatedOperation(bean, "dispatchProductsFromOtherStores", "application");
		verifyDelegatedOperation(bean, "getEnterpriseDeliveryReport", "application");
		verifyDelegatedOperation(bean, "getStockItems", "application");
		verifyDelegatedOperation(bean, "getEnterpriseStockReport", "application");
		verifyDelegatedOperation(bean, "markProductsUnavailableInStock", "application");
		verifyDelegatedOperation(bean, "getStoreStockReport", "application");
		verifyDelegatedOperation(bean, "changePrice", "application");
		verifyDelegatedOperation(bean, "rollInReceivedOrder", "application");
		verifyDelegatedOperation(bean, "getOutstandingOrders", "application");
		verifyDelegatedOperation(bean, "getOrder", "application");
		verifyDelegatedOperation(bean, "orderProducts", "application");
		verifyDelegatedOperation(bean, "getProductsWithStockItems", "application");
		verifyDelegatedOperation(bean, "getAllProducts", "application");
		verifyDelegatedOperation(bean, "getProductsWithLowStock", "application");
		verifyDelegatedOperation(bean, "getStore", "application");
		assertEquals(2, bean.getFields().length);
		assertEquals(16, bean.getMethods().length);
	}

	@Test
	public void verifyBeanPrinterModel() throws NoSuchElementException, JavaModelException {
		String beanName = "PrinterModel";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.RequestScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.RequestScoped"));

		verifyOperation(bean, "tearOffPrintout");
		verifyOperation(bean, "printText");
		verifyOperation(bean, "getCurrentPrintout");
		assertEquals(0, bean.getFields().length);
		assertEquals(3, bean.getMethods().length);
	}

	@Test
	public void verifyBeanProductDispatcherServer() throws NoSuchElementException, JavaModelException {
		String beanName = "ProductDispatcherServer";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue("The bean " + beanName + " must exist.", bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.ApplicationScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.ApplicationScoped"));

		verifyOperation(bean, "dispatchProductsFromOtherStores");
		assertEquals(0, bean.getFields().length);
		assertEquals(1, bean.getMethods().length);
	}

	@Test
	public void verifyBeanReportingConsole() throws NoSuchElementException, JavaModelException {
		String beanName = "ReportingConsole";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.ApplicationScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.ApplicationScoped"));

		verifyInjectedField(bean, "reportingServer", "QReportingServer;");
		assertEquals(1, bean.getFields().length);
		assertEquals(0, bean.getMethods().length);
	}

	@Test
	public void verifyBeanReportingServer() throws NoSuchElementException, JavaModelException {
		String beanName = "ReportingServer";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue("The bean " + beanName + " must exist.", bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.ApplicationScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.ApplicationScoped"));

		verifyOperation(bean, "getStoreStockReport");
		verifyOperation(bean, "getEnterpriseStockReport");
		verifyOperation(bean, "getEnterpriseDeliveryReport");
		assertEquals(0, bean.getFields().length);
		assertEquals(3, bean.getMethods().length);
	}

	@Test
	public void verifyBeanStoreConsole() throws NoSuchElementException, JavaModelException {
		String beanName = "StoreConsole";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue(bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.ApplicationScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.ApplicationScoped"));

		verifyInjectedField(bean, "storeServer", "QStoreServer;");
		assertEquals(1, bean.getFields().length);
		assertEquals(0, bean.getMethods().length);
	}

	@Test
	public void verifyBeanStoreServer() throws NoSuchElementException, JavaModelException {
		String beanName = "StoreServer";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue("The bean " + beanName + " must exist.", bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.ApplicationScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.ApplicationScoped"));

		verifyInjectedField(bean, "productDispatcherServer", "QProductDispatcherServer;");
		verifyOperation(bean, "getStore");
		verifyOperation(bean, "getProductsWithLowStock");
		verifyOperation(bean, "getAllProducts");
		verifyOperation(bean, "getProductsWithStockItems");
		verifyOperation(bean, "orderProducts");
		verifyOperation(bean, "getOrder");
		verifyOperation(bean, "getOutstandingOrders");
		verifyOperation(bean, "rollInReceivedOrder");
		verifyOperation(bean, "changePrice");
		verifyOperation(bean, "markProductsUnavailableInStock");
		verifyOperation(bean, "getStockItems");
		verifyOperation(bean, "getProductWithStockItem");
		assertEquals(1, bean.getFields().length);
		assertEquals(12, bean.getMethods().length);
	}

	@Test
	public void verifyBeanTrivialBankServer() throws NoSuchElementException, JavaModelException {
		String beanName = "TrivialBankServer";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue("The bean " + beanName + " must exist.", bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.ApplicationScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.ApplicationScoped"));

		verifyOperation(bean, "validateCard");
		verifyOperation(bean, "debitCard");
		assertEquals(0, bean.getFields().length);
		assertEquals(2, bean.getMethods().length);
	}

	@Test
	public void verifyBeanUserDisplayModel() throws NoSuchElementException, JavaModelException {
		String beanName = "UserDisplayModel";
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		IType bean = newBeansPackage.getCompilationUnit(beanName + ".java").getType(beanName);
		assertTrue("The bean " + beanName + " must exist.", bean.exists());
		assertTrue("The bean " + beanName + " must have the annotation \"javax.enterprise.context.RequestScoped\"",
				ASTUtils.hasAnnotation(bean, "javax.enterprise.context.RequestScoped"));

		verifyOperation(bean, "setContent");
		verifyOperation(bean, "getMessage");
		verifyOperation(bean, "getMessageKind");
		assertEquals(0, bean.getFields().length);
		assertEquals(3, bean.getMethods().length);
	}

	private void verifyOperation(IType bean, String operationName) throws JavaModelException {
		IMethod method = bean.getMethod(operationName, new String[0]);
		newBeansPackage = ASTUtils.getPackageFragment("new_architecture_elements", Arrays.asList(migratedProject));
		assertTrue("The method " + operationName + " must exist in the bean " + bean.getElementName(), method.exists());
		assertEquals("The method " + operationName + " in the bean " + bean.getElementName()
				+ " does not have the expected return type", "V", method.getReturnType());
		String body = method.getSource();
		body = body.substring(body.indexOf("{") + 1, body.lastIndexOf("}"));
		assertEquals("The body of the operation " + bean.getElementName() + "." + operationName + " should be empty.",
				"", body);
	}

	private void verifyDelegatedOperation(IType bean, String operationName, String delegatee)
			throws JavaModelException {
		IMethod method = bean.getMethod(operationName, new String[0]);
		assertTrue("The method " + operationName + " must exist in the bean " + bean.getElementName(), method.exists());
		assertEquals("The method " + operationName + " in the bean " + bean.getElementName()
				+ " does not have the expected return type", "V", method.getReturnType());
		String body = method.getSource();
		body = body.substring(body.indexOf("{") + 1, body.lastIndexOf("}"));
		assertEquals(delegatee + "." + operationName + "();", body);
	}

	private void verifyChildComponentField(IType bean, String name, String signature) throws JavaModelException {
		IField field = bean.getField(name);
		assertTrue("The field " + name + " should exist in " + bean.getElementName()
				+ " for the corresponding child component.", field.exists());
		assertEquals("The type signature of the child component field " + name + " is not as expected", signature,
				field.getTypeSignature());
		assertTrue(
				"The field " + name + " for the corresponding child component of " + bean.getElementName()
						+ " does not have the expected annotations.",
				ASTUtils.hasAnnotation(field, "org.codeling.lang.ejbWithStatemachine.ial.mm.componenttype_feature.Child",
						"javax.inject.Inject"));
	}

	private void verifyInjectedField(IType bean, String name, String signature) throws JavaModelException {
		IField field = bean.getField(name);
		assertTrue("The field " + name + " should exist in " + bean.getElementName()
				+ " for the corresponding required component.", field.exists());
		assertEquals("The type signature of the required component field " + name + " is not as expected", signature,
				field.getTypeSignature());
		assertTrue(
				"The field " + name + " for the corresponding required component of +" + bean.getElementName()
						+ " does not have the expected annotations.",
				ASTUtils.hasAnnotation(field, "javax.inject.Inject"));
		assertFalse(
				"The field " + name + " for the corresponding required component of +" + bean.getElementName()
						+ " must not have the annotation \"org.codeling.lang.ejbWithStatemachine.ial.mm.componenttype_feature.Child\".",
				ASTUtils.hasAnnotation(field, "org.codeling.lang.ejbWithStatemachine.ial.mm.componenttype_feature.Child"));
	}
}

