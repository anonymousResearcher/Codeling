package org.codeling.examples.case_studies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.codeling.lang.cocome.CoCoMELanguageDefinition;
import org.codeling.transformationmanager.ITransformationManager;
import org.codeling.utils.CodelingException;
import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.palladiosimulator.pcm.core.composition.AssemblyConnector;
import org.palladiosimulator.pcm.core.composition.AssemblyContext;
import org.palladiosimulator.pcm.core.composition.Connector;
import org.palladiosimulator.pcm.core.composition.ProvidedDelegationConnector;
import org.palladiosimulator.pcm.core.composition.RequiredDelegationConnector;
import org.palladiosimulator.pcm.core.entity.ComposedProvidingRequiringEntity;
import org.palladiosimulator.pcm.repository.BasicComponent;
import org.palladiosimulator.pcm.repository.CompositeComponent;
import org.palladiosimulator.pcm.repository.Interface;
import org.palladiosimulator.pcm.repository.OperationInterface;
import org.palladiosimulator.pcm.repository.OperationProvidedRole;
import org.palladiosimulator.pcm.repository.OperationRequiredRole;
import org.palladiosimulator.pcm.repository.OperationSignature;
import org.palladiosimulator.pcm.repository.ProvidedRole;
import org.palladiosimulator.pcm.repository.Repository;
import org.palladiosimulator.pcm.repository.RepositoryComponent;
import org.palladiosimulator.pcm.repository.RequiredRole;
import org.palladiosimulator.pcm.repository.Signature;
import org.palladiosimulator.pcm.system.System;

import de.mkonersmann.advert.pcm.PCMLanguageDefinition;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CoCoME_PCM_CaseStudy extends MayHaveIssues {

	private static List<EObject> pcmModel;

	@Test
	public void _1_translateToPCMModel() throws CoreException, CodelingException, IOException {
		assert (false) : "Checking java assertion must not be switched on during these tests, because HenshinTGG will not work in this mode. Please do not use -ea as VM parameters";

		CaseStudyCommons c = new CaseStudyCommons();
		IJavaProject mainProject = JavaCore.create(c.copyImportProject(c.getCoCoMEPath()));
		LinkedList<IJavaProject> projects = new LinkedList<>();
		projects.addAll(Arrays.asList(mainProject));

		// Translate code to PCM model
		CoCoMELanguageDefinition implementationLanguage = new CoCoMELanguageDefinition();
		PCMLanguageDefinition specificationLanguage = new PCMLanguageDefinition();

		long startToModel = java.lang.System.nanoTime();
		pcmModel = ITransformationManager.createInstance().extractModelFromCode(implementationLanguage,
				specificationLanguage, null, projects);
		long endToModel = java.lang.System.nanoTime();
		java.lang.System.out.println("Required time for translation to model: " + (endToModel - startToModel) + " nano seconds.");
	}

	@Test
	public void _3_verifySystem() {
		System system = getSystem(pcmModel);
		assertEquals(3, system.getAssemblyContexts__ComposedStructure().size());
		verifyDelegatesProvision(system, new String[] { "IProductDispatcher", "inventory" },
				new String[] { "IBank", "TrivialBankServer" }, new String[] { "IReporting", "inventory" },
				new String[] { "IStoreInventoryManager", "inventory" }, new String[] { "IStoreInventory", "inventory" },
				new String[] { "ICashBox", "cashdeskline" }, new String[] { "IExpressLight", "cashdeskline" },
				new String[] { "IBarcodeScanner", "cashdeskline" }, new String[] { "ICardReader", "cashdeskline" },
				new String[] { "IPrinter", "cashdeskline" }, new String[] { "IUserDisplay", "cashdeskline" });
		assertEquals(0, system.getRequiredRoles_InterfaceRequiringEntity().size());
		verifyConnects(system, new String[] { "cashdeskline", "IBank", "TrivialBankServer" },
				new String[] { "cashdeskline", "IStoreInventory", "inventory" });
	}

	@Test
	public void _2_verifyRepository() {
		Repository repository = getRepository(pcmModel);
		EList<Interface> interfaces = repository.getInterfaces__Repository();
		assertEquals(11, interfaces.size());

		EList<RepositoryComponent> components = repository.getComponents__Repository();
		assertEquals(19, components.size());
	}

	@Test
	public void _4_verifyComponents() {
		Repository repository = getRepository(pcmModel);
		EList<RepositoryComponent> components = repository.getComponents__Repository();
		for (RepositoryComponent repositoryComponent : components) {
			switch (repositoryComponent.getEntityName()) {
			case "ProductDispatcherServer":
				verifyComponentProductDispatcher(repositoryComponent);
				break;
			case "ReportingServer":
				verifyComponentReportingServer(repositoryComponent);
				break;
			case "StoreServer":
				verifyComponentStoreServer(repositoryComponent);
				break;
			case "ReportingConsole":
				verifyComponentReportingConsole(repositoryComponent);
				break;
			case "StoreConsole":
				verifyComponentStoreConsole(repositoryComponent);
				break;
			case "TrivialBankServer":
				verifyComponentTrivialBank(repositoryComponent);
				break;
			case "CashBoxModel":
				verifyComponentCashBox(repositoryComponent);
				break;
			case "ExpressLightModel":
				verifyComponentExpressLight(repositoryComponent);
				break;
			case "cashdeskline":
				verifyComponentCashdeskline(repositoryComponent);
				break;
			case "BarcodeScannerModel":
				verifyComponentBarcodeScanner(repositoryComponent);
				break;
			case "CardReaderModel":
				verifyComponentCardReader(repositoryComponent);
				break;
			case "PrinterModel":
				verifyComponentPrinter(repositoryComponent);
				break;
			case "UserDisplayModel":
				verifyComponentUserDisplay(repositoryComponent);
				break;
			case "CoordinatorModel":
				verifyComponentCoordinator(repositoryComponent);
				break;
			case "CashDeskModel":
				verifyComponentCashDesk(repositoryComponent);
				break;
			case "CashDeskModel_Internal":
				verifyComponentCashDeskModel_Internal(repositoryComponent);
				break;
			case "console":
				verifyComponentConsole(repositoryComponent);
				break;
			case "application":
				verifyComponentApplication(repositoryComponent);
				break;
			case "inventory":
				verifyComponentInventory(repositoryComponent);
				break;
			}
		}
	}

	private void verifyHasAllSEFFs(BasicComponent component) {
		// Find all operation signatures of provided interfaces
		List<Signature> operations = new LinkedList<>();
		for (ProvidedRole providedRole : component.getProvidedRoles_InterfaceProvidingEntity()) {
			if (providedRole instanceof OperationProvidedRole) {
				OperationInterface iface = ((OperationProvidedRole) providedRole)
						.getProvidedInterface__OperationProvidedRole();
				for (OperationSignature operation : iface.getSignatures__OperationInterface()) {
					operations.add(operation);
				}
			}
		}

		// Verify one SEFF for each operation signature
		assertTrue("The component " + component.getEntityName() + " does not have all required SEFFs",
				component.getServiceEffectSpecifications__BasicComponent().stream()
						.map(x -> x.getDescribedService__SEFF()).collect(Collectors.toList()).containsAll(operations));
		assertEquals("The component " + component.getEntityName() + " has more than the required SEFFs",
				operations.size(), component.getServiceEffectSpecifications__BasicComponent().size());
	}

	private void verifyIsWellFormedBasicComponent(RepositoryComponent component) {
		assertTrue("Component " + component.getEntityName() + " must be a BasicComponent, but is not.",
				component instanceof BasicComponent);
		verifyHasAllSEFFs((BasicComponent) component);
	}

	private void verifyIsCompositeComponent(RepositoryComponent component) {
		assertTrue("Component " + component.getEntityName() + " must be a CompositeComponent, but is not.",
				component instanceof CompositeComponent);
	}

	private void verifyComponentInventory(RepositoryComponent repositoryComponent) {
		verifyIsCompositeComponent(repositoryComponent);
		CompositeComponent component = (CompositeComponent) repositoryComponent;

		verifyHasAssemblyContextsOfComponents(component, "console", "application");
		verifyDelegatesProvision(component, new String[] { "IStoreInventory", "application" },
				new String[] { "IStoreInventoryManager", "application" },
				new String[] { "IProductDispatcher", "application" }, new String[] { "IReporting", "application" });
		verifyRequiresNone(component);
		verifyConnects(component, new String[] { "console", "IStoreInventoryManager", "application" },
				new String[] { "console", "IReporting", "application" });
	}

	private void verifyComponentApplication(RepositoryComponent repositoryComponent) {
		verifyIsCompositeComponent(repositoryComponent);
		CompositeComponent component = (CompositeComponent) repositoryComponent;

		verifyHasAssemblyContextsOfComponents(component, "StoreServer", "ReportingServer", "ProductDispatcherServer");
		verifyDelegatesProvision(component, new String[] { "IStoreInventory", "StoreServer" },
				new String[] { "IReporting", "ReportingServer" },
				new String[] { "IStoreInventoryManager", "StoreServer" },
				new String[] { "IProductDispatcher", "ProductDispatcherServer" });
		verifyRequiresNone(component);
		verifyConnects(component, new String[] { "StoreServer", "IProductDispatcher", "ProductDispatcherServer" });
	}

	private void verifyComponentConsole(RepositoryComponent repositoryComponent) {
		verifyIsCompositeComponent(repositoryComponent);
		CompositeComponent component = (CompositeComponent) repositoryComponent;

		verifyHasAssemblyContextsOfComponents(component, "StoreConsole", "ReportingConsole");
		verifyProvidesNone(component);
		verifyDelegatesRequirement(component, new String[] { "IStoreInventoryManager", "StoreConsole" },
				new String[] { "IReporting", "ReportingConsole" });
		verifyConnectsNone(component);
	}

	private void verifyComponentCashDesk(RepositoryComponent repositoryComponent) {
		verifyIsCompositeComponent(repositoryComponent);
		CompositeComponent component = (CompositeComponent) repositoryComponent;

		verifyHasAssemblyContextsOfComponents(component, "ExpressLightModel", "CashBoxModel", "BarcodeScannerModel",
				"CardReaderModel", "PrinterModel", "UserDisplayModel", "CashDeskModel_Internal");
		verifyDelegatesProvision(component, new String[] { "IExpressLight", "ExpressLightModel" },
				new String[] { "ICashBox", "CashBoxModel" }, new String[] { "IBarcodeScanner", "BarcodeScannerModel" },
				new String[] { "ICardReader", "CardReaderModel" }, new String[] { "IPrinter", "PrinterModel" },
				new String[] { "IUserDisplay", "UserDisplayModel" });
		verifyDelegatesRequirement(component, new String[] { "IBank", "CashDeskModel_Internal" },
				new String[] { "IStoreInventory", "CashDeskModel_Internal" });
		verifyConnectsNone(component);
	}

	private void verifyComponentCoordinator(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesNone(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentUserDisplay(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentPrinter(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentCardReader(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentBarcodeScanner(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesOnlyStandardInterface(component);
	}

	private void verifyComponentCashDeskModel_Internal(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesNone(component);
		verifyRequires(component, "IBank", "IStoreInventory");
	}

	private void verifyComponentCashdeskline(RepositoryComponent repositoryComponent) {
		verifyIsCompositeComponent(repositoryComponent);
		CompositeComponent component = (CompositeComponent) repositoryComponent;

		verifyHasAssemblyContextsOfComponents(component, "CashDeskModel", "CoordinatorModel");
		verifyDelegatesProvision(component, new String[] { "IExpressLight", "CashDeskModel" },
				new String[] { "ICashBox", "CashDeskModel" }, new String[] { "IBarcodeScanner", "CashDeskModel" },
				new String[] { "ICardReader", "CashDeskModel" }, new String[] { "IPrinter", "CashDeskModel" },
				new String[] { "IUserDisplay", "CashDeskModel" });
		verifyDelegatesRequirement(component, new String[] { "IBank", "CashDeskModel" },
				new String[] { "IStoreInventory", "CashDeskModel" });
		verifyConnectsNone(component);
	}

	private void verifyComponentExpressLight(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentCashBox(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentTrivialBank(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvides(component, "IBank");
		verifyRequiresNone(component);
	}

	private void verifyComponentStoreServer(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvides(component, "IStoreInventoryManager", "IStoreInventory");
		verifyRequires(component, "IProductDispatcher");
	}

	private void verifyComponentStoreConsole(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesNone(component);
		verifyRequires(component, "IStoreInventoryManager");
	}

	private void verifyComponentReportingServer(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyComponentReportingConsole(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesNone(component);
		verifyRequires(component, "IReporting");
	}

	private void verifyComponentProductDispatcher(RepositoryComponent repositoryComponent) {
		verifyIsWellFormedBasicComponent(repositoryComponent);
		BasicComponent component = (BasicComponent) repositoryComponent;
		verifyProvidesOnlyStandardInterface(component);
		verifyRequiresNone(component);
	}

	private void verifyProvidesOnlyStandardInterface(BasicComponent component) {
		String interfaceName = "I";
		if (component.getEntityName().endsWith("Model")) {
			interfaceName += component.getEntityName().substring(0,
					component.getEntityName().length() - "Model".length());
		} else if (component.getEntityName().endsWith("Server")) {
			interfaceName += component.getEntityName().substring(0,
					component.getEntityName().length() - "Server".length());
		} else if (component.getEntityName().endsWith("Console")) {
			interfaceName += component.getEntityName().substring(0,
					component.getEntityName().length() - "Console".length());
		}
		verifyProvides(component, interfaceName);
	}

	private boolean verifyProvidesNone(RepositoryComponent component) {
		return component.getProvidedRoles_InterfaceProvidingEntity().isEmpty();
	}

	private boolean verifyRequiresNone(RepositoryComponent component) {
		return component.getRequiredRoles_InterfaceRequiringEntity().isEmpty();
	}

	private void verifyProvides(RepositoryComponent component, String... interfaceName) {
		LinkedList<String> missing = new LinkedList<>();
		for (String iface : interfaceName) {
			boolean found = false;
			for (ProvidedRole providedRole : component.getProvidedRoles_InterfaceProvidingEntity()) {
				assertTrue(providedRole instanceof OperationProvidedRole);
				OperationProvidedRole p = (OperationProvidedRole) providedRole;
				if (p.getProvidedInterface__OperationProvidedRole().getEntityName().equals(iface)) {
					found = true;
					break;
				}
			}
			if (!found)
				missing.add(iface);
		}

		assertEquals("The following interfaces are not provided in the component " + component.getEntityName() + ": "
				+ (missing.stream().reduce((s1, s2) -> s1 + ", " + s2).orElseGet(() -> "")), 0, missing.size());

		LinkedList<String> tooMany = new LinkedList<>();
		for (ProvidedRole providedRole : component.getProvidedRoles_InterfaceProvidingEntity()) {
			boolean found = false;
			for (String iface : interfaceName) {
				assertTrue(providedRole instanceof OperationProvidedRole);
				OperationProvidedRole p = (OperationProvidedRole) providedRole;
				if (p.getProvidedInterface__OperationProvidedRole().getEntityName().equals(iface)) {
					found = true;
					break;
				}
			}
			if (!found)
				tooMany.add(providedRole.getEntityName());
		}
		assertEquals("The following interfaces should not be provided by the component " + component.getEntityName()
				+ ": " + (tooMany.stream().reduce((s1, s2) -> s1 + ", " + s2).orElseGet(() -> "")), 0, tooMany.size());
	}

	private void verifyRequires(RepositoryComponent component, String... interfaceName) {
		LinkedList<String> missing = new LinkedList<>();
		for (String iface : interfaceName) {
			boolean found = false;
			for (RequiredRole requiredRole : component.getRequiredRoles_InterfaceRequiringEntity()) {
				assertTrue(requiredRole instanceof OperationRequiredRole);
				OperationRequiredRole p = (OperationRequiredRole) requiredRole;
				if (p.getRequiredInterface__OperationRequiredRole().getEntityName().equals(iface)) {
					found = true;
					break;
				}
			}
			if (!found)
				missing.add(iface);
		}

		assertEquals("The following interfaces are not required in the component " + component.getEntityName() + ": "
				+ (missing.stream().reduce((s1, s2) -> s1 + ", " + s2).orElseGet(() -> "")), 0, missing.size());

		LinkedList<String> tooMany = new LinkedList<>();
		for (RequiredRole requiredRole : component.getRequiredRoles_InterfaceRequiringEntity()) {
			boolean found = false;
			for (String iface : interfaceName) {
				assertTrue(requiredRole instanceof OperationRequiredRole);
				OperationRequiredRole p = (OperationRequiredRole) requiredRole;
				if (p.getRequiredInterface__OperationRequiredRole().getEntityName().equals(iface)) {
					found = true;
					break;
				}
			}
			if (!found)
				tooMany.add(requiredRole.getEntityName());
		}
		assertEquals("The following interfaces should not be required by the component " + component.getEntityName()
				+ ": " + (tooMany.stream().reduce((s1, s2) -> s1 + ", " + s2).orElseGet(() -> "")), 0, tooMany.size());
	}

	private void verifyHasAssemblyContextsOfComponents(CompositeComponent component, String... instantiatedComponent) {
		List<String> remainingContexts = new LinkedList<>();
		remainingContexts.addAll(Arrays.asList(instantiatedComponent));
		for (AssemblyContext context : component.getAssemblyContexts__ComposedStructure()) {
			String contextFound = null;
			for (String expectedContext : remainingContexts) {
				if (expectedContext.equals(context.getEncapsulatedComponent__AssemblyContext().getEntityName())) {
					contextFound = expectedContext;
					break;
				}
			}
			if (contextFound != null) {
				remainingContexts.remove(contextFound);
			}

		}

		assertTrue(
				"The following context do not exist in the component " + component.getEntityName() + ": "
						+ (remainingContexts.stream().reduce((s1, s2) -> s1 + ", " + s2).orElseGet(() -> "")),
				remainingContexts.isEmpty());
		assertEquals(
				"The component " + component + " must contain " + instantiatedComponent.length + " assembly contexts.",
				instantiatedComponent.length, component.getAssemblyContexts__ComposedStructure().size());
	}

	/**
	 * @param entity
	 *            The delegating component
	 * @param ifaceDelegatee
	 *            An array of String arrays: [[interface, delegatee],[...], ...]
	 */
	private void verifyDelegatesProvision(ComposedProvidingRequiringEntity entity, String[]... ifaceDelegatee) {
		List<String[]> remainingProvisions = new LinkedList<>();
		remainingProvisions.addAll(Arrays.asList(ifaceDelegatee));
		List<String[]> provisionsFound = new LinkedList<>();

		for (String[] expectedProvision : remainingProvisions) {
			for (Connector connector : entity.getConnectors__ComposedStructure()) {
				if (!(connector instanceof ProvidedDelegationConnector))
					continue;
				ProvidedDelegationConnector con = (ProvidedDelegationConnector) connector;

				if (expectedProvision[0].equals(con.getInnerProvidedRole_ProvidedDelegationConnector()
						.getProvidedInterface__OperationProvidedRole().getEntityName())
						&& expectedProvision[1].equals(con.getInnerProvidedRole_ProvidedDelegationConnector()
								.getProvidingEntity_ProvidedRole().getEntityName())) {
					provisionsFound.add(expectedProvision);
					break;
				}
			}
		}

		List<String[]> toRemove = new LinkedList<>();
		for (String[] f : provisionsFound) {
			for (String[] f2 : remainingProvisions)
				if (Arrays.equals(f, f2)) {
					toRemove.add(f);
					break;
				}
		}
		remainingProvisions.removeAll(toRemove);

		assertEquals("The following provision do not exist in the component " + entity.getEntityName() + ": "
				+ (remainingProvisions.stream().reduce((s1, s2) -> {
					if (s1.length < 2)
						s1 = new String[] { "", "" };
					if (s2.length < 2)
						s2 = new String[] { "", "" };
					return new String[] { "(" + s1[0] + " -> " + s1[1] + ") , (" + s2[0] + " -> " + s2[1] + ")" };
				}).orElseGet(() -> new String[] { "" })[0]), 0, remainingProvisions.size());
		assertEquals(
				"The component " + entity.getEntityName() + " must provide " + ifaceDelegatee.length + " interfaces.",
				ifaceDelegatee.length, entity.getProvidedRoles_InterfaceProvidingEntity().size());
	}

	/**
	 * @param component
	 *            The delegating component
	 * @param ifaceDelegatee
	 *            An array of String arrays: [[interface, delegatee],[interface,
	 *            delegatee], ...]
	 */
	private void verifyDelegatesRequirement(CompositeComponent component, String[]... ifaceDelegatee) {
		List<String[]> remainingRequirements = new LinkedList<>();
		remainingRequirements.addAll(Arrays.asList(ifaceDelegatee));
		List<String[]> requirementsFound = new LinkedList<>();

		for (String[] expectedRequirement : remainingRequirements) {
			for (Connector connector : component.getConnectors__ComposedStructure()) {
				if (!(connector instanceof RequiredDelegationConnector))
					continue;
				RequiredDelegationConnector con = (RequiredDelegationConnector) connector;

				if (expectedRequirement[0].equals(con.getInnerRequiredRole_RequiredDelegationConnector()
						.getRequiredInterface__OperationRequiredRole().getEntityName())
						&& expectedRequirement[1].equals(con.getInnerRequiredRole_RequiredDelegationConnector()
								.getRequiringEntity_RequiredRole().getEntityName())) {
					requirementsFound.add(expectedRequirement);
					break;
				}
			}
		}

		List<String[]> toRemove = new LinkedList<>();
		for (String[] f : requirementsFound) {
			for (String[] f2 : remainingRequirements)
				if (Arrays.equals(f, f2)) {
					toRemove.add(f);
					break;
				}
		}
		remainingRequirements.removeAll(toRemove);

		assertTrue("The following requirement do not exist in the component " + component.getEntityName() + ": "
				+ (remainingRequirements.stream().reduce((s1, s2) -> {
					if (s1.length < 2)
						return new String[] { "(" + s2[0] + " -> " + s2[1] + ")" };
					if (s2.length < 2)
						return new String[] { "(" + s1[0] + " -> " + s1[1] + ")" };
					return new String[] { "(" + s1[0] + " -> " + s1[1] + ") , (" + s2[0] + " -> " + s2[1] + ")" };
				}).orElseGet(() -> new String[] { "" })[0]), remainingRequirements.isEmpty());
		assertEquals(
				"The component " + component.getEntityName() + " must require " + ifaceDelegatee.length
						+ " interfaces.",
				ifaceDelegatee.length, component.getRequiredRoles_InterfaceRequiringEntity().size());
	}

	/**
	 * @param entity
	 *            The composite component
	 * @param connection
	 *            An array of String arrays: [[requirer, interface,
	 *            provider],[...],...]
	 */
	private void verifyConnects(ComposedProvidingRequiringEntity entity, String[]... connection) {
		LinkedList<String[]> missing = new LinkedList<>();
		for (String[] declaredConnector : connection) {
			boolean found = false;

			for (Connector connector : entity.getConnectors__ComposedStructure()) {
				if (!(connector instanceof AssemblyConnector))
					continue;

				AssemblyConnector con = (AssemblyConnector) connector;
				if (con.getRequiringAssemblyContext_AssemblyConnector().getEncapsulatedComponent__AssemblyContext()
						.getEntityName().equals(declaredConnector[0])
						&& con.getProvidedRole_AssemblyConnector().getProvidedInterface__OperationProvidedRole()
								.getEntityName().equals(declaredConnector[1])
						&& con.getProvidingAssemblyContext_AssemblyConnector()
								.getEncapsulatedComponent__AssemblyContext().getEntityName()
								.equals(declaredConnector[2])) {
					found = true;
					break;
				}
			}
			if (!found)
				missing.add(declaredConnector);
		}

		String missingConnections = "";
		for (String[] m : missing) {
			missingConnections += "(requirer: " + m[0] + ", interface: " + m[1] + ", provider: " + m[2] + ")";
			missingConnections += java.lang.System.getProperty("line.separator");
		}
		assertTrue("The following connections do not exist in the component " + entity.getEntityName() + ": "
				+ missingConnections, missing.isEmpty());

		LinkedList<AssemblyConnector> tooMany = new LinkedList<>();
		for (Connector connector : entity.getConnectors__ComposedStructure()) {
			if (!(connector instanceof AssemblyConnector))
				continue;
			AssemblyConnector con = (AssemblyConnector) connector;

			boolean found = false;
			for (String[] declaredConnector : connection) {
				if (con.getRequiringAssemblyContext_AssemblyConnector().getEncapsulatedComponent__AssemblyContext()
						.getEntityName().equals(declaredConnector[0])
						&& con.getProvidedRole_AssemblyConnector().getProvidedInterface__OperationProvidedRole()
								.getEntityName().equals(declaredConnector[1])
						&& con.getProvidingAssemblyContext_AssemblyConnector()
								.getEncapsulatedComponent__AssemblyContext().getEntityName()
								.equals(declaredConnector[2])) {
					found = true;
					break;
				}
			}
			if (!found)
				tooMany.add(con);
		}
		String tooManyString = "";
		for (AssemblyConnector c : tooMany) {
			tooManyString += "(requirer: "
					+ c.getRequiringAssemblyContext_AssemblyConnector().getEncapsulatedComponent__AssemblyContext()
							.getEntityName()
					+ ", interface: "
					+ c.getProvidedRole_AssemblyConnector().getProvidedInterface__OperationProvidedRole()
							.getEntityName()
					+ ", provider: " + c.getProvidingAssemblyContext_AssemblyConnector()
							.getEncapsulatedComponent__AssemblyContext().getEntityName()
					+ ")";
			tooManyString += java.lang.System.getProperty("line.separator");
		}
		assertEquals("The following connections exist in the component " + entity.getEntityName() + " but should not: "
				+ tooManyString, 0, tooMany.size());
	}

	private void verifyConnectsNone(CompositeComponent component) {
		for (Connector connector : component.getConnectors__ComposedStructure())
			if (connector instanceof AssemblyConnector)
				fail("The component " + component.getEntityName() + " should not have an internal connector.");
	}

	@Test
	public void _5_verifyInterfaces() {
		Repository repository = getRepository(pcmModel);
		EList<Interface> interfaces = repository.getInterfaces__Repository();
		for (Interface iface : interfaces) {
			switch (iface.getEntityName()) {
			case "IProductDispatcher":
				verifyInterfaceIProductDispatcher(iface);
				break;
			case "IReporting":
				verifyInterfaceIReporting(iface);
				break;
			case "IStoreInventoryManager":
				verifyInterfaceIStoreInventoryManager(iface);
				break;
			case "IStoreInventory":
				verifyInterfaceIStoreInventory(iface);
				break;
			case "IBarcodeScanner":
				verifyInterfaceIBarcodeScanner(iface);
				break;
			case "ICardReader":
				verifyInterfaceICardReader(iface);
				break;
			case "ICashBox":
				verifyInterfaceICashBox(iface);
				break;
			case "IExpressLight":
				verifyInterfaceIExpressLight(iface);
				break;
			case "IPrinter":
				verifyInterfaceIPrinter(iface);
				break;
			case "IUserDisplay":
				verifyInterfaceIUserDisplay(iface);
				break;
			case "IBank":
				verifyInterfaceIBank(iface);
				break;
			default:
				fail("Found an unexpected interface " + iface.getEntityName());
				break;
			}
		}
	}

	private void verifyIsOperationInterface(Interface iface) {
		assertTrue("Interface " + iface.getEntityName() + " must be an OperationInterface, but is not.",
				iface instanceof OperationInterface);
	}

	private void verifyOperationsExist(OperationInterface opIface, String... operationNames) {
		List<String> remainingNames = new LinkedList<>();
		remainingNames.addAll(Arrays.asList(operationNames));
		for (int i = 0; i < opIface.getSignatures__OperationInterface().size(); i++) {
			OperationSignature operationSignature = opIface.getSignatures__OperationInterface().get(i);
			String nameFound = null;
			for (String expectedName : remainingNames) {
				if (expectedName.equals(operationSignature.getEntityName())) {
					nameFound = expectedName;
					break;
				}
			}
			if (nameFound != null)
				remainingNames.remove(nameFound);
		}
		assertEquals(
				"The following operations do not exist in the interface " + opIface.getEntityName() + ": "
						+ (remainingNames.stream().reduce((s1, s2) -> s1 + ", " + s2).orElseGet(() -> "")),
				0, remainingNames.size());
		assertEquals(operationNames.length, opIface.getSignatures__OperationInterface().size());
	}

	private void verifyInterfaceIBank(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "validateCard", "debitCard");
	}

	private void verifyInterfaceIUserDisplay(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "setContent", "getMessage", "getMessageKind");
	}

	private void verifyInterfaceIPrinter(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "tearOffPrintout", "printText", "getCurrentPrintout");
	}

	private void verifyInterfaceIExpressLight(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "turnExpressLightOn", "turnExpressLightOff", "isOn");
	}

	private void verifyInterfaceICashBox(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "open", "close", "isOpen", "pressControlKey", "pressNumpadKey");
	}

	private void verifyInterfaceIReporting(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "getStoreStockReport", "getEnterpriseStockReport",
				"getEnterpriseDeliveryReport");
	}

	private void verifyInterfaceICardReader(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "sendCreditCardInfo", "sendCreditCardPin");
	}

	private void verifyInterfaceIBarcodeScanner(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "sendProductBarcode");
	}

	private void verifyInterfaceIStoreInventoryManager(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "getStore", "getProductsWithLowStock", "getAllProducts",
				"getProductsWithStockItems", "orderProducts", "getOrder", "getOutstandingOrders", "rollInReceivedOrder",
				"changePrice", "markProductsUnavailableInStock", "getStockItems");
	}

	private void verifyInterfaceIStoreInventory(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "getProductWithStockItem");
	}

	private void verifyInterfaceIProductDispatcher(Interface iface) {
		verifyIsOperationInterface(iface);
		OperationInterface opIface = (OperationInterface) iface;
		verifyOperationsExist(opIface, "dispatchProductsFromOtherStores");
	}

	private Repository getRepository(List<EObject> eObjects) {
		for (EObject eObject : eObjects)
			if (eObject instanceof Repository)
				return (Repository) eObject;
		fail("No repository exists in the PCM model");
		return null;
	}

	private System getSystem(List<EObject> eObjects) {
		for (EObject eObject : eObjects)
			if (eObject instanceof System)
				return (System) eObject;
		fail("No system exists in the PCM model");
		return null;
	}

}
