# $toolName - A tool for co-evolving software architecture specifications and their implementation

Please note: This repository is anonymized a little. You can probably scan the structure without identifying the author, but this cannot be guaranteed.
Due to the anonymization, the installation instruction do not work. To see the actual repository, please use the link at the very bottom of this file.

## Set up Development Environment
Install a fresh [Eclipse Oxygen Modeling](http://eclipse.org/oxygen/).

Then install the following features of the stated update sites:
* Oxygen Update Site (http://download.eclipse.org/releases/oxygen)
    * m2e - Maven Integration for Eclipse
* Modeling Package Updates for Eclipse Oxygen (http://www.eclipse.org/modeling/amalgam/downloads/package/modeling/oxygen/)
    * Xtend IDE
* Codeling All-in-One-Update-Site (http://someUniversity.org/pub/p2/$toolName/)
    * Everything under the category ```$toolName Dependencies```

Restart Eclipse.

Now you should install m2e connectors. Go to Preferences -> Maven -> Discovery -> Open Catalog. Here, install the connectors

* m2e-egit (necessary for the menu item "Import Maven projects" in Git Repository View)
* Tycho Configurators (necessary for building eclipse plugins with maven)

Restart Eclipse again.

Now clone the project:
Go to the Git Perspective: ```Window -> Perspectives -> Open Perspective -> Other -> Git```.
Copy the URL of this repository ```https://gitlab.com/anonymousResearcher/Co-Evolution-Tool.git```  or the SSH link ```git@gitlab.com/anonymousResearcher/Co-Evolution-Tool.git``` (you will need an SSH key pair for this variant) and click onto ```Clone a Git repository```.
Finish the dialog without changing anything.

A new repository has been added. Unfold it, make a right click on ```Working Tree``` and use ```Import Maven Projects```

open the Java-Perspective and you are ready for coding.

## Start the tool
Start a new eclipse instance.

## Running the tool (not developing the tool)
Install a fresh [Eclipse Oxygen Modeling](http://eclipse.org/oxygen/).

Then install the following features of the stated update site:
* $toolName All-in-One-Update-Site (http://someUniversity.org/pub/p2/$toolName/)
    * Everything

Restart Eclipse.

With a right click on any project and a click on 'Show Integrated Architecture Model' you can make the tool extract the integrated architecture model of the project.
The tool will create a new project called 'architecture-carrying-code-temp' with the respective architectural models.




























[Follow this link to see the actual repository](https://codeling.de/).