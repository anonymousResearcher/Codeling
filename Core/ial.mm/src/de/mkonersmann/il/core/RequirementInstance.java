/**
 */
package de.mkonersmann.il.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requirement Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.mkonersmann.il.core.RequirementInstance#getRequirement <em>Requirement</em>}</li>
 * </ul>
 *
 * @see de.mkonersmann.il.core.CorePackage#getRequirementInstance()
 * @model
 * @generated
 */
public interface RequirementInstance extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement</em>' reference.
	 * @see #setRequirement(Requirement)
	 * @see de.mkonersmann.il.core.CorePackage#getRequirementInstance_Requirement()
	 * @model required="true"
	 * @generated
	 */
	Requirement getRequirement();

	/**
	 * Sets the value of the '{@link de.mkonersmann.il.core.RequirementInstance#getRequirement <em>Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requirement</em>' reference.
	 * @see #getRequirement()
	 * @generated
	 */
	void setRequirement(Requirement value);

} // RequirementInstance
