/**
 */
package de.mkonersmann.il.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provision Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.mkonersmann.il.core.ProvisionInstance#getProvision <em>Provision</em>}</li>
 * </ul>
 *
 * @see de.mkonersmann.il.core.CorePackage#getProvisionInstance()
 * @model
 * @generated
 */
public interface ProvisionInstance extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Provision</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provision</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provision</em>' reference.
	 * @see #setProvision(Provision)
	 * @see de.mkonersmann.il.core.CorePackage#getProvisionInstance_Provision()
	 * @model required="true"
	 * @generated
	 */
	Provision getProvision();

	/**
	 * Sets the value of the '{@link de.mkonersmann.il.core.ProvisionInstance#getProvision <em>Provision</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provision</em>' reference.
	 * @see #getProvision()
	 * @generated
	 */
	void setProvision(Provision value);

} // ProvisionInstance
