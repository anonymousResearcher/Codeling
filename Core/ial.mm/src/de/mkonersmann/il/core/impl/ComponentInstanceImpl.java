/**
 */
package de.mkonersmann.il.core.impl;

import de.mkonersmann.il.core.ComponentInstance;
import de.mkonersmann.il.core.ComponentType;
import de.mkonersmann.il.core.CorePackage;
import de.mkonersmann.il.core.ProvisionInstance;
import de.mkonersmann.il.core.RequirementInstance;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.mkonersmann.il.core.impl.ComponentInstanceImpl#getRequirements <em>Requirements</em>}</li>
 *   <li>{@link de.mkonersmann.il.core.impl.ComponentInstanceImpl#getProvisions <em>Provisions</em>}</li>
 *   <li>{@link de.mkonersmann.il.core.impl.ComponentInstanceImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentInstanceImpl extends IdentifiedElementImpl implements ComponentInstance {
	/**
	 * The cached value of the '{@link #getRequirements() <em>Requirements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequirements()
	 * @generated
	 * @ordered
	 */
	protected EList<RequirementInstance> requirements;

	/**
	 * The cached value of the '{@link #getProvisions() <em>Provisions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvisions()
	 * @generated
	 * @ordered
	 */
	protected EList<ProvisionInstance> provisions;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ComponentType type;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.COMPONENT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RequirementInstance> getRequirements() {
		if (requirements == null) {
			requirements = new EObjectContainmentEList<RequirementInstance>(RequirementInstance.class, this, CorePackage.COMPONENT_INSTANCE__REQUIREMENTS);
		}
		return requirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProvisionInstance> getProvisions() {
		if (provisions == null) {
			provisions = new EObjectContainmentEList<ProvisionInstance>(ProvisionInstance.class, this, CorePackage.COMPONENT_INSTANCE__PROVISIONS);
		}
		return provisions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (ComponentType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.COMPONENT_INSTANCE__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(ComponentType newType) {
		ComponentType oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.COMPONENT_INSTANCE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.COMPONENT_INSTANCE__REQUIREMENTS:
				return ((InternalEList<?>)getRequirements()).basicRemove(otherEnd, msgs);
			case CorePackage.COMPONENT_INSTANCE__PROVISIONS:
				return ((InternalEList<?>)getProvisions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.COMPONENT_INSTANCE__REQUIREMENTS:
				return getRequirements();
			case CorePackage.COMPONENT_INSTANCE__PROVISIONS:
				return getProvisions();
			case CorePackage.COMPONENT_INSTANCE__TYPE:
				if (resolve) return getType();
				return basicGetType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.COMPONENT_INSTANCE__REQUIREMENTS:
				getRequirements().clear();
				getRequirements().addAll((Collection<? extends RequirementInstance>)newValue);
				return;
			case CorePackage.COMPONENT_INSTANCE__PROVISIONS:
				getProvisions().clear();
				getProvisions().addAll((Collection<? extends ProvisionInstance>)newValue);
				return;
			case CorePackage.COMPONENT_INSTANCE__TYPE:
				setType((ComponentType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.COMPONENT_INSTANCE__REQUIREMENTS:
				getRequirements().clear();
				return;
			case CorePackage.COMPONENT_INSTANCE__PROVISIONS:
				getProvisions().clear();
				return;
			case CorePackage.COMPONENT_INSTANCE__TYPE:
				setType((ComponentType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.COMPONENT_INSTANCE__REQUIREMENTS:
				return requirements != null && !requirements.isEmpty();
			case CorePackage.COMPONENT_INSTANCE__PROVISIONS:
				return provisions != null && !provisions.isEmpty();
			case CorePackage.COMPONENT_INSTANCE__TYPE:
				return type != null;
		}
		return super.eIsSet(featureID);
	}

	
	/**
	 * @generated not
	 */
	@Override
	public String toString() {
		return String.format("%s (type: %s)", super.toString(), getType() == null ? "null" : getType().getName());
	}
} //ComponentInstanceImpl
