/**
 */
package de.mkonersmann.il.core.impl;

import de.mkonersmann.il.core.ComponentInstance;
import de.mkonersmann.il.core.CorePackage;
import de.mkonersmann.il.core.Provision;
import de.mkonersmann.il.core.ProvisionInstance;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provision Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.mkonersmann.il.core.impl.ProvisionInstanceImpl#getProvision <em>Provision</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProvisionInstanceImpl extends IdentifiedElementImpl implements ProvisionInstance {
	/**
	 * The cached value of the '{@link #getProvision() <em>Provision</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvision()
	 * @generated
	 * @ordered
	 */
	protected Provision provision;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProvisionInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.PROVISION_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Provision getProvision() {
		if (provision != null && provision.eIsProxy()) {
			InternalEObject oldProvision = (InternalEObject)provision;
			provision = (Provision)eResolveProxy(oldProvision);
			if (provision != oldProvision) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PROVISION_INSTANCE__PROVISION, oldProvision, provision));
			}
		}
		return provision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Provision basicGetProvision() {
		return provision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvision(Provision newProvision) {
		Provision oldProvision = provision;
		provision = newProvision;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROVISION_INSTANCE__PROVISION, oldProvision, provision));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.PROVISION_INSTANCE__PROVISION:
				if (resolve) return getProvision();
				return basicGetProvision();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.PROVISION_INSTANCE__PROVISION:
				setProvision((Provision)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.PROVISION_INSTANCE__PROVISION:
				setProvision((Provision)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.PROVISION_INSTANCE__PROVISION:
				return provision != null;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * @generated not
	 */
	@Override
	public String toString() {
		return String.format("%s (Interface: %s, Provided By: %s)", super.toString(),
				getProvision() == null ? "null"
						: getProvision().getInterface() == null ? "null" : getProvision().getInterface().getName(),
				((ComponentInstance) eContainer).toString());
	}
} //ProvisionInstanceImpl
