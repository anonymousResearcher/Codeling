/**
 */
package de.mkonersmann.il.core.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.mkonersmann.il.core.ComponentInstance;
import de.mkonersmann.il.core.CorePackage;
import de.mkonersmann.il.core.Requirement;
import de.mkonersmann.il.core.RequirementInstance;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Requirement Instance</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.mkonersmann.il.core.impl.RequirementInstanceImpl#getRequirement
 * <em>Requirement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequirementInstanceImpl extends IdentifiedElementImpl implements RequirementInstance {
	/**
	 * The cached value of the '{@link #getRequirement() <em>Requirement</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getRequirement()
	 * @generated
	 * @ordered
	 */
	protected Requirement requirement;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected RequirementInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.REQUIREMENT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Requirement getRequirement() {
		if (requirement != null && requirement.eIsProxy()) {
			InternalEObject oldRequirement = (InternalEObject) requirement;
			requirement = (Requirement) eResolveProxy(oldRequirement);
			if (requirement != oldRequirement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							CorePackage.REQUIREMENT_INSTANCE__REQUIREMENT, oldRequirement, requirement));
			}
		}
		return requirement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Requirement basicGetRequirement() {
		return requirement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setRequirement(Requirement newRequirement) {
		Requirement oldRequirement = requirement;
		requirement = newRequirement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.REQUIREMENT_INSTANCE__REQUIREMENT,
					oldRequirement, requirement));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CorePackage.REQUIREMENT_INSTANCE__REQUIREMENT:
			if (resolve)
				return getRequirement();
			return basicGetRequirement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CorePackage.REQUIREMENT_INSTANCE__REQUIREMENT:
			setRequirement((Requirement) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CorePackage.REQUIREMENT_INSTANCE__REQUIREMENT:
			setRequirement((Requirement) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CorePackage.REQUIREMENT_INSTANCE__REQUIREMENT:
			return requirement != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @generated not
	 */
	@Override
	public String toString() {
		return String.format("%s (Interface: %s, Required By: %s)", super.toString(),
				getRequirement() == null ? "null"
						: getRequirement().getInterface() == null ? "null" : getRequirement().getInterface().getName(),
				((ComponentInstance) eContainer).toString());
	}
} // RequirementInstanceImpl
