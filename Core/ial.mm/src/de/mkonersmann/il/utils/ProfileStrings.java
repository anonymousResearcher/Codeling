package de.mkonersmann.il.utils;

import org.eclipse.osgi.util.NLS;

public class ProfileStrings extends NLS {
	private static final String BUNDLE_NAME = "de.mkonersmann.il.utils.profileStrings"; //$NON-NLS-1$
	public static String ANNOTATION_SHARED_CONTEXT_CHILDCOMPONENT_FQN;
	public static String PROFILE_PLUGIN_PATH_SHAREDCONTEXT;
	public static String STEREOTYPE_HIERARCHICAL_COMPONENT_TYPE_SHARED_CONTEXT;
	public static String STEREOTYPE_HIERARCHICAL_COMPONENT_TYPE_SHARED_CONTEXT_REFERENCE_CHILDINSTANCES;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, ProfileStrings.class);
	}

	private ProfileStrings() {
	}
}
