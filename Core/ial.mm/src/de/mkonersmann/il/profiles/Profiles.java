package de.mkonersmann.il.profiles;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.emfprofile.EMFProfilePackage;
import org.modelversioning.emfprofile.Profile;
import org.modelversioning.emfprofileapplication.EMFProfileApplicationPackage;

public enum Profiles {
	BEHAVIOUR_STATEMACHINE("behavior/statemachine", "behaviour-statemachine"),
	COMPONENTS_HIERARCHY_FLAT("componenthierarchy/flat", "components-hierarchy-flat"),
	COMPONENTS_HIERARCHY_SCOPED("componenthierarchy/scoped", "components-hierarchy-scoped"),
	COMPONENTS_HIERARCHY_SHAREDCONTEXT("componenthierarchy/sharedcontext", "components-hierarchy-sharedcontext"),
	COMPONENTS_INSTANTIATION_FIXED("components/instantiation/fixed", "components-instantiation-fixed"),
	COMPONENTS_INSTANTIATION_PERSESSION("components/instantiation/persession", "components-instantiation-persession"),
	COMPONENTS_INSTANTIATION_POOLED("components/instantiation/pooled", "components-instantiation-pooled"),
	COMPONENTS_STATE_STATEFUL("components/stateful", "components-state-stateful"),
	COMPONENTS_STATE_STATELESS("components/stateless", "components-state-stateless"),
	CONNECTORS("connectors", "connectors"),
	CONNECTORS_DELEGATION("connectors/delegation", "connectors-delegation"),
	CONNECTORS_EVENTS_1TON("connectors/events/1ton", "connectors-events-onetomany"),
	CONNECTORS_PROCEDURE_CALLS_SYNCHRONOUS_1TO1("connectors/pc/sync/1to1", "connectors-pc-sync-onetoone"),
	DATATYPES_COMMON("datatypes/common", "datatypes-common"),
	DATATYPES_EVENTS("datatypes/events", "datatypes-events"),
	DATATYPES_OPERATIONS("datatypes/operations", "datatypes-operations"),
	DEPLOYMENT("deployment", "deployment"),
	INTERFACES_SCOPED("interfaces/scoped", "interfaces-scoped"),
	INTERFACES_SHARED("interfaces/shared", "interfaces-shared"),
	INTERFACES_TYPE_EVENTS("interfaces/type/events", "interfaces-type-events"),
	INTERFACES_TYPE_OPERATIONS("interfaces/type/operations", "interfaces-type-operations"),
	NAMESPACES("namespaces", "namespaces"),
	QUALITY_SECURITY_SECURE_INFORMATION_FLOW("quality/sec/sif", "quality-sec-sif"),
	QUALITY_TIME("quality/time", "quality-time");

	static final String URI_PREFIX = "http://mkonersmann.de/il/profiles/";
	static final String URI_POSTFIX = "/1.0";
	private String nsUri;
	static final String PATH_PREFIX = "platform:/plugin/il.core/src/de/mkonersmann/il/profiles/";
	static final String PATH_POSTFIX = ".emfprofile_diagram";
	private String profilePath;


	private Profiles(String nsUri, String profilePathSegment) {
		this.nsUri = nsUri;
		this.profilePath = profilePathSegment;
	}

	public String getTransformationSourcePath() {
		return nsUri;
	}

	public String getTransformationTargetPath() {
		final String[] fragments = nsUri.split("/");
		final String targetPath = fragments[fragments.length - 1];
		return targetPath;
	}

	public String getNsURI() {
		return URI_PREFIX + nsUri + URI_POSTFIX;
	}

	public String getProfilePath() {
		return PATH_PREFIX + profilePath + PATH_POSTFIX;
	}

	public Profile load() {
		// Load the packages
		EMFProfilePackage.eINSTANCE.eClass();
		EMFProfileApplicationPackage.eINSTANCE.eClass();

		final ResourceSet resSet = new ResourceSetImpl();
		final Resource resource = resSet.getResource(URI.createURI(getNsURI(), true), true);
		final Profile profile = (Profile) resource.getContents().get(0);
		return profile;
	}
}
