package org.codeling.transformationmanager.internal;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.codeling.lang.base.java.JDTUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.osgi.framework.FrameworkUtil;

public abstract class AbstractTransformationTaskTest {

	protected final String BUNDLE_NAME = FrameworkUtil.getBundle(getClass()).getSymbolicName();

	protected IJavaProject setupJavaProject()
			throws ExecutionException, CoreException, JavaModelException, IOException {
		final IProject project = JDTUtils.getProject("wrongtitle23");
		if (project != null) {
			project.delete(true, null);
		}
		final IJavaProject javaProject = JDTUtils.createSimpleProject("wrongtitle23");

		final IPackageFragment pf = findSourceFolder(javaProject).createPackageFragment("meinpaketear", true, null);

		final String[] filesToLoad = new String[] { "BusinessInteface.java", "ServiceInterfaceNew2.java",
				"ServiceProvider2.java", "ServiceUser.java", "ThisIsAServiceUser.java" };

		for (final String fileName : filesToLoad) {
			final String source = getTestJavaSource(this.getClass().getClassLoader(),
					pf.getElementName() + System.getProperty("file.separator") + fileName);
			pf.createCompilationUnit(fileName, source, true, null);
		}
		return javaProject;
	}

	/**
	 * Returns the first found source folder in the given java project.
	 *
	 * @param javaProject
	 * @return
	 * @throws JavaModelException
	 */
	private IPackageFragmentRoot findSourceFolder(IJavaProject javaProject) throws JavaModelException {
		for (final IPackageFragmentRoot pfr : javaProject.getPackageFragmentRoots()) {
			if (!pfr.isArchive())
				return pfr;
		}
		return null;
	}

	protected ResourceSet resSet = new ResourceSetImpl();

	protected List<EObject> loadModel(URI uri) {
		final Resource resource = resSet.getResource(uri, true);
		return resource.getContents();
	}

	protected <T extends EObject> T getElementByName(EList<T> list, String name) {
		for (final T t : list) {
			final EStructuralFeature nameFeature = t.eClass().getEStructuralFeature("name");
			final String tName = (String) t.eGet(nameFeature);
			if (name.equals(tName)) {
				return t;
			}
		}
		fail("Could not find element with name '" + name + "'.");
		return null;
	}

	public static String getTestJavaSource(ClassLoader classLoader, String resourcePath) throws IOException {
		URL url;
		BufferedReader in = null;
		String content = "";

		try {
			url = classLoader.getResource("src/test/resources/" + resourcePath);
			final InputStream inputStream = url.openConnection().getInputStream();
			in = new BufferedReader(new InputStreamReader(inputStream));
			String inputLine;
			final String nl = System.getProperty("line.separator");
			while ((inputLine = in.readLine()) != null) {
				content += inputLine + nl;
			}
		} catch (final IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null)
				in.close();
		}
		return content;
	}
}
