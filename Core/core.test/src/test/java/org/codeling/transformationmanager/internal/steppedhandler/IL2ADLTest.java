package org.codeling.transformationmanager.internal.steppedhandler;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Set;

import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.languageregistry.internal.LanguageRegistry;
import org.codeling.test.ecore.CompareEcoreModels;
import org.codeling.transformationmanager.internal.AbstractTransformationTaskTest;
import org.codeling.transformationmanager.internal.steps.TranslationModelToSpecificationModelTask;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.junit.Test;

public class IL2ADLTest extends AbstractTransformationTaskTest {
	@Test
	public void il2adlTest() throws Exception {
		final List<EObject> priorModel = loadILModel();
		final IDRegistry priorIdRegistry = loadIdRegistry();

		final List<EObject> actualModel = executeModelTransformation(priorModel, priorIdRegistry);

		assertNotNull(actualModel);

		final List<EObject> expectedRoots = loadExpectedModel();
		new CompareEcoreModels().ignoringIds().compare(expectedRoots, actualModel);

	}

	private List<EObject> executeModelTransformation(List<EObject> priorModel, IDRegistry priorIdRegistry)
			throws InterruptedException, CoreException, CodelingException {
		SpecificationLanguageDefinition modelingLanguage = null;
		final Set<SpecificationLanguageDefinition> languages = LanguageRegistry.getInstance().getSpecificationLanguages();
		for (final SpecificationLanguageDefinition ld : languages) {
			if (ld.getName().equals("Palladio Component Model")) {
				modelingLanguage = ld;
			}
		}

		final TranslationModelToSpecificationModelTask task = new TranslationModelToSpecificationModelTask(modelingLanguage, priorIdRegistry, priorModel, true);
		task.schedule();
		task.join();

		assertTrue("Errors during translation. See error log / console output for details.", task.isOK());
		return task.getResultModelRoots();
	}

	private List<EObject> loadExpectedModel() {
		return loadModel(URI.createPlatformPluginURI(BUNDLE_NAME + "/src/test/resources/expected/5_il2adl.xmi", true));
	}

	private List<EObject> loadILModel() {
		return loadModel(URI.createPlatformPluginURI(
				BUNDLE_NAME + "/src/test/resources/expected/4_ilModuleTransformations.xmi", true));
	}

	private IDRegistry loadIdRegistry() throws MalformedURLException, IOException {
		return IDRegistry.load(new URL(
				"platform:/plugin/" + BUNDLE_NAME+ "/src/test/resources/expected/4_ilModuleTransformations.ids"));
	}

}
