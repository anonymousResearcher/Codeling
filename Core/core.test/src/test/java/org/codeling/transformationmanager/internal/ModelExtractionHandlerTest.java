package org.codeling.transformationmanager.internal;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.languageregistry.internal.LanguageRegistry;
import org.codeling.test.ecore.CompareEcoreModels;
import org.codeling.test.ecore.Ecore2Dot;
import org.codeling.transformationmanager.internal.ModelExtractionTask;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;
import org.junit.Test;
import org.osgi.framework.FrameworkUtil;

public class ModelExtractionHandlerTest extends AbstractTransformationTaskTest {

	private static final String BUNDLE_NAME = FrameworkUtil.getBundle(ModelExtractionHandlerTest.class)
			.getSymbolicName();

	@Test
	public void testExtractionToADL() throws Exception {
		final LinkedList<IJavaProject> projects = new LinkedList<>();
		projects.add(setupJavaProject());

		ImplementationLanguageDefinition implementationLanguage = null;
		SpecificationLanguageDefinition modelingLanguage = null;
		final Set<LanguageDefinition> languages = LanguageRegistry.getInstance().getLanguages();
		for (final LanguageDefinition ld : languages) {
			if (ld.getName().equals("Enterprise JavaBeans")) {
				implementationLanguage = (ImplementationLanguageDefinition) ld;
			} else if (ld.getName().equals("Palladio Component Model")) {
				modelingLanguage = (SpecificationLanguageDefinition) ld;
			}
		}


		final ModelExtractionTask task = executeModelExtraction(projects, implementationLanguage, modelingLanguage);
		assertTrue(task.isOK());

		final List<EObject> actualRoots = task.getResultModelRoots();
		final Path actual = new Ecore2Dot(actualRoots).storeToFile();
		System.out.println(
				"### To see the actual graph, execute: dot -Tpng " + actual.toAbsolutePath() + " > actual.png");

		final List<EObject> expectedRoots = loadExpectedADLModel();
		final Path expected = new Ecore2Dot(expectedRoots).storeToFile();
		System.out.println(
				"### To see the expected graph, execute: dot -Tpng " + expected.toAbsolutePath() + " > expected.png");

		new CompareEcoreModels().ignoringIds().compare(expectedRoots, actualRoots);
	}

	@Test
	public void testExtractionToCM() throws Exception {
		final LinkedList<IJavaProject> projects = new LinkedList<>();
		projects.add(setupJavaProject());

		ImplementationLanguageDefinition implementationLanguage = null;
		final Set<ImplementationLanguageDefinition> languages = LanguageRegistry.getInstance()
				.getImplementationLanguages();
		for (final ImplementationLanguageDefinition ld : languages) {
			if (ld.getName().equals("Enterprise JavaBeans")) {
				implementationLanguage = ld;
			}
		}

		final ModelExtractionTask task = executeModelExtraction(projects, implementationLanguage, null);
		assertTrue(task.isOK());

		final List<EObject> actualRoots = task.getResultModelRoots();
		final Path actual = new Ecore2Dot(actualRoots).storeToFile();
		System.out.println(
				"### To see the actual graph, execute: dot -Tpng " + actual.toAbsolutePath() + " > actual.png");

		final List<EObject> expectedRoots = loadExpectedAILModel();
		final Path expected = new Ecore2Dot(expectedRoots).storeToFile();
		System.out.println(
				"### To see the expected graph, execute: dot -Tpng " + expected.toAbsolutePath() + " > expected.png");

		new CompareEcoreModels().ignoringIds().compare(expectedRoots, actualRoots);
	}

	private ModelExtractionTask executeModelExtraction(LinkedList<IJavaProject> projects,
			ImplementationLanguageDefinition implementationLanguage, SpecificationLanguageDefinition modelingLanguage)
			throws InterruptedException, CoreException {
		final ModelExtractionTask task = new ModelExtractionTask(implementationLanguage, modelingLanguage, projects);
		task.schedule();
		task.join();

		// assertTrue("Errors during translation. See error log / console output
		// for details.", handler.isOK());
		return task;
	}

	private List<EObject> loadExpectedADLModel() {
		return loadModel(URI.createPlatformPluginURI(BUNDLE_NAME + "/src/test/resources/expected/5_il2adl.xmi", true));
	}

	private List<EObject> loadExpectedAILModel() {
		return loadModel(URI.createPlatformPluginURI(BUNDLE_NAME + "/src/test/resources/expected/1_code2cm.xmi", true));
	}

}
