package org.codeling.transformationmanager.testsuites;

import org.codeling.transformationmanager.internal.steppedhandler.CM2IALTest;
import org.codeling.transformationmanager.internal.steppedhandler.Code2AILTest;
import org.codeling.transformationmanager.internal.steppedhandler.CodeAndAIL2ILTest;
import org.codeling.transformationmanager.internal.steppedhandler.IL2ADLTest;
import org.codeling.transformationmanager.internal.steppedhandler.IntermoduleTransformationsToModelTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CM2IALTest.class, Code2AILTest.class, CodeAndAIL2ILTest.class, IL2ADLTest.class,
		IntermoduleTransformationsToModelTest.class })
public class TransformationStepsTestSuite {

}
