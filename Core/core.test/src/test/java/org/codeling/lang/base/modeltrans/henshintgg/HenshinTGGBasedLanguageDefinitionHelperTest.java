package org.codeling.lang.base.modeltrans.henshintgg;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.codeling.lang.base.modeltrans.henshintgg.HenshinTGGBasedLanguageDefinitionHelper;
import org.eclipse.emf.common.util.URI;
import org.junit.Test;
import org.osgi.framework.FrameworkUtil;

public class HenshinTGGBasedLanguageDefinitionHelperTest {
	/**
	 * Tests whether the profiles are correctly extracted from the tgg
	 * transformation file.
	 */
	@Test
	public void getSelectedProfilesMatchesExpectations_Source() {
		final String[] expected = new String[] { "http://mkonersmann.de/il/core/1.0",
				"http://mkonersmann.de/il/profiles/componenthierarchy/flat/1.0",
				"http://mkonersmann.de/il/profiles/behavior/statemachine/1.0",
				"http://mkonersmann.de/il/profiles/components/stateless/1.0",
				"http://mkonersmann.de/il/profiles/interfaces/scoped/1.0" };

		final URI transformationFileURI = URI
				.createPlatformPluginURI(FrameworkUtil.getBundle(getClass()).getSymbolicName()
						+ "/src/test/resources/dummy/dummySourceLanguageTransformation.henshin", true);
		final List<String> profiles = new HenshinTGGBasedLanguageDefinitionHelper()
				.getSelectedProfiles(transformationFileURI);
		assertEquals(expected.length, profiles.size());
		for (final String e : expected) {
			assertTrue(String.format("Profile '%s' is missing in the result.", e), profiles.contains(e));
		}
	}

	/**
	 * Tests whether the profiles are correctly extracted from the tgg
	 * transformation file.
	 */
	@Test
	public void getSelectedProfilesMatchesExpectations_Target() {
		final String[] expected = new String[] { "http://mkonersmann.de/il/core/1.0",
				"http://mkonersmann.de/il/profiles/componenthierarchy/scoped/1.0",
				"http://mkonersmann.de/il/profiles/interfaces/shared/1.0" };

		final URI transformationFileURI = URI
				.createPlatformPluginURI(FrameworkUtil.getBundle(getClass()).getSymbolicName()
						+ "/src/test/resources/dummy/dummyTargetLanguageTransformation.henshin", true);
		final List<String> profiles = new HenshinTGGBasedLanguageDefinitionHelper()
				.getSelectedProfiles(transformationFileURI);
		assertEquals(expected.length, profiles.size());
		for (final String e : expected) {
			assertTrue(String.format("Profile '%s' is missing in the result.", e), profiles.contains(e));
		}
	}
}
