/**
 *
 */
package org.codeling.lang.base.modeltrans.henshintgg;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.codeling.lang.base.modeltrans.henshintgg.HenshinTGGTransformation;
import org.codeling.lang.base.modeltrans.henshintgg.TGGDirection;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.IDRegistry;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.FrameworkUtil;

import source.Association;
import source.Attribute;
import source.ClassDiagram;
import source.PrimitiveDataType;
import source.SourceFactory;
import targetLanguage.Column;
import targetLanguage.Database;
import targetLanguage.FKey;
import targetLanguage.Table;
import targetLanguage.TargetLanguageFactory;

/**
 * Tests for the M2M component.
 *
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public class M2MTest {

	private static final String BUNDLE_NAME = FrameworkUtil.getBundle(M2MTest.class).getSymbolicName();

	/**
	 * Loads the meta model packages before each test.
	 */
	@Before
	public void setup() {
		source.SourcePackage.eINSTANCE.getName();
		targetLanguage.TargetLanguagePackage.eINSTANCE.getName();
		correspondece.CorrespondecePackage.eINSTANCE.getName();
	}

	/**
	 * Test a successful forward translation.
	 */
	@Test
	public void testForwardTranslation() {
		// Prepare test
		final URI henshinTggFileUri = URI
				.createPlatformPluginURI("/" + BUNDLE_NAME + "/src/test/resources/m2m-test.henshin", true);

		final LinkedList<EObject> modelRoots = new LinkedList<>();
		modelRoots.add(createClassDiagramModel());
		final IDRegistry idRegistry = createIDRegistry(modelRoots);

		// Execute test method
		final HenshinTGGTransformation task = new HenshinTGGTransformation(null, henshinTggFileUri, "",
				TGGDirection.FORWARD, idRegistry, modelRoots);
		task.transformModel(modelRoots, henshinTggFileUri, null);
		final TransformationResult result = task.exportModel(idRegistry, null);

		// Check results
		assertNotNull(result.getModelRoots());
		assertEquals(1, result.getModelRoots().size());
		assertTrue(result.getModelRoots().get(0) instanceof Database);
	}

	/**
	 * Tests a successful backwards translation.
	 */
	@Test
	public void testBackwardsTranslation() {
		// Prepare
		final URI henshinTggFileUri = URI
				.createPlatformPluginURI("/" + BUNDLE_NAME + "/src/test/resources/m2m-test.henshin", true);

		final LinkedList<EObject> modelRoots = new LinkedList<>();
		modelRoots.add(createDatabaseModel());
		final IDRegistry idRegistry = createIDRegistry(modelRoots);

		// Execute test method
		final HenshinTGGTransformation task = new HenshinTGGTransformation(null, henshinTggFileUri, "",
				TGGDirection.BACKWARD, idRegistry, modelRoots);
		task.transformModel(modelRoots, henshinTggFileUri, null);
		final TransformationResult result = task.exportModel(idRegistry, null);

		// Check results
		assertNotNull(result.getModelRoots());
		assertEquals(1, result.getModelRoots().size());
		assertTrue(result.getModelRoots().get(0) instanceof Database);
	}

	/**
	 * Creates a test source model.
	 *
	 * @return
	 */
	public Database createDatabaseModel() {
		final TargetLanguageFactory fac = TargetLanguageFactory.eINSTANCE;

		final Database db = fac.createDatabase();
		final Table person = fac.createTable();
		person.setName("Person");
		db.getTable().add(person);

		final Column cust_id = fac.createColumn();
		cust_id.setName("cust_id");
		cust_id.setType("int");
		person.getCols().add(cust_id);
		person.setPkey(cust_id);

		final Table company = fac.createTable();
		company.setName("Company");
		db.getTable().add(company);

		final Column employee_cust_id = fac.createColumn();
		employee_cust_id.setName("employee_cust_id");
		employee_cust_id.setType("int");
		company.getCols().add(employee_cust_id);

		final FKey fkey = fac.createFKey();
		fkey.setReferences(person);
		fkey.getFcols().add(employee_cust_id);
		company.getFkeys().add(fkey);

		return db;
	}

	/**
	 * Creates a test target model.
	 *
	 * @return
	 */
	public ClassDiagram createClassDiagramModel() {
		final ClassDiagram cd = SourceFactory.eINSTANCE.createClassDiagram();

		final PrimitiveDataType ptype = SourceFactory.eINSTANCE.createPrimitiveDataType();
		ptype.setName("int");
		cd.getPtypes().add(ptype);

		final source.Class class1 = SourceFactory.eINSTANCE.createClass();
		class1.setName("Company");
		cd.getClass_().add(class1);

		final source.Class class2 = SourceFactory.eINSTANCE.createClass();
		class2.setName("Person");
		final Attribute c2a1 = SourceFactory.eINSTANCE.createAttribute();
		c2a1.setIs_primary(true);
		c2a1.setName("id");
		c2a1.setType(class1);
		cd.getClass_().add(class2);

		final source.Class class3 = SourceFactory.eINSTANCE.createClass();
		class3.setName("Customer");
		class3.setParent(class2);

		final Attribute cust_id_attribute = SourceFactory.eINSTANCE.createAttribute();
		cust_id_attribute.setIs_primary(true);
		cust_id_attribute.setName("cust_id");
		cust_id_attribute.setPtype(ptype);
		class3.getAttrs().add(cust_id_attribute);

		cd.getClass_().add(class3);

		final Association assoc = SourceFactory.eINSTANCE.createAssociation();
		assoc.setName("employee");
		assoc.setSrc(class2);
		assoc.setDest(class3);
		cd.getAss().add(assoc);

		return cd;
	}

	private IDRegistry createIDRegistry(List<EObject> modelRoots) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IJavaProject mockElement = JavaCore.create(root.getProject("mockProject"));
		final IDRegistry idRegistry = new IDRegistry();
		for (final EObject o : modelRoots) {
			idRegistry.registerImplementationModelElement(mockElement, o);
			for (final Iterator<EObject> it = o.eAllContents(); it.hasNext();) {
				final EObject child = it.next();
				idRegistry.registerImplementationModelElement(mockElement, child);
			}
		}
		return idRegistry;
	}
}
