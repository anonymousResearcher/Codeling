package org.codeling.interprofile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;

import org.codeling.test.ecore.CompareEcoreModels;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IContributor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.UnitApplication;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.impl.EngineImpl;
import org.eclipse.emf.henshin.interpreter.impl.LoggingApplicationMonitor;
import org.eclipse.emf.henshin.interpreter.impl.UnitApplicationImpl;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modelversioning.emfprofile.Profile;
import org.modelversioning.emfprofile.registry.IProfileRegistry;
import org.osgi.framework.FrameworkUtil;

import de.mkonersmann.il.core.CorePackage;

public class TransformationTest {

	private final String BUNDLE_NAME = FrameworkUtil.getBundle(getClass()).getSymbolicName();

	Engine engine = new EngineImpl();

	private static void loadProfiles() {
		final ResourceSet resourceSet = new ResourceSetImpl();

		// Search all plugins that extend the profile extension point
		final IConfigurationElement[] config = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(IProfileRegistry.PROFILE_EXTENSION_POINT_ID);

		for (final IConfigurationElement configElement : config) {
			final IContributor contributor = configElement.getContributor();
			final String profileResourceName = configElement
					.getAttribute(IProfileRegistry.PROFILE_EXTENSION_POINT_RESOURCE_NAME);

			String platformPluginURIString = "platform:/plugin/" + contributor.getName() //$NON-NLS-1$
					+ "/" + profileResourceName;
			URI uri = URI.createURI(platformPluginURIString);
			Resource profileResource = resourceSet.getResource(uri, false);
			if (profileResource == null) {
				// The plugins define the source path to the profile. When the tests are run
				// with maven, they need to be registered with from the root directory.
				platformPluginURIString = "platform:/plugin/" + contributor.getName() //$NON-NLS-1$
						+ "/" + profileResourceName.substring("/src".length());
				uri = URI.createURI(platformPluginURIString);
				profileResource = resourceSet.getResource(uri, true);
			}

			final Profile profile = (Profile) profileResource.getContents().get(0);
			EPackage.Registry.INSTANCE.put(profile.getNsURI(), profile);
			// Cannot use IProfileRegistry.eINSTANCE.registerProfile(n); because
			// the profiles are considered invalid when the tests are run with
			// maven.
		}
	}

	@BeforeClass
	public static void init() {
		// When the tests are run with maven, the profiles cannot be registered,
		// because they are considerd invalid. Therefore it is necessary to
		// initialize them manually.
		loadProfiles();
		CorePackage.eINSTANCE.getName();
	}

	/* Component Hierarchy */

	@Test
	public void testComponentHierarchyScopedToShared() throws IOException {
		final URI transformationFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/componenthierarchy/scoped_to_sharedcontext.henshin", true);
		final URI sourceFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/scoped_to_shared/component_hierarchy_scoped.xmi", true);
		final URI targetFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/scoped_to_shared/component_hierarchy_scoped-shared.xmi",
				true);
		executeAndCompare(transformationFileURI, sourceFileURI, targetFileURI);
	}

	@Test
	public void testComponentHierarchyScopedToFlat() throws IOException {
		final URI transformationFileURI = URI
				.createPlatformPluginURI("/" + BUNDLE_NAME + "/componenthierarchy/scoped_to_flat.henshin", true);
		final URI sourceFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/scoped_to_flat/component_hierarchy_scoped.xmi", true);
		final URI targetFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/scoped_to_flat/component_hierarchy_scoped-flat.xmi", true);
		executeAndCompare(transformationFileURI, sourceFileURI, targetFileURI);
	}

	@Test
	public void testComponentHierarchySharedToFlat() throws IOException {
		final URI transformationFileURI = URI
				.createPlatformPluginURI("/" + BUNDLE_NAME + "/componenthierarchy/sharedcontext_to_flat.henshin", true);
		final URI sourceFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/shared_to_flat/component_hierarchy_shared.xmi", true);
		final URI targetFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/shared_to_flat/component_hierarchy_shared-flat.xmi", true);
		executeAndCompare(transformationFileURI, sourceFileURI, targetFileURI);
	}

	@Test
	public void testComponentHierarchySharedToScoped() throws IOException {
		final URI transformationFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/componenthierarchy/sharedcontext_to_scoped.henshin", true);
		final URI sourceFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/shared_to_scoped/component_hierarchy_shared.xmi", true);
		final URI targetFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/shared_to_scoped/component_hierarchy_shared-scoped.xmi",
				true);
		executeAndCompare(transformationFileURI, sourceFileURI, targetFileURI);
	}

	@Test
	public void testComponentHierarchyFlatToScoped() throws IOException {
		final URI transformationFileURI = URI
				.createPlatformPluginURI("/" + BUNDLE_NAME + "/componenthierarchy/flat_to_scoped.henshin", true);
		final URI sourceFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/flat_to_scoped/component_hierarchy_flat.xmi", true);
		final URI targetFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/flat_to_scoped/component_hierarchy_flat-scoped.xmi", true);
		executeAndCompare(transformationFileURI, sourceFileURI, targetFileURI);
	}

	@Test
	public void testComponentHierarchyFlatToShared() throws IOException {
		final URI transformationFileURI = URI
				.createPlatformPluginURI("/" + BUNDLE_NAME + "/componenthierarchy/flat_to_sharedcontext.henshin", true);
		final URI sourceFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/flat_to_shared/component_hierarchy_flat.xmi", true);
		final URI targetFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/component_hierarchy/flat_to_shared/component_hierarchy_flat-shared.xmi", true);
		executeAndCompare(transformationFileURI, sourceFileURI, targetFileURI);
	}

	/* Interface Hierarchy */

	@Test
	public void testInterfaceHierarchyScopedToShared_sharedComponentHierarchy() throws IOException {
		final URI transformationFileURI = URI
				.createPlatformPluginURI("/" + BUNDLE_NAME + "/interfacehierarchy/scoped_to_shared.henshin", true);
		final URI sourceFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/interface_hierarchy/scoped_to_shared/shared_component_hierarchy.xmi", true);
		final URI targetFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/interface_hierarchy/scoped_to_shared/shared_component_hierarchy-shared.xmi",
				true);
		executeAndCompare(transformationFileURI, sourceFileURI, targetFileURI);
	}

	@Test
	public void testInterfaceHierarchyScopedToShared_scopedComponentHierarchy() throws IOException {
		final URI transformationFileURI = URI
				.createPlatformPluginURI("/" + BUNDLE_NAME + "/interfacehierarchy/scoped_to_shared.henshin", true);
		final URI sourceFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/interface_hierarchy/scoped_to_shared/scoped_component_hierarchy.xmi", true);
		final URI targetFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/interface_hierarchy/scoped_to_shared/scoped_component_hierarchy-shared.xmi",
				true);
		executeAndCompare(transformationFileURI, sourceFileURI, targetFileURI);
	}

	@Test
	public void testInterfaceHierarchySharedToScoped_sharedComponentHierarchy() throws IOException {
		final URI transformationFileURI = URI
				.createPlatformPluginURI("/" + BUNDLE_NAME + "/interfacehierarchy/shared_to_scoped.henshin", true);
		final URI sourceFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/interface_hierarchy/shared_to_scoped/shared_component_hierarchy.xmi", true);
		final URI targetFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/interface_hierarchy/shared_to_scoped/shared_component_hierarchy-scoped.xmi",
				true);
		executeAndCompare(transformationFileURI, sourceFileURI, targetFileURI);
	}

	@Test
	public void testInterfaceHierarchySharedToScoped_scopedComponentHierarchy() throws IOException {
		final URI transformationFileURI = URI
				.createPlatformPluginURI("/" + BUNDLE_NAME + "/interfacehierarchy/shared_to_scoped.henshin", true);
		final URI sourceFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/interface_hierarchy/shared_to_scoped/scoped_component_hierarchy.xmi", true);
		final URI targetFileURI = URI.createPlatformPluginURI(
				"/" + BUNDLE_NAME + "/interface_hierarchy/shared_to_scoped/scoped_component_hierarchy-scoped.xmi",
				true);
		executeAndCompare(transformationFileURI, sourceFileURI, targetFileURI);
	}

	/* General Test Methods */

	public void executeAndCompare(URI transformationFileUri, URI sourceFileURI, URI targetFileURI) throws IOException {
		final HenshinResourceSet resourceSet = new HenshinResourceSet();

		// Load the Henshin module. "Fix imports" while doing so (or get a
		// "Missing Factory for Node :null" problem otherwise...)
		final Module module = (Module) resourceSet.getResource(transformationFileUri, true).getContents().get(0);

		// Initialize the graph:
		final EGraph graph = new EGraphImpl(resourceSet.getResource(sourceFileURI, true).getContents());

		// Find the unit to be applied:
		final Unit unit = module.getUnit("main");

		// Apply the unit:
		final UnitApplication application = new UnitApplicationImpl(engine, graph, unit, null);
		application.execute(new LoggingApplicationMonitor());

		// Compare with the expected graph
		final EGraph expectedGraph = new EGraphImpl(resourceSet.getResource(targetFileURI, true).getContents());

		try {
			new CompareEcoreModels().compare(expectedGraph.getRoots(), graph.getRoots(), true);
		} catch (final AssertionError e) {
			// Store the wrong graph for comparison
			final URI uri = URI.createFileURI(
					Files.createTempFile("codeling-TransformationTest", "-failing-graph.xmi", new FileAttribute<?>[0])
							.toString());
			final Resource res = resourceSet.createResource(uri);
			res.getContents().addAll(graph.getRoots());
			res.save(null);
			System.out.println("Wrong graph was stored under " + uri.toString());
			throw e;
		}
	}
}
