package org.codeling.interprofile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.codeling.interprofile.internal.LanguageMatcher;
import org.eclipse.emf.common.util.URI;
import org.junit.Test;

public class LanguageMatcherTest {

	@Test
	public void testGetSourceToTargetTransformationURIs() {
		final List<String> implementationLanguageProfiles = Arrays.asList( "http://mkonersmann.de/il/core/1.0",
				"http://mkonersmann.de/il/profiles/componenthierarchy/flat/1.0",
				"http://mkonersmann.de/il/profiles/behavior/statemachine/1.0",
				"http://mkonersmann.de/il/profiles/components/stateless/1.0",
				"http://mkonersmann.de/il/profiles/interfaces/scoped/1.0" );

		final List<String> modelingLanguageProfiles = Arrays.asList("http://mkonersmann.de/il/core/1.0",
				"http://mkonersmann.de/il/profiles/componenthierarchy/scoped/1.0",
				"http://mkonersmann.de/il/profiles/interfaces/shared/1.0");

		final String[] expected = new String[] {
				"/il.moduleTransformations/src/main/resources/componenthierarchy/flat_to_scoped.henshin",
				"/il.moduleTransformations/src/main/resources/interfaces/scoped_to_shared.henshin" };

		final LanguageMatcher matcher = new LanguageMatcher(implementationLanguageProfiles, modelingLanguageProfiles);
		final List<URI> sourceToTargetTransformationURIs = matcher.getSourceToTargetTransformationURIs();
		assertEquals(expected.length, sourceToTargetTransformationURIs.size());
		for (final String e : expected) {
			assertTrue(sourceToTargetTransformationURIs.contains(URI.createPlatformPluginURI(e, true)));
		}
	}
}
