package org.codeling.interprofile;

import java.util.List;

import org.codeling.interprofile.internal.InterProfileTransformation;
import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.IDRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;

public interface IInterprofileTransformation {

	public TransformationResult transform(LanguageDefinition sourceLanguage, LanguageDefinition targetLanguage,
			List<EObject> priorModelRoots, IDRegistry priorIdRegistry, String taskName,
			IProgressMonitor progressMonitor);

	public static IInterprofileTransformation createInstance() {
		return new InterProfileTransformation();
	}

}