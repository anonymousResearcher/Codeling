package org.codeling.ui.handlers;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.codeling.languageregistry.ILanguageRegistry;
import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.ui.MultiListSelectionDialog;
import org.codeling.ui.SingleListSelectionDialog;
import org.codeling.utils.CodelingException;
import org.codeling.utils.CodelingLogger;
import org.codeling.utils.errorcontainers.IMayHaveIssues;
import org.codeling.utils.errorcontainers.Issue;
import org.codeling.utils.errorcontainers.Level;
import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

public abstract class AbstractSteppedHandler extends AbstractHandler implements IMayHaveIssues {

	public static final String LABEL_SELECT_SOURCE_LANG = "Select Source Language";
	public static final String LABEL_SELECT_TARGET_LANG = "Select Target Language";

	protected CodelingLogger log = new CodelingLogger(getClass());
	boolean executedAsSingleStep = false;
	boolean isImplementationMigration = false;

	protected LinkedList<IJavaProject> getSelectedProjects(ExecutionEvent event) throws ExecutionException {
		// get workbench window
		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		// set selection service
		final ISelectionService service = window.getSelectionService();
		// set structured selection
		final IStructuredSelection structured = (IStructuredSelection) service.getSelection();

		final LinkedList<IJavaProject> selectedProjects = new LinkedList<IJavaProject>();
		for (final Iterator<?> iterator = structured.iterator(); iterator.hasNext();) {
			final Object element = iterator.next();
			// check if it is an IProject
			if (element instanceof IProject) {
				final IProject project = (IProject) element;
				final IJavaProject javaProject = JavaCore.create(project);

				selectedProjects.add(javaProject);
			}
		}
		return selectedProjects;
	}

	protected ImplementationLanguageDefinition selectImplementationLanguage(String label) {
		try {
			final SingleListSelectionDialog dialog = openLanguageSelectionDialog(
					ILanguageRegistry.getInstance().getImplementationLanguages(), label, false);
			if (dialog.getReturnCode() == Dialog.CANCEL) {
				// User pressed Cancel
				return null;
			}
			return (ImplementationLanguageDefinition) dialog.getSecondResult();

		} catch (final CodelingException e) {
			addError("Could not select implementation language.", e);
			return null;
		}
	}

	protected SpecificationLanguageDefinition selectSpecificationLanguage(String label) {

		try {
			final SingleListSelectionDialog dialog = openLanguageSelectionDialog(
					ILanguageRegistry.getInstance().getSpecificationLanguages(), label, false);

			if (dialog.getReturnCode() == Dialog.CANCEL) {
				// User pressed Cancel
				return null;
			}
			return (SpecificationLanguageDefinition) dialog.getSecondResult();
		} catch (final CodelingException e) {
			addError("Could not select modeling language.", e);
			return null;
		}
	}

	protected SpecificationLanguageDefinition selectLanguage(String label) {

		try {
			final SingleListSelectionDialog dialog = openLanguageSelectionDialog(
					ILanguageRegistry.getInstance().getLanguages(), label, true);

			if (dialog.getReturnCode() == Dialog.CANCEL) {
				// User pressed Cancel
				return null;
			}
			return (SpecificationLanguageDefinition) dialog.getSecondResult();
		} catch (final CodelingException e) {
			addError("Could not select modeling language.", e);
			return null;
		}
	}

	protected SingleListSelectionDialog openLanguageSelectionDialog(Collection<? extends LanguageDefinition> languages,
			String title, boolean addImplementationFilterCheckBox) {
		final ILabelProvider labelProvider = new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((LanguageDefinition) element).getName() + " (" + ((LanguageDefinition) element).getVersion()
						+ ")";
			}
		};

		final SingleListSelectionDialog dialog = new SingleListSelectionDialog(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), labelProvider);
		dialog.setData(languages.toArray(new LanguageDefinition[languages.size()]));
		dialog.setHasImplementationMigrationButton(addImplementationFilterCheckBox);
		dialog.open();
		return dialog;
	}

	protected MultiListSelectionDialog openMultiLanguageSelectionDialog(
			Collection<ImplementationLanguageDefinition> sourceLanguages,
			Collection<LanguageDefinition> targetLanguages) {
		final ILabelProvider labelProvider = new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((LanguageDefinition) element).getName() + " (" + ((LanguageDefinition) element).getVersion()
						+ ")";
			}
		};

		final MultiListSelectionDialog dialog = new MultiListSelectionDialog(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), labelProvider);
		dialog.setData(sourceLanguages.toArray(new LanguageDefinition[sourceLanguages.size()]),
				targetLanguages.toArray(new LanguageDefinition[targetLanguages.size()]));
		dialog.open();
		return dialog;
	}

	protected LanguageDefinition[] requestLanguagesFromUser() throws CodelingException {
		final ILanguageRegistry languageRegistry = ILanguageRegistry.getInstance();

		final Set<LanguageDefinition> targetLanguages = languageRegistry.getLanguages();

		final MultiListSelectionDialog dialog = openMultiLanguageSelectionDialog(
				languageRegistry.getImplementationLanguages(), targetLanguages);
		if (dialog.getReturnCode() == Dialog.CANCEL) {
			// User pressed Cancel
			return new LanguageDefinition[0];
		}

		final LanguageDefinition sourceLanguage = (LanguageDefinition) dialog.getFirstResult();
		LanguageDefinition targetLanguage = (LanguageDefinition) dialog.getSecondResult();

		return new LanguageDefinition[] { sourceLanguage, targetLanguage };
	}

	// Methods from IMayHaveIssues.
	protected MayHaveIssues issues = new MayHaveIssues(this.getClass());
	private final List<IMayHaveIssues> childIssues = new LinkedList<IMayHaveIssues>();

	protected void addChildIssue(IMayHaveIssues child) {
		childIssues.add(child);
	}

	@Override
	public boolean isOK() {
		return issues.isOK() || childIssues.stream().allMatch(x -> x.isOK());

	}

	@Override
	public boolean hasErrorsOrWarnings() {
		return issues.hasErrorsOrWarnings() || childIssues.stream().allMatch(x -> x.hasErrorsOrWarnings());

	}

	@Override
	public boolean hasErrors() {
		return issues.hasErrors() || childIssues.stream().allMatch(x -> x.hasErrors());

	}

	@Override
	public boolean hasWarnings() {
		return issues.hasWarnings() || childIssues.stream().allMatch(x -> x.hasWarnings());

	}

	@Override
	public boolean hasInfos() {
		return issues.hasInfos() || childIssues.stream().allMatch(x -> x.hasInfos());

	}

	@Override
	public List<Issue> issues() {
		final List<Issue> result = issues.issues();
		childIssues.forEach(x -> result.addAll(x.issues()));
		return result;

	}

	@Override
	public List<Issue> errorsAndWarnings() {
		final List<Issue> result = issues.errorsAndWarnings();
		childIssues.forEach(x -> result.addAll(x.errorsAndWarnings()));
		return result;

	}

	@Override
	public List<Issue> errors() {
		final List<Issue> result = issues.errors();
		childIssues.forEach(x -> result.addAll(x.errors()));
		return result;

	}

	@Override
	public List<Issue> warnings() {
		final List<Issue> result = issues.warnings();
		childIssues.forEach(x -> result.addAll(x.warnings()));
		return result;

	}

	@Override
	public List<Issue> infos() {
		final List<Issue> result = issues.infos();
		childIssues.forEach(x -> result.addAll(x.infos()));
		return result;

	}

	@Override
	public void addIssue(Level level, String message, Throwable e) {
		issues.addIssue(level, message, e);
	}

	@Override
	public void addIssue(Level level, String message) {
		issues.addIssue(level, message);
	}

	@Override
	public void addWarning(String message, Throwable e) {
		issues.addWarning(message, e);
	}

	@Override
	public void addWarning(String message) {
		issues.addWarning(message);
	}

	@Override
	public void addError(String message, Throwable e) {
		issues.addError(message, e);
	}

	@Override
	public void addError(String message) {
		issues.addError(message);
	}

	@Override
	public void addInfo(String message, Throwable e) {
		issues.addInfo(message, e);
	}

	@Override
	public void addInfo(String message) {
		issues.addInfo(message);
	}

	@Override
	public Class<?> getOwningClass() {
		return issues.getOwningClass();
	}

	@Override
	public String getName() {
		return issues.getName();
	}
}
