package org.codeling.ui.handlers;

import java.util.LinkedList;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.internal.steps.TranslationModelToProgramCodeTask;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;

public class ILAndAIL2CodeHandler extends AbstractSteppedHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		executedAsSingleStep = true;

		final ImplementationLanguageDefinition implementationLanguage = selectImplementationLanguage(LABEL_SELECT_TARGET_LANG);
		if (implementationLanguage == null)
			return Status.CANCEL_STATUS;

		// Load source code and translation to IL model
		final LinkedList<IJavaProject> projects = getSelectedProjects(event);

		final Job job = new TranslationModelToProgramCodeTask(implementationLanguage, projects, null);

		job.setUser(true);
		job.schedule();
		return Status.OK_STATUS;
	}


}
