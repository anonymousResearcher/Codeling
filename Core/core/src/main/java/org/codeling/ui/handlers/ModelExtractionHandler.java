package org.codeling.ui.handlers;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.codeling.languageregistry.ILanguageRegistry;
import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.internal.ImplementationMigrationTask;
import org.codeling.transformationmanager.internal.ModelExtractionTask;
import org.codeling.ui.EmptySpecificationLanguage;
import org.codeling.ui.MultiListSelectionDialog;
import org.codeling.ui.ResourceChangeListener;
import org.codeling.utils.CodelingException;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

public class ModelExtractionHandler extends AbstractHandler {
	public static final String EXTRACTION_TASK_NAME = "Model Extraction";
	public static final String MIGRATION_TASK_NAME = "Implementation Migration";

	IResourceChangeListener resourceChangeListener;
	List<EObject> resultModelRoots = null;

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		// If a resource change listener one already existed, delete it first.
		if (resourceChangeListener != null)
			ResourcesPlugin.getWorkspace().removeResourceChangeListener(resourceChangeListener);

		LanguageDefinition[] selectedLanguages;
		try {
			selectedLanguages = requestLanguagesFromUser();
		} catch (final CodelingException e) {
			throw new ExecutionException("Could not get languages from the user.", e);
		}

		if (selectedLanguages == null || selectedLanguages.length == 0)
			return Status.CANCEL_STATUS;

		final ImplementationLanguageDefinition sourceLanguage = (ImplementationLanguageDefinition) selectedLanguages[0];
		final LanguageDefinition targetLanguage = selectedLanguages[1];
		final LinkedList<IJavaProject> projects = getSelectedProjects(event);

		boolean isImplementationMigration = targetLanguage instanceof ImplementationLanguageDefinition;
		if (isImplementationMigration) {
			final ImplementationMigrationTask job = new ImplementationMigrationTask(sourceLanguage,
					(ImplementationLanguageDefinition) targetLanguage, projects);
			job.setUser(true);
			job.schedule();
		} else {
			final ModelExtractionTask job = new ModelExtractionTask(sourceLanguage,
					(SpecificationLanguageDefinition) targetLanguage, projects);
			job.setUser(true);
			job.schedule();

			// When the job is finished, wait for changes in the model.
			job.addJobChangeListener(new JobChangeAdapter() {
				@Override
				public void done(IJobChangeEvent event) {
					final Resource modelResource = job.getResultModelRoots().get(0).eResource();
					try {
						// Sleep for a while. Sometimes the code changes are not in the
						// resource yet.
						Thread.sleep(5000);
					} catch (InterruptedException e) {
					}
					// Create resource change listener on created resource.
					resourceChangeListener = new ResourceChangeListener(modelResource.getURI(), sourceLanguage,
							(SpecificationLanguageDefinition) targetLanguage, projects);
					ResourcesPlugin.getWorkspace().addResourceChangeListener(resourceChangeListener,
							IResourceChangeEvent.POST_CHANGE);
				}
			});
		}

		return Status.OK_STATUS;
	}

	private LanguageDefinition[] requestLanguagesFromUser() throws CodelingException {
		final ILanguageRegistry languageRegistry = ILanguageRegistry.getInstance();

		final LinkedList<LanguageDefinition> targetLanguages = new LinkedList<LanguageDefinition>();
		// Add the empty language definition as selection.
		final SpecificationLanguageDefinition emptyLanguage = new EmptySpecificationLanguage();
		targetLanguages.add(emptyLanguage);
		targetLanguages.addAll(languageRegistry.getLanguages());

		final MultiListSelectionDialog dialog = openLanguageSelectionDialog(
				languageRegistry.getImplementationLanguages(), targetLanguages);
		if (dialog.getReturnCode() == Dialog.CANCEL) {
			// User pressed Cancel
			return new LanguageDefinition[0];
		}

		final LanguageDefinition sourceLanguage = (LanguageDefinition) dialog.getFirstResult();
		LanguageDefinition targetLanguage = (LanguageDefinition) dialog.getSecondResult();

		// The extraction handler expects null for the specification language if
		// none is selected.
		if (targetLanguage == emptyLanguage)
			targetLanguage = null;

		return new LanguageDefinition[] { sourceLanguage, targetLanguage };
	}

	private MultiListSelectionDialog openLanguageSelectionDialog(
			Collection<ImplementationLanguageDefinition> sourceLanguages,
			Collection<LanguageDefinition> targetLanguages) {
		final ILabelProvider labelProvider = new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((LanguageDefinition) element).getName() + " (" + ((LanguageDefinition) element).getVersion()
						+ ")";
			}
		};

		final MultiListSelectionDialog dialog = new MultiListSelectionDialog(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), labelProvider);
		dialog.setData(sourceLanguages.toArray(new LanguageDefinition[sourceLanguages.size()]),
				targetLanguages.toArray(new LanguageDefinition[targetLanguages.size()]));
		dialog.open();
		return dialog;
	}

	private LinkedList<IJavaProject> getSelectedProjects(ExecutionEvent event) throws ExecutionException {
		// get workbench window
		IWorkbenchWindow window;
		window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		// set selection service
		final ISelectionService service = window.getSelectionService();
		// set structured selection
		final IStructuredSelection structured = (IStructuredSelection) service.getSelection();

		final LinkedList<IJavaProject> selectedProjects = new LinkedList<IJavaProject>();
		for (final Iterator<?> iterator = structured.iterator(); iterator.hasNext();) {
			final Object element = iterator.next();
			// check if it is an IProject
			if (element instanceof IProject) {
				final IProject project = (IProject) element;
				final IJavaProject javaProject = JavaCore.create(project);

				selectedProjects.add(javaProject);
			}
		}
		return selectedProjects;
	}

}
