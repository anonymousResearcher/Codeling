package org.codeling.ui.handlers;

import java.util.List;

import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.internal.steps.InterProfileTransformationToSpecificationModelTask;
import org.codeling.transformationmanager.internal.steps.TranslationModelToSpecificationModelTask;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;

public class IL2ADLHandler extends AbstractSteppedHandler {


	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		executedAsSingleStep = true;

		final SpecificationLanguageDefinition modelingLanguage = selectSpecificationLanguage(LABEL_SELECT_TARGET_LANG);
		if (modelingLanguage == null)
			return Status.CANCEL_STATUS;

		final IDRegistry priorIdRegistry = IDRegistry.load(InterProfileTransformationToSpecificationModelTask.ID_FILEPATH);
		final List<EObject> priorModelRoots = Models.loadFrom(InterProfileTransformationToSpecificationModelTask.MODEL_FILEPATH);

		final Job job = new TranslationModelToSpecificationModelTask(modelingLanguage, priorIdRegistry, priorModelRoots, true);
		job.setUser(true);
		job.schedule();

		return Status.OK_STATUS;
	}


}
