package org.codeling.ui;

import java.util.Collections;
import java.util.List;

import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.IDRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;

public class EmptySpecificationLanguage extends SpecificationLanguageDefinition {

	@Override
	public String getName() {
		return "None";
	}

	@Override
	public String getVersion() {
		return "Only Translate to Architecture Implementation Language";
	}

	@Override
	public List<String> getSelectedProfiles() {
		return Collections.emptyList();
	}

	@Override
	public TransformationResult transformToSM(List<EObject> ilRoots, IDRegistry idRegistry, IProgressMonitor monitor) {
		return new TransformationResult(Collections.emptyList(), new IDRegistry());

	}

	@Override
	public TransformationResult transformToTM(List<EObject> adlModelRoots, List<EObject> preChangeILRoots,
			IDRegistry idRegistry, IProgressMonitor monitor) {
		return new TransformationResult(Collections.emptyList(), new IDRegistry());

	}

}
