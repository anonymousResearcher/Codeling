package org.codeling.ui;

import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.internal.ModelIntegrationTask;
import org.codeling.utils.CodelingLogger;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jdt.core.IJavaProject;

public class ResourceChangeListener implements IResourceChangeListener, IResourceDeltaVisitor {
	CodelingLogger log = new CodelingLogger(getClass());

	private URI resourceURI;
	private final ImplementationLanguageDefinition implementationLanguage;
	private final SpecificationLanguageDefinition specificationLanguage;
	private final List<IJavaProject> projects;

	public ResourceChangeListener(URI resourceURI, ImplementationLanguageDefinition implementationLanguage,
			SpecificationLanguageDefinition modelingLanguage, List<IJavaProject> projects) {
		this.implementationLanguage = implementationLanguage;
		this.specificationLanguage = modelingLanguage;
		this.resourceURI = resourceURI;
		this.projects = projects;
	}

	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		try {
			event.getDelta().accept(this);
		} catch (CoreException e) {
			log.error("Could not detect changes in the specification model.", e);
		}
	}

	@Override
	public boolean visit(IResourceDelta delta) throws CoreException {
		// We are only interested in content changes
		if ((delta.getFlags() & IResourceDelta.CONTENT) != 0)
			// Only the specification model file
			if (delta.getResource().getFullPath().toString().equals(resourceURI.toPlatformString(true))) {
				startModelToCodeJob();
				return false;
			}
		return true;
	}

	public void startModelToCodeJob() {
		ResourceSet rSet = new ResourceSetImpl();
		Resource resource = rSet.getResource(resourceURI, true);
		final ModelIntegrationTask job = new ModelIntegrationTask(resource.getContents(), implementationLanguage,
				specificationLanguage, projects);
		job.setUser(true);
		job.schedule();
	}
}