package org.codeling.ui.handlers;

import java.util.List;

import org.codeling.languageregistry.LanguageDefinition;
import org.codeling.transformationmanager.internal.steps.InterProfileTransformationToImplementationModelTask;
import org.codeling.transformationmanager.internal.steps.SpecificationModelToTranslationModelTask;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;

public class ILProfileTransformationToCodeHandler extends AbstractSteppedHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		executedAsSingleStep = true;

		LanguageDefinition[] languageDefinitions;
		try {
			languageDefinitions = requestLanguagesFromUser();
		} catch (final CodelingException e) {
			throw new ExecutionException("Could not get languages from the user.", e);
		}

		final IDRegistry priorIdRegistry = IDRegistry.load(SpecificationModelToTranslationModelTask.ID_FILEPATH);
		final List<EObject> priorModelRoots = Models.loadFrom(SpecificationModelToTranslationModelTask.MODEL_FILEPATH);

		final Job job = new InterProfileTransformationToImplementationModelTask(languageDefinitions[1], languageDefinitions[0],
				priorIdRegistry, priorModelRoots, true);
		job.setUser(true);
		job.schedule();

		return Status.OK_STATUS;
	}

}
