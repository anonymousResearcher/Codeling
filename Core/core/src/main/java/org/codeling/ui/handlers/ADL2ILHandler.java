package org.codeling.ui.handlers;

import java.util.List;

import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.internal.steps.SpecificationModelToTranslationModelTask;
import org.codeling.transformationmanager.internal.steps.TranslationModelToSpecificationModelTask;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;

public class ADL2ILHandler extends AbstractSteppedHandler {
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final SpecificationLanguageDefinition modelingLanguage = selectSpecificationLanguage(LABEL_SELECT_SOURCE_LANG);
		if (modelingLanguage == null)
			return Status.CANCEL_STATUS;

		final IDRegistry priorIdRegistry = IDRegistry.load(TranslationModelToSpecificationModelTask.ID_FILEPATH);
		final List<EObject> currentADLRoots = Models.loadFrom(TranslationModelToSpecificationModelTask.MODEL_FILEPATH);
		final Job job = new SpecificationModelToTranslationModelTask(modelingLanguage, currentADLRoots, priorIdRegistry, true, true);

		job.setUser(true);
		job.schedule();
		return Status.OK_STATUS;
	}

}
