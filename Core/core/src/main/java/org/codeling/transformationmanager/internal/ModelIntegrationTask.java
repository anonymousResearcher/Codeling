package org.codeling.transformationmanager.internal;

import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.internal.steps.ImplementationModelToProgramCodeTask;
import org.codeling.transformationmanager.internal.steps.InterProfileTransformationToImplementationModelTask;
import org.codeling.transformationmanager.internal.steps.SpecificationModelToTranslationModelTask;
import org.codeling.transformationmanager.internal.steps.TranslationModelToImplementationModelTask;
import org.codeling.transformationmanager.internal.steps.TranslationModelToProgramCodeTask;
import org.codeling.transformationmanager.internal.steps.TranslationModelToSpecificationModelTask;
import org.codeling.utils.IDRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;

public class ModelIntegrationTask extends AbstractTask {

	private ImplementationLanguageDefinition implementationLanguage;
	private SpecificationLanguageDefinition specificationLanguage;
	private List<IJavaProject> projects;
	private List<EObject> updatedModel;

	public ModelIntegrationTask(List<EObject> updatedModel, ImplementationLanguageDefinition implementationLanguage,
			SpecificationLanguageDefinition specificationLanguage, List<IJavaProject> projects) {
		super("Propagating Model Changes to the Program Code");
		this.updatedModel = updatedModel;
		this.implementationLanguage = implementationLanguage;
		this.specificationLanguage = specificationLanguage;
		this.projects = projects;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		monitor.beginTask("Propagating Model Changes to the Program Code", 5);

		try {
			SpecificationModelToTranslationModelTask adl2il = null;
			if (specificationLanguage != null) {
				// The complete process from code to ADL is executed. Otherwise only
				// the component model has been extracted.
				IDRegistry priorIdRegistry = IDRegistry.load(TranslationModelToSpecificationModelTask.ID_FILEPATH);
				adl2il = new SpecificationModelToTranslationModelTask(specificationLanguage, updatedModel, priorIdRegistry, true, false);
				adl2il.setProgressGroup(monitor, 1);
				adl2il.schedule();
				adl2il.join();

				final InterProfileTransformationToImplementationModelTask profileTransformation = new InterProfileTransformationToImplementationModelTask(
						specificationLanguage, implementationLanguage, adl2il.getIdRegistry(),
						adl2il.getResultModelRoots(), true);
				profileTransformation.setProgressGroup(monitor, 1);
				profileTransformation.schedule();
				profileTransformation.join();
				
				final TranslationModelToImplementationModelTask il2ail = new TranslationModelToImplementationModelTask(implementationLanguage, profileTransformation.getIdRegistry(), profileTransformation.getResultModelRoots(), true, false);
				il2ail.setProgressGroup(monitor, 1);
				il2ail.schedule();
				il2ail.join();
			}

			ImplementationModelToProgramCodeTask ail2code = new ImplementationModelToProgramCodeTask(implementationLanguage, projects, false);
			ail2code.setProgressGroup(monitor, specificationLanguage == null ? 2 : 1);
			ail2code.schedule();
			ail2code.join();

			if (specificationLanguage != null) {
				TranslationModelToProgramCodeTask il2code = new TranslationModelToProgramCodeTask(implementationLanguage, projects,
						adl2il.getResultModelRoots());
				il2code.setProgressGroup(monitor, 1);
				il2code.schedule();
				il2code.join();
			}

			return Status.OK_STATUS;
		} catch (final InterruptedException e) {
			addError("Exception while Propagating Model Changes to the Program Code", e);
			return Status.CANCEL_STATUS;
		} finally {
			monitor.done();
		}
	}
}
