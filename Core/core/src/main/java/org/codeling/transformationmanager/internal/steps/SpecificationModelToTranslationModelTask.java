package org.codeling.transformationmanager.internal.steps;

import java.util.List;

import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.transformationmanager.internal.AbstractTask;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;

public class SpecificationModelToTranslationModelTask extends AbstractTask {

	public static final String MODEL_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "6_adl2il.xmi";
	public static final String ID_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "6_adl2il.ids";
	public static final String TASK_NAME = "Architecture Description Language To Intermediate Language Model Transformation";

	final SpecificationLanguageDefinition specificationLanguage;
	boolean storeResult = false;
	final List<EObject> model;
	IDRegistry priorIdRegistry;

	public SpecificationModelToTranslationModelTask(SpecificationLanguageDefinition modelingLanguage, List<EObject> model, IDRegistry priorIdRegistry,
			boolean storeResult, boolean executedAsSingleStep) {
		super(TASK_NAME);
		this.specificationLanguage = modelingLanguage;
		this.model = model;
		this.priorIdRegistry = priorIdRegistry;
		this.storeResult = storeResult;
		this.executedAsSingleStep = executedAsSingleStep;
	}

	@Override
	protected IStatus run(IProgressMonitor progressMonitor) {
		progressMonitor.beginTask("Executing " + TASK_NAME, 10);

		// Call the transformation pre start hook for the modeling
		// language
		progressMonitor.subTask("Executing pre processing");
		specificationLanguage.transformationToCodePreStartHook(TranslationModelToSpecificationModelTask.MODEL_FILEPATH, progressMonitor);

		// Load IL model from before the ADL transformation
		progressMonitor.subTask(
				String.format("Loading pre-change model: %s", InterProfileTransformationToSpecificationModelTask.MODEL_FILEPATH));
		final List<EObject> preChangeILRoots = Models.loadFrom(InterProfileTransformationToSpecificationModelTask.MODEL_FILEPATH);
		progressMonitor.worked(1);

		final TransformationResult result = specificationLanguage.transformToTM(model, preChangeILRoots,
				priorIdRegistry, progressMonitor);
		resultModelRoots = result.getModelRoots();
		idRegistry = result.getIdRegistry();
		progressMonitor.worked(8);

		if (storeResult) {
			Models.store(resultModelRoots, MODEL_FILEPATH);
			idRegistry.saveToFile(ID_FILEPATH);
		}
		progressMonitor.worked(1);

		if (executedAsSingleStep)
			progressMonitor.done();
		return Status.OK_STATUS;
	}

};