/**
 *
 */
package org.codeling.transformationmanager;

import java.util.LinkedList;
import java.util.List;

import org.codeling.utils.IDRegistry;
import org.eclipse.emf.ecore.EObject;

/**
 * A class carrying the result data of a code-to-model or a model-to-model
 * transformation.
 *
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public class TransformationResult {

	List<EObject> modelRoots = new LinkedList<>();
	IDRegistry idRegistry;

	/**
	 * Creates a {@link TransformationResult}.
	 *
	 * @param rootObjects
	 *            The root objects of the source model. There may be multiple
	 *            source model roots, e.g. when multiple projects are
	 *            translated.
	 * @param idRegistry
	 *            A registry of ids and their corresponding model elements.
	 */
	public TransformationResult(List<EObject> rootObjects, IDRegistry idRegistry) {
		this.modelRoots = rootObjects;
		this.idRegistry = idRegistry;
	}

	// Getters and setters
	public List<EObject> getModelRoots() {
		return modelRoots;
	}

	public void setModelRoots(List<EObject> modelRoots) {
		this.modelRoots = modelRoots;
	}

	public IDRegistry getIdRegistry() {
		return idRegistry;
	}

	public void setIdRegistry(IDRegistry idRegistry) {
		this.idRegistry = idRegistry;
	}

}
