package org.codeling.transformationmanager.internal;

import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.internal.steps.ImplementationModelToTranslationModelTask;
import org.codeling.transformationmanager.internal.steps.InterProfileTransformationToSpecificationModelTask;
import org.codeling.transformationmanager.internal.steps.ProgramCodeToImplementationModelTask;
import org.codeling.transformationmanager.internal.steps.ProgramCodeToTranslationModelTask;
import org.codeling.transformationmanager.internal.steps.TranslationModelToSpecificationModelTask;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaProject;

public class ModelExtractionTask extends AbstractTask {
	final List<IJavaProject> projects;
	final ImplementationLanguageDefinition implementationLanguage;
	final SpecificationLanguageDefinition modelingLanguage;
	boolean storeResult = false;

	public ModelExtractionTask(ImplementationLanguageDefinition implementationLanguage,
			SpecificationLanguageDefinition modelingLanguage, List<IJavaProject> projects) {
		super("Extracting Specification Model from Program Code");
		this.implementationLanguage = implementationLanguage;
		this.modelingLanguage = modelingLanguage;
		this.projects = projects;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		try {
			monitor.beginTask("Extracting Specification Model from Program Code", 5);

			boolean translateToImplementationModelOnly = modelingLanguage == null;
			final ProgramCodeToImplementationModelTask code2ail = new ProgramCodeToImplementationModelTask(implementationLanguage, projects, true,
					translateToImplementationModelOnly);
			code2ail.setProgressGroup(monitor, modelingLanguage == null ? 5 : 1);
			code2ail.schedule();
			code2ail.join();

			if (translateToImplementationModelOnly) {
				// Return the component model
				resultModelRoots = code2ail.getResultModelRoots();
				return Status.OK_STATUS;
			}

			final ImplementationModelToTranslationModelTask ail2il = new ImplementationModelToTranslationModelTask(implementationLanguage, code2ail.getIdRegistry(),
					code2ail.getResultModelRoots(), true, false);
			ail2il.setProgressGroup(monitor, 1);
			ail2il.schedule();
			ail2il.join();

			final ProgramCodeToTranslationModelTask code2il = new ProgramCodeToTranslationModelTask(implementationLanguage, projects,
					ail2il.getIdRegistry(), code2ail.getResultModelRoots(), ail2il.getResultModelRoots(), true);
			code2il.setProgressGroup(monitor, 1);
			code2il.schedule();
			code2il.join();

			final InterProfileTransformationToSpecificationModelTask profileTransformation = new InterProfileTransformationToSpecificationModelTask(
					implementationLanguage, modelingLanguage, code2il.getIdRegistry(),
					code2il.getResultModelRoots(), true);
			profileTransformation.setProgressGroup(monitor, 1);
			profileTransformation.schedule();
			profileTransformation.join();

			final TranslationModelToSpecificationModelTask il2adl = new TranslationModelToSpecificationModelTask(modelingLanguage, profileTransformation.getIdRegistry(),
					profileTransformation.getResultModelRoots(), true);
			il2adl.setProgressGroup(monitor, 1);
			il2adl.schedule();
			il2adl.join();

			resultModelRoots = il2adl.getResultModelRoots();
			return Status.OK_STATUS;
		} catch (final InterruptedException e) {
			addError("Exception while Extracting a Model from the Program Code.", e);
			return Status.CANCEL_STATUS;
		} finally {
			monitor.done();
		}
	}

};