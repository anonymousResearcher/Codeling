package org.codeling.transformationmanager.internal.steps;

import java.util.List;

import org.codeling.languageregistry.ImplementationLanguageDefinition;
import org.codeling.transformationmanager.internal.AbstractTask;
import org.codeling.utils.CodelingConfiguration;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.Models;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;

public class TranslationModelToProgramCodeTask extends AbstractTask {
	public static final String TASK_NAME = "Intermediate Language Model to Architecture Implementation Model Transformation";
	public static final String ID_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "8_il2cm.ids";
	public static final String MODEL_FILEPATH = CodelingConfiguration.DEBUG_MODELDIR_PATH + "8_il2cm.xmi";

	final List<IJavaProject> projects;
	final ImplementationLanguageDefinition implementationLanguage;
	final List<EObject> ilModelRoots;
	private boolean isImplementationMigration = false;

	public TranslationModelToProgramCodeTask(ImplementationLanguageDefinition implementationLanguage,
			List<IJavaProject> projects, List<EObject> ilModelRoots) {
		super(TASK_NAME);
		this.projects = projects;
		this.implementationLanguage = implementationLanguage;
		this.ilModelRoots = ilModelRoots;
	}

	@Override
	protected IStatus run(IProgressMonitor progress) {
		if (progress == null)
			progress = new NullProgressMonitor();
		progress.beginTask("Executing " + TASK_NAME, 20);

		progress.subTask(String.format("Loading prior registry: %s", ProgramCodeToTranslationModelTask.ID_FILEPATH));
		IDRegistry priorIdRegistry = IDRegistry.load(ProgramCodeToTranslationModelTask.ID_FILEPATH);
		progress.worked(1);

		progress.subTask(String.format("Loading IDRegistry: %s", ImplementationModelToProgramCodeTask.ID_FILEPATH));
		final IDRegistry idRegistry = IDRegistry.load(ImplementationModelToProgramCodeTask.ID_FILEPATH);
		progress.worked(1);

		progress.subTask(String.format("Loading prior Component Model: %s",
				ProgramCodeToImplementationModelTask.MODEL_FILEPATH));
		List<EObject> priorComponentModelRoots = Models.loadFrom(ProgramCodeToImplementationModelTask.MODEL_FILEPATH);
		progress.worked(1);

		progress.subTask(
				String.format("Loading Component Model: %s", TranslationModelToImplementationModelTask.MODEL_FILEPATH));
		final List<EObject> componentModelRoots = Models
				.loadFrom(TranslationModelToImplementationModelTask.MODEL_FILEPATH);
		progress.worked(1);

		progress.subTask(String.format("Loading prior Intermediate Language Model: %s",
				InterProfileTransformationToSpecificationModelTask.MODEL_FILEPATH));
		List<EObject> priorIlModelRoots = Models
				.loadFrom(InterProfileTransformationToSpecificationModelTask.MODEL_FILEPATH);
		progress.worked(1);

		progress.subTask(String.format("Loading Intermediate Language Model Model: %s",
				InterProfileTransformationToImplementationModelTask.MODEL_FILEPATH));
		List<EObject> ilModelRoots = null;
		String tmFilePath = InterProfileTransformationToSpecificationModelTask.MODEL_FILEPATH;
		if (!isImplementationMigration)
			tmFilePath = InterProfileTransformationToImplementationModelTask.MODEL_FILEPATH;
		ilModelRoots = Models.loadFrom(tmFilePath);
		progress.worked(1);
		try {
			progress.subTask(String.format("Translating Transformation Model to Program Code",
					ProgramCodeToTranslationModelTask.MODEL_FILEPATH));
			implementationLanguage.transformTMToCode(componentModelRoots, priorComponentModelRoots, ilModelRoots,
					priorIlModelRoots, idRegistry, priorIdRegistry, projects);
		} catch (final CodelingException e) {
			addError("Could not transform IL model and AIL model to code", e);
			progress.done();
			return Status.CANCEL_STATUS;
		}

		if (executedAsSingleStep)
			progress.done();
		return Status.OK_STATUS;
	}

	public void setImplementationMigration(boolean isImplementationMigration) {
		this.isImplementationMigration = isImplementationMigration;
	}

};
