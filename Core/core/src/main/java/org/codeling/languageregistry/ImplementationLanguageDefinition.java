package org.codeling.languageregistry;

import java.util.List;

import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;

public abstract class ImplementationLanguageDefinition extends LanguageDefinition {

	public abstract TransformationResult transformCodeToIM(List<IJavaProject> projects, IDRegistry idregistry)
			throws CodelingException;

	public abstract TransformationResult transformIMToTM(List<EObject> cmRoots, IDRegistry idregistry)
			throws CodelingException;

	public abstract TransformationResult transformCodeToTM(List<IJavaProject> projects, List<EObject> cmRoots,
			List<EObject> ilRoots, IDRegistry idregistry) throws CodelingException;

	public abstract TransformationResult transformTMToIM(List<EObject> ilRoots, IDRegistry idRegistry)
			throws CodelingException;

	public abstract void transformIMToCode(List<EObject> newModelRoots, List<EObject> oldModelRoots,
			IDRegistry idRegistry_newModel, IDRegistry idregistry_oldModel, List<IJavaProject> projects)
			throws CodelingException;

	public abstract void transformTMToCode(List<EObject> newIMRoots, List<EObject> oldIMRoots, List<EObject> newTMRoots,
			List<EObject> oldTMRoots, IDRegistry idRegistry_newModel, IDRegistry idregistry_oldModel,
			List<IJavaProject> projects) throws CodelingException;

}
