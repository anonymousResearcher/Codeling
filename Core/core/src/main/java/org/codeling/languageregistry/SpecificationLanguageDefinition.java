package org.codeling.languageregistry;

import java.text.MessageFormat;
import java.util.List;

import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.IDRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

public abstract class SpecificationLanguageDefinition extends LanguageDefinition {

	public abstract TransformationResult transformToSM(List<EObject> ilRoots, IDRegistry idRegistry,
			IProgressMonitor monitor);

	public abstract TransformationResult transformToTM(List<EObject> adlModelRoots, List<EObject> preChangeILRoots,
			IDRegistry idRegistry, IProgressMonitor monitor);

	/**
	 * This method is called when the transformation into this language is
	 * completed, and can be overridden when necessary. This can e.g. be used to
	 * generated diagram files from models.
	 */
	public void transformationCompleteHook(List<EObject> resultModelRoots, IProgressMonitor monitor) {
	}

	/**
	 * This method is called when the transformation from this language to code is
	 * about to be started, and can be overridden when necessary. This can e.g. be
	 * used to aggregate multiple files into one.
	 */
	public void transformationToCodePreStartHook(String modelFilepath, IProgressMonitor monitor) {
	}

	public void setID(EObject object, String id) {
		if (verifyIDAttribute(object))
			EcoreUtil.setID(object, id);
	}

	public String getID(EObject object) {
		if (verifyIDAttribute(object))
			return EcoreUtil.getID(object);
		else
			return null;
	}

	private boolean verifyIDAttribute(EObject object) {
		if (object.eClass().getEIDAttribute() == null) {
			addError(MessageFormat.format(
					"Could not set the ID of an object of the eClass {0}, because it has no ID attribute. Either create an ID attribute or overwrite the method \"setID\" of the SpecificationLanguageDefinition class.",
					object.eClass().getName()));
			return false;
		}
		return true;
	}

}
