package org.codeling.languageregistry;

import java.util.Set;

import org.codeling.languageregistry.internal.LanguageRegistry;
import org.codeling.utils.CodelingException;

public interface ILanguageRegistry {

	static final String EXTENSIONPOINT_ID = "org.codeling.languageRegistry";

	/**
	 * Add a language to the language registry.
	 *
	 * @param languageDefinition
	 */
	void addLanguageDefinition(LanguageDefinition languageDefinition);

	/**
	 * Returns an unmodifiable list of languages registered at this lagnauge
	 * registry.
	 *
	 * @return
	 * @throws CodelingException
	 *             when the languages need to be resolved (see
	 *             resolveLanguages())
	 */
	Set<LanguageDefinition> getLanguages() throws CodelingException;

	/**
	 * Returns an unmodifiable list of architecture specification languages
	 * registered at the registry.
	 *
	 * @throws CodelingException
	 *             when the languages need to be resolved (see
	 *             resolveLanguages())
	 */
	Set<SpecificationLanguageDefinition> getSpecificationLanguages() throws CodelingException;

	/**
	 * Returns an unmodifiable list of architecture implementation languages
	 * registered at the registry.
	 *
	 * @throws CodelingException
	 *             when the languages need to be resolved (see
	 *             resolveLanguages())
	 */
	Set<ImplementationLanguageDefinition> getImplementationLanguages() throws CodelingException;

	public static ILanguageRegistry getInstance() {
		return LanguageRegistry.getInstance();
	}
}