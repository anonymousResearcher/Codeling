/**
 *
 */
package org.codeling.languageregistry;

import java.util.List;

import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;

/**
 * An abstract class for language definitions.
 *
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public abstract class LanguageDefinition extends MayHaveIssues {
	String name = null;
	String version = null;
	String symbolicName = null;

	protected IProgressMonitor monitor = new NullProgressMonitor();

	/**
	 * Returns a list of strings representing the IL modules the language uses. It
	 * returns the nsURIs of all packages that start with
	 * "http://mkonersmann.de/il/profiles/" in the henshin tgg transformation file.
	 */
	public abstract List<String> getSelectedProfiles();

	/**
	 * The language's name.
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * The symbolic name of the language. Used during translations to create default
	 * package names for meta model notations.
	 */
	public String getSymbolicName() {
		return symbolicName;
	}
	
	/**
	 * The symbolic name of the language. Used during translations to create default
	 * package names for meta model notations. Needs to be settable for tests.
	 */
	public void setSymbolicName(String symbolicName) {
		this.symbolicName = symbolicName;
	}

	/**
	 * The version of the language definition.
	 */
	public String getVersion() {
		return version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((symbolicName == null) ? 0 : symbolicName.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final LanguageDefinition other = (LanguageDefinition) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (symbolicName == null) {
			if (other.symbolicName != null)
				return false;
		} else if (!symbolicName.equals(other.symbolicName))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	public void configure(IConfigurationElement e) {
		name = e.getAttribute("name");
		symbolicName = e.getAttribute("symbolicName");
		version = e.getAttribute("version");
	}

	public void setMonitor(IProgressMonitor monitor) {
		this.monitor = monitor;
	}

}