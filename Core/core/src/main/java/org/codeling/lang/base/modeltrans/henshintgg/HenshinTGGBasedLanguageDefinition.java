package org.codeling.lang.base.modeltrans.henshintgg;

import java.util.List;

import org.codeling.languageregistry.SpecificationLanguageDefinition;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.IDRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

public class HenshinTGGBasedLanguageDefinition extends SpecificationLanguageDefinition {

	protected URI henshinTGGFileURI;

	public HenshinTGGBasedLanguageDefinition(URI henshinTGGFileURI) {
		this.henshinTGGFileURI = henshinTGGFileURI;
	}

	public TransformationResult transformToSM(List<EObject> tmRoots, IDRegistry idRegistry, IProgressMonitor monitor) {
		return new HenshinTGGBasedLanguageDefinitionHelper().transformTMToSM(this, henshinTGGFileURI, tmRoots,
				idRegistry, monitor);
	}

	public TransformationResult transformToTM(List<EObject> smRoots, List<EObject> preChangeTMRoots,
			IDRegistry idRegistry, IProgressMonitor monitor) {
		return new HenshinTGGBasedLanguageDefinitionHelper().transformSMToTM(this, henshinTGGFileURI, smRoots,
				preChangeTMRoots, idRegistry, monitor);
	}

	@Override
	public List<String> getSelectedProfiles() {
		return new HenshinTGGBasedLanguageDefinitionHelper().getSelectedProfiles(henshinTGGFileURI);
	}

}
