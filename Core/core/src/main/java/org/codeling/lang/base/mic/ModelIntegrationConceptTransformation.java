/**
 *
 */
package org.codeling.lang.base.mic;

import java.nio.file.Path;
import java.util.List;

import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.CodelingLogger;
import org.codeling.utils.IDRegistry;
import org.codeling.utils.TraceLogTopic;
import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.core.runtime.IProgressMonitor;

/**
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public abstract class ModelIntegrationConceptTransformation extends MayHaveIssues {

	protected final CodelingLogger log = new CodelingLogger(getClass());
	protected final List<Path> projectPaths;
	protected final IDRegistry idRegistry;
	protected final String taskName;
	protected final IProgressMonitor monitor;
	protected TransformationResult transformationResult;
	
	public ModelIntegrationConceptTransformation(List<Path> projectPaths, IDRegistry idRegistry, String taskName, IProgressMonitor monitor) {
		this.projectPaths = projectPaths;
		this.idRegistry = idRegistry;
		this.taskName = taskName;
		this.monitor = monitor;
	}

	public void execute(IProgressMonitor monitor) {
		monitor.beginTask("Executing " + taskName, 100);
		log.trace(TraceLogTopic.TASK_EXECUTION, String.format("Starting task: %s", taskName));
		doExecute(monitor);
		log.trace(TraceLogTopic.TASK_EXECUTION, String.format("Finished task: %s", taskName));
		monitor.done();
	}

	protected abstract void doExecute(IProgressMonitor monitor);
	
	public TransformationResult getTransformationResult() {
		return transformationResult;
	}

	public IDRegistry getIdRegistry() {
		return idRegistry;
	}

}
