/**
 */
package org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.StateMachine#getName <em>Name</em>}</li>
 *   <li>{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.StateMachine#getInitial <em>Initial</em>}</li>
 *   <li>{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.StateMachine#getStates <em>States</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getStateMachine()
 * @model
 * @generated
 */
public interface StateMachine extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getStateMachine_Name()
	 * @model dataType="org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.StateMachine#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Initial</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial</em>' reference.
	 * @see #setInitial(State)
	 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getStateMachine_Initial()
	 * @model required="true"
	 * @generated
	 */
	State getInitial();

	/**
	 * Sets the value of the '{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.StateMachine#getInitial <em>Initial</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial</em>' reference.
	 * @see #getInitial()
	 * @generated
	 */
	void setInitial(State value);

	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getStateMachine_States()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<State> getStates();

} // StateMachine
