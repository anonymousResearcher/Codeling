/**
 */
package org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.State#getName <em>Name</em>}</li>
 *   <li>{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.State#getTransition <em>Transition</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getState()
 * @model
 * @generated
 */
public interface State extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getState_Name()
	 * @model dataType="org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.State#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' containment reference list.
	 * @see org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage#getState_Transition()
	 * @model containment="true"
	 * @generated
	 */
	EList<Transition> getTransition();

} // State
