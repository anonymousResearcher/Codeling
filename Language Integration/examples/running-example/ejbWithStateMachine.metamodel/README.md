This is a project that shows an example of integration mechanisms. It shows the interplay between an EJB meta model and a state machine meta model

* The *meta model* is in the folder ```src/main/resources``` in the folder ```meta-model```. It includes the example meta model. 
* The *model* is in the folder ```src/main/resources``` in the folder ```model```. It includes the example model.
* The *meta model representations* are in the source folder ```src/main/generated``` under the namespaces ```org.codeling.lang.ejb.mm``` and ```org.codeling.lang.statemachine.mm```.
* The *model representations* are in the source folder ```src/main/java```.
* The *execution runtime* is in the source folder ```executionRuntime``` under the namespaces ```org.codeling.lang.ejb.runtime``` and ```org.codeling.lang.statemachine.runtime```.

To start the execution runtime, run the class ```launch.Launch``` from the source folder ```src/main/java/```.