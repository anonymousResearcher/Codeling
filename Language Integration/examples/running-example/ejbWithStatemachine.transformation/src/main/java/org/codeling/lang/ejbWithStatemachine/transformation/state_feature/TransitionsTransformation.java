package org.codeling.lang.ejbWithStatemachine.transformation.state_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.State;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Transition;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.lang.ejbWithStatemachine.transformation.TransitionTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.ContainmentOperationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;

public class TransitionsTransformation extends ContainmentOperationTransformation<State, Transition> {

	public TransitionsTransformation(ClassMechanismTransformation<State, IType> parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getState_Transition());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new TransitionTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		resolveCodeElement();
		for (IMethod m : methods) {
			TransitionTransformation t = new TransitionTransformation(this);
			t.setCodeRoot(this.getCodeRoot());
			t.setCodeElement(m);
			result.add(t);
		}
	}
}
