package org.codeling.lang.ejbWithStatemachine.transformation.component_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.BusinessOperation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.lang.ejbWithStatemachine.transformation.BusinessOperationTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.ContainmentOperationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;

public class OperationsTransformation extends ContainmentOperationTransformation<Component, BusinessOperation> {

	public OperationsTransformation(ClassMechanismTransformation<Component, IType> parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getComponent_Operations());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new BusinessOperationTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		resolveCodeElement();
		for (IMethod m : methods) {
			BusinessOperationTransformation t = new BusinessOperationTransformation(this);
			t.setCodeElement(m);
			result.add(t);
		}
	}
}
