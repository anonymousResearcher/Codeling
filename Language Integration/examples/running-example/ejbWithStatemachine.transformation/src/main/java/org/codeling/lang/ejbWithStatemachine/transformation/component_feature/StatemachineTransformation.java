package org.codeling.lang.ejbWithStatemachine.transformation.component_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.StateMachine;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.lang.ejbWithStatemachine.transformation.StateMachineTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.AnnotatedMemberReferenceTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class StatemachineTransformation extends AnnotatedMemberReferenceTransformation<Component, StateMachine> {

	public StatemachineTransformation(ClassMechanismTransformation<Component, IType> parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getComponent_Statemachine());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new StateMachineTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		for (IType targetType : resolveTargetTypes()) {
			StateMachineTransformation t = new StateMachineTransformation(this);
			t.setCodeRoot(this.getCodeRoot());
			t.setCodeElement(targetType);
			result.add(t);
		}
	}
}
