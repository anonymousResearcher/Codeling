package org.codeling.lang.ejbWithStatemachine.transformation.component_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.AnnotatedMemberReferenceTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class ReferencesTransformation extends AnnotatedMemberReferenceTransformation<Component, Component> {

	public ReferencesTransformation(ClassMechanismTransformation<Component, IType> parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getComponent_References());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	public boolean hasExpectedAnnotation(IField field) {
		return field.getAnnotation("EJB").exists() || field.getAnnotation("javax.ejb.EJB").exists();
	}
}
