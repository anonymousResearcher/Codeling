package org.codeling.lang.ejbWithStatemachine.transformation;

import java.util.Arrays;
import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.Component;
import org.codeling.lang.ejbWithStatemachine.mm.ejbWithSM.ejbWithSMPackage;
import org.codeling.lang.ejbWithStatemachine.transformation.component_feature.OperationsTransformation;
import org.codeling.lang.ejbWithStatemachine.transformation.component_feature.ReferencesTransformation;
import org.codeling.lang.ejbWithStatemachine.transformation.component_feature.StatemachineTransformation;
import org.codeling.mechanisms.transformations.classes.TypeAnnotationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class ComponentTransformation extends TypeAnnotationTransformation<Component> {

	public ComponentTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, ejbWithSMPackage.eINSTANCE.getComponent());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ReferencesTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new StatemachineTransformation(this));
		result.add(new OperationsTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new StatemachineTransformation(this));
		result.add(new OperationsTransformation(this));
	}

	@Override
	public boolean hasExpectedAnnotation(IType type) {
		List<String> ls = Arrays.asList("Stateful", "javax.ejb.Stateful", "Stateless", "javax.ejb.Stateless",
				"Singleton", "javax.ejb.Singleton");
		for (String s : ls) {
			if (type.getAnnotation(s).exists())
				return true;
		}
		return false;
	}

	@Override
	protected String getNewAnnotationName() {
		return "javax.ejb.Stateful";
	}
}
