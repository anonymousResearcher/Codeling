package org.codeling.lang.ejbWithStatemachine.runtime.component_feature;

import org.codeling.lang.ejbWithStatemachine.mm.component_feature.References;
import org.codeling.lang.ejbWithStatemachine.runtime.ComponentRuntime;
import org.codeling.mechanism.runtime.AnnotatedMemberReferenceRuntime;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class ReferencesRuntime extends AnnotatedMemberReferenceRuntime {

	public ReferencesRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, References.class, ComponentRuntime.class, false);
	}
	
}
