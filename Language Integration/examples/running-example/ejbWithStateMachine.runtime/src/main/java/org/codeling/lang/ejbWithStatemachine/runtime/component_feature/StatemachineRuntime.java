package org.codeling.lang.ejbWithStatemachine.runtime.component_feature;

import org.codeling.lang.ejbWithStatemachine.mm.component_feature.Statemachine;
import org.codeling.lang.ejbWithStatemachine.runtime.StateMachineRuntime;
import org.codeling.mechanism.runtime.AnnotatedMemberReferenceRuntime;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class StatemachineRuntime extends AnnotatedMemberReferenceRuntime {

	public StatemachineRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, Statemachine.class, StateMachineRuntime.class, true);
	}
	
}
