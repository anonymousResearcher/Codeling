package org.codeling.example.ejbsm;

import java.util.Random;

import javax.ejb.Stateful;

import org.codeling.lang.ejbWithStatemachine.mm.component_feature.Operations;

@Stateful
public class BarcodeScanner {
	Random random = new Random();

	@Operations
	public String scanItem() {
		return "Code: " + random.nextInt();
	}
}
