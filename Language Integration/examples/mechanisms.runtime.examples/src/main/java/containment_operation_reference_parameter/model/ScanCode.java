package containment_operation_reference_parameter.model;

import containment_operation_reference_parameter.metamodel.Job;

public class ScanCode implements Job {
	/*
	 * The following operations are part of the entry point. They are part of
	 * the job's execution semantics. They have to be implemented because the
	 * Marker Interface "Job" declares them.
	 */
	boolean scannerAreaFilled = false;

	public void moveItemInFrontOfScanner() {
		scannerAreaFilled = true;
	}

	public void emptyScannerArea() {
		scannerAreaFilled = false;
	}

	public boolean validate() {
		return scannerAreaFilled;
	}

	public void execute() {
		System.out.println("### beep ###");
	}

	public void undo() {
		System.out.println("### beep-beep ###");
	}
}
