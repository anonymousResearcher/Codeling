package containment_operation_reference_parameter.runtime;

import org.codeling.mechanism.runtime.ContainmentOperationRuntime;
import org.codeling.mechanism.runtime.MarkerInterfaceRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

import containment_operation.type.metamodel.Component;
import containment_operation.type.metamodel.Operations;

public class BarCodeScannerRuntimeWithJobsRuntime extends MarkerInterfaceRuntime {

	OperationRuntime operationsRuntime;

	public BarCodeScannerRuntimeWithJobsRuntime(Class<? extends Component> implementingClass)
			throws IntegratedModelException {
		super(implementingClass, Component.class);
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();

		// Create runtimes for attributes and references
		operationsRuntime = new OperationRuntime(this);
		operationsRuntime.initialize();
	}

	public OperationRuntime getOperationsRuntime() {
		return operationsRuntime;
	}
}

class OperationRuntime extends ContainmentOperationRuntime {

	public OperationRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, Operations.class);
	}

}