package type_annotation.runtime;

import java.text.MessageFormat;

import org.codeling.mechanism.runtime.TypeAnnotationRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;

import type_annotation.metamodel.Component;

public class BarCodeScannerRuntime<T> extends TypeAnnotationRuntime<T> {
	public BarCodeScannerRuntime(Class<T> implementingClass) {
		super(implementingClass, Component.class);
	}

	public void startComponent() throws IntegratedModelException {
		System.out.println(MessageFormat.format("Starting {0}.", instance.getClass().getCanonicalName()));

		// Use the entry point to execute individual behavior.
		invokeMethod("onStart", getInstance());
	}

	public void stopComponent() throws IntegratedModelException {
		System.out.println(MessageFormat.format("Stopping {0}.", instance.getClass().getCanonicalName()));

		// Use the entry point to execute individual behavior.
		invokeMethod("onStop", getInstance());
	}
}
