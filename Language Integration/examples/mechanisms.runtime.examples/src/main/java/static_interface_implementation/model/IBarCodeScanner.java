package static_interface_implementation.model;

import static_interface.metamodel.ComponentInterface;

/**
 * This interface represents an instance of ComponentInterface with the name
 * IBarCodeScanner, based on the Static Interface mechanism.
 * 
 * It implies that the meta model element has no execution semantics.
 */
@ComponentInterface
public interface IBarCodeScanner  {
	/**
	 * This is an operation as part of the entry point of the Static Interface mechanism.
	 */
	public String doBeep();
}
