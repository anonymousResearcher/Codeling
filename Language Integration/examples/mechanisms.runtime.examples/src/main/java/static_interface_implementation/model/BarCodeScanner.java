package static_interface_implementation.model;

import static_interface_implementation.metamodel.Component;

/**
 * This class represents an instance of Component with the name BarCodeScanner
 * based on the Marker Interface mechanism.
 * 
 * The focus here lies on the implementation of the interface IBarCodeScanner,
 * which is representation of a class via the Static Interface mechanism.
 * 
 * When the IBarCodeScanner class declares operation signatures as part of its
 * entry point or representing classes using the specific containment operation
 * mechanism, these methods have to be implemented here.
 */
public class BarCodeScanner implements Component, IBarCodeScanner {

	/**
	 * This is an implementation of the operation declared in the entry point of
	 * IBarCodeScanner.
	 */
	public String doBeep() {
		return "beep";
	}

}
