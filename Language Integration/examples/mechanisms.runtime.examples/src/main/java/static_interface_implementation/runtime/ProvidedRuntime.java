package static_interface_implementation.runtime;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.codeling.mechanism.runtime.StaticInterfaceImplementationRuntime;

import static_interface_implementation.model.IBarCodeScanner;

public class ProvidedRuntime<T extends IBarCodeScanner>
		extends StaticInterfaceImplementationRuntime<IBarCodeScanner, T> {

	public ProvidedRuntime(Class<T> implementingClass) {
		super(implementingClass, IBarCodeScanner.class);
	}

	/**
	 * This is some execution semantics of the provided runtime
	 */
	public String[] getProvidedOperationNames() {
		return Arrays.asList(implementingClass.getDeclaredMethods()).stream().map(Method::getName)
				.collect(Collectors.toList()).toArray(new String[0]);
	}
}
