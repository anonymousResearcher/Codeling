package annotated_member_reference.to.static_interface.model;

import annotated_member_reference.to.static_interface.metamodel.Component;

/**
 * This class represents an instance of Component with the name BarCodeScanner
 * based on the Type Annotation mechanism. It provides the ComponentInteface
 * IBarCodeScanner using the Static Interface Implementation mechanism.
 */
@Component
public class BarCodeScanner implements IBarCodeScanner {

	/**
	 * This is an implementation of the operation declared in the entry point of
	 * IBarCodeScanner.
	 */
	public void doBeep() {
		System.out.println("I found a bar code! - Beep");
	}

}
