package annotated_member_reference.marker_interface.multi.model;

import annotated_member_reference.marker_interface.multi.metamodel.SecurityContext;

public class EnterpriseSecurityContext implements SecurityContext {

	@Override
	public String[] getAllowedRoles() {
		return new String[] { "EnterpriseAdmin" };
	}
}
