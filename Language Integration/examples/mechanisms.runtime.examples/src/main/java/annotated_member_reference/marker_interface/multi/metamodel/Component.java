package annotated_member_reference.marker_interface.multi.metamodel;

public interface Component {
	public void onStart();

	public void onStop();
}