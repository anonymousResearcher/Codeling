package annotated_member_reference.marker_interface.single.runtime;

import org.codeling.mechanism.runtime.AnnotatedMemberReferenceRuntime;
import org.codeling.mechanism.runtime.MarkerInterfaceRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

import annotated_member_reference.marker_interface.single.metamodel.Component;
import annotated_member_reference.marker_interface.single.metamodel.Context;
import annotated_member_reference.marker_interface.single.metamodel.SecurityContext;

public class ComponentRuntimeSingle<T extends Component> extends MarkerInterfaceRuntime<Component, T> {

	ContextRuntime referenceRuntime;

	public ComponentRuntimeSingle(Class<T> implementingClass) throws IntegratedModelException {
		super(implementingClass, Component.class);
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}

	@Override
	public void initializeContainments() throws IntegratedModelException {
		referenceRuntime = new ContextRuntime(this);
		referenceRuntime.initialize();
	}
}

class ContextRuntime extends AnnotatedMemberReferenceRuntime {

	public ContextRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, Context.class, SecurityContextRuntime.class, true);
	}

}

class SecurityContextRuntime<T extends SecurityContext> extends MarkerInterfaceRuntime<SecurityContext, T> {
	public SecurityContextRuntime(Class<T> implementingClass) {
		super(implementingClass, SecurityContext.class);
	}
}