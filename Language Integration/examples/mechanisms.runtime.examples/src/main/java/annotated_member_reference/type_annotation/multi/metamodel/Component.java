package annotated_member_reference.type_annotation.multi.metamodel;

public interface Component {
	public void onStart();

	public void onStop();
}