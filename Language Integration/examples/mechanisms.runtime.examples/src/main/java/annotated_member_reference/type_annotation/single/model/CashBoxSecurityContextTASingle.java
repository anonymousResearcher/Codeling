package annotated_member_reference.type_annotation.single.model;

import annotated_member_reference.type_annotation.single.metamodel.SecurityContext;

@SecurityContext
public class CashBoxSecurityContextTASingle {

	public String[] getAllowedRoles() {
		return new String[] { "Admin", "Teacher", "Moderator" };
	}
}
