package annotated_member_reference.type_annotation.single.metamodel;

public interface Component {
	public void onStart();

	public void onStop();
}