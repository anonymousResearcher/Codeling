package annotated_member_reference.type_annotation.multi.metamodel.runtime;

import org.codeling.mechanism.runtime.AnnotatedMemberReferenceRuntime;
import org.codeling.mechanism.runtime.MarkerInterfaceRuntime;
import org.codeling.mechanism.runtime.TypeAnnotationRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

import annotated_member_reference.type_annotation.multi.metamodel.Component;
import annotated_member_reference.type_annotation.multi.metamodel.ResourceContainer;
import annotated_member_reference.type_annotation.multi.metamodel.Resources;

public class ComponentRuntimeTAMulti<T extends Component> extends MarkerInterfaceRuntime<Component, T> {

	ContextRuntime resourcesRuntime;

	public ComponentRuntimeTAMulti(Class<T> implementingClass) throws IntegratedModelException {
		super(implementingClass, Component.class);
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}

	@Override
	public void initializeContainments() throws IntegratedModelException {
		resourcesRuntime = new ContextRuntime(this);
		resourcesRuntime.initialize();
	}
}

class ContextRuntime extends AnnotatedMemberReferenceRuntime {

	public ContextRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, Resources.class, ResourceContainerRuntime.class, true);
	}

}

class ResourceContainerRuntime<T> extends TypeAnnotationRuntime<T> {
	public ResourceContainerRuntime(Class<T> implementingClass) {
		super(implementingClass, ResourceContainer.class);
	}
}