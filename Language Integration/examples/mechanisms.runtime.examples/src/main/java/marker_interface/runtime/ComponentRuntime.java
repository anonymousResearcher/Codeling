package marker_interface.runtime;

import java.text.MessageFormat;

import org.codeling.mechanism.runtime.MarkerInterfaceRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;

import marker_interface.metamodel.Component;

public class ComponentRuntime<T extends Component> extends MarkerInterfaceRuntime<Component, T> {

	public ComponentRuntime(Class<T> implementingClass) {
		super(implementingClass, Component.class);
	}

	public void startComponent() throws IntegratedModelException {
		System.out.println(MessageFormat.format("Starting {0}.", getInstance().getClass().getCanonicalName()));

		// Use the entry point to execute individual behavior.
		invokeMethod("onStart", getInstance());
	}

	public void stopComponent() throws IntegratedModelException {
		System.out.println(MessageFormat.format("Stopping {0}.", getInstance().getClass().getCanonicalName()));

		// Use the entry point to execute individual behavior.
		invokeMethod("onStop", getInstance());
	}
}
