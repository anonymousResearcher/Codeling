package containment_operation_reference_to_static_interface.model;

import containment_operation_reference_to_static_interface.metamodel.Component;

public class BarCodeScanner implements IBarCodeScanner, Component {

	public void scanCode() {
		System.out.println("Beep");
	}

}
