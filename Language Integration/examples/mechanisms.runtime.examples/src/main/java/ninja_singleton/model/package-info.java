/**
 * This empty package represents the Ninja Singleton mechanism. No explicit
 * representation of a meta model element or model element exists.
 * 
 * It implies, that the meta model element has no attributes (not even "name"),
 * and all references must be containments, and their targets must not be
 * referenced by any other containment reference.
 * 
 * In the resulting model, exactly one instance of this meta model element
 * exists. For all target meta model classes of containment references, each
 * target instance is contained by this one instance.
 * 
 * As an example, Architecture is represented by this mechanism. It has no
 * source code representation, and all components, interfaces, connectors, and
 * namespaces are owned by the same architecture.
 */
package ninja_singleton.model;