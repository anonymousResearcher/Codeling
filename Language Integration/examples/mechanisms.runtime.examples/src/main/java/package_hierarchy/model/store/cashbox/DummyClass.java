package package_hierarchy.model.store.cashbox;

/**
 * A package requires a class to be loaded by a classloader for the reflection
 * mechanisms to know the package. Therefore this dummy class exists.
 */
public class DummyClass {

}
