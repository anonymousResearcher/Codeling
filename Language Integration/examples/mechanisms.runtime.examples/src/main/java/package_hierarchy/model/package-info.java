/**
 * This package represents the root of the mechanism Package Hierarchy. Its
 * subpackages represent instances of the meta model class Namespace.
 */
package package_hierarchy.model;