package containment_operation_attribute.model;

import java.text.MessageFormat;

import containment_operation_attribute.metamodel.Component;
import containment_operation_attribute.metamodel.Operations;

/**
 * This type represents an instance of the class Component via the Marker
 * Interface mechanism. This is also possible to be used in source classes
 * represented by the Type Annotation mechanism.
 */
public class BarCodeScanner implements Component {

	/**
	 * This operation represents an instance of the class ComponentOperation via
	 * the Containment Operation mechanism. Please note that the marker
	 * interface declares the operation signature, for the execution semantics
	 * to be referenced by other elements.
	 * 
	 * The focus here lies on the attribute "version" of the class
	 * "ComponentOperation". The attribute can be set via the annotation
	 * parameter.
	 */
	@Operations(version = "1.0")
	public void scanCode() {
		System.out.println(MessageFormat.format(
				"The component [{0}] is executing the semantics of the conainment reference to [scanCode].",
				this.getClass().getSimpleName()));
	}

}
