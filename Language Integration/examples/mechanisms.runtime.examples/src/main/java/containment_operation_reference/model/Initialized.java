package containment_operation_reference.model;

import containment_operation_reference.metamodel.State;
import containment_operation_reference.metamodel.Transition;

/**
 * This type could represent a class using the Annotation Type or Marker
 * Interface mechanism.
 */
public class Initialized implements State {

	/**
	 * The runtime sets a target. The runtime can use the target type. The entry
	 * point can use the target static interface.
	 *
	 * This style is useful when the reference has it's own executional
	 * semantics, besides the semantics of the target class, and the reference's
	 * semantics is not standardized, and - the target class is not contained,
	 * or - the target class is referenced by other classes.
	 */
	@Transition(target=Finished.class)
	public void finish() {
		System.out.println("Executing the transition 'finish'.");
	}

}
