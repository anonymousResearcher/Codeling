package containment_operation.type.model;

import java.text.MessageFormat;

import containment_operation.type.metamodel.Component;
import containment_operation.type.metamodel.Operations;

/**
 * This type represents an instance of the class Component via the Marker
 * Interface mechanism. This is also possible to be used in source classes
 * represented by the Type Annotation mechanism.
 */
public class BarCodeScanner implements Component {

	private boolean scannedCode = false;

	/**
	 * This operation represents an instance of the class ComponentOperation via
	 * the Containment Operation mechanism. Please note that the marker
	 * interface declares the operation signature, for the execution semantics
	 * to be referenced by other elements.
	 */
	@Operations
	public void scanCode() {
		scannedCode = true;
		System.out.println(MessageFormat.format("The component [{0}] is executing the semantics of the conainment reference to [scanCode].",
				this.getClass().getSimpleName()));
	}

}
