package containment_operation.static_interface.runtime;

import org.codeling.mechanism.runtime.ContainmentOperationRuntime;
import org.codeling.mechanism.runtime.StaticInterfaceImplementationRuntime;
import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

import containment_operation.static_interface.metamodel.Component;
import containment_operation.static_interface.metamodel.Operations;

public class BarCodeScannerRuntimeWithInterfaceRuntime<T extends Component>
		extends StaticInterfaceImplementationRuntime<Component, T> {

	OperationRuntime operationsRuntime;

	public BarCodeScannerRuntimeWithInterfaceRuntime(Class<T> implementingClass) throws IntegratedModelException {
		super(implementingClass, Component.class);
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}

	@Override
	public void initializeContainments() throws IntegratedModelException {
		super.initializeContainments();

		// Create runtimes for attributes and references
		operationsRuntime = new OperationRuntime(this);
		operationsRuntime.initialize();
	}

	public OperationRuntime getOperationsRuntime() {
		return operationsRuntime;
	}
}

class OperationRuntime extends ContainmentOperationRuntime {

	public OperationRuntime(TypeMechanismRuntime owningRuntime) {
		super(owningRuntime, Operations.class);
	}

}