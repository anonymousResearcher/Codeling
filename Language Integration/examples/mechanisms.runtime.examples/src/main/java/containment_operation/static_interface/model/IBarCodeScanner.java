package containment_operation.static_interface.model;

import containment_operation.static_interface.metamodel.ComponentInterface;
import containment_operation.static_interface.metamodel.Operations;

/**
 * This interface represents an instance of ComponentInterface with the name
 * IBarCodeScanner, based on the Static Interface mechanism.
 * 
 * It implies that the meta model element has no execution semantics.
 */
@ComponentInterface
public interface IBarCodeScanner {
	/**
	 * This represents the containment reference using the Containment Operation
	 * mechanism in the variant of Static Interfaces.
	 */
	@Operations
	public void scanCode();

}
