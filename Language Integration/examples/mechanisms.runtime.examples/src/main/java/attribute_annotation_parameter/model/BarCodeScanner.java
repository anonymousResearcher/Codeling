package attribute_annotation_parameter.model;

import attribute_annotation_parameter.metamodel.Component;

/**
 * The Constant Member Attribute mechanism depends on the attribute's owner to
 * be represented by the Type Annotation mechanism.
 * 
 * The Attribute Annotation Parameter mechanism represents the attribute version
 * with the annotation parameter version of the annotation Component.
 */
@Component(version = "1.1")
public class BarCodeScanner {

}
