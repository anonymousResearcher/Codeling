package org.codeling.examples.ccm.model.cashDesk.statemachine.states;

import java.util.Queue;

import org.codeling.examples.ccm.mm.State;
import org.codeling.examples.ccm.mm.state_feature.Transitions;
import org.codeling.examples.ccm.model.IStoreServer;
import org.codeling.examples.ccm.model.cashDesk.CashDeskVariables;
import org.codeling.examples.ccm.model.cashDesk.IPrinter;
import org.codeling.examples.ccm.model.cashDesk.statemachine.contracts.FinishSalesContract;

public class AwaitingPayment implements State {

	@Transitions(target = Resetting.class, contracts = { FinishSalesContract.class })
	public void finishSale(IPrinter iPrinter, IStoreServer iStoreServer, CashDeskVariables variables) {
		Queue<String> scannedItems = variables.getScannedItems();
		iPrinter.print(createBill(scannedItems));
		iStoreServer.addSoldItems(scannedItems);
		variables.resetScannedItems();
	}

	private final static String NL = System.getProperty("line.separator");

	private String createBill(Queue<String> scannedItems) {
		return scannedItems.stream().reduce(null, (i1, i2) -> i1 == null ? i2 : i1 + "," + NL + i2);
	}
}
