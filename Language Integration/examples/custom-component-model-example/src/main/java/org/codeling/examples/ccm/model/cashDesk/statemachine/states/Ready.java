package org.codeling.examples.ccm.model.cashDesk.statemachine.states;

import org.codeling.examples.ccm.mm.State;
import org.codeling.examples.ccm.mm.state_feature.Transitions;
import org.codeling.examples.ccm.model.cashDesk.CashDeskVariables;
import org.codeling.examples.ccm.model.cashDesk.IBarCodeScanner;
import org.codeling.examples.ccm.model.cashDesk.statemachine.contracts.ScanItemContract;
import org.codeling.examples.ccm.model.cashDesk.statemachine.contracts.StartSaleContract;

public class Ready implements State {

	@Transitions(target = Scanning.class, contracts = { StartSaleContract.class, ScanItemContract.class })
	public void scanItem(IBarCodeScanner iBarCodeScanner, CashDeskVariables variables) {
		String scannedItem = iBarCodeScanner.scan();
		variables.addScannedItem(scannedItem);
	}
}
