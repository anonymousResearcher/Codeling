package org.codeling.examples.ccm.mm.architecture_feature;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.codeling.examples.ccm.mm.State;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface States {
	
	Class<? extends State>[] value();
}
