package org.codeling.examples.ccm.model;

import org.codeling.examples.ccm.mm.Interface;
import org.codeling.examples.ccm.mm.interface_feature.Operations;
import org.codeling.examples.ccm.model.roles.Cashier;
import org.codeling.examples.ccm.model.roles.StoreManager;
import org.codeling.examples.ccm.model.strategies.ExecuteLocally;

@Interface
public interface ICashDesk {
	
	@Operations(
			timeResourceDemand="IntPMF[(10;0.3)(20;0.5)(50;0.2)]",
			executionStrategy=ExecuteLocally.class,
			rolesAllowed={Cashier.class, StoreManager.class})
	public void scanItem();
	
	@Operations(
			timeResourceDemand="IntPMF[(10;0.7)(20;0.3)]",
			executionStrategy=ExecuteLocally.class,
			rolesAllowed={Cashier.class, StoreManager.class})
	public void openCashBox();
	
	@Operations(
			timeResourceDemand="IntPMF[(30;0.3)(50;0.4)(80;0.3)]",
			executionStrategy=ExecuteLocally.class,
			rolesAllowed={Cashier.class, StoreManager.class})
	public void finish();
}
