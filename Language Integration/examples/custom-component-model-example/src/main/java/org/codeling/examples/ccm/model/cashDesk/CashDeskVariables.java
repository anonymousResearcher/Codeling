package org.codeling.examples.ccm.model.cashDesk;

import java.util.Queue;

public interface CashDeskVariables {
	public void addScannedItem(String item);

	public Queue<String> getScannedItems();
	
	public void resetScannedItems();

	public void clearScannedItems();
}
