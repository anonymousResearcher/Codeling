package org.codeling.examples.ccm.model;

import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Queue;

import org.codeling.examples.ccm.mm.Attribute;
import org.codeling.examples.ccm.mm.ComponentType;
import org.codeling.examples.ccm.model.external.StoreServerDBSimulation;

@ComponentType(version = "1.1")
public class StoreServer implements IStoreServer {

	@Attribute
	public static final boolean parallel = true;

	final StoreServerDBSimulation database = StoreServerDBSimulation.getInstance();
	final String NL = System.getProperty("line.separator");
	StringBuffer report = new StringBuffer();

	@Override
	public void addSoldItems(Queue<String> scannedItems) {
		database.addSoldItems(scannedItems);
	}

	@Override
	public void generateReport() {
		if (parallel) {
			synchronized (database) {
				doGenerateReport();
			}
		} else {
			doGenerateReport();
		}
	}

	@Override
	public String getLatestReport() {
		return report.toString();
	}

	private void doGenerateReport() {
		report = new StringBuffer();
		// Create report header
		report.append("=== Sold Items ===" + NL);

		// Create a map of <item, sold count>
		LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
		for (String item : database.getSoldItems()) {
			Integer count = map.get(item);
			if (count == null)
				count = 0;
			map.put(item, ++count);
		}

		// Fill the report with the map entries
		for (Entry<String, Integer> e : map.entrySet())
			report.append(MessageFormat.format("Sold {0} {1} times", e.getKey(), e.getValue()) + NL);
	}

}
