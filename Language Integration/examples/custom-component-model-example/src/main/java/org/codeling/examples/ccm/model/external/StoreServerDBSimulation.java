package org.codeling.examples.ccm.model.external;

import java.util.Collection;
import java.util.LinkedList;

public class StoreServerDBSimulation {

	private static StoreServerDBSimulation INSTANCE;
	private StoreServerDBSimulation() {/* private due to singleton pattern */}
	
	public static StoreServerDBSimulation getInstance() {
		if (INSTANCE == null)
			INSTANCE = new StoreServerDBSimulation();
		return INSTANCE;
	}

	LinkedList<String> soldItems = new LinkedList<String>();

	public void addSoldItems(Collection<String> items) {
		soldItems.addAll(items);
	}

	@SuppressWarnings("unchecked")
	public Collection<String> getSoldItems() {
		return (Collection<String>) soldItems.clone();
	}

}
