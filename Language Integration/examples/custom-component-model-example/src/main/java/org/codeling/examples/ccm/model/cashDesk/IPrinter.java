package org.codeling.examples.ccm.model.cashDesk;

import org.codeling.examples.ccm.mm.Interface;
import org.codeling.examples.ccm.mm.interface_feature.Operations;
import org.codeling.examples.ccm.model.roles.Cashier;
import org.codeling.examples.ccm.model.roles.StoreManager;
import org.codeling.examples.ccm.model.strategies.ExecuteLocally;

@Interface
public interface IPrinter {

	@Operations(
			timeResourceDemand="10",
			executionStrategy=ExecuteLocally.class,
			rolesAllowed={Cashier.class, StoreManager.class})
	public void print(String content);
	
}
