package org.codeling.examples.ccm.mm.state_feature;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.codeling.examples.ccm.mm.Contract;
import org.codeling.examples.ccm.mm.State;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Transitions {

	Class<? extends State> target();

	Class<? extends Contract>[] contracts();

}
