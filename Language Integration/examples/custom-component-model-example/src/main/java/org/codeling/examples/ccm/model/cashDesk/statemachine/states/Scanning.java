package org.codeling.examples.ccm.model.cashDesk.statemachine.states;

import org.codeling.examples.ccm.mm.State;
import org.codeling.examples.ccm.mm.state_feature.Immediate;
import org.codeling.examples.ccm.mm.state_feature.Transitions;
import org.codeling.examples.ccm.model.ICashDesk;
import org.codeling.examples.ccm.model.cashDesk.CashDeskVariables;
import org.codeling.examples.ccm.model.cashDesk.IBarCodeScanner;
import org.codeling.examples.ccm.model.cashDesk.statemachine.contracts.ScanItemContract;

@Immediate(true)
public class Scanning implements State {

	@Transitions(target = Scanning.class, contracts = { ScanItemContract.class })
	public void scanItem(IBarCodeScanner iBarCodeScanner, CashDeskVariables variables) {
		String scannedItem = iBarCodeScanner.scan();
		variables.addScannedItem(scannedItem);
	}

	@Transitions(target = AwaitingPayment.class, contracts = {})
	public void openCashBox(ICashDesk iCashDesk) {
		iCashDesk.openCashBox();
	}
}
