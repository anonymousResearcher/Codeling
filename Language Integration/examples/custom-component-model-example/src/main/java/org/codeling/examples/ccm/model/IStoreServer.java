package org.codeling.examples.ccm.model;

import java.util.Queue;

import org.codeling.examples.ccm.mm.Interface;
import org.codeling.examples.ccm.mm.interface_feature.Operations;
import org.codeling.examples.ccm.model.roles.Cashier;
import org.codeling.examples.ccm.model.roles.EnterpriseManager;
import org.codeling.examples.ccm.model.roles.StoreManager;
import org.codeling.examples.ccm.model.strategies.ExecuteInCloud;
import org.codeling.examples.ccm.model.strategies.ExecuteLocally;

@Interface
public interface IStoreServer {

	@Operations(
			timeResourceDemand="IntPMF[(20000;0.1)(400000;0.2)(50000;0.5)(100000;0.3)]",
			executionStrategy=ExecuteInCloud.class,
			rolesAllowed={StoreManager.class})
	public void generateReport();
	
	@Operations(
			timeResourceDemand="IntPMF[(100;0.3)(200;0.5)(500;0.2)]",
			executionStrategy=ExecuteLocally.class,
			rolesAllowed={StoreManager.class, EnterpriseManager.class})
	public String getLatestReport();

	@Operations(
			timeResourceDemand="100",
			executionStrategy=ExecuteLocally.class,
			rolesAllowed={Cashier.class})
	public void addSoldItems(Queue<String> scannedItems);
	
}
