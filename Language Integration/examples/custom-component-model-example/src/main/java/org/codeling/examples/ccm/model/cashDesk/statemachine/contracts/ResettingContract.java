package org.codeling.examples.ccm.model.cashDesk.statemachine.contracts;

import org.codeling.examples.ccm.mm.Contract;
import org.codeling.examples.ccm.model.cashDesk.CashDeskVariables;

public class ResettingContract implements Contract {

	CashDeskVariables post;

	@Override
	public boolean validatePreCondition() {
		return true;
	}

	@Override
	public boolean validatePostCondition() {
		return post.getScannedItems() != null && post.getScannedItems().size() == 0;
	}

}
