package org.codeling.examples.ccm.model.external;

import java.util.LinkedList;
import java.util.Queue;

import org.codeling.examples.ccm.model.cashDesk.CashDeskVariables;

public class CashDeskDBSimulation implements CashDeskVariables {

	LinkedList<String> scannedItems = new LinkedList<String>();

	@Override
	public void addScannedItem(String item) {
		scannedItems.add(item);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Queue<String> getScannedItems() {
		return (Queue<String>) scannedItems.clone();
	}

	@Override
	public void resetScannedItems() {
		scannedItems.clear();
	}

	@Override
	public void clearScannedItems() {
		scannedItems.clear();
	}

}
