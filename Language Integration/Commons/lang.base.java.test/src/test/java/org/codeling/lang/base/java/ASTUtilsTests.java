package org.codeling.lang.base.java;

import static org.codeling.lang.base.java.TransformationTestUtils.getSimpleProject;
import static org.codeling.lang.base.java.TransformationTestUtils.getTestJavaSource;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.codeling.lang.base.java.ASTUtils;
import org.eclipse.jdt.core.IAnnotation;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.junit.Test;

import testComponent.CMComponent;
import testComponent.VersionedComponent;

public class ASTUtilsTests {
	@Test
	public void deleteAnnotationWithoutParentheses() throws Exception {
		final IJavaProject project = getSimpleProject("deleteAnnotationWithoutParentheses");

		final IPackageFragmentRoot srcFolder = project.getPackageFragmentRoots()[0];
		final IPackageFragment fragment = srcFolder.createPackageFragment("testComponent", true, null);

		// Load all prepared java files
		fragment.createCompilationUnit("VersionedComponent.java",
				getTestJavaSource("testComponent/VersionedComponent.java"), true, null);

		fragment.createCompilationUnit("CMComponent.java", getTestJavaSource("testComponent/CMComponent.java"), true,
				null);

		final ICompilationUnit cu = fragment.createCompilationUnit("DummyClass.java",
				getTestJavaSource("testComponent/DummyClass.java"), true, null);

		// Resolve annotation
		final IType type = cu.getType("DummyClass");
		final Class<?> annotationClass = CMComponent.class;
		final IAnnotation a = type.getAnnotation(annotationClass.getSimpleName());
		assertTrue("Annotation does not exist before deletion, but it should.",
				type.getSource().contains(annotationClass.getSimpleName()));
		ASTUtils.deleteAnnotationFromType(a, type);
		assertFalse("Annotation was not removed, but should be.", type.getSource().contains(annotationClass.getSimpleName()));
	}

	@Test
	public void deleteAnnotationWithParentheses() throws Exception {
		final IJavaProject project = getSimpleProject("deleteAnnotationWithParentheses");

		final IPackageFragmentRoot srcFolder = project.getPackageFragmentRoots()[0];
		final IPackageFragment fragment = srcFolder.createPackageFragment("testComponent", true, null);

		// Load all prepared java files
		fragment.createCompilationUnit("VersionedComponent.java",
				getTestJavaSource("testComponent/VersionedComponent.java"), true, null);

		fragment.createCompilationUnit("CMComponent.java", getTestJavaSource("testComponent/CMComponent.java"), true,
				null);

		final ICompilationUnit cu = fragment.createCompilationUnit("DummyClass.java",
				getTestJavaSource("testComponent/DummyClass.java"), true, null);

		// Resolve annotation
		final IType type = cu.getType("DummyClass");
		final Class<?> annotationClass = VersionedComponent.class;
		final IAnnotation a = type.getAnnotation(annotationClass.getSimpleName());

		assertTrue("Annotation does not exist before deletion, but it should.",
				type.getSource().contains(annotationClass.getSimpleName()));
		ASTUtils.deleteAnnotationFromType(a, type);
		assertFalse("Annotation was not removed, but should be.", type.getSource().contains(annotationClass.getSimpleName()));
	}
}
