package org.codeling.mechanisms.transformations

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation
import java.util.Arrays
import java.util.List
import java.util.concurrent.ExecutionException
import org.codeling.mechanisms.transformations.classes.TypeAnnotationTransformation
import org.codeling.test.jdt.JDTUtils
import org.eclipse.core.runtime.CoreException
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.jdt.core.ICompilationUnit
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IJavaProject
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IType
import org.junit.Test
import testPackage.Named
import testPackage.TestPackageFactory
import testPackage.TestPackagePackage

import static org.junit.Assert.*
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull

class TypeAnnotationTest {
	static final String LANG = "testLanguage";

	@Test
	def void toModel() throws InterruptedException, ExecutionException, CoreException {

		val String testName = "toModel";
		val String typeName = "ExpectedName";

		val projectAndPackageName = LANG + "_" + this.getClass().getSimpleName() + "_" + testName;

		// Create code element
		val String contents = '''
		package «projectAndPackageName»;
		
		import org.codeling.lang.testLanguage.mm.Named;
		
		@Named
		public class «typeName» {}''';

		val IJavaProject project = JDTUtils.createSimpleProject(projectAndPackageName);
		val IPackageFragment pf = project.getPackageFragmentRoots().get(0).createPackageFragment(testName, true, null);
		val ICompilationUnit cu = pf.createCompilationUnit(typeName + ".java", contents, true, null);

		// Execute transformation
		val SUT_TA t = new SUT_TA(null, TestPackagePackage.eINSTANCE.getNamed(), LANG);
		t.codeElement = cu.getTypes.get(0);
		val Named result = t.transformToModel();

		// Validate results
		assertNotNull(result);
		assertEquals(typeName, result.getName());
	}

	@Test
	def void createCodeFragments() throws InterruptedException, ExecutionException, CoreException {

		val String testName = "createCodeFragments";
		val String typeName = "ExpectedName";

		val projectAndPackageName = LANG + "_" + this.getClass().getSimpleName() + "_" + testName;

		// Create modelelement
		val Named m = TestPackageFactory.eINSTANCE.createNamed;
		m.name = typeName;

		val IJavaProject project = JDTUtils.createSimpleProject(projectAndPackageName);
		project.getPackageFragmentRoots().get(0).createPackageFragment(testName, true, null);

		// ensure that the element does not exist before the transformation is executed
		assertNull(project.findType(projectAndPackageName + "." + typeName));

		// Execute transformation
		val SUT_TA t = new SUT_TA(null, TestPackagePackage.eINSTANCE.getNamed(), LANG);
		t.codeRoot = Arrays.asList(project);
		t.modelElement = m;
		t.createCodeFragments;
		val IType result = project.findType(projectAndPackageName + "." + typeName);

		// Validate results
		assertNotNull(result);

		val String expectedContent = '''
		package «projectAndPackageName»;
		
		import org.codeling.lang.testLanguage.mm.Named;
		
		@Named
		public class «typeName» {}''';

		assertEquals(expectedContent, result.compilationUnit.source);
	}

	@Test
	def deleteCodeFragment() {
		val String testName = "deleteCodeFragment";
		val String typeName = "ExpectedName";

		val projectAndPackageName = LANG + "_" + this.getClass().getSimpleName() + "_" + testName;

		// Create code element
		val String contents = '''
		package «projectAndPackageName»;
		
		import org.codeling.lang.testLanguage.mm.Named;
		
		@Named
		public class «typeName» {}''';

		val IJavaProject project = JDTUtils.createSimpleProject(projectAndPackageName);
		val IPackageFragment pf = project.getPackageFragmentRoots().get(0).createPackageFragment(projectAndPackageName,
			true, null);
		val ICompilationUnit cu = pf.createCompilationUnit(typeName + ".java", contents, true, null);

		// ensure that the element does exists before the transformation is executed
		assertNotNull(project.findType(projectAndPackageName + "." + typeName));

		// Execute transformation
		val SUT_TA t = new SUT_TA(null, TestPackagePackage.eINSTANCE.getNamed(), LANG);
		t.codeElement = cu.getType(typeName);
		t.deleteCodeFragments;
		val IType result = project.findType(projectAndPackageName + "." + typeName);

		assertTrue(result === null || !result.exists);
	}

	@Test
	def updateCodeFragment() {
		val String testName = "updateCodeFragment";
		val String typeName = "UnexpectedName";
		val String newTypeName = "ExpectedName";

		val projectAndPackageName = LANG + "_" + this.getClass().getSimpleName() + "_" + testName;

		// Create prior code element
		val String contents = '''
		package «projectAndPackageName»;
		
		import org.codeling.lang.testLanguage.mm.Named;
		
		@Named
		@Deprecated // Just to show that it is preserved
		public class «typeName» {
			// This is something in the entry point, to be preserved
		}''';

		val IJavaProject project = JDTUtils.createSimpleProject(projectAndPackageName);
		val IPackageFragment pf = project.getPackageFragmentRoots().get(0).createPackageFragment(projectAndPackageName,
			true, null);
		val ICompilationUnit cu = pf.createCompilationUnit(typeName + ".java", contents, true, null);

		// Create prior model element
		val Named m = TestPackageFactory.eINSTANCE.createNamed;
		m.name = typeName;

		// Create prior model element
		val Named newM = TestPackageFactory.eINSTANCE.createNamed;
		newM.name = newTypeName;

		// ensure that the new element does not exist and the old element exists before the transformation is executed
		assertNull(project.findType(projectAndPackageName + "." + newTypeName));
		assertNotNull(project.findType(projectAndPackageName + "." + typeName));

		// Execute transformation
		val SUT_TA t = new SUT_TA(null, TestPackagePackage.eINSTANCE.getNamed(), LANG);
		t.codeRoot = Arrays.asList(project);
		t.codeElement = cu.getType(typeName);
		t.priorModelElement = m;
		t.modelElement = newM;
		t.updateCodeFragments;

		val IType result = project.findType(projectAndPackageName + "." + newTypeName);
		assertNotNull(result);
		assertNull(project.findType(projectAndPackageName + "." + typeName));

		val String expectedContent = '''
		package «projectAndPackageName»;
		
		import org.codeling.lang.testLanguage.mm.Named;
		
		@Named
		@Deprecated // Just to show that it is preserved
		public class «newTypeName» {
			// This is something in the entry point, to be preserved
		}''';

		assertEquals(expectedContent, result.compilationUnit.source);
	}
}

class SUT_TA extends TypeAnnotationTransformation<Named> {

	new(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation, EClass eClass,
		String languageName) {
		super(parentTransformation, eClass)
	}

	override protected doCreateChildTransformationsToCode(
		List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override protected doCreateChildTransformationsToModel(
		List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override doCreateCrossReferencesTransformations(
		List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

}
