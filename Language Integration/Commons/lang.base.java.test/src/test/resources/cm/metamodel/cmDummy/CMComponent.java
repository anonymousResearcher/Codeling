/**
 */
package cm.metamodel.cmDummy;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CM Component</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see cm.metamodel.cmDummy.CmDummyPackage#getCMComponent()
 * @model
 * @generated
 */
public interface CMComponent extends EObject {
} // CMComponent
