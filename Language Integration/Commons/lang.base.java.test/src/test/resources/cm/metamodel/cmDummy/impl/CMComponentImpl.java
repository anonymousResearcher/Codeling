/**
 */
package cm.metamodel.cmDummy.impl;

import cm.metamodel.cmDummy.CMComponent;
import cm.metamodel.cmDummy.CmDummyPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CM Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CMComponentImpl extends MinimalEObjectImpl.Container implements CMComponent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CMComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CmDummyPackage.Literals.CM_COMPONENT;
	}

} //CMComponentImpl
