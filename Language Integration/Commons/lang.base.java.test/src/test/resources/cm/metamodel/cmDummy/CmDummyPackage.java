/**
 */
package cm.metamodel.cmDummy;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see cm.metamodel.cmDummy.CmDummyFactory
 * @model kind="package"
 * @generated
 */
public interface CmDummyPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "cmDummy";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://example.org/cmDummy/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "cmDummy";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CmDummyPackage eINSTANCE = cm.metamodel.cmDummy.impl.CmDummyPackageImpl.init();

	/**
	 * The meta object id for the '{@link cm.metamodel.cmDummy.impl.CMComponentImpl <em>CM Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see cm.metamodel.cmDummy.impl.CMComponentImpl
	 * @see cm.metamodel.cmDummy.impl.CmDummyPackageImpl#getCMComponent()
	 * @generated
	 */
	int CM_COMPONENT = 0;

	/**
	 * The number of structural features of the '<em>CM Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CM_COMPONENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>CM Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CM_COMPONENT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link cm.metamodel.cmDummy.CMComponent <em>CM Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CM Component</em>'.
	 * @see cm.metamodel.cmDummy.CMComponent
	 * @generated
	 */
	EClass getCMComponent();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CmDummyFactory getCmDummyFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link cm.metamodel.cmDummy.impl.CMComponentImpl <em>CM Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see cm.metamodel.cmDummy.impl.CMComponentImpl
		 * @see cm.metamodel.cmDummy.impl.CmDummyPackageImpl#getCMComponent()
		 * @generated
		 */
		EClass CM_COMPONENT = eINSTANCE.getCMComponent();

	}

} //CmDummyPackage
