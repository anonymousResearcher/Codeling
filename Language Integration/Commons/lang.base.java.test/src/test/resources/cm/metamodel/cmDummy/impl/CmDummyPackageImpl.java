/**
 */
package cm.metamodel.cmDummy.impl;

import cm.metamodel.cmDummy.CMComponent;
import cm.metamodel.cmDummy.CmDummyFactory;
import cm.metamodel.cmDummy.CmDummyPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CmDummyPackageImpl extends EPackageImpl implements CmDummyPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cmComponentEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see cm.metamodel.cmDummy.CmDummyPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CmDummyPackageImpl() {
		super(eNS_URI, CmDummyFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CmDummyPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CmDummyPackage init() {
		if (isInited) return (CmDummyPackage)EPackage.Registry.INSTANCE.getEPackage(CmDummyPackage.eNS_URI);

		// Obtain or create and register package
		CmDummyPackageImpl theCmDummyPackage = (CmDummyPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CmDummyPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CmDummyPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theCmDummyPackage.createPackageContents();

		// Initialize created meta-data
		theCmDummyPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCmDummyPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CmDummyPackage.eNS_URI, theCmDummyPackage);
		return theCmDummyPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCMComponent() {
		return cmComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CmDummyFactory getCmDummyFactory() {
		return (CmDummyFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		cmComponentEClass = createEClass(CM_COMPONENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(cmComponentEClass, CMComponent.class, "CMComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //CmDummyPackageImpl
