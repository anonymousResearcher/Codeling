/**
 */
package cm.metamodel.cmDummy;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see cm.metamodel.cmDummy.CmDummyPackage
 * @generated
 */
public interface CmDummyFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CmDummyFactory eINSTANCE = cm.metamodel.cmDummy.impl.CmDummyFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>CM Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CM Component</em>'.
	 * @generated
	 */
	CMComponent createCMComponent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CmDummyPackage getCmDummyPackage();

} //CmDummyFactory
