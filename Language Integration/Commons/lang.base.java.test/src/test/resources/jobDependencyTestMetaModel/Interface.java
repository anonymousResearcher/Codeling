/**
 */
package jobDependencyTestMetaModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see jobDependencyTestMetaModel.JobDependencyTestMetaModelPackage#getInterface()
 * @model
 * @generated
 */
public interface Interface extends EObject {
} // Interface
