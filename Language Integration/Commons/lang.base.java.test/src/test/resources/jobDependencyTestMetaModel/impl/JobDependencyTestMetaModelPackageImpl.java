/**
 */
package jobDependencyTestMetaModel.impl;

import jobDependencyTestMetaModel.Bean;
import jobDependencyTestMetaModel.Interface;
import jobDependencyTestMetaModel.JobDependencyTestMetaModelFactory;
import jobDependencyTestMetaModel.JobDependencyTestMetaModelPackage;
import jobDependencyTestMetaModel.Provision;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JobDependencyTestMetaModelPackageImpl extends EPackageImpl implements JobDependencyTestMetaModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass beanEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass provisionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see jobDependencyTestMetaModel.JobDependencyTestMetaModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private JobDependencyTestMetaModelPackageImpl() {
		super(eNS_URI, JobDependencyTestMetaModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link JobDependencyTestMetaModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static JobDependencyTestMetaModelPackage init() {
		if (isInited) return (JobDependencyTestMetaModelPackage)EPackage.Registry.INSTANCE.getEPackage(JobDependencyTestMetaModelPackage.eNS_URI);

		// Obtain or create and register package
		JobDependencyTestMetaModelPackageImpl theJobDependencyTestMetaModelPackage = (JobDependencyTestMetaModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof JobDependencyTestMetaModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new JobDependencyTestMetaModelPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theJobDependencyTestMetaModelPackage.createPackageContents();

		// Initialize created meta-data
		theJobDependencyTestMetaModelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theJobDependencyTestMetaModelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(JobDependencyTestMetaModelPackage.eNS_URI, theJobDependencyTestMetaModelPackage);
		return theJobDependencyTestMetaModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBean() {
		return beanEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterface() {
		return interfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvision() {
		return provisionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JobDependencyTestMetaModelFactory getJobDependencyTestMetaModelFactory() {
		return (JobDependencyTestMetaModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		beanEClass = createEClass(BEAN);

		interfaceEClass = createEClass(INTERFACE);

		provisionEClass = createEClass(PROVISION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(beanEClass, Bean.class, "Bean", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(interfaceEClass, Interface.class, "Interface", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(provisionEClass, Provision.class, "Provision", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //JobDependencyTestMetaModelPackageImpl
