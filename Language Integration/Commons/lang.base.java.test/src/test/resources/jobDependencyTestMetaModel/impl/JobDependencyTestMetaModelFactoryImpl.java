/**
 */
package jobDependencyTestMetaModel.impl;

import jobDependencyTestMetaModel.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JobDependencyTestMetaModelFactoryImpl extends EFactoryImpl implements JobDependencyTestMetaModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static JobDependencyTestMetaModelFactory init() {
		try {
			JobDependencyTestMetaModelFactory theJobDependencyTestMetaModelFactory = (JobDependencyTestMetaModelFactory)EPackage.Registry.INSTANCE.getEFactory(JobDependencyTestMetaModelPackage.eNS_URI);
			if (theJobDependencyTestMetaModelFactory != null) {
				return theJobDependencyTestMetaModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new JobDependencyTestMetaModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JobDependencyTestMetaModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case JobDependencyTestMetaModelPackage.BEAN: return createBean();
			case JobDependencyTestMetaModelPackage.INTERFACE: return createInterface();
			case JobDependencyTestMetaModelPackage.PROVISION: return createProvision();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Bean createBean() {
		BeanImpl bean = new BeanImpl();
		return bean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface createInterface() {
		InterfaceImpl interface_ = new InterfaceImpl();
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Provision createProvision() {
		ProvisionImpl provision = new ProvisionImpl();
		return provision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JobDependencyTestMetaModelPackage getJobDependencyTestMetaModelPackage() {
		return (JobDependencyTestMetaModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static JobDependencyTestMetaModelPackage getPackage() {
		return JobDependencyTestMetaModelPackage.eINSTANCE;
	}

} //JobDependencyTestMetaModelFactoryImpl
