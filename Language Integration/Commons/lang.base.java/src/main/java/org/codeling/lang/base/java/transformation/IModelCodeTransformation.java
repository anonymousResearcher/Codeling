package org.codeling.lang.base.java.transformation;

import java.util.List;

import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;

public interface IModelCodeTransformation<ELEMENTECLASS extends EObject, JAVAELEMENTCLASS extends IJavaElement> {

	void setToCode();

	void setToModel();

	void setOnlyCrossReferences(boolean b);

	JAVAELEMENTCLASS resolveCodeElement() throws CodelingException;

	void createCodeFragments() throws CodelingException;

	void updateCodeFragments() throws CodelingException;

	void deleteCodeFragments() throws CodelingException;

	ELEMENTECLASS transformToModel() throws CodelingException;

	void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result);

	List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> createChildTransformationsToCode();

	List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> createChildTransformationsToModel();

	List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> createCrossReferencesTransformations();

	// Utility Methods
	/**
	 * Returns whether this transformation is able to translate the given meta model
	 * element to code.
	 */
	boolean canHandle(EModelElement metaModelElement);

	void setWhereToFindTranslatedElements(FindTranslatedElements findTranslatedElements);

	void setPriorIDRegistry(IDRegistry priorIDRegistry);

	IDRegistry getPriorIDRegistry();

	// Getters and Setters

	List<IJavaProject> getCodeRoot();

	void setCodeRoot(List<IJavaProject> codeRoot);

	ELEMENTECLASS getModelElement();

	void setModelElement(EObject modelElement);

	JAVAELEMENTCLASS getCodeElement();

	void setCodeElement(JAVAELEMENTCLASS codeElement);

	void setParentCodeElement(IJavaElement parent);

	IModelCodeTransformation<? extends EObject, ? extends IJavaElement> getParentTransformation();

	ELEMENTECLASS getPriorModelElement();

	void setPriorModelElement(EObject priorModelElement);

	IDRegistry getIdRegistry();

	void setIDRegistry(IDRegistry idRegistry);

	EObject getParentModelElement();

	void setParentModelElement(EObject parentModelElement);

}