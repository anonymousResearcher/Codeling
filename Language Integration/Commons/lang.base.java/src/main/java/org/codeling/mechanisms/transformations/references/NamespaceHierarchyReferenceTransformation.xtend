package org.codeling.mechanisms.transformations.references

import java.util.LinkedList
import java.util.List
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IPackageFragment

abstract class NamespaceHierarchyReferenceTransformation<OWNERECLASS extends EObject, TARGETECLASS extends EObject> extends ReferenceMechanismTransformation<OWNERECLASS, TARGETECLASS, IPackageFragment> {

	new(ClassMechanismTransformation<OWNERECLASS, IPackageFragment> parentTransformation,
		EReference eReference) {
		super(parentTransformation, eReference)
	}

	override createCodeFragments() throws CodelingException {
		// There is no code representation
	}

	override updateCodeFragments() throws CodelingException {
		// There is no code representation
	}

	override deleteCodeFragments() {
		// There is no code representation
	}

	override transformToModel() throws CodelingException {
		val List<String> targetTypeNames = new LinkedList();
		// Find all types or interfaces that represent a model object of the targeted type.
		codeElement.compilationUnits.forEach [ cu |
			cu.types.forEach [ t |
				val transformations = whereToFindTranslatedElements.getTransformationsFor(t);
				if (!transformations.nullOrEmpty && transformations.exists [ transformation |
					transformation.canHandle(eReference.EReferenceType)
				]) {
					targetTypeNames.add(t.elementName);
				}
			]
		];

		val List<TARGETECLASS> targetElements = getTargetObjects(modelElement, eReference, targetTypeNames);
		if (eReference.isMany) {
			(modelElement.eGet(eReference) as List<TARGETECLASS>).addAll(targetElements);
		} else {
			modelElement.eSet(eReference, targetElements.head);
		}

		return modelElement;
	}
}
