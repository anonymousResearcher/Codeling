package org.codeling.lang.base.java;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.codeling.utils.CodelingException;
import org.codeling.utils.CodelingLogger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IAnnotatable;
import org.eclipse.jdt.core.IAnnotation;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IMemberValuePair;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ChildPropertyDescriptor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MarkerAnnotation;
import org.eclipse.jdt.core.dom.MemberValuePair;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NormalAnnotation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleMemberAnnotation;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.TypeLiteral;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;
import org.eclipse.jdt.core.refactoring.IJavaRefactorings;
import org.eclipse.jdt.core.refactoring.descriptors.MoveDescriptor;
import org.eclipse.jdt.core.refactoring.descriptors.RenameJavaElementDescriptor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringContribution;
import org.eclipse.ltk.core.refactoring.RefactoringCore;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;

public class ASTUtils {
	private static final CodelingLogger log = new CodelingLogger(ASTUtils.class);

	/**
	 * Returns the value of a member value pair of an annotation. The result is
	 * casted to the given return type. Must not be used for class type members. Use
	 * getAnnotationMemberValueClassType(..) instead in that case.
	 *
	 * @param annotation
	 * @param memberKey
	 * @param returnType
	 * @return
	 * @throws JavaModelException
	 */
	public static <T> T getAnnotationMemberValue(IAnnotation annotation, String memberKey, Class<T> returnType)
			throws JavaModelException {
		if (Class.class.isAssignableFrom(returnType)) {
			throw new IllegalArgumentException(
					"Return Type 'Class' is not allowed for getAnnotationMemberValue(..). Use getAnnotationMemberValueClassType(..) instead.");
		}
		final IMemberValuePair[] mv = annotation.getMemberValuePairs();
		for (final IMemberValuePair iMemberValuePair : mv)
			if (iMemberValuePair.getMemberName().equals(memberKey)) {
				return returnType.cast(iMemberValuePair.getValue());
			}
		return null;
	}

	/**
	 * Returns the class typed value of a member value pair of an annotation as
	 * resolved fully qualified name string.
	 *
	 * @param annotation
	 * @param memberKey
	 * @return
	 * @throws JavaModelException
	 */
	public static String getAnnotationMemberValueClassType(IAnnotation annotation, String memberKey)
			throws JavaModelException {
		final IJavaElement parent = annotation.getParent();
		if (!(parent instanceof IType || parent instanceof IField || parent instanceof IMethod))
			throw new IllegalArgumentException(
					"Cannot only get annotation member values for annotations attached to types, fields, or methods.");

		final IMemberValuePair[] mv = annotation.getMemberValuePairs();
		for (final IMemberValuePair iMemberValuePair : mv)
			if (iMemberValuePair.getMemberName().equals(memberKey)) {
				final String memberValue = (String) iMemberValuePair.getValue();

				IJavaElement type = annotation.getParent();
				while (!(type instanceof IType))
					type = type.getParent();

				final String[][] resolvedNames = ((IType) type).resolveType(memberValue);
				final String resolvedName = resolvedNames[0][0] + "." + resolvedNames[0][1];
				if (resolvedNames.length > 1)
					log.warning(String.format(
							"The class name '%s' resolves to more than one possible type in the compilation unit '%s'. Using the first resolved class name: '%s'.",
							memberValue, ((IType) annotation.getParent()).getCompilationUnit().getPath(), resolvedName),
							null);
				return resolvedName;
			}
		return null;
	}

	public synchronized static void addAnnotationToMember(final IMember member, final Annotation originalAnnotation,
			final Annotation replacementAnnotation) {
		CompilationUnit astRoot = parse(member.getCompilationUnit(), null);
		final ASTRewrite rewriter = createRewriter(astRoot);
		if (originalAnnotation != null) {
			rewriter.replace(originalAnnotation, replacementAnnotation, null);
		} else {
			astRoot.accept(new ASTVisitor() {

				@Override
				public boolean visit(FieldDeclaration node) {
					if (((VariableDeclarationFragment) node.fragments().get(0)).getName().getIdentifier()
							.equals(member.getElementName()))
						rewriter.getListRewrite(node, node.getModifiersProperty()).insertAt(replacementAnnotation, 0,
								null);
					return false;
				}

				@Override
				public boolean visit(MethodDeclaration node) {
					if (node.getName().getIdentifier().equals(member.getElementName()))
						rewriter.getListRewrite(node, node.getModifiersProperty()).insertAt(replacementAnnotation, 0,
								null);
					return false;
				}
			});
		}

		// Do rewrite
		final ICompilationUnit cu = member.getCompilationUnit();

		try {
			final IDocument document = new Document(cu.getSource());
			doRewrite(document, rewriter, cu, null);
		} catch (IllegalArgumentException | MalformedTreeException | CoreException | BadLocationException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public synchronized static void addAnnotation(final IAnnotatable annotatable, final String annotationName,
			final Map<String, Object> memberValuePairs) throws JavaModelException {

		if (!(annotatable instanceof IMember))
			throw new IllegalArgumentException("member must be an IMember.");
		IMember member = ((IMember) annotatable);

		List<Annotation> originalAnnotation = new LinkedList<>();
		List<TypeDeclaration> td = new LinkedList<>();

		final CompilationUnit astRoot = parse(member.getCompilationUnit(), new NullProgressMonitor());
		// Create a new Annotation, or update it.
		final IAnnotation iAnnotation = annotatable.getAnnotation(annotationName);

		if (iAnnotation.exists()) {
			// Find the AST representation of this annotation
			astRoot.accept(new ASTVisitor() {
				@Override
				public boolean visit(final TypeDeclaration tdNode) {
					tdNode.accept(new ASTVisitor() {
						@Override
						public boolean visit(NormalAnnotation node) {
							if (node.getTypeName().getFullyQualifiedName().equals(annotationName)) {
								originalAnnotation.add(node);
								td.add(tdNode);
							}
							return false;
						}

						@Override
						public boolean visit(SingleMemberAnnotation node) {
							if (node.getTypeName().getFullyQualifiedName().equals(annotationName)) {
								originalAnnotation.add(node);
								td.add(tdNode);
							}
							return false;
						}

						@Override
						public boolean visit(MarkerAnnotation node) {
							if (node.getTypeName().getFullyQualifiedName().equals(annotationName)) {
								originalAnnotation.add(node);
								td.add(tdNode);
							}
							return false;
						}
					});
					return false;
				}
			});
		}

		Annotation replacementAnnotation = null;
		if (memberValuePairs == null) {
			replacementAnnotation = astRoot.getAST().newMarkerAnnotation();
		} else {
			replacementAnnotation = astRoot.getAST().newNormalAnnotation();
			for (final Entry<String, Object> entry : memberValuePairs.entrySet()) {
				((NormalAnnotation) replacementAnnotation).values()
						.add(createAnnotationMember(astRoot.getAST(), entry.getKey(), entry.getValue()));
			}
		}
		final Name newName = astRoot.getAST().newName(annotationName);
		replacementAnnotation.setTypeName(newName);

		if (annotatable instanceof IMember) {
			Annotation o = originalAnnotation.size() > 0 ? originalAnnotation.get(0) : null;
			addAnnotationToMember((IMember) annotatable, o, replacementAnnotation);
		} else {
			throw new IllegalArgumentException("annotatable must be an IMember");
		}
	}

	public synchronized static void deleteAnnotationFromType(final IType type, final String annotationName,
			final Map<String, Object> memberValuePairs)
			throws MalformedTreeException, CoreException, BadLocationException {

		final LinkedList<Annotation> originalAnnotation = new LinkedList<>();
		final LinkedList<TypeDeclaration> td = new LinkedList<>();

		final CompilationUnit astRoot = parse(type.getCompilationUnit(), null);
		final ASTRewrite rewriter = createRewriter(astRoot);

		// Create a new Annotation, or update it.
		final IAnnotation iAnnotation = type.getAnnotation(annotationName);
		if (iAnnotation.exists()) {
			// Find the AST representation of this annotation
			astRoot.accept(new ASTVisitor() {
				@Override
				public boolean visit(final TypeDeclaration tdNode) {
					tdNode.accept(new ASTVisitor() {
						@Override
						public boolean visit(MarkerAnnotation node) {
							return identifyAnnotation(node);
						}

						@Override
						public boolean visit(NormalAnnotation node) {

							return identifyAnnotation(node);
						}

						@Override
						public boolean visit(SingleMemberAnnotation node) {
							return identifyAnnotation(node);
						}

						private boolean identifyAnnotation(Annotation node) {
							if (node.getTypeName().getFullyQualifiedName().equals(annotationName)) {
								originalAnnotation.add(node);
								td.add(tdNode);
							}
							return false;
						}
					});
					return false;
				}
			});
		}

		if (!originalAnnotation.isEmpty())
			rewriter.replace(originalAnnotation.getFirst(), null, null);

		// Do rewrite
		final Document doc = new Document(type.getCompilationUnit().getSource());
		doRewrite(doc, rewriter, type.getCompilationUnit(), null);
	}

	public synchronized static MemberValuePair createAnnotationMember(final AST ast, final String name,
			final Object value) {
		final MemberValuePair mV = ast.newMemberValuePair();
		mV.setName(ast.newSimpleName(name));

		Expression exp = null;
		if (value instanceof String) {
			final StringLiteral strLiteral = ast.newStringLiteral();
			strLiteral.setLiteralValue((String) value);
			exp = strLiteral;
		} else if (value instanceof Boolean) {
			exp = ast.newBooleanLiteral((Boolean) value);
		} else if (value instanceof Character) {
			final CharacterLiteral lit = ast.newCharacterLiteral();
			lit.setCharValue((Character) value);
			exp = lit;
		} else if (value instanceof Type) {
			final TypeLiteral lit = ast.newTypeLiteral();
			lit.setType((Type) value);
		}
		mV.setValue(exp);

		return mV;
	}

	/**
	 * For a member value pair with a type of Class, this method replaces the value.
	 */
	public static void replaceMemberValueTypeLiteral(IMember element, String annotationName, String memberName,
			String newValueTypeName) throws JavaModelException {
		if (!(element instanceof IAnnotatable))
			throw new IllegalArgumentException(
					"The element " + element.getElementName() + " must implement IAnnotatable.");

		ICompilationUnit icu = element.getCompilationUnit();
		CompilationUnit astRoot = parse(icu, null);

		ASTRewrite rewriter = createRewriter(astRoot);

		astRoot.accept(new ASTVisitor() {
			public boolean visit(NormalAnnotation annotation) {
				try {
					ChildPropertyDescriptor nameProperty = getNameProperty(annotation.getParent());
					if (!element.getElementName().equals(
							((SimpleName) annotation.getParent().getStructuralProperty(nameProperty)).getIdentifier()))
						return false;

					annotation.accept(new ASTVisitor() {
						public boolean visit(MemberValuePair mvp) {
							if (mvp.getName().getIdentifier().equals(memberName)) {
								AST ast = mvp.getAST();
								TypeLiteral typeLiteral = ast.newTypeLiteral();
								typeLiteral.setType(ast.newSimpleType(ast.newSimpleName(newValueTypeName)));
								rewriter.replace(mvp.getValue(), typeLiteral, null);
							}
							return false;
						}
					});
				} catch (Throwable e) {
					e.printStackTrace();
				}
				return false;
			}
		});

		// apply the text edits to the compilation unit
		try {
			doRewrite(rewriter, icu, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method adds a new member value pair with a type literal as target
	 */
	public static void addMemberValueTypeLiteral(IMember element, String annotationName, String memberName,
			String newValueTypeName) throws JavaModelException {
		if (!(element instanceof IAnnotatable))
			throw new IllegalArgumentException(
					"The element " + element.getElementName() + " must implement IAnnotatable.");

		ICompilationUnit icu = element.getCompilationUnit();
		CompilationUnit astRoot = parse(icu, null);

		ASTRewrite rewriter = createRewriter(astRoot);

		astRoot.accept(new ASTVisitor() {
			public boolean visit(MarkerAnnotation annotation) {
				// Replace the marker annotation with a normal annotation.
				replaceWithNormalAnnotation(annotation);
				return false;
			}

			public boolean visit(NormalAnnotation annotation) {
				MemberValuePair mvp = createMemberValuePair(annotation);
				rewriter.set(annotation, NormalAnnotation.VALUES_PROPERTY, mvp, null);
				return false;
			}

			public boolean visit(SingleMemberAnnotation annotation) {
				replaceWithNormalAnnotation(annotation);
				return false;
			}

			@SuppressWarnings("unchecked")
			private void replaceWithNormalAnnotation(Annotation annotation) {
				// Replace the marker annotation with a normal annotation.
				AST ast = annotation.getAST();
				NormalAnnotation a = ast.newNormalAnnotation();
				a.setTypeName(ast.newSimpleName(annotationName));
				MemberValuePair mvp = createMemberValuePair(a);
				a.values().add(mvp);
				rewriter.replace(annotation, a, null);
			}

			private MemberValuePair createMemberValuePair(Annotation annotation) {
				AST ast = annotation.getAST();
				MemberValuePair mvp = ast.newMemberValuePair();
				mvp.setName(ast.newSimpleName(memberName));
				TypeLiteral typeLiteral = ast.newTypeLiteral();
				typeLiteral.setType(ast.newSimpleType(ast.newSimpleName(newValueTypeName)));
				mvp.setValue(typeLiteral);
				return mvp;
			}
		});

		// apply the text edits to the compilation unit
		try {
			doRewrite(rewriter, icu, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static ChildPropertyDescriptor getNameProperty(ASTNode node) throws Throwable {
		@SuppressWarnings("unchecked")
		Optional<ChildPropertyDescriptor> result = node.structuralPropertiesForType().stream()
				.filter(p -> ((StructuralPropertyDescriptor) p).getId().equals("name")).findFirst();
		if (result.isPresent())
			return result.get();
		else
			throw new IllegalArgumentException("Could not retreive name property of node " + node);
	}

	public static String getValue(final IField field) throws JavaModelException {
		CompilationUnit cu = parse(field.getCompilationUnit(), null);
		final LinkedList<String> value = new LinkedList<>();
		cu.accept(new ASTVisitor() {
			@Override
			public boolean visit(FieldDeclaration node) {
				VariableDeclarationFragment declaration = (VariableDeclarationFragment) node.fragments().get(0);
				if (field.getElementName().equals(declaration.getName().toString())) {
					if (declaration.getInitializer() != null) {
						value.add(declaration.getInitializer().toString());
						return false;
					}
				}
				return false;
			}
		});
		if (value.isEmpty())
			return null;
		return value.get(0);

	}

	private static CompilationUnit parse(ICompilationUnit cu, IProgressMonitor monitor) {
		final ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setResolveBindings(true);
		parser.setSource(cu);
		return (CompilationUnit) parser.createAST(monitor);
	}

	private synchronized static ASTRewrite createRewriter(CompilationUnit astRoot) {
		final AST ast = astRoot.getAST();
		return ASTRewrite.create(ast);
	}

	private synchronized static void doRewrite(IDocument document, ASTRewrite rewriter, ICompilationUnit cu,
			IProgressMonitor monitor)
			throws IllegalArgumentException, CoreException, MalformedTreeException, BadLocationException {
		final TextEdit edit = rewriter.rewriteAST();
		edit.apply(document);
		cu.getBuffer().setContents(document.get());
		cu.save(monitor, true);
	}

	private synchronized static void doRewrite(ASTRewrite rewriter, ICompilationUnit cu, IProgressMonitor monitor)
			throws IllegalArgumentException, CoreException, MalformedTreeException, BadLocationException {
		final Document document = new Document(cu.getSource());
		final TextEdit edit = rewriter.rewriteAST(document, null);
		edit.apply(document);
		cu.getBuffer().setContents(document.get());
		cu.save(monitor, true);
	}

	public static <T> void createOrUpdateAnnotationMemberValue(Class<?> annotationClass, IAnnotatable annotatable,
			String annotationMemberName, Class<T> annotationMemberType, T newValue) throws JavaModelException {
		// Try to get the annotation with its simple name
		String annotationName = annotationClass.getSimpleName();
		IAnnotation annotation = annotatable.getAnnotation(annotationName);
		if (!annotation.exists()) {
			// Try to get the annotation with the canonical name
			annotationName = annotationClass.getCanonicalName();
			annotation = annotatable.getAnnotation(annotationName);

			if (!annotation.exists()) {
				// The annotation does not exist
				throw new IllegalStateException(String.format(
						"Could not create annotation member value: Type: '%s', Annotation: '%s', Annotation Member: '%s', New Value: '%s'. The annotation has to be created separately before.",
						((IJavaElement) annotatable).getElementName(), annotation.getElementName(),
						annotationMemberName, newValue));
			}
		}

		final T version = ASTUtils.getAnnotationMemberValue(annotation, annotationMemberName, annotationMemberType);
		if (!version.equals(newValue)) {
			final Map<String, Object> memberValuePairs = new HashMap<>();
			memberValuePairs.put(annotationMemberName, newValue);
			try {
				ASTUtils.addAnnotation(annotatable, annotationName, memberValuePairs);
			} catch (MalformedTreeException | CoreException e) {
				// TODO Auto-generated catch block
				throw new IllegalStateException(String.format("Could not add annotation '%s' to java element '%s'.",
						annotationName, annotatable), e);
			}
		}
	}

	public static void deleteAnnotationFromType(IAnnotation a, IType type) throws JavaModelException {
		final IDocument document = new Document(type.getCompilationUnit().getSource());

		// ... edit the document here ...
		final String annotationName = a.getElementName();
		final CompilationUnit astRoot = parse(type.getCompilationUnit(), new NullProgressMonitor());

		// Remove an Annotation
		final IAnnotation iAnnotation = type.getAnnotation(annotationName);
		final ASTRewrite rewriter = createRewriter(astRoot);
		if (iAnnotation.exists()) {
			// Find the AST representation of this annotation
			astRoot.accept(new ASTVisitor() {
				@Override
				public boolean visit(final TypeDeclaration tdNode) {
					tdNode.accept(new ASTVisitor() {
						@Override
						public boolean visit(MarkerAnnotation node) {
							return doRemove(node);
						}

						@Override
						public boolean visit(NormalAnnotation node) {
							return doRemove(node);
						}

						@Override
						public boolean visit(SingleMemberAnnotation node) {
							return doRemove(node);
						}

						private boolean doRemove(Annotation node) {
							if (node.getTypeName().getFullyQualifiedName().equals(annotationName)) {
								rewriter.remove(node, null);
							}
							return false;
						}
					});
					return false;
				}
			});
		}

		// Write the new content to the file
		try {
			doRewrite(document, rewriter, type.getCompilationUnit(), new NullProgressMonitor());
		} catch (IllegalArgumentException | CoreException | MalformedTreeException | BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static final Map<String, Class<?>> IFieldTypes2JavaTypes = new HashMap<String, Class<?>>();

	static {
		IFieldTypes2JavaTypes.put("I", Integer.class);
		IFieldTypes2JavaTypes.put("J", Long.class);
		IFieldTypes2JavaTypes.put("D", Double.class);
		IFieldTypes2JavaTypes.put("F", Float.class);
		IFieldTypes2JavaTypes.put("Z", Boolean.class);
		IFieldTypes2JavaTypes.put("C", Character.class);
		IFieldTypes2JavaTypes.put("B", Byte.class);
		IFieldTypes2JavaTypes.put("V", Void.class);
		IFieldTypes2JavaTypes.put("S", Short.class);
		IFieldTypes2JavaTypes.put("QString;", String.class);
	}

	public static final Class<?> getTypeByJavaSignature(final String primitiveTypeSignature) {
		return IFieldTypes2JavaTypes.get(primitiveTypeSignature);
	}

	public static void addSuperInterface(final IType type, String interfaceName) throws JavaModelException {
		final Document doc = new Document(type.getCompilationUnit().getSource());
		final ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(doc.get().toCharArray());
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		final AST ast = cu.getAST();
		cu.recordModifications();
		final TypeDeclaration typeDeclaration = (TypeDeclaration) cu.types().get(0);
		final ASTRewrite rewriter = ASTRewrite.create(ast);
		ListRewrite listRewriter;
		listRewriter = rewriter.getListRewrite(typeDeclaration, TypeDeclaration.SUPER_INTERFACE_TYPES_PROPERTY);
		listRewriter.insertLast(ast.newSimpleName(interfaceName), null);
		final TextEdit edits = rewriter.rewriteAST(doc, null);
		try {
			edits.apply(doc);
		} catch (final MalformedTreeException e) {
			e.printStackTrace();
		} catch (final BadLocationException e) {
			e.printStackTrace();
		}
		try {
			type.getCompilationUnit().getBuffer().setContents(doc.get());
		} catch (final JavaModelException e) {
			e.printStackTrace();
		}
	}

	/** Uses refactoring to rename a type **/
	public static void renameType(IType type, String newName) {
		final RefactoringContribution contribution = RefactoringCore
				.getRefactoringContribution(IJavaRefactorings.RENAME_TYPE);
		final RenameJavaElementDescriptor descriptor = (RenameJavaElementDescriptor) contribution.createDescriptor();
		descriptor.setProject(type.getResource().getProject().getName());
		descriptor.setJavaElement(type);
		descriptor.setNewName(newName);

		final RefactoringStatus status = new RefactoringStatus();
		try {
			final Refactoring refactoring = descriptor.createRefactoring(status);
			final IProgressMonitor monitor = new NullProgressMonitor();
			refactoring.checkInitialConditions(monitor);
			refactoring.checkFinalConditions(monitor);
			final Change change = refactoring.createChange(monitor);
			change.perform(monitor);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public static IType getFieldType(IField field) throws JavaModelException {
		final IType parentType = (IType) field.getParent();

		String typeSignature = resolveTypeSignature(field.getTypeSignature(), field.getDeclaringType());

		String[][] resolvedType = parentType.resolveType(typeSignature);
		if (resolvedType == null)
			throw new IllegalArgumentException(MessageFormat.format(
					"Could not resolve the type {0} type in the field source [{1}]", typeSignature, field.getSource()));

		final String typeFQN = resolvedType[0][0] + "." + resolvedType[0][1];
		return parentType.getJavaProject().findType(typeFQN);
	}

	/**
	 * Returns the "core" type of the signature. For a primitive type the fully
	 * qualified name (fqn) of the corresponding java.lang.* class will be returned.
	 * For an array the fqn of the element type will be returned (for arbitrarily
	 * nested arrays). For a type with type arguments, the first type argument's fqn
	 * will be returned. For a non-primitive type, the corresponding fqn will be
	 * returned.
	 * 
	 * @param signature
	 * @param baseType
	 *            a type as basis for resolving a FQN
	 * @return
	 * @throws JavaModelException
	 */
	public static String resolveTypeSignature(String signature, IType baseType) throws JavaModelException {
		if (Signature.getTypeSignatureKind(signature) == Signature.BASE_TYPE_SIGNATURE) {
			// A primitive type. Find the non-primitive equivalent
			signature = getTypeByJavaSignature(signature).getName();
		} else {
			// A non-primitive type
			// If the type is a collection or array, return what is inside
			String[] typeArgument = Signature.getTypeArguments(signature);
			String elementType = Signature.getElementType(signature);
			if (typeArgument.length != 0) {
				signature = Signature.toString(typeArgument[0]);
			} else if (elementType != signature) {
				signature = resolveTypeSignature(elementType, baseType);
			} else {
				signature = Signature.toString(signature);
			}
		}
		return getType(signature, baseType).getFullyQualifiedName();
	}

	public static IType getType(String name, IType anotherType) throws JavaModelException {
		final String[][] resolvedType = anotherType.resolveType(name);
		if (resolvedType == null)
			throw new IllegalArgumentException(MessageFormat.format("Could not resolve the type {0} based on type {1}.",
					name, anotherType.getElementName()));

		final String typeFQN = resolvedType[0][0] + "." + resolvedType[0][1];
		return anotherType.getJavaProject().findType(typeFQN);
	}

	public static List<IType> getType(final String typeName, List<IJavaProject> projects) throws JavaModelException {
		return getTypes(projects).stream().filter(t -> t.getElementName().equals(typeName))
				.collect(Collectors.toList());
	}

	public static boolean implementsInterface(IType type, String interfaceName) throws JavaModelException {
		String[][] resolvedExpectedInterfaceName = type.resolveType(interfaceName);

		String[] superInterfaceNames = type.getSuperInterfaceNames();
		for (String implementedInterfaceName : superInterfaceNames) {
			String[][] resolvedImplementedInterfaceName = type.resolveType(implementedInterfaceName);
			if (Arrays.deepEquals(resolvedExpectedInterfaceName, resolvedImplementedInterfaceName))
				return true;
		}
		return false;
	}

	public static List<IType> getTypes(List<IJavaProject> projects) throws JavaModelException {
		LinkedList<IType> result = new LinkedList<>();
		for (IJavaProject project : projects)
			for (IPackageFragmentRoot r : project.getPackageFragmentRoots())
				if (r.getKind() == IPackageFragmentRoot.K_SOURCE)
					for (IJavaElement pf : r.getChildren())
						result.addAll(getTypes((IPackageFragment) pf));
		return result;
	}

	public static List<IType> getTypes(IPackageFragment fragment) throws JavaModelException {
		LinkedList<IType> result = new LinkedList<>();
		for (ICompilationUnit cu : fragment.getCompilationUnits())
			for (IType t : cu.getAllTypes())
				result.add(t);
		return result;
	}

	/**
	 * Returns whether the java element has the given annotation. The annotatable
	 * must be an instance of {@link IAnnotatable}. The annotations must be given as
	 * FQN.
	 */
	public static boolean hasAnnotation(IMember member, String... annotations) {
		if (member == null)
			throw new IllegalArgumentException("member must not be null");
		if (!(member instanceof IAnnotatable))
			throw new IllegalArgumentException("member must be an IAnnotatable");
		IAnnotatable annotatable = (IAnnotatable) member;
		if (annotations == null)
			throw new IllegalArgumentException("annotations must not be null");

		for (String fqn : annotations) {
			int lastDotIndex = fqn.lastIndexOf(".");
			if (lastDotIndex == -1)
				throw new IllegalArgumentException("All annotations must be declared with their fully qualfied name");
			String typename = fqn.substring(lastDotIndex + 1);
			String packageName = fqn.substring(0, lastDotIndex);
			String genericImport = packageName + ".*";
			if (annotatable.getAnnotation(fqn).exists() || annotatable.getAnnotation(typename).exists()
					&& (member.getCompilationUnit().getImport(fqn).exists()
							|| member.getCompilationUnit().getImport(genericImport).exists()))
				return true;
		}
		return false;
	}

	public static List<IPackageFragment> getSourcePackageFragments(List<IJavaProject> projects)
			throws JavaModelException {
		LinkedList<IPackageFragment> result = new LinkedList<>();
		for (IPackageFragmentRoot r : getSourcePackageFragmentRoots(projects))
			result.addAll(getSourcePackageFragments(r));
		return result;
	}

	public static List<IPackageFragmentRoot> getSourcePackageFragmentRoots(List<IJavaProject> projects)
			throws JavaModelException {
		LinkedList<IPackageFragmentRoot> result = new LinkedList<>();
		for (IJavaProject project : projects)
			for (IPackageFragmentRoot r : project.getPackageFragmentRoots())
				if ((r.getKind() & IPackageFragmentRoot.K_SOURCE) != 0)
					result.add(r);
		return result;
	}

	public static List<IPackageFragment> getSourcePackageFragments(IPackageFragmentRoot root)
			throws JavaModelException {
		LinkedList<IPackageFragment> result = new LinkedList<>();
		for (IJavaElement pf : root.getChildren())
			if (pf instanceof IPackageFragment)
				result.add((IPackageFragment) pf);
		return result;
	}

	public static List<IPackageFragment> getDirectChildPackageFragmentsOf(IPackageFragment parent)
			throws JavaModelException {
		LinkedList<IPackageFragment> result = new LinkedList<>();
		String[] parentSegments = parent.getElementName().split("\\.");
		List<IPackageFragment> allFragments = getSourcePackageFragments(Arrays.asList(parent.getJavaProject()));
		frag: for (IPackageFragment pf : allFragments) {
			String[] pfSegments = pf.getElementName().split("\\.");
			if (pfSegments.length != parentSegments.length + 1) // Must be a direct child
				continue;
			for (int i = 0; i < parentSegments.length; i++)
				if (!parentSegments[i].equals(pfSegments[i]))
					continue frag; // Differences in the base segments
			result.add(pf);
		}
		return result;
	}

	/**
	 * Returns a list of packages in the given package fragment root that have only
	 * one segment. E.g. the packages "a" and "b" would be returned, but not the
	 * packages "a.d", "a.d.e", or "b.f".
	 */
	public static List<IPackageFragment> getDirectChildPackageFragmentsOf(IPackageFragmentRoot parent)
			throws JavaModelException {
		LinkedList<IPackageFragment> result = new LinkedList<>();
		List<IPackageFragment> allFragments = getSourcePackageFragments(parent);
		for (IPackageFragment pf : allFragments) {
			String[] pfSegments = pf.getElementName().split("\\.");
			if (pfSegments.length == 1)
				result.add(pf);
		}
		return result;
	}

	public static LinkedList<IPackageFragment> getDistinctDirectChildPackageFragmentsOfAll(List<IJavaProject> projects)
			throws JavaModelException {
		LinkedList<IPackageFragment> result = new LinkedList<>();
		for (IJavaProject project : projects)
			for (IPackageFragmentRoot root : project.getPackageFragmentRoots())
				if (root.getKind() == IPackageFragmentRoot.K_SOURCE)
					for (IPackageFragment pf : getDirectChildPackageFragmentsOf(root))
						if (!result.stream().anyMatch(p -> p.getElementName().equals(pf.getElementName())))
							result.add(pf);
		return result;
	}

	public static LinkedList<IPackageFragment> getAllDistinctChildPackageFragmentsOfAll(List<IJavaProject> projects)
			throws JavaModelException {
		LinkedList<IPackageFragment> result = new LinkedList<>();
		for (IJavaProject project : projects)
			for (IPackageFragmentRoot root : project.getPackageFragmentRoots())
				if (root.getKind() == IPackageFragmentRoot.K_SOURCE)
					for (IPackageFragment pf : getSourcePackageFragments(root))
						if (!result.stream().anyMatch(p -> p.getElementName().equals(pf.getElementName())))
							result.add(pf);
		return result;
	}

	/**
	 * Returns whether the given type signature is an array or collection. Needs a
	 * base type for resolving types.
	 */
	public static boolean isArrayOrCollection(String signature, IType baseType) throws CodelingException {
		boolean isArray = Signature.getTypeSignatureKind(signature) == Signature.ARRAY_TYPE_SIGNATURE;
		if (isArray) // Shortcut to avoid unnecessary newSupertypeHierarchy below.
			return true;

		boolean isCollection = false;
		try {
			// If it is a primitive or void it is not a collection
			if (Signature.getTypeSignatureKind(signature) != Signature.BASE_TYPE_SIGNATURE) {
				IType fieldType = ASTUtils.getType(Signature.toString(Signature.getTypeErasure(signature)), baseType);
				for (IType t : fieldType.newSupertypeHierarchy(null).getAllSupertypes(fieldType)) {
					if (t.getFullyQualifiedName().equals("java.util.Collection")) {
						isCollection = true;
						break;
					}
				}
			}
		} catch (JavaModelException | IllegalArgumentException e) {
			throw new CodelingException("Could not get a type hierarchy.", e);
		}
		return isCollection;
	}

	/**
	 * Searches for a package fragment with the given fqn in the given projects, in
	 * the order given by the list 'projects'. Returns the first finding.
	 * 
	 * When no package fragment with that name exists in the projects, it returns
	 * null.
	 * 
	 * @throws JavaModelException
	 */
	public static IPackageFragment getPackageFragment(String packageFQN, List<IJavaProject> projects)
			throws NoSuchElementException, JavaModelException {
		return getSourcePackageFragments(projects).stream().filter(p -> p.getElementName().equals(packageFQN))
				.findFirst().orElse(null);
	}

	/**
	 * Moves a type to the given project. The package stays the same.
	 * 
	 * @throws JavaModelException
	 * @throws NoSuchElementException
	 */
	public static void moveType(final IType type, final IJavaProject project)
			throws NoSuchElementException, JavaModelException {
		final String packageFragmentName = type.getPackageFragment().getElementName();
		IPackageFragment packageFragment = getPackageFragment(packageFragmentName, Arrays.asList(project));
		if (packageFragment == null)
			packageFragment = createPackageFragment(packageFragmentName, project);

		final RefactoringContribution contribution = RefactoringCore.getRefactoringContribution(IJavaRefactorings.MOVE);
		final MoveDescriptor descriptor = (MoveDescriptor) contribution.createDescriptor();
		descriptor.setDestination(packageFragment);
		descriptor.setMoveResources(new IFile[0], new IFolder[0], new ICompilationUnit[] { type.getCompilationUnit() });

		final RefactoringStatus status = new RefactoringStatus();
		try {
			final Refactoring refactoring = descriptor.createRefactoring(status);
			final IProgressMonitor monitor = new NullProgressMonitor();
			refactoring.checkInitialConditions(monitor);
			refactoring.checkFinalConditions(monitor);
			final Change change = refactoring.createChange(monitor);
			change.perform(monitor);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Recursively creates a package fragment in the first found package fragment
	 * root of kind {@link IPackageFragmentRoot}.SOURCE;
	 * 
	 * @throws JavaModelException
	 */
	public static IPackageFragment createPackageFragment(String packageFragmentName, IJavaProject project)
			throws JavaModelException {
		return getSourcePackageFragmentRoots(Arrays.asList(project)).get(0).createPackageFragment(packageFragmentName,
				true, null);
	}
}
