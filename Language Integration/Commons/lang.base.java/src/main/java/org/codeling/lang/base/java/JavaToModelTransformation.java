package org.codeling.lang.base.java;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.mic.ModelIntegrationConceptTransformation;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.IDRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;

public class JavaToModelTransformation extends ModelIntegrationConceptTransformation {

	protected IProgressMonitor monitor = new NullProgressMonitor();
	final static private String taskName = "Java Code to Model";
	private final AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> rootTransformation;
	private List<EClass> crossReferenceOrder;

	public <E extends EObject, J extends IJavaElement> JavaToModelTransformation(List<IJavaProject> projects,
			IDRegistry idRegistry,
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> rootTransformation,
			List<EClass> crossReferenceOrder) {
		super(projects.stream().map(p -> p.getProject().getLocation().toFile().toPath()).collect(Collectors.toList()),
				idRegistry, taskName, null);
		this.rootTransformation = rootTransformation;
		this.crossReferenceOrder = crossReferenceOrder;
	}

	@Override
	protected void doExecute(IProgressMonitor monitor) {
		rootTransformation.setToModel();

		// Translate all classes, their attributes, and the containment
		// references
		ForkJoinPool.commonPool().invoke(rootTransformation);

		// Translate non-containment references
		if (crossReferenceOrder == null)
			crossReferenceOrder = new LinkedList<>();
		for (final EClass eclass : crossReferenceOrder) {
			rootTransformation.getWhereToFindTranslatedElements().getTransformationsFor(eclass).forEach(t -> {
				t.setOnlyCrossReferences(true);
				t.reinitialize();
				ForkJoinPool.commonPool().invoke(t);
			});
		}

		// Register all new model elements
		rootTransformation.getWhereToFindTranslatedElements().getAllTransformations().forEach(t -> {
			// Register code and model element at the ID registry
			if (!idRegistry.knowsImplementationModelElement(t.getModelElement()))
				// Id does not exist yet.
				idRegistry.generateID(t.getCodeElement(), t.getModelElement());
		});

		transformationResult = new TransformationResult(Arrays.asList(rootTransformation.getModelElement()),
				idRegistry);
	}

}