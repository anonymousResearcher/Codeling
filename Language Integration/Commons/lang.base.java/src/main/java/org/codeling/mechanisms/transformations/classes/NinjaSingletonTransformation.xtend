package org.codeling.mechanisms.transformations.classes

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.jdt.core.IJavaElement

abstract class NinjaSingletonTransformation<ELEMENTECLASS extends EObject> extends ClassMechanismTransformation<ELEMENTECLASS, IJavaElement> {

	new(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation,
		EClass eClass) {
		super(parentTransformation, eClass)
	}

	override createCodeFragments() throws CodelingException {
		// There is no code representation
	}

	override updateCodeFragments() throws CodelingException {
		// There is no code representation
	}

	override deleteCodeFragments() {
		// There is no code representation
	}

	override transformToModel() throws CodelingException {
		// Create element
		modelElement = eClass.EPackage.EFactoryInstance.create(eClass);
		return modelElement;
	}

}
