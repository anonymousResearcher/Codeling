package org.codeling.lang.base.java;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.modelversioning.emfprofile.Extension;
import org.modelversioning.emfprofile.IProfileFacade;
import org.modelversioning.emfprofile.Profile;
import org.modelversioning.emfprofile.Stereotype;
import org.modelversioning.emfprofile.impl.ProfileFacadeImpl;
import org.modelversioning.emfprofileapplication.ProfileApplication;
import org.modelversioning.emfprofileapplication.StereotypeApplication;

public class ProfilesUtils {
	private static final String STEREOTYPE_NOT_APPLICABLE = "Stereotype '%s' is not applicable to the object of eClass '%s'.";
	private static final String STEREOTYPE_NOT_FOUND = "Could not find stereotype with the name '%s'.";
	private static final String ESTRUCTURAL_FEATURE_NOT_AVAILABLE = "The stereotype '%s' has no EStructuralFeature '%s'.";
	private static final String ESTRUCTURAL_FEATURE_NO_EREFERENCE = "The EStructuralFeature '%s.%s' is not an EReference.";
	private static final String ESTRUCTURAL_FEATURE_NO_EATTRIBUTE = "The EStructuralFeature '%s.%s' is not an EAttribute.";

	public static Stereotype getStereotype(String stereotypeName, Profile profile) {
		return profile.getStereotype(stereotypeName);
	}

	public static EReference getEReference(Stereotype stereotype, String referenceName) {
		EStructuralFeature feature = stereotype.getEStructuralFeature(referenceName);
		if (feature == null)
			throw new IllegalArgumentException(
					String.format(ESTRUCTURAL_FEATURE_NOT_AVAILABLE, stereotype.getName(), referenceName));
		if (!(feature instanceof EReference))
			throw new IllegalArgumentException(
					String.format(ESTRUCTURAL_FEATURE_NO_EREFERENCE, stereotype.getName(), referenceName));
		return (EReference) feature;
	}

	public static EAttribute getEAttribute(Stereotype stereotype, String attributeName) {
		EStructuralFeature feature = stereotype.getEStructuralFeature(attributeName);
		if (feature == null)
			throw new IllegalArgumentException(
					String.format(ESTRUCTURAL_FEATURE_NOT_AVAILABLE, stereotype.getName(), attributeName));
		if (!(feature instanceof EAttribute))
			throw new IllegalArgumentException(
					String.format(ESTRUCTURAL_FEATURE_NO_EATTRIBUTE, stereotype.getName(), attributeName));
		return (EAttribute) feature;
	}

	public static EReference getEReference(Profile profile, String stereotypeName, String referenceName) {
		Stereotype stereotype = getStereotype(stereotypeName, profile);
		return getEReference(stereotype, referenceName);
	}

	public static EAttribute getEAttribute(Profile profile, String stereotypeName, String attributeName) {
		Stereotype stereotype = getStereotype(stereotypeName, profile);
		return getEAttribute(stereotype, attributeName);
	}

	/**
	 * Creates a new IProfileFacade instance.
	 * 
	 * @throws IOException
	 *             when the profile application resource cannot be set.
	 */
	public static IProfileFacade createProfileFacadeInstance(Profile profile, Resource profileApplicationResource)
			throws IOException {
		IProfileFacade profileFacade = new ProfileFacadeImpl();
		profileFacade.loadProfile(profile);
		profileFacade.setProfileApplicationResource(profileApplicationResource);
		return profileFacade;
	}

	public static Extension getDefaultExtension(Stereotype stereotype, EObject eObject,
			Resource profileApplicationResource) {
		EList<Extension> applicableExtensions = getApplicableExtensions(stereotype, eObject);
		if (applicableExtensions.size() > 0) {
			return applicableExtensions.get(0);
		} else {
			throw new IllegalArgumentException(
					String.format(STEREOTYPE_NOT_APPLICABLE, stereotype.getName(), eObject.eClass()));
		}
	}

	private static EList<Extension> getApplicableExtensions(Stereotype stereotype, EObject eObject) {
		return stereotype.getApplicableExtensions(eObject, extractAppliedExtensions(getAppliedStereotypes(eObject)));
	}

	public static EList<StereotypeApplication> getAppliedStereotypes(EObject eObject) {
		EList<StereotypeApplication> stereotypeApplications = new BasicEList<StereotypeApplication>();

		// There might be stereotype applications within profile applications
		for (ProfileApplication profileApplication : getProfileApplications(eObject.eResource())) {
			stereotypeApplications.addAll(profileApplication.getStereotypeApplications(eObject));
		}

		// There might be stereotype applications within the resource directly
		for (EObject stereotypeApplication : eObject.eResource().getContents()) {
			if (stereotypeApplication instanceof StereotypeApplication) {
				if (((StereotypeApplication) stereotypeApplication).getAppliedTo() == eObject)
					if (!stereotypeApplications.contains(stereotypeApplication))
						stereotypeApplications.add((StereotypeApplication) stereotypeApplication);
			}
		}
		return ECollections.unmodifiableEList(stereotypeApplications);
	}

	/**
	 * Returns a list of {@link ProfileApplication ProfileApplications} contained by
	 * the specified <code>resource</code>.
	 * 
	 * @param resource
	 *            the resource that might contain {@link ProfileApplication}s.
	 * @return the list of {@link ProfileApplication ProfileApplications}.
	 */
	private static EList<ProfileApplication> getProfileApplications(Resource resource) {
		EList<ProfileApplication> profileApplications = new BasicEList<ProfileApplication>();
		for (EObject eObject : resource.getContents())
			if (eObject instanceof ProfileApplication)
				profileApplications.add((ProfileApplication) eObject);
		return profileApplications;
	}

	private static EList<Extension> extractAppliedExtensions(EList<StereotypeApplication> appliedStereotypes) {
		EList<Extension> appliedExtensions = new BasicEList<Extension>();
		for (StereotypeApplication stereotypeApplication : appliedStereotypes)
			appliedExtensions.add(stereotypeApplication.getExtension());
		return appliedExtensions;
	}

	@SuppressWarnings("unchecked")
	public static void addToStereotypeEReferences(EObject foundationalILElement, String stereotypeName,
			String metaReferenceName, EObject referencedEObject, Profile profile, Resource profileApplicationResource)
			throws IOException {
		Stereotype stereotype = getStereotype(stereotypeName, profile);

		List<StereotypeApplication> applications = findStereotypeApplications(foundationalILElement, stereotype,
				profile, profileApplicationResource);
		StereotypeApplication application = null;
		if (applications.size() == 0) {
			application = applyStereotype(stereotype, profile, foundationalILElement, profileApplicationResource);
		} else {
			application = applications.get(0);
		}

		EStructuralFeature childFeature = stereotype.getEStructuralFeature(metaReferenceName);
		((EList<EObject>) application.eGet(childFeature)).add(referencedEObject);
		application.eResource().save(null);
	}

	private static LinkedList<StereotypeApplication> findStereotypeApplications(EObject foundationalILElement,
			Stereotype stereotype, Profile profile, Resource profileApplicationResource) throws IOException {
		LinkedList<StereotypeApplication> applications = new LinkedList<>();

		List<StereotypeApplication> apps = createProfileFacadeInstance(profile, profileApplicationResource)
				.getAppliedStereotypes(foundationalILElement);

		for (StereotypeApplication app : apps) {
			if (app.getStereotype() == stereotype) {
				applications.add(app);
			}
		}
		return applications;
	}

	public static StereotypeApplication applyStereotype(String stereotypeName, Profile profile, EObject eObject,
			Resource profileApplicationResource) throws IOException {
		Stereotype stereotype = getStereotype(stereotypeName, profile);
		if (stereotype == null)
			throw new IllegalArgumentException(String.format(STEREOTYPE_NOT_FOUND, stereotypeName));
		return applyStereotype(stereotype, profile, eObject, profileApplicationResource);
	}

	private static StereotypeApplication applyStereotype(Stereotype stereotype, Profile profile, EObject eObject,
			Resource profileApplicationResource) throws IOException {
		Extension extension = getDefaultExtension(stereotype, eObject, profileApplicationResource);
		StereotypeApplication application = createProfileFacadeInstance(profile, profileApplicationResource)
				.apply(stereotype, eObject, extension);
		application.setExtension(null);
		application.eResource().save(null);
		return application;
	}

	/**
	 * Finds and returns an existing stereotype application on an EObject. If the
	 * stereotype has not already been applied, a new one is created and applied.
	 * 
	 * @throws IOException
	 *             when the profile application resource cannot be set.
	 */
	public static StereotypeApplication findOrApplyStereotype(String stereotypeName, Profile profile, EObject appliedTo,
			Resource profileApplicationResource) throws IOException {
		Stereotype stereotype = getStereotype(stereotypeName, profile);

		List<StereotypeApplication> applications = createProfileFacadeInstance(profile, profileApplicationResource)
				.getAppliedStereotypes(appliedTo);

		for (StereotypeApplication app : applications)
			if (app.getStereotype().getName().equals(stereotype.getName())
					&& app.getStereotype().getEPackage().getNsURI().equals(stereotype.getEPackage().getNsURI()))
				return app;

		return applyStereotype(stereotypeName, profile, appliedTo, profileApplicationResource);
	}
}
