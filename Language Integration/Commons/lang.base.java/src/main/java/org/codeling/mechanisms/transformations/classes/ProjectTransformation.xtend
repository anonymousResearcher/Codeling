package org.codeling.mechanisms.transformations.classes

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation
import java.text.MessageFormat
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.IProjectDescription
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.CoreException
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IJavaProject
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jdt.launching.JavaRuntime

abstract class ProjectTransformation<ELEMENTECLASS extends EObject> extends ClassMechanismTransformation<ELEMENTECLASS, IJavaProject> {

	public static final String SRCFOLDER = "src/main/java";
	public static final String OUTPUTFOLDER = "target/classes";

	new(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation,
		EClass eClass) {
		super(parentTransformation, eClass)
	}

	override createCodeFragments() throws CodelingException {
		try {
			// create a project
			val IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			val IProject project = root.getProject(modelElement.nameAttributeValue);
			project.create(null);
			project.open(null);

			// set the Java nature
			val IProjectDescription description = project.getDescription();
			val natures = newArrayOfSize(1);
			natures.add(JavaCore.NATURE_ID);
			description.setNatureIds(natures);

			// create the project
			project.setDescription(description, null);
			codeElement = JavaCore.create(project);

			// set the build path
			val buildPath = newArrayOfSize(1);
			buildPath.add(JavaCore.newSourceEntry(project.getFullPath().append(SRCFOLDER)));
			buildPath.add(JavaRuntime.getDefaultJREContainerEntry());

			codeElement.setRawClasspath(buildPath, project.getFullPath().append(OUTPUTFOLDER), null);

			// create folder by using the resources package
			val IFolder folder = project.getFolder(SRCFOLDER);
			folder.create(true, true, null);
		} catch (Exception e) {
			addError("Could not create code fragments.", e);
		}
	}

	override deleteCodeFragments() throws CodelingException {
		val IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		val IProject proj = root.getProject(priorModelElement.nameAttributeValue);
		try {
			proj.delete(true, true, null);
		} catch (CoreException e) {
			addError(MessageFormat.format("Could not delete project {}", priorModelElement.nameAttributeValue), e);
		}
	}

	override transformToModel() throws CodelingException {
		modelElement = eClass.EPackage.EFactoryInstance.create(eClass);
		modelElement.nameAttributeValue = codeElement.getElementName();
		return modelElement;
	}

	override updateCodeFragments() throws CodelingException {
		val IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		val IProject project = root.getProject(priorModelElement.nameAttributeValue);
		val IJavaProject priorProject = JavaCore.create(project);

		if (!priorModelElement.nameAttributeValue.equals(modelElement.nameAttributeValue)) {
			try {
				moveProject(priorProject, modelElement.nameAttributeValue);
			} catch (CoreException e) {
				addError(MessageFormat.format("Could not rename project [{0}] to [{1}].", priorProject.getElementName(),
					modelElement.nameAttributeValue), e);
			}
		}
	}

	private def void moveProject(IJavaProject priorProject, String newName) throws CoreException {
		// The project description is only a copy.
		val IProjectDescription desc = priorProject.getProject().getDescription();
		desc.setName(newName);
		priorProject.getProject().move(desc, true, null);

		codeElement = JavaCore.create(ResourcesPlugin.getWorkspace().getRoot().getProject(newName));
	}
}
