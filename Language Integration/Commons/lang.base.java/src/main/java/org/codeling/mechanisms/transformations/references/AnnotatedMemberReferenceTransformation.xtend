package org.codeling.mechanisms.transformations.references

import org.codeling.lang.base.java.ASTUtils
import org.codeling.lang.base.java.transformation.references.IALTransformation
import java.util.Collection
import java.util.LinkedHashSet
import java.util.LinkedList
import java.util.List
import java.util.Set
import org.codeling.mechanisms.MechanismsMapping
import org.codeling.mechanisms.classes.MarkerInterfaceMechanism
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IBuffer
import org.eclipse.jdt.core.ICompilationUnit
import org.eclipse.jdt.core.IField
import org.eclipse.jdt.core.IType
import org.eclipse.jdt.core.JavaModelException
import org.eclipse.jdt.core.dom.AST

abstract class AnnotatedMemberReferenceTransformation<OWNERECLASS extends EObject, TARGETECLASS extends EObject> extends ReferenceMechanismTransformation<OWNERECLASS, TARGETECLASS, IType> {

	val boolean targetIsMarkerInterface
	protected val Set<IField> fields = new LinkedHashSet();
	protected val Set<IType> targetTypes = new LinkedHashSet();

	new(ClassMechanismTransformation<OWNERECLASS, IType> parentTransformation, EReference eReference) {
		super(parentTransformation, eReference)
		targetIsMarkerInterface = MechanismsMapping.getInstance().get(eReference.getEType()) == MarkerInterfaceMechanism
	}

	override createCodeFragments() throws CodelingException {
		var String fieldCode = "";
		if (eReference.isMany) {
			val LinkedList<EObject> targets = new LinkedList();
			targets.addAll(modelElement.eGet(eReference) as Collection<EObject>);
			if (targetIsMarkerInterface) {
				fieldCode = createMarkerInterfaceReferenceMulti(targets);
			} else {
				fieldCode = createTypeAnnotationReferenceMulti(targets);
			}
		} else {
			val EObject target = modelElement.eGet(eReference) as EObject;
			if (targetIsMarkerInterface) {
				fieldCode = createMarkerInterfaceReferenceSingle(target);
			} else {
				fieldCode = createTypeAnnotationReferenceSingle(target);
			}
		}
		if (fieldCode !== null && !fieldCode.isEmpty)
			codeElement.createField(fieldCode, null, true, null);
	}

	def String createTypeAnnotationReferenceSingle(EObject target) {
		'''
		«identifyingAnnotation»
		«target.nameAttributeValue.toFirstUpper» «target.nameAttributeValue.toFirstLower»;''';
	}

	def String createTypeAnnotationReferenceMulti(LinkedList<EObject> targets) {
		val StringBuffer content = new StringBuffer;
		val String lineSeparator = System.getProperty("line.separator");
		for (EObject target : targets) {
			content.append(createTypeAnnotationReferenceSingle(target));
			content.append(lineSeparator);
			content.append(lineSeparator);
		}
		return content.toString;
	}

	def String createMarkerInterfaceReferenceSingle(EObject target) {
		'''
		«identifyingAnnotation»
		«eReference.EType.name.toFirstUpper» «eReference.name.toFirstLower»;''';
	}

	def String createMarkerInterfaceReferenceMulti(LinkedList<EObject> targets) {
		'''
		«identifyingAnnotation»({«FOR target : targets SEPARATOR ', '»«target.nameAttributeValue.toFirstUpper».class«ENDFOR»})
		«eReference.EType.name.toFirstUpper»[] «eReference.name.toFirstLower»;''';
	}

	override updateCodeFragments() throws CodelingException {
		// The only change operation can be to change the reference's targets. To do so, no refactoring is necessary. 
		if (eReference.isMany) {
			// We want to keep any custom annotations on the "states" field in the 'x..* MarkerInterface' scenario. Therefore we change the annotation parameters.
			val LinkedList<EObject> targets = new LinkedList();
			targets.addAll(modelElement.eGet(eReference) as Collection<EObject>);
			val LinkedList<EObject> priorTargets = new LinkedList();
			priorTargets.addAll(priorModelElement.eGet(eReference) as Collection<EObject>);
			if (targets === null || priorTargets === null)
				return;
			if (targetIsMarkerInterface) {
				updateMarkerInterfaceReferenceMulti(targets);
			} else {
				updateTypeAnnotationReferenceMulti(priorTargets, targets);
			}
		} else {
			val EObject target = modelElement.eGet(eReference) as EObject;
			val priorTarget = priorModelElement.eGet(eReference) as EObject;
			if (target === null || priorTarget === null)
				return;
			if (targetIsMarkerInterface) {
				updateMarkerInterfaceReferenceSingle(target);
			} else {
				updateTypeAnnotationReferenceSingle(priorTarget, target);
			}
		}
	}

	def void updateTypeAnnotationReferenceSingle(EObject priorTarget, EObject target) {}

	def void updateMarkerInterfaceReferenceSingle(EObject target) {}

	def void updateTypeAnnotationReferenceMulti(LinkedList<EObject> priorTargets, LinkedList<EObject> targets) {
		// Create fields
		for (EObject target : targets) {
			val String originalTargetName = getNameAttributeValue(target);
			var String targetName = originalTargetName;
			targetName = Character.toLowerCase(targetName.charAt(0)) + targetName.substring(1);
			val IType type = if (this instanceof IALTransformation<?, ?>) {
					(this as IALTransformation<?, ?>).getIALHolder().getIALCodeElement() as IType
				} else {
					codeElement;
				};
			val IField field = type.getField(targetName);
			if (field === null || !field.exists()) {
				try {
					var String content = "@Inject\r";
					content += "" + originalTargetName + " " + targetName + ";\r\r";
					type.createField(content, null, true, null);
				} catch (JavaModelException e) {
					e.printStackTrace();
				}
			}
		}
		// Delete fields
		for (EObject priorTarget : priorTargets) {
			val String priorName = priorTarget.nameAttributeValue;
			if (!targets.exists[it.nameAttributeValue == priorName]) {
				val String originalTargetName = getNameAttributeValue(priorTarget);
				var String targetName = originalTargetName;
				targetName = Character.toLowerCase(targetName.charAt(0)) + targetName.substring(1);
				val IType type = if (this instanceof IALTransformation<?, ?>) {
						(this as IALTransformation<?, ?>).getIALHolder().getIALCodeElement() as IType
					} else {
						codeElement;
					};
				val IField field = type.getField(targetName);
				field.delete(true, null);
			}
		}
	}

	def void updateMarkerInterfaceReferenceMulti(LinkedList<EObject> targets) {
		var ICompilationUnit cu = codeElement.compilationUnit;
		cu.becomeWorkingCopy(null);
		// TODO: Handle all possibly identifying annotations. Currently only the standard annotation is respected, but not anything from hasExpectedAnnotation(..)
		val newSource = codeElement.source.
			replaceFirst('''@«eReference.name.toFirstUpper»(.+)''', '''@«eReference.name.toFirstUpper»({«FOR t : targets SEPARATOR ', '»«t.nameAttributeValue».class«ENDFOR»})''');
		val IBuffer b = cu.buffer;
		b.replace(codeElement.sourceRange.offset, codeElement.sourceRange.length, newSource)
		cu.reconcile(AST.JLS8, false, null, null);
		cu.save(null, true);
	}

	override deleteCodeFragments() {
		fields.forEach[f|f.delete(true, null)];
	}

	override transformToModel() throws CodelingException {
		if (modelElement === null)
			throw new IllegalStateException("modelElement must not be null");

		resolveCodeElement();
		if (fields.size == 0) {
			// No target object. Do not change the model.
			return modelElement;
		}

		// Find the element that is referenced by the field's type and set it as eReference value
		val List<String> typeNames = resolveTargetTypes().map(t|t.elementName).toList;

		val List<TARGETECLASS> targetElements = getTargetObjects(modelElement, eReference, typeNames);
		if (eReference.isMany) {
			(modelElement.eGet(eReference) as List<TARGETECLASS>).addAll(targetElements);
		} else {
			modelElement.eSet(eReference, targetElements.head);
		}

		return modelElement;
	}

	override IType resolveCodeElement() {
		if (codeElement === null)
			super.resolveCodeElement();
		if (fields.isEmpty)
			for (IField field : (codeElement as IType).fields)
				if (hasExpectedAnnotation(field))
					fields.add(field);
		return codeElement;
	}

	public def boolean hasExpectedAnnotation(IField field) {
		return field.getAnnotation(eReference.name.toFirstUpper).exists;
	}

	protected def String getNewAnnotationName() {
		return '''org.codeling.lang.«getLanguageName».mm.«eReference.name.toFirstUpper»''';
	}

	private def String getIdentifyingAnnotation() {
		if (newAnnotationName === null) {
			return "";
		}

		return '''@«newAnnotationName»''';
	}

	def Set<IType> resolveTargetTypes() {
		if (!targetTypes.isEmpty)
			return targetTypes;

		if (fields.isEmpty)
			resolveCodeElement();
		if (fields.isEmpty()) {
			return targetTypes;
		}

		if (targetIsMarkerInterface) {
			val Object annotationValues = ASTUtils.getAnnotationMemberValue(
				fields.get(0).getAnnotation(eReference.name.toFirstUpper), "value", Object);
			if (eReference.isMany) {
				val Object[] typeNamesArray = (annotationValues as Object[]);
				typeNamesArray.forEach [ n |
					{
						val IType targetType = ASTUtils.getType((n as String), codeElement as IType);
						targetTypes.add(targetType);
					}
				];
			} else {
				val String typeName = annotationValues as String;
				val IType targetType = ASTUtils.getType(typeName, codeElement as IType);
				targetTypes.add(targetType);
			}
		} else {
			for (IField f : fields) {
				val IType targetType = ASTUtils.getFieldType(f);
				targetTypes.add(targetType);
			}
		}

		return targetTypes;
	}

	def Set<IField> getFields() {
		return fields;
	}

}
