package org.codeling.mechanisms.transformations.references

import org.codeling.lang.base.java.JDTUtils
import java.util.Collection
import java.util.LinkedHashSet
import java.util.LinkedList
import java.util.List
import java.util.Set
import org.codeling.mechanisms.transformations.ClassMechanismTransformation
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation
import org.codeling.utils.CodelingException
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IMethod
import org.eclipse.jdt.core.IType

abstract class ContainmentOperationTransformation<OWNERECLASS extends EObject, TARGETECLASS extends EObject> extends ReferenceMechanismTransformation<OWNERECLASS, TARGETECLASS, IType> {
	protected var LinkedHashSet<IMethod> methods = new LinkedHashSet();

	new(ClassMechanismTransformation<OWNERECLASS, IType> parentTransformation, EReference eReference) {
		super(parentTransformation, eReference)
	}

	override createCodeFragments() throws CodelingException {
		// update will handle delete and create aswell
		updateCodeFragments();
	}

	override updateCodeFragments() throws CodelingException {
		var LinkedList<EObject> targets = new LinkedList();
		if (eReference.isMany) {
			targets.addAll(modelElement.eGet(eReference) as Collection<EObject>);
		} else {
			targets.add(modelElement.eGet(eReference) as EObject);
		}

		val IType type = codeElement as IType;
		for (EObject target : targets) {
			if (type.getMethod(target.nameAttributeValue, newArrayOfSize(0)).exists) {
				// Method already exists. Do nothing
			} else {
				val IMethod method = type.methods.findFirst[it.elementName.equals(target.nameAttributeValue)]
				if (method !== null && method.exists) {
					// We also allow the methods to have parameters. Do nothing if that already exists
					// This is kept separate, so that it can be easily removed when necessary.
				} else {
					// Method does not exist. Create a new one.
					val String targetName = target.nameAttributeValue;

					val String content = '''
						«IF newAnnotationName !== null»
							@«newAnnotationName»
						«ENDIF»
						public void «targetName.toFirstLower»(){}
					''';

					methods.add(type.createMethod(content, null, true, null));
					JDTUtils.addImportIfNecessary(type, newAnnotationName, null);
				}
			}
		}
	}

	override deleteCodeFragments() {
		// update will handle delete and create aswell
		updateCodeFragments();
	}

	override transformToModel() throws CodelingException {
		// Find the element that is referenced by the field's type and set it as eReference value
		// Create an EObject of the given eclass with the given name
		if (modelElement === null)
			throw new IllegalStateException("modelElement must not be null");

		resolveCodeElement();

		// Find the element that is referenced by the field's type and set it as eReference value
		val List<String> typeNames = methods.map [ m |
			m.elementName
		].toList;

		val List<TARGETECLASS> targetElements = getTargetObjects(modelElement, eReference, typeNames);
		if (eReference.isMany) {
			(modelElement.eGet(eReference) as List<TARGETECLASS>).addAll(targetElements);
		} else {
			modelElement.eSet(eReference, targetElements.head);
		}

		return modelElement;
	}

	override IType resolveCodeElement() {
		if (methods.isEmpty)
			for (IMethod method : (parentCodeElement as IType).methods)
				if (hasExpectedAnnotation(method))
					methods.add(method);
		return codeElement;
	}

	def Set<IMethod> getMethods() {
		return methods;
	}

	def boolean hasExpectedAnnotation(IMethod method) {
		return method.getAnnotation(eReference.name.toFirstUpper).exists;
	}

	protected def String getNewAnnotationName() {
		return '''org.codeling.lang.«getLanguageName».mm.«eReference.name.toFirstUpper»''';
	}
}
