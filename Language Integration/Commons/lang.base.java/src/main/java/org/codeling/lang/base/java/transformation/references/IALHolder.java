package org.codeling.lang.base.java.transformation.references;

import java.util.List;

import org.codeling.utils.errorcontainers.MayHaveIssues;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class IALHolder extends MayHaveIssues {

	// Prior Element
	private EObject priorFoundationalIALElement;

	// Current (possibly changed) element
	private EObject foundationalIALElement;

	// Code Element
	private IJavaElement ialCodeElement;
	
	// IAL roots for adding stereotype applications
	private List<EObject> ialRoots; 

	public EObject getPriorFoundationalIALElement() {
		return priorFoundationalIALElement;
	}

	public void setPriorFoundationalIALElement(EObject priorIALElement) {
		this.priorFoundationalIALElement = priorIALElement;
	}

	public EObject getFoundationalIALElement() {
		return foundationalIALElement;
	}

	public void setFoundationalIALElement(EObject ialElement) {
		this.foundationalIALElement = ialElement;
	}

	public IJavaElement getIALCodeElement() {
		return ialCodeElement;
	}

	public void setIALCodeElement(IJavaElement ialCodeElement) {
		this.ialCodeElement = ialCodeElement;
	}

	public List<EObject> getIALRoots() {
		return ialRoots;
	}

	public void setIALRoots(List<EObject> ialRoots) {
		this.ialRoots = ialRoots;
	}
}
