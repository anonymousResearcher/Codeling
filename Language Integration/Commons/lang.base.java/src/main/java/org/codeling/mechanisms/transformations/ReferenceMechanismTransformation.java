package org.codeling.mechanisms.transformations;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.java.transformation.IModelCodeTransformation;
import org.codeling.lang.base.java.transformation.references.IALTransformation;
import org.codeling.utils.Models;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.core.IJavaElement;

public abstract class ReferenceMechanismTransformation<OWNERECLASS extends EObject, TARGETECLASS extends EObject, JAVAELEMENT extends IJavaElement>
		extends AbstractModelCodeTransformation<OWNERECLASS, JAVAELEMENT> {

	protected EReference eReference = null;
	protected List<TARGETECLASS> targetObjects = null;

	public ReferenceMechanismTransformation(ClassMechanismTransformation<OWNERECLASS, JAVAELEMENT> parentTransformation,
			EReference eReference) {
		super(parentTransformation);
		this.eReference = eReference;
		if (parentTransformation != null) {
			setModelElement(parentTransformation.getModelElement());
			setPriorModelElement(parentTransformation.getPriorModelElement());
			setCodeElement(parentTransformation.getCodeElement());
			
			if (this instanceof IALTransformation && parentTransformation instanceof IALTransformation) {
				IALTransformation<?, ?> thisIAL = ((IALTransformation<?, ?>) this);
				IALTransformation<?, ?> parentIAL = ((IALTransformation<?, ?>) parentTransformation);
				thisIAL.getIALHolder().setFoundationalIALElement(parentIAL.getIALHolder().getFoundationalIALElement());
				thisIAL.getIALHolder().setPriorFoundationalIALElement(parentIAL.getIALHolder().getPriorFoundationalIALElement());
				thisIAL.getIALHolder().setIALCodeElement(parentIAL.getIALHolder().getIALCodeElement());
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void compute() {
		// When translating towards the specification language, transformations for
		// containment references require the target to exist before they can be
		// translated. Therefore the target transformations are executed before.
		if (!toCode && eReference.isContainment()) {
			final List<TARGETECLASS> targets = new LinkedList<>();
			final List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> children = createChildTransformationsToModel();
			children.forEach(t -> t.setOnlyCrossReferences(onlyCrossReferences));
			children.forEach(t -> t.setToModel());
			invokeAll(children);

			for (final IModelCodeTransformation<?, ?> child : children) {
				TARGETECLASS target = (TARGETECLASS) child.getModelElement();
				// When the transformation returns null because of an error, this should not be
				// added to the list
				if (target != null)
					targets.add(target);
			}
			setTargetObjects(targets);
		}

		super.compute();
	}

	/**
	 * Returns all {@link EObject}s with the given targetElementNames, which are
	 * within the tree where modelElement is in, and has the eclass of the
	 * reference's target type.
	 */
	public List<TARGETECLASS> getTargetObjects(EObject modelElement, EReference eReference,
			List<String> targetElementNames) {
		if (targetObjects != null)
			// The target objects might already be set, when this reference is a
			// containment.
			return targetObjects;

		targetObjects = new LinkedList<>();
		final EClass targetEClass = eReference.getEReferenceType();
		final HashSet<EObject> allElements = new HashSet<>();
		final EObject root = EcoreUtil.getRootContainer(modelElement);
		allElements.add(root);
		for (final Iterator<EObject> ti = root.eAllContents(); ti.hasNext();)
			allElements.add(ti.next());

		for (final Iterator<EObject> ti = allElements.iterator(); ti.hasNext();) {
			final EObject e = ti.next();
			if (targetEClass.isInstance(e)) {
				for (final String name : targetElementNames)
					if (getNameAttributeValue(e).equals(name)) {
						targetObjects.add((TARGETECLASS) e);
						break;
					}
			}
		}
		return targetObjects;
	}

	public void setTargetObjects(List<TARGETECLASS> targetObjects) {
		this.targetObjects = targetObjects;
	}

	abstract protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement);

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		for (EObject target : Models.getTargetsAsList(modelElement, eReference)) {
			try {
				AbstractModelCodeTransformation<?, ?> t = createSpecificTransformation(target);
				if (t == null)
					continue;
				
				t.setModelElement(target);
				// PriorModelElement will be set by AbstractModelCodeTransformation
				result.add(t);
			} catch (Throwable t) {
				addError("Could not create target transformation.", t);
			}
		}
		for (EObject target : Models.getTargetsAsList(priorModelElement, eReference)) {
			try {
				String priorID = priorIDRegistry.getIDFromImplementationModelElement(target);
				EObject newElement = idRegistry.resolveImplementationModelElement(priorID,
						EcoreUtil.getRootContainer(modelElement));
				if (newElement == null) {
					AbstractModelCodeTransformation<?, ?> t = createSpecificTransformation(target);
					if (t == null)
						continue;
					
					t.setPriorModelElement(target);
					result.add(t);
				}
			} catch (Throwable t) {
				addError("Could not create target transformation.", t);
			}
		}
	}

	@Override
	public boolean canHandle(EModelElement metaModelElement) {
		return metaModelElement == eReference;
	}

	@Override
	public boolean validateToModel(List<String> validationMessages) {
		super.validateToModel(validationMessages);
		if (modelElement == null) {
			validationMessages.add("modelElement must not be null when translating references.");
		}
		return validationMessages.isEmpty();
	}
}