/**
 */
package src.source;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Diagram</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link src.source.ClassDiagram#getClass_ <em>Class</em>}</li>
 *   <li>{@link src.source.ClassDiagram#getAss <em>Ass</em>}</li>
 *   <li>{@link src.source.ClassDiagram#getPtypes <em>Ptypes</em>}</li>
 * </ul>
 *
 * @see src.source.SourcePackage#getClassDiagram()
 * @model
 * @generated
 */
public interface ClassDiagram extends EObject {
	/**
	 * Returns the value of the '<em><b>Class</b></em>' containment reference list.
	 * The list contents are of type {@link src.source.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' containment reference list.
	 * @see src.source.SourcePackage#getClassDiagram_Class()
	 * @model containment="true"
	 * @generated
	 */
	EList<src.source.Class> getClass_();

	/**
	 * Returns the value of the '<em><b>Ass</b></em>' containment reference list.
	 * The list contents are of type {@link src.source.Association}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ass</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ass</em>' containment reference list.
	 * @see src.source.SourcePackage#getClassDiagram_Ass()
	 * @model containment="true"
	 * @generated
	 */
	EList<Association> getAss();

	/**
	 * Returns the value of the '<em><b>Ptypes</b></em>' containment reference list.
	 * The list contents are of type {@link src.source.PrimitiveDataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ptypes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ptypes</em>' containment reference list.
	 * @see src.source.SourcePackage#getClassDiagram_Ptypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<PrimitiveDataType> getPtypes();

} // ClassDiagram
