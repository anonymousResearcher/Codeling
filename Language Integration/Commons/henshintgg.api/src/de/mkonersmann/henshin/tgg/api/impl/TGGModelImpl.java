package de.mkonersmann.henshin.tgg.api.impl;

import java.util.List;

import org.codeling.utils.CodelingLogger;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.henshin.model.Module;

import de.mkonersmann.henshin.tgg.api.TGGExecutor;
import de.mkonersmann.henshin.tgg.api.TGGModel;

/**
 * This class loads a TGG definition file (i.e. the rules).
 *
 * @author Anonymous Researcher <anonymous@example.org>
 *
 */
public class TGGModelImpl implements TGGModel {

	final CodelingLogger log = new CodelingLogger(getClass());

	Module module;

	public TGGModelImpl(Module module) {
		this.module = module;
	}

	private TGGExecutor createTGGExecutor() {
		// Load all imported packages from the module
		registerPackages();

		final TGGExecutorImpl tggExecutor = new TGGExecutorImpl(this);

		return tggExecutor;
	}

	private void registerPackages() {
		for (final EPackage p : module.getImports()) {
			if (!EPackage.Registry.INSTANCE.containsKey(p.getNsURI())) {
				EPackage.Registry.INSTANCE.put(p.getNsURI(), p);
			}
		}
	}

	@Override
	public Module getModule() {
		return module;
	}

	@Override
	public TGGExecutor createTGGExecutor(List<EObject> modelRoots) {
		final TGGExecutor executor = createTGGExecutor();
		executor.importModel(modelRoots);
		return executor;
	}

	@Override
	public TGGExecutor createTGGExecutor(EObject modelRoot) {
		final TGGExecutor executor = createTGGExecutor();
		executor.importModel(modelRoot);
		return executor;
	}

}
