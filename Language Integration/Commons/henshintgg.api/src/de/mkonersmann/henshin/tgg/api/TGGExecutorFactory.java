package de.mkonersmann.henshin.tgg.api;

import org.eclipse.emf.common.util.URI;

import de.mkonersmann.henshin.tgg.api.impl.TGGExecutorFactoryImpl;

public interface TGGExecutorFactory {
	public TGGExecutorFactory INSTANCE = new TGGExecutorFactoryImpl();

	public TGGModel createTGGModel(URI henshinFileUri);
	
}
