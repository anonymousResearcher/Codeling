package de.mkonersmann.henshin.tgg.api.impl;

import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.HenshinPackage;
import org.eclipse.emf.henshin.model.Module;

import de.mkonersmann.henshin.tgg.api.TGGExecutorFactory;
import de.mkonersmann.henshin.tgg.api.TGGModel;
import de.tub.tfs.henshin.tgg.TGG;
import de.tub.tfs.henshin.tgg.TggPackage;
import de.tub.tfs.muvitor.ui.utils.EMFModelManager;

public class TGGExecutorFactoryImpl implements TGGExecutorFactory {

	public static final String HENSHIN_EXTENSION = "henshin";
	HashMap<URI, Module> modules = new HashMap<>();
	HashMap<URI, TGG> tggs = new HashMap<>();

	public TGGExecutorFactoryImpl() {
		HenshinPackage.eINSTANCE.getName();
		TggPackage.eINSTANCE.getName();

		EMFModelManager manager = EMFModelManager.createModelManager(HENSHIN_EXTENSION);
		manager.cleanUp();
	}

	private void registerClassConversions() {
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap();
		if (EMFModelManager.hasClassConversion(HenshinPackage.eINSTANCE, "Node", TggPackage.Literals.TNODE))
			return;

		EMFModelManager.registerClassConversion(HenshinPackage.eINSTANCE, HenshinPackage.Literals.NODE,
				TggPackage.Literals.TNODE);
		EMFModelManager.registerClassConversion(HenshinPackage.eINSTANCE, HenshinPackage.Literals.EDGE,
				TggPackage.Literals.TEDGE);
		EMFModelManager.registerClassConversion(HenshinPackage.eINSTANCE, HenshinPackage.Literals.ATTRIBUTE,
				TggPackage.Literals.TATTRIBUTE);
		EMFModelManager.registerClassConversion(HenshinPackage.eINSTANCE, HenshinPackage.Literals.RULE,
				TggPackage.Literals.TGG_RULE);
		EMFModelManager.registerClassConversion(HenshinPackage.eINSTANCE, HenshinPackage.Literals.GRAPH,
				TggPackage.Literals.TRIPLE_GRAPH);
		EMFModelManager.registerClassConversion(HenshinPackage.eINSTANCE, HenshinPackage.Literals.MODULE,
				TggPackage.Literals.TGG);
		EMFModelManager.registerClassConversion(HenshinPackage.eINSTANCE, null, TggPackage.Literals.IMPORTED_PACKAGE);

	}

	private void unregisterClassConversions() {
		HenshinPackage.eINSTANCE.setEFactoryInstance(HenshinFactory.eINSTANCE);
		EMFModelManager.createModelManager(HENSHIN_EXTENSION).cleanUp();
	}

	@Override
	public TGGModel createTGGModel(URI henshinFileUri) {
		registerClassConversions();
		TGGModel result = null;
		try {
			String uriString = henshinFileUri.toString();
			uriString = uriString.substring(0, uriString.lastIndexOf('.'));
			URI moduleUri = URI.createURI(uriString + ".henshin");

			// Load Module
			Module module = loadModule(moduleUri);

			result = new TGGModelImpl(module);
		} finally {
			unregisterClassConversions();
		}
		return result;
	}

	private Module loadModule(URI moduleUri) {
		Module module = modules.get(moduleUri);
		if (module == null) {
			Resource r = new ResourceSetImpl().getResource(moduleUri, true);
			module = (Module) r.getContents().get(0);
			modules.put(moduleUri, module);
		}
		return module;
	}

}
