package de.mkonersmann.henshin.tgg.api;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Module;

public interface TGGModel {

	public TGGExecutor createTGGExecutor(List<EObject> modelRoots);

	public TGGExecutor createTGGExecutor(EObject modelRoot);

	public Module getModule();
	
}
