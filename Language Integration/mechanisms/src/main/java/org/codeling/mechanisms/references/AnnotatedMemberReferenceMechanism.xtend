package org.codeling.mechanisms.references

import org.codeling.mechanisms.MechanismsMapping
import org.codeling.mechanisms.ReferenceMechanism
import org.codeling.mechanisms.classes.MarkerInterfaceMechanism
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IType

class AnnotatedMemberReferenceMechanism extends ReferenceMechanism {

	override getName() {
		"Annotated Member Reference"
	}

	override createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
		var String elementName = element.name.toFirstUpper;
		val String owningElementName = (element as EReference).EType.name.toFirstUpper;

		// If the target is a marker interface, add an parameter to give the concrete type.
		val boolean setParam = MechanismsMapping.getInstance().get((element as EReference).EType) ==
			MarkerInterfaceMechanism;
		val boolean isMany = (element as EReference).many;

		val content = '''
			package «packageFragment.elementName»;
			
			@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
			@java.lang.annotation.Target(java.lang.annotation.ElementType.FIELD)
			public @interface «elementName» {
				
				«IF setParam»
					Class<? extends org.codeling.lang.«languageName».mm.«owningElementName»>«IF isMany»[]«ENDIF» value();
				«ENDIF»
			}
		''';

		return packageFragment.createCompilationUnit('''«elementName».java''', content, true, monitor).getType(
			elementName);
	}

	override createRuntime(IPackageFragment packageFragment, ENamedElement element) {
		val String typeName = element.name.toFirstUpper;
		val String owningElementName = (element as EReference).EContainingClass.name;
		val String targetTypeName = (element as EReference).EType.name;
		val boolean isContainment = (element as EReference).containment;

		val content = '''
			package «packageFragment.elementName»;
			
			import «languageName».mm.«owningElementName.toLowerCase»_feature.«typeName»;
			import org.codeling.lang.«languageName».runtime.«targetTypeName»Runtime;
			import org.codeling.mechanism.runtime.AnnotatedMemberReferenceRuntime;
			import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;
			
			public class «typeName»Runtime extends AnnotatedMemberReferenceRuntime {
			
				public «typeName»Runtime(TypeMechanismRuntime owningRuntime) {
					super(owningRuntime, «typeName».class, «targetTypeName»Runtime.class, «isContainment»);
				}
				
			}
		''';

		packageFragment.createCompilationUnit('''«typeName»Runtime.java''', content, true, monitor);
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		var String typeName = element.name.toFirstUpper;
		val EReference eReference = element as EReference;
		val String eReferenceInstantiation = eReference.
			EFeatureInstantiation

		val content = '''
			package «packageFragment.elementName»;
			
			import java.util.List;
			
			import org.codeling.mechanisms.transformations.references.AnnotatedMemberReferenceTransformation;
			import org.eclipse.emf.ecore.EObject;
			import org.eclipse.jdt.core.IJavaElement;
			import org.eclipse.jdt.core.IType;
			
			import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
			import «eReference.packageName».«eReference.packageName»Package;
			import «eReference.packageName».«eReference.EContainingClass.name»;
			import «eReference.packageName».«eReference.EType.name»;
			import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
			import org.codeling.lang.«languageName».transformation.«eReference.EType.name»Transformation;
						
			public class «typeName»Transformation
					extends AnnotatedMemberReferenceTransformation<«eReference.EContainingClass.name», «eReference.EType.name»> {
			
				public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, IType> parentTransformation) {
					super(parentTransformation, «eReferenceInstantiation»);
				}
			
				@Override
					public void doCreateCrossReferencesTransformations(
							List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			
				@Override
				protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
						EObject targetModelElement) {
					return new «eReference.EType.name»Transformation(this);
				}
			
				@Override
				protected void doCreateChildTransformationsToModel(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			}
		'''

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return codeElement instanceof IType && (codeElement as IType).fields.stream.anyMatch(
			f |
				f.getAnnotation(metaModelElement.name.toFirstUpper).exists
		);
	}

}
