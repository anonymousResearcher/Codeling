package org.codeling.mechanisms;

import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import org.codeling.mechanisms.attributes.ConstantMemberAttributeMechanism;
import org.codeling.mechanisms.classes.ContainmentOperationMechanism;
import org.codeling.mechanisms.classes.MarkerInterfaceMechanism;
import org.codeling.mechanisms.classes.NinjaSingletonMechanism;
import org.codeling.mechanisms.classes.ProjectMechanism;
import org.codeling.mechanisms.classes.StaticInterfaceMechanism;
import org.codeling.mechanisms.classes.TypeAnnotationMechanism;
import org.codeling.mechanisms.containment.NinjaSingletonContainmentReferenceMechanism;
import org.codeling.mechanisms.references.AnnotatedMemberReferenceMechanism;
import org.codeling.mechanisms.references.StaticInterfaceImplementationMechanism;
import org.codeling.utils.CodelingException;

public enum Mechanisms {
	NINJA_SINGLETON(NinjaSingletonMechanism.class),
	PROJECT(ProjectMechanism.class),
	MARKER_INTERFACE(MarkerInterfaceMechanism.class),
	TYPE_ANNOTATION(TypeAnnotationMechanism.class),
	CONSTANT_MEMBER_ATTRIBUTES(ConstantMemberAttributeMechanism.class),
	CONTAINMENT_OPERATION(ContainmentOperationMechanism.class),
	ANNOTATED_MEMBER_REFERENCE(AnnotatedMemberReferenceMechanism.class),
	CONTAINMENT_OPERATION_REFERENCE(NinjaSingletonContainmentReferenceMechanism.class),
	STATIC_INTERFACE(StaticInterfaceMechanism.class),
	STATIC_INTERFACE_IMPLEMENTATION(StaticInterfaceImplementationMechanism.class);

	private Class<?> mechanismClass;

	private Mechanisms(Class<?> mechanismClass) {
		this.mechanismClass = mechanismClass;
	}

	public Class<?> getMechanismClass() {
		return mechanismClass;
	}

	static LinkedList<ClassMechanism> classMechanisms = null;
	static LinkedList<AttributeMechanism> attributeMechanisms = null;
	static LinkedList<ReferenceMechanism> referenceMechanisms = null;
	static LinkedList<ContainmentMechanism> containmentMechanisms = null;

	public static List<ClassMechanism> getClassMechanisms() {
		if (classMechanisms == null)
			classMechanisms = createMechanismsList(ClassMechanism.class);
		return classMechanisms;
	}

	public static List<AttributeMechanism> getAttributeMechanisms() {
		if (attributeMechanisms == null)
			attributeMechanisms = createMechanismsList(AttributeMechanism.class);
		return attributeMechanisms;
	}

	public static List<ReferenceMechanism> getReferenceMechanisms() {
		if (referenceMechanisms == null)
			referenceMechanisms = createMechanismsList(ReferenceMechanism.class);
		return referenceMechanisms;
	}

	public static List<ContainmentMechanism> getContainmentMechanisms() {
		if (containmentMechanisms == null)
			containmentMechanisms = createMechanismsList(ContainmentMechanism.class);
		return containmentMechanisms;
	}

	@SuppressWarnings("unchecked")
	private static <T extends Mechanism> LinkedList<T> createMechanismsList(Class<T> type) {
		LinkedList<T> list = new LinkedList<>();
		for (Mechanisms m : Mechanisms.values())
			if (type.isAssignableFrom(m.getMechanismClass()))
				try {
					list.add((T) create(m));
				} catch (CodelingException e) {
					e.printStackTrace();
				}
		return list;
	}

	@SuppressWarnings("unchecked")
	public static <T extends Mechanism> T create(Mechanisms mechanism) throws CodelingException {
		return create((Class<T>) mechanism.getMechanismClass());
	}

	public static <T extends Mechanism> T create(Class<T> mechanismClass) throws CodelingException {
		try {
			return (T) mechanismClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new CodelingException(MessageFormat.format("Could not create an instance of the mechanism class {0}.",
					mechanismClass.getName()), e);
		}
	}

}