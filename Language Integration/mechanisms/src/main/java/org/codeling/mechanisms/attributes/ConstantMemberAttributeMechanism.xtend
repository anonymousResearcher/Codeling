package org.codeling.mechanisms.attributes;

import org.codeling.mechanisms.AttributeMechanism
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IPackageFragmentRoot
import org.eclipse.jdt.core.IType

class ConstantMemberAttributeMechanism extends AttributeMechanism {

	override getName() {
		"Constant Member Attribute"
	}

	override createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
		val String parentPackageName = packageFragment.elementName.substring(0,
			packageFragment.elementName.lastIndexOf("."));
		val IPackageFragment parentPackage = (packageFragment.parent as IPackageFragmentRoot).
			getPackageFragment(parentPackageName);
		if (parentPackage.getCompilationUnit("Attribute.java").exists())
			return parentPackage.getCompilationUnit("Attribute.java").getType("Attribute");

		val content = '''
			package «parentPackage.elementName»;
			
			@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
			@java.lang.annotation.Target(java.lang.annotation.ElementType.FIELD)
			public @interface Attribute {
			}
		''';

		return parentPackage.createCompilationUnit("Attribute.java", content, true, monitor).getType("Attribute");
	}

	override createRuntime(IPackageFragment packageFragment, ENamedElement element) {
		val String typeName = element.name.toFirstUpper;
		val String targetTypeName = (element as EAttribute).EType.name;
		
		val content = '''
			package «packageFragment.elementName»;
			
			import constant_member_attribute.runtime.ConstantMemberAttributeRuntime;
			
			public class «typeName»Runtime extends ConstantMemberAttributeRuntime {
				public «typeName»Runtime(TypeMechanismRuntime owningRuntime) {
					super(owningRuntime, «typeName».class, «targetTypeName»Runtime.class);
				}
			}
		''';

		packageFragment.createCompilationUnit('''«typeName»Runtime.java''', content, true, monitor);
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		var String typeName = element.name.toFirstUpper;
		val EAttribute eAttribute = element as EAttribute;
		val String eAttributeInstantiation = eAttribute.EFeatureInstantiation;

		val content = '''
			package «packageFragment.elementName»;
			
			import java.util.List;
			
			import org.codeling.mechanisms.transformations.attributes.ConstantMemberAttributeTransformation;
			import org.eclipse.emf.ecore.EObject;
			import org.eclipse.jdt.core.IJavaElement;
			import org.eclipse.jdt.core.IType;
			
			import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
			import «eAttribute.packageName».«eAttribute.packageName.toFirstUpper»Package;
			import «eAttribute.packageName».«eAttribute.EContainingClass.name»;
			
			public class «typeName»Transformation
					extends ConstantMemberAttributeTransformation<«eAttribute.EContainingClass.name»> {
			
				public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, IType> parentTransformation) {
					super(parentTransformation, «eAttributeInstantiation»);
				}
			
				@Override
				public void doCreateCrossReferencesTransformations(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			
				@Override
				protected void doCreateChildTransformationsToCode(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			
				@Override
				protected void doCreateChildTransformationsToModel(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
				// TODO Auto-generated method stub
				}
			}
		'''

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return codeElement instanceof IType && (codeElement as IType).fields.stream.anyMatch(
			f |
				f.getAnnotation("Attribute").exists
		);
	}

}
