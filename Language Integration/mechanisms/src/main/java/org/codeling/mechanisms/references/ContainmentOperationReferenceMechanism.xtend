package org.codeling.mechanisms.references

import org.codeling.mechanisms.ReferenceMechanism
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IMethod
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IType

class ContainmentOperationReferenceMechanism extends ReferenceMechanism {

	override getName() {
		"Reference Parameter"
	}

	override createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
		val EClass targetClass = (element as EReference).EReferenceType;
		var String parameterName = element.name.toFirstLower;

		val content = '''
			Class<? extends org.codeling.lang.«languageName».mm.«targetClass.name»> «parameterName»();
		''';

		val EClass owningClass = (element as EReference).EContainingClass;
		var EReference containmentOperationReference = getEReferenceThatOwns(owningClass);
		val IType parentAnnotation = modelToCodeMap.get(containmentOperationReference) as IType;
		return parentAnnotation.createMethod(content, null, false, monitor);
	}

	override createRuntime(IPackageFragment packageFragment, ENamedElement element) {
		val String typeName = element.name.toFirstUpper;
		val String referenceName = element.name.toFirstLower;
		val String targetTypeName = (element as EReference).EType.name;
		val boolean isContainment = (element as EReference).containment;

		val content = '''
			package «packageFragment.elementName»;
			
			import org.codeling.lang.ejbWithStatemachine.runtime.StateRuntime;
			import org.codeling.mechanism.runtime.ContainmentOperationReferenceRuntime;
			import org.codeling.mechanism.runtime.ContainmentOperationRuntime;
			
			public class «typeName»Runtime extends ContainmentOperationReferenceRuntime {
			
				public «typeName»Runtime(ContainmentOperationRuntime owningRuntime) {
					super(owningRuntime, "«referenceName»", «targetTypeName»Runtime.class, «isContainment»);
				}
				
			}
		''';

		packageFragment.createCompilationUnit('''«typeName»Runtime.java''', content, true, monitor);
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		var String typeName = element.name.toFirstUpper;
		val EReference eReference = element as EReference;
		val String eReferenceInstantiation = eReference.
			EFeatureInstantiation

		val content = '''
			package «packageFragment.elementName»;
			
			import java.util.List;
			
			import org.codeling.mechanisms.transformations.references.ReferenceParameterTransformation;
			import org.eclipse.emf.ecore.EObject;
			import org.eclipse.jdt.core.IJavaElement;
			import org.eclipse.jdt.core.IType;
			
			import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
			import «eReference.packageName».«eReference.packageName»Package;
			import «eReference.packageName».«eReference.EContainingClass.name»;
			import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
			import org.codeling.lang.«languageName».transformation.«eReference.EType.name»Transformation;
			
			public class «typeName»Transformation
					extends ReferenceParameterTransformation<«eReference.EContainingClass.name», «eReference.EType.name»> {
			
				public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, IType> parentTransformation) {
					super(parentTransformation, «eReferenceInstantiation»);
				}
			
				@Override
				public void doCreateCrossReferencesTransformations(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			
				@Override
				protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
						EObject targetModelElement) {
					return new «eReference.EType.name»Transformation(this);
				}
			
				@Override
				protected void doCreateChildTransformationsToModel(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			}
		'''

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return codeElement instanceof IMethod &&
			(codeElement as IMethod).getAnnotation((metaModelElement.eContainer as ENamedElement).name.toFirstUpper).
		exists;
	}
}
		