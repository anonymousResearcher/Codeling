package org.codeling.mechanisms;

import java.util.LinkedHashMap;

import org.eclipse.emf.ecore.ENamedElement;

/**
 * A mapping between an ENamedElement to translate and a the mechanism used to
 * translate it.
 */
public class MechanismsMapping {

	static MechanismsMapping INSTANCE = new MechanismsMapping();

	private MechanismsMapping() {
	}

	public static MechanismsMapping getInstance() {
		return INSTANCE;
	}

	LinkedHashMap<ENamedElement, Class<? extends Mechanism>> map = new LinkedHashMap<>();

	public void put(ENamedElement translatedModelElement, Class<? extends Mechanism> runtimeClass) {
		map.put(translatedModelElement, runtimeClass);
	}

	public Class<? extends Mechanism> get(ENamedElement translatedModelElement) {
		return map.get(translatedModelElement);
	}
}
