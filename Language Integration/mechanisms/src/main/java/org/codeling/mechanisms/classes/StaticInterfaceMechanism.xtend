package org.codeling.mechanisms.classes

import org.codeling.mechanisms.ClassMechanism
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IType

class StaticInterfaceMechanism extends ClassMechanism {

	override getName() {
		"Static Interface"
	}

	override createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
		val String ifaceName = element.name.toFirstUpper;

		val content = '''
		package «packageFragment.elementName»;
		
		public @interface «ifaceName» {
			
		}''';

		return packageFragment.createCompilationUnit('''«ifaceName».java''', content, true, monitor).getType(
			ifaceName
		);
	}

	override createRuntime(IPackageFragment packageFragment, ENamedElement element) {
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		val String typeName = element.name.toFirstUpper;
		val EClass eClass = element as EClass;
		val String eClassName = eClass.name;
		val String eClassInstantiation = getEClassInstantiation(eClass);

		val content = '''
		package «packageFragment.elementName»;
		
		import java.util.List;
		
		import org.codeling.mechanisms.transformations.classes.StaticInterfaceTransformation;
		import org.eclipse.emf.ecore.EObject;
		import org.eclipse.jdt.core.IJavaElement;
		
		import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
		import org.codeling.utils.CodelingException;
		import «eClass.packageName».«eClass.packageName»Package;
		import «eClass.packageName».«eClass.name»;
		
		
		«IF eClass.requiresFeaturePackageImport»
			import «packageFragment.elementName».«typeName.toLowerCase»_feature.*;
		«ENDIF»
		
		public class «typeName»Transformation extends StaticInterfaceTransformation<«eClassName»> {
		
			public «typeName»Transformation(AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
				super(parentTransformation, «eClassInstantiation»);
			}
		
			«createCrossReferencesTrasformations»
			
			«createChildTransformationsToCode»
		
			«createChildTransformationsToModel»
		}''';

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		if (!(codeElement instanceof IType))
			return false;
		val IType type = codeElement as IType;
		if (!type.isInterface)
			return false;

		return type.superInterfaceNames.stream.anyMatch(
			s | s.equals(metaModelElement.name.toFirstUpper)
		);
	}
}
