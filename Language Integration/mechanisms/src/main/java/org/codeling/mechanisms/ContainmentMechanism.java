package org.codeling.mechanisms;

import org.eclipse.emf.ecore.EReference;

public abstract class ContainmentMechanism extends Mechanism {
	protected EReference eReference;

	public void setEReference(EReference eReference) {
		this.eReference = eReference;
	}
}