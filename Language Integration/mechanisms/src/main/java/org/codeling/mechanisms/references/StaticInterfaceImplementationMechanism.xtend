package org.codeling.mechanisms.references

import org.codeling.mechanisms.ReferenceMechanism
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EReference
import org.eclipse.jdt.core.IJavaElement
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IType

class StaticInterfaceImplementationMechanism extends ReferenceMechanism {

	override getName() {
		"Static Interface Implementation"
	}

	override createMetaModelLibrary(IPackageFragment packageFragment, ENamedElement element) {
	}

	override createRuntime(IPackageFragment packageFragment, ENamedElement element) {
		
	}

	override createTransformation(IPackageFragment packageFragment, ENamedElement element) {
		var String typeName = element.name.toFirstUpper;
		val EReference eReference = element as EReference;
		val String eReferenceInstantiation = eReference.
			EFeatureInstantiation

		val content = '''
			package «packageFragment.elementName»;
			
			import java.util.List;
			
			import org.codeling.mechanisms.transformations.references.StaticInterfaceImplementationTransformation;
			import org.eclipse.emf.ecore.EObject;
			import org.eclipse.jdt.core.IJavaElement;
			import org.eclipse.jdt.core.IType;
			
			import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
			import «eReference.packageName».«eReference.packageName»Package;
			import «eReference.packageName».«eReference.EContainingClass.name»;
			import «eReference.packageName».«eReference.EType.name»;
			import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
			import org.codeling.lang.«languageName».transformation.«eReference.EType.name»Transformation;
			
			public class «typeName»Transformation
					extends StaticInterfaceImplementationTransformation<«eReference.EContainingClass.name», «eReference.EType.name»> {
			
				public «typeName»Transformation(ClassMechanismTransformation<«eReference.EContainingClass.name», IType> parentTransformation) {
					super(parentTransformation, «eReferenceInstantiation»);
				}
			
				@Override
					public void doCreateCrossReferencesTransformations(
							List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			
				@Override
				protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
						EObject targetModelElement) {
					return new «eReference.EType.name»Transformation(this);
				}
			
				@Override
				protected void doCreateChildTransformationsToModel(
						List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
					// TODO Auto-generated method stub
				}
			}
		'''

		packageFragment.createCompilationUnit('''«typeName»Transformation.java''', content, true, monitor);
	}

	override canHandle(IJavaElement codeElement, ENamedElement metaModelElement) {
		return codeElement instanceof IType && (codeElement as IType).fields.stream.anyMatch(
			f |
				f.getAnnotation(metaModelElement.name.toFirstUpper).exists
		);
	}

}
