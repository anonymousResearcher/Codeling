package org.codeling.mechanisms

import java.util.LinkedList
import java.util.List
import org.codeling.mechanisms.classes.ContainmentOperationMechanism
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EReference

abstract class ClassMechanism extends Mechanism {
	var EClass eClass;

	def void setEClass(EClass eClass) {
		this.eClass = eClass;
	}

	protected def createChildTransformationsToCode() {
		return '''
		@Override
		protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		«IF !eClass.EAttributes.isEmpty»
			// Child transformations for attributes
			«FOR feature : eClass.EAttributes»
				«IF !("name".equals(feature.name) || "id".equals(feature.name))»
					result.add(new «feature.name.toFirstUpper»Transformation(this));
				«ENDIF»
			«ENDFOR»
		«ENDIF»
		
		«IF !eClass.EReferences.isEmpty»
			// Child transformations for containment references
			«FOR feature : eClass.EReferences»
				«IF (feature as EReference).isContainment»
					result.add(new «feature.name.toFirstUpper»Transformation(this));
				«ENDIF»
			«ENDFOR»
		«ENDIF»
		}''';
	}

	def createChildTransformationsToModel() {
		return '''
			@Override
			protected void doCreateChildTransformationsToModel(List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
			«IF !eClass.EAttributes.isEmpty»
				// Child transformations for attributes
				«FOR feature : eClass.EAttributes»
					«IF !("name".equals(feature.name) || "id".equals(feature.name))»
						result.add(new «feature.name.toFirstUpper»Transformation(this));
					«ENDIF»
				«ENDFOR»
			«ENDIF»
			
			«IF !eClass.EReferences.isEmpty»
				// Child transformations for containment references
				«FOR feature : eClass.EReferences»
					«IF (feature as EReference).isContainment»
						result.add(new «feature.name.toFirstUpper»Transformation(this));
					«ENDIF»
				«ENDFOR»
			«ENDIF»
			}
		''';
	}

	def createCrossReferencesTrasformations() {
		return '''
		@Override
		public void doCreateCrossReferencesTransformations(
				List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		«IF !eClass.EReferences.isEmpty»
			// Child transformations for cross references
			«FOR feature : eClass.EReferences»
				«IF !(feature as EReference).isContainment»
					result.add(new «feature.name.toFirstUpper»Transformation(this));
				«ENDIF»
			«ENDFOR»
		«ENDIF»
		}''';
	}

	def boolean requiresFeaturePackageImport(EClass eClass) {
		return eClass.EStructuralFeatures.filter[f|!("name".equals(f.name) || "id".equals(f.name))].size > 0;
	}

	def String getAttributeRuntimeInitialization() {
		val List<EAttribute> features = new LinkedList();
		features.addAll(eClass.EAttributes.filter[f|!("name".equals(f.name) || "id".equals(f.name))].toList);

		return '''
			«IF !features.isEmpty»
				// Attribute Runtimes
				«FOR feature : features»
					«feature.name.toFirstLower»Runtime = new org.codeling.lang.«languageName».runtime.«eClass.name.toLowerCase»_feature.«feature.name.toFirstUpper»Runtime(this);
					«feature.name.toFirstLower»Runtime.initialize();
					
				«ENDFOR»
			«ENDIF»
		''';
	}

	def String getContainmentReferenceRuntimeInitialization() {
		return getReferenceRuntimeInitialization(true);
	}

	def String getCrossReferenceRuntimeInitialization() {
		return getReferenceRuntimeInitialization(false);
	}

	def String getReferenceRuntimeInitialization(boolean containment) {
		val Iterable<EReference> references = eClass.EReferences.filter[f|f.isContainment == containment];
		return '''
			«IF !references.isEmpty»
				// Reference Runtimes
				«FOR feature : references»
					«feature.name.toFirstLower»Runtime = new org.codeling.lang.«languageName».runtime.«eClass.name.toLowerCase»_feature.«feature.name.toFirstUpper»Runtime(this);
					«feature.name.toFirstLower»Runtime.initialize();
					«IF ContainmentOperationMechanism.isAssignableFrom(MechanismsMapping.getInstance().get(feature))»
						«IF containment»
							«feature.name.toFirstLower»Runtime.initializeContainments();
							«feature.name.toFirstLower»Runtime.initializeCrossReferences();
						«ENDIF»
					«ENDIF»
					
				«ENDFOR»
			«ENDIF»
		''';
	}

	def String getAttributeRuntimeFields() {
		val List<EAttribute> features = new LinkedList();
		features.addAll(eClass.EAttributes.filter[f|!("name".equals(f.name) || "id".equals(f.name))].toList);

		return '''
			«IF !features.isEmpty»
				// Attribute Runtimes
				«FOR feature : features»
					org.codeling.lang.«languageName».runtime.«eClass.name.toLowerCase»_feature.«feature.name.toFirstUpper»Runtime «feature.name.toFirstLower»Runtime;
				«ENDFOR»
			«ENDIF»
		'''
	}

	def String getReferenceRuntimeFields() {
		return '''
			«IF !eClass.EReferences.isEmpty»
				// Reference Runtimes
				«FOR feature : eClass.EReferences»
					org.codeling.lang.«languageName».runtime.«eClass.name.toLowerCase»_feature.«feature.name.toFirstUpper»Runtime «feature.name.toFirstLower»Runtime;
				«ENDFOR»
			«ENDIF»
		'''
	}

	def String getReferenceRuntimeFieldGetters() {
		return '''
			«IF !eClass.EReferences.isEmpty»
				«FOR feature : eClass.EReferences»
					public org.codeling.lang.«languageName».runtime.«eClass.name.toLowerCase»_feature.«feature.name.toFirstUpper»Runtime get«feature.name.toFirstUpper»Runtime() {
						return «feature.name.toFirstLower»Runtime;
					}
					
				«ENDFOR»
			«ENDIF»
		'''
	}

	def String createImportsForTargetTransformations() {
		return '''
			«IF !eClass.EStructuralFeatures.isEmpty»
				«FOR feature : eClass.EStructuralFeatures»
					import org.codeling.lang.«languageName».transformation.«eClass.name.toFirstLower»_feature.«feature»Transformation;
				«ENDFOR»
			«ENDIF»
		'''
	}

}
