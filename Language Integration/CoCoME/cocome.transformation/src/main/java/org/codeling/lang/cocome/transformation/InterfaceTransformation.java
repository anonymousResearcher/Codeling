package org.codeling.lang.cocome.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Interface;
import org.codeling.lang.cocome.transformation.interface_feature.OperationsTransformation;
import org.codeling.mechanisms.transformations.classes.StaticInterfaceTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class InterfaceTransformation extends StaticInterfaceTransformation<Interface> {

	public InterfaceTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getInterface());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// Child transformations for cross references
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

		// Child transformations for containment references
		result.add(new OperationsTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

		// Child transformations for containment references
		result.add(new OperationsTransformation(this));
	}

	@Override
	public boolean hasExpectedAnnotation(IType type) {
		return true;
	}

	@Override
	protected String getNewAnnotationName() {
		return null;
	}
}