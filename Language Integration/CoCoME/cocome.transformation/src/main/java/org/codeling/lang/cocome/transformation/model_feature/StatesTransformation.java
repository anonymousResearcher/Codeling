package org.codeling.lang.cocome.transformation.model_feature;

import java.util.LinkedList;
import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Model;
import org.codeling.lang.cocome.mm.CoCoME.State;
import org.codeling.lang.cocome.transformation.StateTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class StatesTransformation extends ReferenceMechanismTransformation<Model, State, IType> {

	LinkedList<String> targetNames = new LinkedList<>();

	public StatesTransformation(ClassMechanismTransformation<Model, IType> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getModel_States());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		ICompilationUnit[] cus = new ICompilationUnit[0];
		try {
			cus = codeElement.getPackageFragment().getCompilationUnits();
		} catch (JavaModelException e) {
			addError("Could not get CompilationUnits", e);
		}
		for (ICompilationUnit cu : cus) {
			if (cu.getElementName().endsWith("State.java")) {
				try {
					IType toType = cu.getTypes()[0];
					for (IField f : toType.getFields()) {
						targetNames.add(toType.getElementName().substring(0,
								toType.getElementName().length() - "State".length()));
						StateTransformation t = new StateTransformation(this);
						t.setCodeElement(f);
						result.add(t);
					}
				} catch (JavaModelException e) {
					addError("Could not get main type from compilation unit " + cu.getElementName()
							+ ". Ignoring the unit.");
				}
			}
		}
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public Model transformToModel() throws CodelingException {
		List<State> targetElements = getTargetObjects(modelElement, eReference, targetNames);
		((List<State>) modelElement.eGet(eReference)).addAll(targetElements);
		return modelElement;
	}
}
