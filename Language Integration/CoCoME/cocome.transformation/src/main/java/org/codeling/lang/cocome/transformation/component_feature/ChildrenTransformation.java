package org.codeling.lang.cocome.transformation.component_feature;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.transformation.ComponentTransformation;
import org.codeling.lang.cocome.transformation.ConsoleTransformation;
import org.codeling.lang.cocome.transformation.ModelTransformation;
import org.codeling.lang.cocome.transformation.ServerTransformation;
import org.codeling.lang.cocome.transformation.architecture_feature.ComponentsTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class ChildrenTransformation<OWNINGCOMPONENTTYPE extends Component, TARGETCOMPONENTTYPE extends Component, CODEELEMENT extends IJavaElement>
		extends ReferenceMechanismTransformation<OWNINGCOMPONENTTYPE, TARGETCOMPONENTTYPE, CODEELEMENT> {

	public ChildrenTransformation(ClassMechanismTransformation<OWNINGCOMPONENTTYPE, CODEELEMENT> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getComponent_Children());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		try {
			for (IPackageFragment f : ComponentsTransformation
					.findSubpackageSubcomponents(getCurrentPackageFragment())) {
				ComponentTransformation t = new ComponentTransformation(this);
				t.setCodeElement(f);
				result.add(t);
			}

			for (IPackageFragment f : ASTUtils.getDirectChildPackageFragmentsOf(getCurrentPackageFragment())) {
				for (IType type : ComponentsTransformation.findTypeSubcomponents(f)) {
					if (type == codeElement)
						continue;
					// Is a direct subpackage of this type's package
					AbstractModelCodeTransformation<?, IType> t = null;
					ServerTransformation serverT = new ServerTransformation(this);
					ConsoleTransformation consoleT = new ConsoleTransformation(this);
					ModelTransformation modelT = new ModelTransformation(this);

					if (serverT.isServer(type))
						t = serverT;
					else if (consoleT.isConsole(type))
						t = consoleT;
					else if (modelT.isModel(type))
						t = modelT;
					else
						continue;

					t.setCodeElement(type);
					result.add(t);
				}
			}
		} catch (Exception e) {
			addError("Error while getting types.", e);
		}
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public OWNINGCOMPONENTTYPE transformToModel() throws CodelingException {
		if (modelElement == null)
			throw new IllegalStateException(
					"modelElement must not be null. Please set the field value to the object that owns the reference.");

		List<String> targets = new LinkedList<>();
		try {
			targets.addAll(ComponentsTransformation.findSubpackageSubcomponents(getCurrentPackageFragment()).stream()
					.map(f -> f.getElementName()).collect(Collectors.toList()));
			targets.addAll(
					ComponentsTransformation.findTypeSubcomponents(getCurrentPackageFragment()).stream().map(t -> {
						String name = t.getElementName();
						if (name.endsWith("Model"))
							return name.substring(0, "Model".length());
						else if (name.endsWith("Server"))
							return name.substring(0, "Server".length());
						else if (name.endsWith("Console"))
							return name.substring(0, "Console".length());
						return name;
					}).collect(Collectors.toList()));
		} catch (JavaModelException e) {
			addError("Error while finding subpackge components.", e);
		}

		List<TARGETCOMPONENTTYPE> targetElements = getTargetObjects(modelElement, eReference, targets);
		if (eReference.isMany()) {
			((List<Component>) modelElement.eGet(eReference)).addAll(targetElements);
		} else {
			modelElement.eSet(eReference, targetElements.get(0));
		}

		return modelElement;
	}

	private IPackageFragment getCurrentPackageFragment() {
		if (codeElement instanceof IPackageFragment)
			return (IPackageFragment) codeElement;
		else if (codeElement instanceof IType)
			return ((IType) codeElement).getPackageFragment();
		else
			throw new IllegalArgumentException(
					"codeElement of " + getClass().getName() + " must be an IPackageFragment or IType");
	}

}
