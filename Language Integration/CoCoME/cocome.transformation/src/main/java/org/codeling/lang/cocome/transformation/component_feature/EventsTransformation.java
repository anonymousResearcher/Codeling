package org.codeling.lang.cocome.transformation.component_feature;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.mm.CoCoME.Event;
import org.codeling.lang.cocome.transformation.EventTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class EventsTransformation<COMPONENTTYPE extends Component, OWNERTYPE extends IJavaElement> extends ReferenceMechanismTransformation<COMPONENTTYPE, Event, OWNERTYPE> {

	public EventsTransformation(ClassMechanismTransformation<COMPONENTTYPE, OWNERTYPE> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getComponent_Events());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		try {
			for (IType type : getEventTypes()) {
				EventTransformation t = new EventTransformation(this);
				t.setCodeElement(type);
				result.add(t);
			}
		} catch (JavaModelException e) {
			addError("Could not get event types.", e);
		}
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public COMPONENTTYPE transformToModel() throws CodelingException {
		try {
			List<String> targets = getEventTypes().stream()
					.map(t -> t.getElementName().substring(0, t.getElementName().length() - "Event".length()))
					.collect(Collectors.toList());

			List<Event> targetElements = getTargetObjects(modelElement, eReference, targets);
			((List<Event>) modelElement.eGet(eReference)).addAll(targetElements);
		} catch (JavaModelException e) {
			addError("Could not transform reference Architecture.events to model representation.", e);
		}
		return modelElement;
	}

	private List<IType> getEventTypes() throws JavaModelException {
		List<IType> types = new LinkedList<>();
		List<IPackageFragment> childFragments = ASTUtils.getDirectChildPackageFragmentsOf(getCurrentPackageFragment());
		for (IPackageFragment child : childFragments) {
			if (child.getElementName().endsWith(".events")) {
				for (ICompilationUnit u : child.getCompilationUnits()) {
					types.add(
							u.getType(u.getElementName().substring(0, u.getElementName().length() - ".java".length())));
				}
				break;
			}
		}
		return types;
	}

	private IPackageFragment getCurrentPackageFragment() {
		if (codeElement instanceof IPackageFragment)
			return (IPackageFragment) codeElement;
		else if (codeElement instanceof IType)
			return ((IType) codeElement).getPackageFragment();
		else
			throw new IllegalArgumentException(
					"codeElement of " + getClass().getName() + " must be an IPackageFragment or IType");
	}
}
