package org.codeling.lang.cocome.transformation.interface_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Interface;
import org.codeling.lang.cocome.mm.CoCoME.Operation;
import org.codeling.lang.cocome.transformation.OperationTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.ContainmentOperationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;

public class OperationsTransformation extends ContainmentOperationTransformation<Interface, Operation> {

	public OperationsTransformation(ClassMechanismTransformation<Interface, IType> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getInterface_Operations());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		resolveCodeElement();
		for (IMethod m : methods) {
			OperationTransformation t = new OperationTransformation(this);
			t.setCodeElement(m);
			result.add(t);
		}
	}

	@Override
	public boolean hasExpectedAnnotation(IMethod method) {
		return true;
	}

	@Override
	protected String getNewAnnotationName() {
		return null;
	}
}
