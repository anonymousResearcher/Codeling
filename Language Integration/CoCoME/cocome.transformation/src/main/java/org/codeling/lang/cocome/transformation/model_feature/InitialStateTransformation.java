package org.codeling.lang.cocome.transformation.model_feature;

import java.util.LinkedList;
import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Model;
import org.codeling.lang.cocome.mm.CoCoME.State;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class InitialStateTransformation extends ReferenceMechanismTransformation<Model, State, IType> {

	public InitialStateTransformation(ClassMechanismTransformation<Model, IType> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getModel_InitialState());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public Model transformToModel() throws CodelingException {
		LinkedList<String> targetNames = new LinkedList<>();
		try {
			for (IField f : codeElement.getFields()) {
				if (f.getElementName().equals("state")) {
					String value = ASTUtils.getValue(f);
					if (value != null) {
						int dotIndex = value.indexOf(".");
						if (dotIndex == -1)
							continue;
						String stateName = value.substring(dotIndex + 1, value.length());
						targetNames.add(stateName);
					}
				}
			}
		} catch (JavaModelException e) {
			addError("Could not evaluate fields of Model for initial state", e);
			return modelElement;
		}

		List<State> targetElements = getTargetObjects(modelElement, eReference, targetNames);
		if (!targetElements.isEmpty())
			modelElement.eSet(eReference, targetElements.get(0));
		return modelElement;
	}
}
