package org.codeling.lang.cocome.transformation.component_feature;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.mm.CoCoME.Interface;
import org.codeling.lang.cocome.transformation.InterfaceTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.StaticInterfaceImplementationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;

public class ProvidesTransformation<COMPONENTTYPE extends Component>
		extends StaticInterfaceImplementationTransformation<COMPONENTTYPE, Interface> {

	public ProvidesTransformation(ClassMechanismTransformation<COMPONENTTYPE, IType> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getComponent_Provides());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		try {
			String[] superInterfaceSignatures = codeElement.getSuperInterfaceTypeSignatures();
			for (String sig : superInterfaceSignatures) {

				IType iface = ASTUtils.getType(Signature.toString(sig), codeElement);
				InterfaceTransformation t = new InterfaceTransformation(this);
				t.setCodeElement(iface);
				result.add(t);
			}
		} catch (JavaModelException e) {
			addError("Could not get the IType of a super interface", e);
		}

	}
}
