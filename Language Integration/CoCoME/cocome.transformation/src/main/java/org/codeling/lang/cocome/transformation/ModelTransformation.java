package org.codeling.lang.cocome.transformation;

import java.text.MessageFormat;
import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEFactory;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.mm.CoCoME.Console;
import org.codeling.lang.cocome.mm.CoCoME.Model;
import org.codeling.lang.cocome.transformation.component_feature.ChildrenTransformation;
import org.codeling.lang.cocome.transformation.component_feature.DispatchedTransformation;
import org.codeling.lang.cocome.transformation.component_feature.EventsTransformation;
import org.codeling.lang.cocome.transformation.component_feature.HandledTransformation;
import org.codeling.lang.cocome.transformation.component_feature.ProvidesTransformation;
import org.codeling.lang.cocome.transformation.component_feature.RequiresTransformation;
import org.codeling.lang.cocome.transformation.model_feature.InitialStateTransformation;
import org.codeling.lang.cocome.transformation.model_feature.StatesTransformation;
import org.codeling.lang.cocome.transformation.model_feature.TransitionsTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;

public class ModelTransformation extends ClassMechanismTransformation<Model, IType> {

	public ModelTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getModel());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new RequiresTransformation<Model>(this));
		result.add(new HandledTransformation<Model,IType>(this));
		result.add(new DispatchedTransformation<Model,IType>(this));

		// Child transformations for cross references
		result.add(new InitialStateTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ChildrenTransformation<Model, Component, IType>(this));
		result.add(new ProvidesTransformation<Model>(this));
		result.add(new EventsTransformation<Model,IType>(this));

		// Child transformations for containment references
		result.add(new StatesTransformation(this));
		result.add(new TransitionsTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ChildrenTransformation<Model, Console, IType>(this));
		result.add(new ProvidesTransformation<Model>(this));
		result.add(new EventsTransformation<Model,IType>(this));

		// Child transformations for containment references
		result.add(new StatesTransformation(this));
		result.add(new TransitionsTransformation(this));
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public Model transformToModel() throws CodelingException {
		modelElement = CoCoMEFactory.eINSTANCE.createModel();
		modelElement.setName(getNameFromCodeElement(codeElement));
		return modelElement;
	}

	public boolean isModel(IType type) {
		String name = getNameFromCodeElement(type);
		if (name == null)
			return false;

		String superclassSignature;
		try {
			superclassSignature = type.getSuperclassTypeSignature();
			if (superclassSignature == null)
				return false;
		} catch (JavaModelException e) {
			addError(MessageFormat.format(
					"Could not get superclass type signature of type [{0}]. Not considering the class as Component.",
					type.getFullyQualifiedName()));
			return false;
		}

		String elementType = Signature.getSignatureSimpleName(Signature.getTypeErasure(superclassSignature));
		if (!elementType.equals("AbstractModel"))
			return false;
		String[] typeArguments = Signature.getTypeArguments(superclassSignature);
		return typeArguments.length == 1
				&& Signature.getSignatureSimpleName(typeArguments[0]).equals(type.getElementName());
	}

	private String getNameFromCodeElement(IType type) {
		String name = type.getElementName();
		if (name.endsWith("Model")) {
			return name;
		} else
			return null;
	}
}