package org.codeling.lang.cocome.transformation.component_feature;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.java.transformation.IModelCodeTransformation;
import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.mm.CoCoME.Interface;
import org.codeling.lang.cocome.transformation.InterfaceTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public class RequiresTransformation<COMPONENTTYPE extends Component> extends ReferenceMechanismTransformation<COMPONENTTYPE, Interface, IType> {

	private List<IType> targetTypes = new LinkedList<>();

	public RequiresTransformation(ClassMechanismTransformation<COMPONENTTYPE, IType> parentTransformation) {
		super(parentTransformation, CoCoMEPackage.eINSTANCE.getComponent_Requires());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return null;
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public COMPONENTTYPE transformToModel() throws CodelingException {
		// Find the element that is referenced by the field's type and set it as
		// eReference value
		List<String> typeNames = resolveTargetTypes().stream().map(t -> t.getElementName())
				.collect(Collectors.toList());

		List<Interface> targetElements = getTargetObjects(modelElement, eReference, typeNames);
		((List<Interface>) modelElement.eGet(eReference)).addAll(targetElements);

		return modelElement;
	}

	public List<IType> resolveTargetTypes() {
		if (!targetTypes.isEmpty())
			return targetTypes;

		try {
			// Evaluate methods
			IMethod[] methods = codeElement.getMethods();
			for (IMethod m : methods) {
				try {
					String[] parameterTypes = m.getParameterTypes();
					// Evaluate constructors
					if (m.isConstructor() || m.getElementName().equals("createAndShowGUI")) {
						for (String parameter : parameterTypes) {
							String typeName = ASTUtils.resolveTypeSignature(parameter, codeElement);
							IType targetType = codeElement.getJavaProject().findType(typeName);
							if (isHandledByInterfaceTransformation(targetType)) {
								if (!targetTypes.contains(targetType))
									targetTypes.add(targetType);
							}
						}
					}
				} catch (Exception e) {
					addWarning("Could not evaluate method " + m.getElementName() + " for Component.requires", e);
				}
			}

		} catch (JavaModelException e) {
			addError("Could not resolve targets", e);
		}

		return targetTypes;
	}

	private boolean isHandledByInterfaceTransformation(IType type) {
		List<AbstractModelCodeTransformation<?, ?>> ts = getWhereToFindTranslatedElements().getTransformationsFor(type);
		for (IModelCodeTransformation<?, ?> t : ts) {
			if (t instanceof InterfaceTransformation) {
				return true;
			}
		}
		return false;
	}
}
