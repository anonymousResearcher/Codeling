/**
 */
package org.codeling.lang.cocome.mm.CoCoME;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Console</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getConsole()
 * @model
 * @generated
 */
public interface Console extends Component {
} // Console
