/**
 */
package org.codeling.lang.cocome.mm.CoCoME.impl;

import java.util.Collection;

import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Component;
import org.codeling.lang.cocome.mm.CoCoME.Event;
import org.codeling.lang.cocome.mm.CoCoME.Interface;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.impl.ComponentImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.impl.ComponentImpl#getProvides <em>Provides</em>}</li>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.impl.ComponentImpl#getRequires <em>Requires</em>}</li>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.impl.ComponentImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.impl.ComponentImpl#getHandled <em>Handled</em>}</li>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.impl.ComponentImpl#getDispatched <em>Dispatched</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentImpl extends NameImpl implements Component {
	/**
	 * The cached value of the '{@link #getChildren() <em>Children</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EList<Component> children;

	/**
	 * The cached value of the '{@link #getProvides() <em>Provides</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvides()
	 * @generated
	 * @ordered
	 */
	protected EList<Interface> provides;

	/**
	 * The cached value of the '{@link #getRequires() <em>Requires</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequires()
	 * @generated
	 * @ordered
	 */
	protected EList<Interface> requires;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * The cached value of the '{@link #getHandled() <em>Handled</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandled()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> handled;

	/**
	 * The cached value of the '{@link #getDispatched() <em>Dispatched</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDispatched()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> dispatched;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoCoMEPackage.Literals.COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Component> getChildren() {
		if (children == null) {
			children = new EObjectContainmentEList<Component>(Component.class, this, CoCoMEPackage.COMPONENT__CHILDREN);
		}
		return children;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Interface> getProvides() {
		if (provides == null) {
			provides = new EObjectContainmentEList<Interface>(Interface.class, this, CoCoMEPackage.COMPONENT__PROVIDES);
		}
		return provides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Interface> getRequires() {
		if (requires == null) {
			requires = new EObjectResolvingEList<Interface>(Interface.class, this, CoCoMEPackage.COMPONENT__REQUIRES);
		}
		return requires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectContainmentEList<Event>(Event.class, this, CoCoMEPackage.COMPONENT__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getHandled() {
		if (handled == null) {
			handled = new EObjectResolvingEList<Event>(Event.class, this, CoCoMEPackage.COMPONENT__HANDLED);
		}
		return handled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getDispatched() {
		if (dispatched == null) {
			dispatched = new EObjectResolvingEList<Event>(Event.class, this, CoCoMEPackage.COMPONENT__DISPATCHED);
		}
		return dispatched;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoCoMEPackage.COMPONENT__CHILDREN:
				return ((InternalEList<?>)getChildren()).basicRemove(otherEnd, msgs);
			case CoCoMEPackage.COMPONENT__PROVIDES:
				return ((InternalEList<?>)getProvides()).basicRemove(otherEnd, msgs);
			case CoCoMEPackage.COMPONENT__EVENTS:
				return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoCoMEPackage.COMPONENT__CHILDREN:
				return getChildren();
			case CoCoMEPackage.COMPONENT__PROVIDES:
				return getProvides();
			case CoCoMEPackage.COMPONENT__REQUIRES:
				return getRequires();
			case CoCoMEPackage.COMPONENT__EVENTS:
				return getEvents();
			case CoCoMEPackage.COMPONENT__HANDLED:
				return getHandled();
			case CoCoMEPackage.COMPONENT__DISPATCHED:
				return getDispatched();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoCoMEPackage.COMPONENT__CHILDREN:
				getChildren().clear();
				getChildren().addAll((Collection<? extends Component>)newValue);
				return;
			case CoCoMEPackage.COMPONENT__PROVIDES:
				getProvides().clear();
				getProvides().addAll((Collection<? extends Interface>)newValue);
				return;
			case CoCoMEPackage.COMPONENT__REQUIRES:
				getRequires().clear();
				getRequires().addAll((Collection<? extends Interface>)newValue);
				return;
			case CoCoMEPackage.COMPONENT__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends Event>)newValue);
				return;
			case CoCoMEPackage.COMPONENT__HANDLED:
				getHandled().clear();
				getHandled().addAll((Collection<? extends Event>)newValue);
				return;
			case CoCoMEPackage.COMPONENT__DISPATCHED:
				getDispatched().clear();
				getDispatched().addAll((Collection<? extends Event>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoCoMEPackage.COMPONENT__CHILDREN:
				getChildren().clear();
				return;
			case CoCoMEPackage.COMPONENT__PROVIDES:
				getProvides().clear();
				return;
			case CoCoMEPackage.COMPONENT__REQUIRES:
				getRequires().clear();
				return;
			case CoCoMEPackage.COMPONENT__EVENTS:
				getEvents().clear();
				return;
			case CoCoMEPackage.COMPONENT__HANDLED:
				getHandled().clear();
				return;
			case CoCoMEPackage.COMPONENT__DISPATCHED:
				getDispatched().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoCoMEPackage.COMPONENT__CHILDREN:
				return children != null && !children.isEmpty();
			case CoCoMEPackage.COMPONENT__PROVIDES:
				return provides != null && !provides.isEmpty();
			case CoCoMEPackage.COMPONENT__REQUIRES:
				return requires != null && !requires.isEmpty();
			case CoCoMEPackage.COMPONENT__EVENTS:
				return events != null && !events.isEmpty();
			case CoCoMEPackage.COMPONENT__HANDLED:
				return handled != null && !handled.isEmpty();
			case CoCoMEPackage.COMPONENT__DISPATCHED:
				return dispatched != null && !dispatched.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComponentImpl
