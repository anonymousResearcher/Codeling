/**
 */
package org.codeling.lang.cocome.mm.CoCoME;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Server</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.Server#getTransferObjects <em>Transfer Objects</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getServer()
 * @model
 * @generated
 */
public interface Server extends Component {
	/**
	 * Returns the value of the '<em><b>Transfer Objects</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.cocome.mm.CoCoME.TransferObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transfer Objects</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transfer Objects</em>' containment reference list.
	 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getServer_TransferObjects()
	 * @model containment="true"
	 * @generated
	 */
	EList<TransferObject> getTransferObjects();

} // Server
