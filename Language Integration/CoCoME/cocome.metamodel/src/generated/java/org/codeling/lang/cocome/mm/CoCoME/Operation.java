/**
 */
package org.codeling.lang.cocome.mm.CoCoME;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getOperation()
 * @model
 * @generated
 */
public interface Operation extends Name {
} // Operation
