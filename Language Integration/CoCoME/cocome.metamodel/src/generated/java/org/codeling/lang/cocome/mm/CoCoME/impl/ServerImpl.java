/**
 */
package org.codeling.lang.cocome.mm.CoCoME.impl;

import java.util.Collection;

import org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage;
import org.codeling.lang.cocome.mm.CoCoME.Server;
import org.codeling.lang.cocome.mm.CoCoME.TransferObject;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Server</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.impl.ServerImpl#getTransferObjects <em>Transfer Objects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ServerImpl extends ComponentImpl implements Server {
	/**
	 * The cached value of the '{@link #getTransferObjects() <em>Transfer Objects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransferObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<TransferObject> transferObjects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoCoMEPackage.Literals.SERVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransferObject> getTransferObjects() {
		if (transferObjects == null) {
			transferObjects = new EObjectContainmentEList<TransferObject>(TransferObject.class, this, CoCoMEPackage.SERVER__TRANSFER_OBJECTS);
		}
		return transferObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoCoMEPackage.SERVER__TRANSFER_OBJECTS:
				return ((InternalEList<?>)getTransferObjects()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoCoMEPackage.SERVER__TRANSFER_OBJECTS:
				return getTransferObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoCoMEPackage.SERVER__TRANSFER_OBJECTS:
				getTransferObjects().clear();
				getTransferObjects().addAll((Collection<? extends TransferObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoCoMEPackage.SERVER__TRANSFER_OBJECTS:
				getTransferObjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoCoMEPackage.SERVER__TRANSFER_OBJECTS:
				return transferObjects != null && !transferObjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ServerImpl
