/**
 */
package org.codeling.lang.cocome.mm.CoCoME;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getState()
 * @model
 * @generated
 */
public interface State extends Name {
} // State
