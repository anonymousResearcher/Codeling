/**
 */
package org.codeling.lang.cocome.mm.CoCoME;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends Name {
} // Event
