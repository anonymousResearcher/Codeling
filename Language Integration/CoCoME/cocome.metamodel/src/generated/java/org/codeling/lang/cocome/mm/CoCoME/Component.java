/**
 */
package org.codeling.lang.cocome.mm.CoCoME;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.Component#getChildren <em>Children</em>}</li>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.Component#getProvides <em>Provides</em>}</li>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.Component#getRequires <em>Requires</em>}</li>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.Component#getEvents <em>Events</em>}</li>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.Component#getHandled <em>Handled</em>}</li>
 *   <li>{@link org.codeling.lang.cocome.mm.CoCoME.Component#getDispatched <em>Dispatched</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getComponent()
 * @model
 * @generated
 */
public interface Component extends Name {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.cocome.mm.CoCoME.Component}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getComponent_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<Component> getChildren();

	/**
	 * Returns the value of the '<em><b>Provides</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.cocome.mm.CoCoME.Interface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provides</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provides</em>' containment reference list.
	 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getComponent_Provides()
	 * @model containment="true"
	 * @generated
	 */
	EList<Interface> getProvides();

	/**
	 * Returns the value of the '<em><b>Requires</b></em>' reference list.
	 * The list contents are of type {@link org.codeling.lang.cocome.mm.CoCoME.Interface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requires</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requires</em>' reference list.
	 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getComponent_Requires()
	 * @model
	 * @generated
	 */
	EList<Interface> getRequires();

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.cocome.mm.CoCoME.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getComponent_Events()
	 * @model containment="true"
	 * @generated
	 */
	EList<Event> getEvents();

	/**
	 * Returns the value of the '<em><b>Handled</b></em>' reference list.
	 * The list contents are of type {@link org.codeling.lang.cocome.mm.CoCoME.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Handled</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handled</em>' reference list.
	 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getComponent_Handled()
	 * @model
	 * @generated
	 */
	EList<Event> getHandled();

	/**
	 * Returns the value of the '<em><b>Dispatched</b></em>' reference list.
	 * The list contents are of type {@link org.codeling.lang.cocome.mm.CoCoME.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatched</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatched</em>' reference list.
	 * @see org.codeling.lang.cocome.mm.CoCoME.CoCoMEPackage#getComponent_Dispatched()
	 * @model
	 * @generated
	 */
	EList<Event> getDispatched();

} // Component
