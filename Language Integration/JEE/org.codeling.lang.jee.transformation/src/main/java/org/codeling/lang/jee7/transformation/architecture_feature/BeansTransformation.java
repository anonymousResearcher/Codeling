package org.codeling.lang.jee7.transformation.architecture_feature;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Architecture;
import org.codeling.lang.jee7.mm.JEE7.Bean;
import org.codeling.lang.jee7.mm.JEE7.CDIBean;
import org.codeling.lang.jee7.mm.JEE7.FacesBean;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.SessionBean;
import org.codeling.lang.jee7.transformation.CDIBeanTransformation;
import org.codeling.lang.jee7.transformation.FacesBeanTransformation;
import org.codeling.lang.jee7.transformation.SessionBeanTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.NinjaSingletonContainmentTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class BeansTransformation extends NinjaSingletonContainmentTransformation<Architecture, Bean> {

	public BeansTransformation(ClassMechanismTransformation<Architecture, IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getArchitecture_Beans());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	protected ClassMechanismTransformation<? extends Bean, IType> createSpecificTransformation(EObject modelElement) {
		if (modelElement instanceof CDIBean)
			return new CDIBeanTransformation(this);
		else if (modelElement instanceof SessionBean)
			return new SessionBeanTransformation(this);
		else if (modelElement instanceof FacesBean)
			return new FacesBeanTransformation(this);
		throw new IllegalArgumentException(
				"Model element must be instance of CDI, Session, or FacesBeanTransformation");
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		try {
			for (IType type : ASTUtils.getTypes(codeRoot)) {
				if (!type.isClass())
					continue;

				AbstractModelCodeTransformation<?, IType> t = null;
				CDIBeanTransformation cdiT = new CDIBeanTransformation(this);
				SessionBeanTransformation sbT = new SessionBeanTransformation(this);
				FacesBeanTransformation facesT = new FacesBeanTransformation(this);

				if (cdiT.hasExpectedAnnotation(type))
					t = cdiT;
				else if (sbT.hasExpectedAnnotation(type))
					t = sbT;
				else if (facesT.hasExpectedAnnotation(type))
					t = facesT;
				else
					continue;

				t.setCodeElement(type);
				result.add(t);
			}
		} catch (Exception e) {
			String projectName = "(no project name available)";
			if (codeRoot != null)
				projectName = codeElement.getElementName();
			addError("Error while getting types in project " + projectName, e);
		}
	}

}
