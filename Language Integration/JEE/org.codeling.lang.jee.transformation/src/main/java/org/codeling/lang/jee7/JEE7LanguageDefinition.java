package org.codeling.lang.jee7;

import java.util.Arrays;
import java.util.List;

import org.codeling.lang.base.java.IALTransformationTuple;
import org.codeling.lang.base.java.JavaBasedImplementationLanguageDefinition;
import org.codeling.lang.base.java.ProfilesUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.base.modeltrans.henshintgg.HenshinTGGBasedLanguageDefinitionHelper;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.transformation.ArchitectureTransformation;
import org.codeling.lang.jee7.transformation.bean_feature.ChildTypeTransformation;
import org.codeling.lang.jee7.transformation.operation_feature.TimeResourceDemandTransformation;
import org.codeling.mechanisms.MechanismsMapping;
import org.codeling.mechanisms.classes.NinjaSingletonMechanism;
import org.codeling.mechanisms.classes.ProjectMechanism;
import org.codeling.mechanisms.classes.TypeAnnotationMechanism;
import org.codeling.transformationmanager.TransformationResult;
import org.codeling.utils.CodelingException;
import org.codeling.utils.IDRegistry;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.osgi.framework.FrameworkUtil;

import de.mkonersmann.il.core.CorePackage;
import de.mkonersmann.il.profiles.Profiles;

public class JEE7LanguageDefinition extends JavaBasedImplementationLanguageDefinition {

	final URI henshinTGGFileURI = URI.createPlatformPluginURI(
			"/" + FrameworkUtil.getBundle(getClass()).getSymbolicName() + "/AIL2IAL.henshin", true);

	public JEE7LanguageDefinition() {
		// Create the mechanism mapping
		MechanismsMapping m = MechanismsMapping.getInstance();
		JEE7Package i = JEE7Package.eINSTANCE;
		m.put(i.getArchitecture(), NinjaSingletonMechanism.class);
		m.put(i.getArchive(), ProjectMechanism.class);
		m.put(i.getBean(), TypeAnnotationMechanism.class);
		m.put(i.getCDIBean(), TypeAnnotationMechanism.class);
		m.put(i.getSessionBean(), TypeAnnotationMechanism.class);
		m.put(i.getFacesBean(), TypeAnnotationMechanism.class);

		CorePackage core = CorePackage.eINSTANCE;
		m.put(core.getComponentType(), TypeAnnotationMechanism.class);

		// Create the map of component and their il meta model elements to reference
		// transformations
		// For each bean the reference childTypes of the stereotype application
		// HierarchicalComponentTypeScoped is translated via the given transformation
		// class.
		ialTransformations.put(i.getBean(),
				new IALTransformationTuple(
						ProfilesUtils.getEReference(Profiles.COMPONENTS_HIERARCHY_SCOPED.load(),
								"HierarchicalComponentTypeScoped", "childTypes"),
						Arrays.asList(ChildTypeTransformation.class)));
		ialTransformations.put(i.getOperation(),
				new IALTransformationTuple(
						ProfilesUtils.getEAttribute(Profiles.QUALITY_TIME.load(), "TimeResourceDemand", "duration"),
						Arrays.asList(TimeResourceDemandTransformation.class)));

		// Create the list of cross references
		for (final EClassifier c : i.getEClassifiers())
			if (!crossReferenceOrder.contains(c) && c instanceof EClass)
				crossReferenceOrder.add((EClass) c);
	}

	@Override
	public AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> createRootTransformationInstance() {
		return new ArchitectureTransformation();
	}

	@Override
	public TransformationResult transformIMToTM(List<EObject> cmRoots, IDRegistry idregistry) throws CodelingException {
		return new HenshinTGGBasedLanguageDefinitionHelper().transformIMToTM(this, henshinTGGFileURI, cmRoots,
				idregistry, monitor);
	}

	@Override
	public TransformationResult transformTMToIM(List<EObject> ilRoots, IDRegistry idRegistry) throws CodelingException {
		return new HenshinTGGBasedLanguageDefinitionHelper().transformTMToIM(this, henshinTGGFileURI, ilRoots,
				idRegistry, monitor);
	}

	@Override
	public List<String> getSelectedProfiles() {
		return new HenshinTGGBasedLanguageDefinitionHelper().getSelectedProfiles(henshinTGGFileURI);
	}

}
