package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.CDIBean;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.transformation.cdibean_feature.IsApplicationScopedTransformation;
import org.codeling.lang.jee7.transformation.cdibean_feature.IsConversationScopedTransformation;
import org.codeling.lang.jee7.transformation.cdibean_feature.IsRequestScopedTransformation;
import org.codeling.lang.jee7.transformation.cdibean_feature.IsSessionScopedTransformation;
import org.codeling.mechanisms.transformations.classes.TypeAnnotationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class CDIBeanTransformation extends TypeAnnotationTransformation<CDIBean> {

	public CDIBeanTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getCDIBean());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<?, ?>> result) {
		new BeanTransformation<CDIBean>().doCreateCrossReferencesTransformations(this, result);
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<?, ?>> result) {
		new BeanTransformation<CDIBean>().doCreateChildTransformationsToCode(this, result);

		// Child transformations for attributes
		result.add(new IsRequestScopedTransformation(this));
		result.add(new IsSessionScopedTransformation(this));
		result.add(new IsApplicationScopedTransformation(this));
		result.add(new IsConversationScopedTransformation(this));

	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		new BeanTransformation<CDIBean>().doCreateChildTransformationsToModel(this, result);

		// Child transformations for attributes
		result.add(new IsRequestScopedTransformation(this));
		result.add(new IsSessionScopedTransformation(this));
		result.add(new IsApplicationScopedTransformation(this));
		result.add(new IsConversationScopedTransformation(this));
	}

	@Override
	public boolean hasExpectedAnnotation(IType type) {
		return ASTUtils.hasAnnotation(type, "javax.enterprise.context.RequestScoped",
				"javax.enterprise.context.SessionScoped", "javax.enterprise.context.ApplicationScoped",
				"javax.enterprise.context.ConversationScoped");
	}

	@Override
	protected String getNewAnnotationName() {
		if (modelElement.isIsConversationScoped())
			return "javax.enterprise.context.ConversationScoped";
		else if (modelElement.isIsSessionScoped())
			return "javax.enterprise.context.SessionScoped";
		else if (modelElement.isIsApplicationScoped())
			return "javax.enterprise.context.ApplicationScoped";
		else
			return "javax.enterprise.context.RequestScoped";
	}
}
