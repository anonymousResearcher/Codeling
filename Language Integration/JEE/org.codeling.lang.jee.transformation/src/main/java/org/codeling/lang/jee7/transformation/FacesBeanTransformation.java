package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.FacesBean;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.transformation.facesbean_feature.IsApplicationScopedTransformation;
import org.codeling.lang.jee7.transformation.facesbean_feature.IsRequestScopedTransformation;
import org.codeling.lang.jee7.transformation.facesbean_feature.IsSessionScopedTransformation;
import org.codeling.lang.jee7.transformation.facesbean_feature.IsViewScopedTransformation;
import org.codeling.mechanisms.transformations.classes.TypeAnnotationTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class FacesBeanTransformation extends TypeAnnotationTransformation<FacesBean> {

	public FacesBeanTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getFacesBean());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		new BeanTransformation<FacesBean>().doCreateCrossReferencesTransformations(this, result);
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		new BeanTransformation<FacesBean>().doCreateChildTransformationsToCode(this, result);

		// Child transformations for attributes
		result.add(new IsRequestScopedTransformation(this));
		result.add(new IsSessionScopedTransformation(this));
		result.add(new IsApplicationScopedTransformation(this));
		result.add(new IsViewScopedTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		new BeanTransformation<FacesBean>().doCreateChildTransformationsToModel(this, result);

		// Child transformations for attributes
		result.add(new IsRequestScopedTransformation(this));
		result.add(new IsSessionScopedTransformation(this));
		result.add(new IsApplicationScopedTransformation(this));
		result.add(new IsViewScopedTransformation(this));
	}

	@Override
	public boolean hasExpectedAnnotation(IType type) {
		return ASTUtils.hasAnnotation(type, "javax.faces.view.RequestScoped", "javax.faces.view.SessionScoped",
				"javax.faces.view.ApplicationScoped", "javax.faces.view.ViewScoped");
	}

	@Override
	protected String getNewAnnotationName() {
		if (modelElement.isIsApplicationScoped())
			return "javax.faces.view.ApplicationScoped";
		else if (modelElement.isIsSessionScoped())
			return "javax.faces.view.SessionScoped";
		else if (modelElement.isIsViewScoped())
			return "javax.faces.view.ViewScoped";
		else
			return "javax.faces.view.RequestScoped";
	}
}
