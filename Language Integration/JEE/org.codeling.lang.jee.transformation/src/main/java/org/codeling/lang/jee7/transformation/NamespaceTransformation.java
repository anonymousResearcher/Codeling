package org.codeling.lang.jee7.transformation;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.Namespace;
import org.codeling.lang.jee7.transformation.namespace_feature.BeansTransformation;
import org.codeling.lang.jee7.transformation.namespace_feature.ChildTransformation;
import org.codeling.lang.jee7.transformation.namespace_feature.EntitiesTransformation;
import org.codeling.mechanisms.transformations.classes.NamespaceHierarchyTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;

public class NamespaceTransformation extends NamespaceHierarchyTransformation<Namespace> {

	public NamespaceTransformation(
			AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getNamespace(),
				JEE7Package.eINSTANCE.getArchitecture_Namespaces(), JEE7Package.eINSTANCE.getNamespace_Child());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new BeansTransformation(this));
		result.add(new EntitiesTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToCode(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ChildTransformation(this));
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		result.add(new ChildTransformation(this));
	}

}