package org.codeling.lang.jee7.transformation.architecture_feature;

import java.util.List;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Architecture;
import org.codeling.lang.jee7.mm.JEE7.Entity;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.transformation.EntityTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.NinjaSingletonContainmentTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;

public class EntitiesTransformation extends NinjaSingletonContainmentTransformation<Architecture, Entity> {

	public EntitiesTransformation(ClassMechanismTransformation<Architecture, IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getArchitecture_Entities());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub

	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new EntityTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		try {
			for (IType type : ASTUtils.getTypes(codeRoot)) {
				if (!type.isClass())
					continue;

				EntityTransformation t = new EntityTransformation(this);
				if (t.hasExpectedAnnotation(type)) {
					t.setCodeElement(type);
					result.add(t);
				}
			}
		} catch (Exception e) {
			String projectName = "(no project name available)";
			if (codeRoot != null)
				projectName = codeElement.getElementName();
			addError("Error while getting types in project " + projectName, e);
		}
	}
}
