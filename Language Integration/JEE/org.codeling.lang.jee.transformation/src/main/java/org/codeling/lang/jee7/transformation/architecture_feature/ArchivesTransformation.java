package org.codeling.lang.jee7.transformation.architecture_feature;

import java.util.List;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.Architecture;
import org.codeling.lang.jee7.mm.JEE7.Archive;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.transformation.ArchiveTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.references.NinjaSingletonContainmentTransformation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;

public class ArchivesTransformation extends NinjaSingletonContainmentTransformation<Architecture, Archive> {

	public ArchivesTransformation(ClassMechanismTransformation<Architecture, IJavaElement> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getArchitecture_Archives());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new ArchiveTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		for (IJavaProject project : codeRoot) {
			ArchiveTransformation t = new ArchiveTransformation(this);
			t.setCodeElement(project);
			result.add(t);
		}
	}

}
