package org.codeling.lang.jee7.transformation.operation_feature;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.codeling.lang.base.java.ASTUtils;
import org.codeling.lang.base.java.JDTUtils;
import org.codeling.lang.base.java.ProfilesUtils;
import org.codeling.lang.base.java.transformation.references.IALHolder;
import org.codeling.lang.base.java.transformation.references.IALTransformation;
import org.codeling.lang.jee7.mm.JEE7.Operation;
import org.codeling.lang.jee7.transformation.OperationTransformation;
import org.codeling.mechanisms.transformations.AttributeMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;
import org.modelversioning.emfprofile.Stereotype;
import org.modelversioning.emfprofileapplication.StereotypeApplication;

import de.mkonersmann.il.profiles.Profiles;

public class TimeResourceDemandTransformation
		// extends
		// ContainmentOperationAttributeAnnotationParameterTransformation<StereotypeApplication,
		// ComponentType>
		extends AttributeMechanismTransformation<StereotypeApplication, IMethod> implements IALTransformation<StereotypeApplication, IMethod> {

	private IALHolder holder = new IALHolder();

	public TimeResourceDemandTransformation(OperationTransformation parentTransformation, EAttribute eAttribute) {
		super(parentTransformation, eAttribute);
	}

	@Override
	public IALHolder getIALHolder() {
		return holder;
	}

	@Override
	public StereotypeApplication resolveTranslatedIALElement(EObject foundationalElement) {
		EList<StereotypeApplication> appliedStereotypes = ProfilesUtils.getAppliedStereotypes(foundationalElement);
		Stereotype stereotype = ProfilesUtils.getStereotype("TimeResourceDemand", Profiles.QUALITY_TIME.load());
		for (StereotypeApplication app : appliedStereotypes) {
			if (app.getStereotype() == stereotype) {
				return app;
			}
		}
		return null;
	}

	@Override
	public boolean codeFragmentExists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		IMethod method = (IMethod) holder.getIALCodeElement();

		EAttribute durationAttribute = ProfilesUtils.getEAttribute(Profiles.QUALITY_TIME.load(), "TimeResourceDemand",
				"duration");
		String value = (String) modelElement.eGet(durationAttribute);

		Map<String, Object> mvp = new HashMap<>();
		mvp.put("duration", value);
		try {
			ASTUtils.addAnnotation(method, "TimeResourceDemand", mvp);
			JDTUtils.addImportIfNecessary(method.getDeclaringType(),
					"org.codeling.lang.jee.ial.quality.time.TimeResourceDemand", null);
		} catch (JavaModelException e) {
			addError(MessageFormat.format("Could not add the annotation %s or its import to the method %s.%s(?)", "TimeResourceDemand",
					method.getParent().getElementName(), method.getElementName()));
		}
	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public StereotypeApplication transformToModel() throws CodelingException {
		// TODO Auto-generated method stub
		return null;
	}

}
