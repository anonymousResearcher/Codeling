package org.codeling.lang.jee7.transformation.operation_feature;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.codeling.lang.base.java.transformation.AbstractModelCodeTransformation;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.Operation;
import org.codeling.lang.jee7.mm.JEE7.OperationParameter;
import org.codeling.lang.jee7.transformation.OperationParameterTransformation;
import org.codeling.mechanisms.transformations.ClassMechanismTransformation;
import org.codeling.mechanisms.transformations.ReferenceMechanismTransformation;
import org.codeling.utils.CodelingException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ILocalVariable;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;

public class ParametersTransformation extends ReferenceMechanismTransformation<Operation, OperationParameter, IMethod> {

	public ParametersTransformation(ClassMechanismTransformation<Operation, IMethod> parentTransformation) {
		super(parentTransformation, JEE7Package.eINSTANCE.getOperation_Parameters());
	}

	@Override
	public void doCreateCrossReferencesTransformations(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {
		// TODO Auto-generated method stub
	}

	@Override
	protected ClassMechanismTransformation<? extends EObject, ? extends IJavaElement> createSpecificTransformation(
			EObject targetModelElement) {
		return new OperationParameterTransformation(this);
	}

	@Override
	protected void doCreateChildTransformationsToModel(
			List<AbstractModelCodeTransformation<? extends EObject, ? extends IJavaElement>> result) {

		try {
			ILocalVariable[] parameters = codeElement.getParameters();
			for (ILocalVariable p : parameters) {
				OperationParameterTransformation t = new OperationParameterTransformation(this);
				t.setCodeElement(p);
				result.add(t);
			}
		} catch (JavaModelException e) {
			addError(MessageFormat.format("Could not get parameters of method [{0}].[{1}]",
					codeElement.getDeclaringType().getElementName(), codeElement.getElementName()));
		}
	}

	@Override
	public void createCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCodeFragments() throws CodelingException {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	public Operation transformToModel() throws CodelingException {
		if (modelElement == null)
			throw new IllegalStateException("modelElement must not be null");

		List<ILocalVariable> parameters = null;
		try {
			parameters = Arrays.asList(codeElement.getParameters());
		} catch (JavaModelException e) {
			addError(MessageFormat.format("Could not get parameters of method [{0}].[{1}]",
					codeElement.getDeclaringType().getElementName(), codeElement.getElementName()));
		}

		if (parameters.isEmpty()) {
			// No target object. Do not change the model.
			return modelElement;
		}

		// Find the element that is referenced by the field's type and set it as
		// eReference value
		final List<String> typeNames = parameters.stream().map(p -> p.getElementName()).collect(Collectors.toList());

		final List<OperationParameter> targetElements = getTargetObjects(modelElement, eReference, typeNames);
		if (eReference.isMany()) {
			((List<OperationParameter>) modelElement.eGet(eReference)).addAll(targetElements);
		} else {
			modelElement.eSet(eReference, targetElements.get(0));
		}

		return modelElement;
	}

}
