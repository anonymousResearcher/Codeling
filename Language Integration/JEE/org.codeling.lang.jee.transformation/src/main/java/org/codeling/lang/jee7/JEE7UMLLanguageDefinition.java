package org.codeling.lang.jee7;

import org.codeling.lang.base.modeltrans.henshintgg.HenshinTGGBasedLanguageDefinition;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.UMLFactory;
import org.osgi.framework.FrameworkUtil;

public class JEE7UMLLanguageDefinition extends HenshinTGGBasedLanguageDefinition {

	final static URI henshinTGGFileURI = URI.createPlatformPluginURI(
			"/" + FrameworkUtil.getBundle(JEE7UMLLanguageDefinition.class).getSymbolicName() + "/IAL2UML.henshin",
			true);

	public JEE7UMLLanguageDefinition() {
		super(henshinTGGFileURI);
	}

	/**
	 * Creates a comment in the UML model to save the element's id in Codeling
	 */
	@Override
	public void setID(EObject object, String id) {
		Comment comment = UMLFactory.eINSTANCE.createComment();
		comment.setBody("Codeling-ID: " + id);
		((Element) object).getOwnedComments().add(comment);
	}

	/**
	 * Retrieves the id from a comment in the UML model as it was stored by setID.
	 */
	@Override
	public String getID(EObject object) {
		for (Comment c : ((Element) object).getOwnedComments())
			if (c.getBody().matches("Codeling-ID: .+"))
				return c.getBody().substring("Codeling-ID: ".length());
		return null;
	}

}