/**
 */
package org.codeling.lang.jee7.mm.JEE7;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Architecture</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.Architecture#getBeans <em>Beans</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.Architecture#getEntities <em>Entities</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.Architecture#getNamespaces <em>Namespaces</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.Architecture#getArchives <em>Archives</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getArchitecture()
 * @model
 * @generated
 */
public interface Architecture extends EObject {
	/**
	 * Returns the value of the '<em><b>Beans</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.jee7.mm.JEE7.Bean}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Beans</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Beans</em>' containment reference list.
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getArchitecture_Beans()
	 * @model containment="true"
	 * @generated
	 */
	EList<Bean> getBeans();

	/**
	 * Returns the value of the '<em><b>Entities</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.jee7.mm.JEE7.Entity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entities</em>' containment reference list.
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getArchitecture_Entities()
	 * @model containment="true"
	 * @generated
	 */
	EList<Entity> getEntities();

	/**
	 * Returns the value of the '<em><b>Namespaces</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.jee7.mm.JEE7.Namespace}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Namespaces</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Namespaces</em>' containment reference list.
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getArchitecture_Namespaces()
	 * @model containment="true"
	 * @generated
	 */
	EList<Namespace> getNamespaces();

	/**
	 * Returns the value of the '<em><b>Archives</b></em>' containment reference list.
	 * The list contents are of type {@link org.codeling.lang.jee7.mm.JEE7.Archive}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Archives</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Archives</em>' containment reference list.
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getArchitecture_Archives()
	 * @model containment="true"
	 * @generated
	 */
	EList<Archive> getArchives();

} // Architecture
