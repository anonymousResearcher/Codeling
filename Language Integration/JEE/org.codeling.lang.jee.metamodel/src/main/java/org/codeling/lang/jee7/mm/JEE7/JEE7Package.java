/**
 */
package org.codeling.lang.jee7.mm.JEE7;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Factory
 * @model kind="package"
 * @generated
 */
public interface JEE7Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "JEE7";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://codeling.org/lang/jee/7.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "jee7";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JEE7Package eINSTANCE = org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl.init();

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.BeanImpl <em>Bean</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.BeanImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getBean()
	 * @generated
	 */
	int BEAN = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEAN__NAME = 0;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEAN__OPERATIONS = 1;

	/**
	 * The feature id for the '<em><b>Referenced</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEAN__REFERENCED = 2;

	/**
	 * The number of structural features of the '<em>Bean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEAN_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Bean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEAN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.SessionBeanImpl <em>Session Bean</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.SessionBeanImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getSessionBean()
	 * @generated
	 */
	int SESSION_BEAN = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_BEAN__NAME = BEAN__NAME;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_BEAN__OPERATIONS = BEAN__OPERATIONS;

	/**
	 * The feature id for the '<em><b>Referenced</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_BEAN__REFERENCED = BEAN__REFERENCED;

	/**
	 * The feature id for the '<em><b>Is Stateless</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_BEAN__IS_STATELESS = BEAN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Stateful</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_BEAN__IS_STATEFUL = BEAN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Singleton</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_BEAN__IS_SINGLETON = BEAN_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Session Bean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_BEAN_FEATURE_COUNT = BEAN_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Session Bean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_BEAN_OPERATION_COUNT = BEAN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.CDIBeanImpl <em>CDI Bean</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.CDIBeanImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getCDIBean()
	 * @generated
	 */
	int CDI_BEAN = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDI_BEAN__NAME = BEAN__NAME;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDI_BEAN__OPERATIONS = BEAN__OPERATIONS;

	/**
	 * The feature id for the '<em><b>Referenced</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDI_BEAN__REFERENCED = BEAN__REFERENCED;

	/**
	 * The feature id for the '<em><b>Is Request Scoped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDI_BEAN__IS_REQUEST_SCOPED = BEAN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Session Scoped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDI_BEAN__IS_SESSION_SCOPED = BEAN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Application Scoped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDI_BEAN__IS_APPLICATION_SCOPED = BEAN_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is Conversation Scoped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDI_BEAN__IS_CONVERSATION_SCOPED = BEAN_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>CDI Bean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDI_BEAN_FEATURE_COUNT = BEAN_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>CDI Bean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDI_BEAN_OPERATION_COUNT = BEAN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.FacesBeanImpl <em>Faces Bean</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.FacesBeanImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getFacesBean()
	 * @generated
	 */
	int FACES_BEAN = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACES_BEAN__NAME = BEAN__NAME;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACES_BEAN__OPERATIONS = BEAN__OPERATIONS;

	/**
	 * The feature id for the '<em><b>Referenced</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACES_BEAN__REFERENCED = BEAN__REFERENCED;

	/**
	 * The feature id for the '<em><b>Is Request Scoped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACES_BEAN__IS_REQUEST_SCOPED = BEAN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Session Scoped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACES_BEAN__IS_SESSION_SCOPED = BEAN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is Application Scoped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACES_BEAN__IS_APPLICATION_SCOPED = BEAN_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is View Scoped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACES_BEAN__IS_VIEW_SCOPED = BEAN_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Faces Bean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACES_BEAN_FEATURE_COUNT = BEAN_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Faces Bean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACES_BEAN_OPERATION_COUNT = BEAN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.ArchitectureImpl <em>Architecture</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.ArchitectureImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getArchitecture()
	 * @generated
	 */
	int ARCHITECTURE = 4;

	/**
	 * The feature id for the '<em><b>Beans</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE__BEANS = 0;

	/**
	 * The feature id for the '<em><b>Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE__ENTITIES = 1;

	/**
	 * The feature id for the '<em><b>Namespaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE__NAMESPACES = 2;

	/**
	 * The feature id for the '<em><b>Archives</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE__ARCHIVES = 3;

	/**
	 * The number of structural features of the '<em>Architecture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Architecture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.OperationImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__PARAMETERS = 1;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__RETURN_TYPE = 2;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.OperationParameterImpl <em>Operation Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.OperationParameterImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getOperationParameter()
	 * @generated
	 */
	int OPERATION_PARAMETER = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_PARAMETER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Is Many</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_PARAMETER__IS_MANY = 1;

	/**
	 * The feature id for the '<em><b>Entity Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_PARAMETER__ENTITY_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Primitive Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_PARAMETER__PRIMITIVE_TYPE = 3;

	/**
	 * The number of structural features of the '<em>Operation Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_PARAMETER_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Operation Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.EntityImpl <em>Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.EntityImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getEntity()
	 * @generated
	 */
	int ENTITY = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__ATTRIBUTES = 1;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__OPERATIONS = 2;

	/**
	 * The feature id for the '<em><b>References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__REFERENCES = 3;

	/**
	 * The number of structural features of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.EntityAttributeImpl <em>Entity Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.EntityAttributeImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getEntityAttribute()
	 * @generated
	 */
	int ENTITY_ATTRIBUTE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_ATTRIBUTE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Is Many</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_ATTRIBUTE__IS_MANY = 1;

	/**
	 * The feature id for the '<em><b>Primitive Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_ATTRIBUTE__PRIMITIVE_TYPE = 2;

	/**
	 * The number of structural features of the '<em>Entity Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_ATTRIBUTE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Entity Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_ATTRIBUTE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.NamespaceImpl <em>Namespace</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.NamespaceImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getNamespace()
	 * @generated
	 */
	int NAMESPACE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Beans</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE__BEANS = 1;

	/**
	 * The feature id for the '<em><b>Entities</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE__ENTITIES = 2;

	/**
	 * The feature id for the '<em><b>Child</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE__CHILD = 3;

	/**
	 * The number of structural features of the '<em>Namespace</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Namespace</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMESPACE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.ArchiveImpl <em>Archive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.ArchiveImpl
	 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getArchive()
	 * @generated
	 */
	int ARCHIVE = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHIVE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Beans</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHIVE__BEANS = 1;

	/**
	 * The feature id for the '<em><b>Entities</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHIVE__ENTITIES = 2;

	/**
	 * The number of structural features of the '<em>Archive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHIVE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Archive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHIVE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.Bean <em>Bean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bean</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Bean
	 * @generated
	 */
	EClass getBean();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.Bean#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Bean#getName()
	 * @see #getBean()
	 * @generated
	 */
	EAttribute getBean_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.jee7.mm.JEE7.Bean#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Bean#getOperations()
	 * @see #getBean()
	 * @generated
	 */
	EReference getBean_Operations();

	/**
	 * Returns the meta object for the reference list '{@link org.codeling.lang.jee7.mm.JEE7.Bean#getReferenced <em>Referenced</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Referenced</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Bean#getReferenced()
	 * @see #getBean()
	 * @generated
	 */
	EReference getBean_Referenced();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.SessionBean <em>Session Bean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session Bean</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.SessionBean
	 * @generated
	 */
	EClass getSessionBean();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsStateless <em>Is Stateless</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Stateless</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsStateless()
	 * @see #getSessionBean()
	 * @generated
	 */
	EAttribute getSessionBean_IsStateless();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsStateful <em>Is Stateful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Stateful</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsStateful()
	 * @see #getSessionBean()
	 * @generated
	 */
	EAttribute getSessionBean_IsStateful();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsSingleton <em>Is Singleton</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Singleton</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.SessionBean#isIsSingleton()
	 * @see #getSessionBean()
	 * @generated
	 */
	EAttribute getSessionBean_IsSingleton();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.CDIBean <em>CDI Bean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CDI Bean</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.CDIBean
	 * @generated
	 */
	EClass getCDIBean();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsRequestScoped <em>Is Request Scoped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Request Scoped</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsRequestScoped()
	 * @see #getCDIBean()
	 * @generated
	 */
	EAttribute getCDIBean_IsRequestScoped();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsSessionScoped <em>Is Session Scoped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Session Scoped</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsSessionScoped()
	 * @see #getCDIBean()
	 * @generated
	 */
	EAttribute getCDIBean_IsSessionScoped();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsApplicationScoped <em>Is Application Scoped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Application Scoped</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsApplicationScoped()
	 * @see #getCDIBean()
	 * @generated
	 */
	EAttribute getCDIBean_IsApplicationScoped();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsConversationScoped <em>Is Conversation Scoped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Conversation Scoped</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsConversationScoped()
	 * @see #getCDIBean()
	 * @generated
	 */
	EAttribute getCDIBean_IsConversationScoped();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.FacesBean <em>Faces Bean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Faces Bean</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.FacesBean
	 * @generated
	 */
	EClass getFacesBean();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.FacesBean#isIsRequestScoped <em>Is Request Scoped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Request Scoped</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.FacesBean#isIsRequestScoped()
	 * @see #getFacesBean()
	 * @generated
	 */
	EAttribute getFacesBean_IsRequestScoped();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.FacesBean#isIsSessionScoped <em>Is Session Scoped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Session Scoped</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.FacesBean#isIsSessionScoped()
	 * @see #getFacesBean()
	 * @generated
	 */
	EAttribute getFacesBean_IsSessionScoped();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.FacesBean#isIsApplicationScoped <em>Is Application Scoped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Application Scoped</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.FacesBean#isIsApplicationScoped()
	 * @see #getFacesBean()
	 * @generated
	 */
	EAttribute getFacesBean_IsApplicationScoped();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.FacesBean#isIsViewScoped <em>Is View Scoped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is View Scoped</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.FacesBean#isIsViewScoped()
	 * @see #getFacesBean()
	 * @generated
	 */
	EAttribute getFacesBean_IsViewScoped();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.Architecture <em>Architecture</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Architecture</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Architecture
	 * @generated
	 */
	EClass getArchitecture();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.jee7.mm.JEE7.Architecture#getBeans <em>Beans</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Beans</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Architecture#getBeans()
	 * @see #getArchitecture()
	 * @generated
	 */
	EReference getArchitecture_Beans();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.jee7.mm.JEE7.Architecture#getEntities <em>Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entities</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Architecture#getEntities()
	 * @see #getArchitecture()
	 * @generated
	 */
	EReference getArchitecture_Entities();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.jee7.mm.JEE7.Architecture#getNamespaces <em>Namespaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Namespaces</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Architecture#getNamespaces()
	 * @see #getArchitecture()
	 * @generated
	 */
	EReference getArchitecture_Namespaces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.jee7.mm.JEE7.Architecture#getArchives <em>Archives</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Archives</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Architecture#getArchives()
	 * @see #getArchitecture()
	 * @generated
	 */
	EReference getArchitecture_Archives();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.Operation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Operation#getName()
	 * @see #getOperation()
	 * @generated
	 */
	EAttribute getOperation_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.jee7.mm.JEE7.Operation#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Operation#getParameters()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_Parameters();

	/**
	 * Returns the meta object for the containment reference '{@link org.codeling.lang.jee7.mm.JEE7.Operation#getReturnType <em>Return Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Return Type</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Operation#getReturnType()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_ReturnType();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.OperationParameter <em>Operation Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Parameter</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.OperationParameter
	 * @generated
	 */
	EClass getOperationParameter();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.OperationParameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.OperationParameter#getName()
	 * @see #getOperationParameter()
	 * @generated
	 */
	EAttribute getOperationParameter_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.OperationParameter#isIsMany <em>Is Many</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Many</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.OperationParameter#isIsMany()
	 * @see #getOperationParameter()
	 * @generated
	 */
	EAttribute getOperationParameter_IsMany();

	/**
	 * Returns the meta object for the reference '{@link org.codeling.lang.jee7.mm.JEE7.OperationParameter#getEntityType <em>Entity Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Entity Type</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.OperationParameter#getEntityType()
	 * @see #getOperationParameter()
	 * @generated
	 */
	EReference getOperationParameter_EntityType();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.OperationParameter#getPrimitiveType <em>Primitive Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Primitive Type</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.OperationParameter#getPrimitiveType()
	 * @see #getOperationParameter()
	 * @generated
	 */
	EAttribute getOperationParameter_PrimitiveType();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Entity
	 * @generated
	 */
	EClass getEntity();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.Entity#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Entity#getName()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_Name();

	/**
	 * Returns the meta object for the reference list '{@link org.codeling.lang.jee7.mm.JEE7.Entity#getReferences <em>References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>References</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Entity#getReferences()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_References();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.jee7.mm.JEE7.Entity#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Entity#getAttributes()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_Attributes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.jee7.mm.JEE7.Entity#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Entity#getOperations()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_Operations();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.EntityAttribute <em>Entity Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Attribute</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.EntityAttribute
	 * @generated
	 */
	EClass getEntityAttribute();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.EntityAttribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.EntityAttribute#getName()
	 * @see #getEntityAttribute()
	 * @generated
	 */
	EAttribute getEntityAttribute_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.EntityAttribute#isIsMany <em>Is Many</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Many</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.EntityAttribute#isIsMany()
	 * @see #getEntityAttribute()
	 * @generated
	 */
	EAttribute getEntityAttribute_IsMany();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.EntityAttribute#getPrimitiveType <em>Primitive Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Primitive Type</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.EntityAttribute#getPrimitiveType()
	 * @see #getEntityAttribute()
	 * @generated
	 */
	EAttribute getEntityAttribute_PrimitiveType();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.Namespace <em>Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Namespace</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Namespace
	 * @generated
	 */
	EClass getNamespace();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.Namespace#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Namespace#getName()
	 * @see #getNamespace()
	 * @generated
	 */
	EAttribute getNamespace_Name();

	/**
	 * Returns the meta object for the reference list '{@link org.codeling.lang.jee7.mm.JEE7.Namespace#getBeans <em>Beans</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Beans</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Namespace#getBeans()
	 * @see #getNamespace()
	 * @generated
	 */
	EReference getNamespace_Beans();

	/**
	 * Returns the meta object for the reference list '{@link org.codeling.lang.jee7.mm.JEE7.Namespace#getEntities <em>Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Entities</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Namespace#getEntities()
	 * @see #getNamespace()
	 * @generated
	 */
	EReference getNamespace_Entities();

	/**
	 * Returns the meta object for the containment reference list '{@link org.codeling.lang.jee7.mm.JEE7.Namespace#getChild <em>Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Child</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Namespace#getChild()
	 * @see #getNamespace()
	 * @generated
	 */
	EReference getNamespace_Child();

	/**
	 * Returns the meta object for class '{@link org.codeling.lang.jee7.mm.JEE7.Archive <em>Archive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Archive</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Archive
	 * @generated
	 */
	EClass getArchive();

	/**
	 * Returns the meta object for the attribute '{@link org.codeling.lang.jee7.mm.JEE7.Archive#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Archive#getName()
	 * @see #getArchive()
	 * @generated
	 */
	EAttribute getArchive_Name();

	/**
	 * Returns the meta object for the reference list '{@link org.codeling.lang.jee7.mm.JEE7.Archive#getBeans <em>Beans</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Beans</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Archive#getBeans()
	 * @see #getArchive()
	 * @generated
	 */
	EReference getArchive_Beans();

	/**
	 * Returns the meta object for the reference list '{@link org.codeling.lang.jee7.mm.JEE7.Archive#getEntities <em>Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Entities</em>'.
	 * @see org.codeling.lang.jee7.mm.JEE7.Archive#getEntities()
	 * @see #getArchive()
	 * @generated
	 */
	EReference getArchive_Entities();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	JEE7Factory getJEE7Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.BeanImpl <em>Bean</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.BeanImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getBean()
		 * @generated
		 */
		EClass BEAN = eINSTANCE.getBean();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BEAN__NAME = eINSTANCE.getBean_Name();

		/**
		 * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEAN__OPERATIONS = eINSTANCE.getBean_Operations();

		/**
		 * The meta object literal for the '<em><b>Referenced</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEAN__REFERENCED = eINSTANCE.getBean_Referenced();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.SessionBeanImpl <em>Session Bean</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.SessionBeanImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getSessionBean()
		 * @generated
		 */
		EClass SESSION_BEAN = eINSTANCE.getSessionBean();

		/**
		 * The meta object literal for the '<em><b>Is Stateless</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION_BEAN__IS_STATELESS = eINSTANCE.getSessionBean_IsStateless();

		/**
		 * The meta object literal for the '<em><b>Is Stateful</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION_BEAN__IS_STATEFUL = eINSTANCE.getSessionBean_IsStateful();

		/**
		 * The meta object literal for the '<em><b>Is Singleton</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION_BEAN__IS_SINGLETON = eINSTANCE.getSessionBean_IsSingleton();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.CDIBeanImpl <em>CDI Bean</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.CDIBeanImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getCDIBean()
		 * @generated
		 */
		EClass CDI_BEAN = eINSTANCE.getCDIBean();

		/**
		 * The meta object literal for the '<em><b>Is Request Scoped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CDI_BEAN__IS_REQUEST_SCOPED = eINSTANCE.getCDIBean_IsRequestScoped();

		/**
		 * The meta object literal for the '<em><b>Is Session Scoped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CDI_BEAN__IS_SESSION_SCOPED = eINSTANCE.getCDIBean_IsSessionScoped();

		/**
		 * The meta object literal for the '<em><b>Is Application Scoped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CDI_BEAN__IS_APPLICATION_SCOPED = eINSTANCE.getCDIBean_IsApplicationScoped();

		/**
		 * The meta object literal for the '<em><b>Is Conversation Scoped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CDI_BEAN__IS_CONVERSATION_SCOPED = eINSTANCE.getCDIBean_IsConversationScoped();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.FacesBeanImpl <em>Faces Bean</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.FacesBeanImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getFacesBean()
		 * @generated
		 */
		EClass FACES_BEAN = eINSTANCE.getFacesBean();

		/**
		 * The meta object literal for the '<em><b>Is Request Scoped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FACES_BEAN__IS_REQUEST_SCOPED = eINSTANCE.getFacesBean_IsRequestScoped();

		/**
		 * The meta object literal for the '<em><b>Is Session Scoped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FACES_BEAN__IS_SESSION_SCOPED = eINSTANCE.getFacesBean_IsSessionScoped();

		/**
		 * The meta object literal for the '<em><b>Is Application Scoped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FACES_BEAN__IS_APPLICATION_SCOPED = eINSTANCE.getFacesBean_IsApplicationScoped();

		/**
		 * The meta object literal for the '<em><b>Is View Scoped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FACES_BEAN__IS_VIEW_SCOPED = eINSTANCE.getFacesBean_IsViewScoped();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.ArchitectureImpl <em>Architecture</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.ArchitectureImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getArchitecture()
		 * @generated
		 */
		EClass ARCHITECTURE = eINSTANCE.getArchitecture();

		/**
		 * The meta object literal for the '<em><b>Beans</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARCHITECTURE__BEANS = eINSTANCE.getArchitecture_Beans();

		/**
		 * The meta object literal for the '<em><b>Entities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARCHITECTURE__ENTITIES = eINSTANCE.getArchitecture_Entities();

		/**
		 * The meta object literal for the '<em><b>Namespaces</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARCHITECTURE__NAMESPACES = eINSTANCE.getArchitecture_Namespaces();

		/**
		 * The meta object literal for the '<em><b>Archives</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARCHITECTURE__ARCHIVES = eINSTANCE.getArchitecture_Archives();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.OperationImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION__NAME = eINSTANCE.getOperation_Name();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__PARAMETERS = eINSTANCE.getOperation_Parameters();

		/**
		 * The meta object literal for the '<em><b>Return Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__RETURN_TYPE = eINSTANCE.getOperation_ReturnType();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.OperationParameterImpl <em>Operation Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.OperationParameterImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getOperationParameter()
		 * @generated
		 */
		EClass OPERATION_PARAMETER = eINSTANCE.getOperationParameter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_PARAMETER__NAME = eINSTANCE.getOperationParameter_Name();

		/**
		 * The meta object literal for the '<em><b>Is Many</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_PARAMETER__IS_MANY = eINSTANCE.getOperationParameter_IsMany();

		/**
		 * The meta object literal for the '<em><b>Entity Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_PARAMETER__ENTITY_TYPE = eINSTANCE.getOperationParameter_EntityType();

		/**
		 * The meta object literal for the '<em><b>Primitive Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_PARAMETER__PRIMITIVE_TYPE = eINSTANCE.getOperationParameter_PrimitiveType();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.EntityImpl <em>Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.EntityImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getEntity()
		 * @generated
		 */
		EClass ENTITY = eINSTANCE.getEntity();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__NAME = eINSTANCE.getEntity_Name();

		/**
		 * The meta object literal for the '<em><b>References</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__REFERENCES = eINSTANCE.getEntity_References();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__ATTRIBUTES = eINSTANCE.getEntity_Attributes();

		/**
		 * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__OPERATIONS = eINSTANCE.getEntity_Operations();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.EntityAttributeImpl <em>Entity Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.EntityAttributeImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getEntityAttribute()
		 * @generated
		 */
		EClass ENTITY_ATTRIBUTE = eINSTANCE.getEntityAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_ATTRIBUTE__NAME = eINSTANCE.getEntityAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Is Many</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_ATTRIBUTE__IS_MANY = eINSTANCE.getEntityAttribute_IsMany();

		/**
		 * The meta object literal for the '<em><b>Primitive Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_ATTRIBUTE__PRIMITIVE_TYPE = eINSTANCE.getEntityAttribute_PrimitiveType();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.NamespaceImpl <em>Namespace</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.NamespaceImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getNamespace()
		 * @generated
		 */
		EClass NAMESPACE = eINSTANCE.getNamespace();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMESPACE__NAME = eINSTANCE.getNamespace_Name();

		/**
		 * The meta object literal for the '<em><b>Beans</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMESPACE__BEANS = eINSTANCE.getNamespace_Beans();

		/**
		 * The meta object literal for the '<em><b>Entities</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMESPACE__ENTITIES = eINSTANCE.getNamespace_Entities();

		/**
		 * The meta object literal for the '<em><b>Child</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMESPACE__CHILD = eINSTANCE.getNamespace_Child();

		/**
		 * The meta object literal for the '{@link org.codeling.lang.jee7.mm.JEE7.impl.ArchiveImpl <em>Archive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.ArchiveImpl
		 * @see org.codeling.lang.jee7.mm.JEE7.impl.JEE7PackageImpl#getArchive()
		 * @generated
		 */
		EClass ARCHIVE = eINSTANCE.getArchive();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARCHIVE__NAME = eINSTANCE.getArchive_Name();

		/**
		 * The meta object literal for the '<em><b>Beans</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARCHIVE__BEANS = eINSTANCE.getArchive_Beans();

		/**
		 * The meta object literal for the '<em><b>Entities</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARCHIVE__ENTITIES = eINSTANCE.getArchive_Entities();

	}

} //JEE7Package
