/**
 */
package org.codeling.lang.jee7.mm.JEE7.impl;

import org.codeling.lang.jee7.mm.JEE7.CDIBean;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CDI Bean</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.CDIBeanImpl#isIsRequestScoped <em>Is Request Scoped</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.CDIBeanImpl#isIsSessionScoped <em>Is Session Scoped</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.CDIBeanImpl#isIsApplicationScoped <em>Is Application Scoped</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.CDIBeanImpl#isIsConversationScoped <em>Is Conversation Scoped</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CDIBeanImpl extends BeanImpl implements CDIBean {
	/**
	 * The default value of the '{@link #isIsRequestScoped() <em>Is Request Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsRequestScoped()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_REQUEST_SCOPED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsRequestScoped() <em>Is Request Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsRequestScoped()
	 * @generated
	 * @ordered
	 */
	protected boolean isRequestScoped = IS_REQUEST_SCOPED_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsSessionScoped() <em>Is Session Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsSessionScoped()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_SESSION_SCOPED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsSessionScoped() <em>Is Session Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsSessionScoped()
	 * @generated
	 * @ordered
	 */
	protected boolean isSessionScoped = IS_SESSION_SCOPED_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsApplicationScoped() <em>Is Application Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsApplicationScoped()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_APPLICATION_SCOPED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsApplicationScoped() <em>Is Application Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsApplicationScoped()
	 * @generated
	 * @ordered
	 */
	protected boolean isApplicationScoped = IS_APPLICATION_SCOPED_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsConversationScoped() <em>Is Conversation Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsConversationScoped()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CONVERSATION_SCOPED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsConversationScoped() <em>Is Conversation Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsConversationScoped()
	 * @generated
	 * @ordered
	 */
	protected boolean isConversationScoped = IS_CONVERSATION_SCOPED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CDIBeanImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JEE7Package.Literals.CDI_BEAN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsRequestScoped() {
		return isRequestScoped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsRequestScoped(boolean newIsRequestScoped) {
		boolean oldIsRequestScoped = isRequestScoped;
		isRequestScoped = newIsRequestScoped;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JEE7Package.CDI_BEAN__IS_REQUEST_SCOPED, oldIsRequestScoped, isRequestScoped));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsSessionScoped() {
		return isSessionScoped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsSessionScoped(boolean newIsSessionScoped) {
		boolean oldIsSessionScoped = isSessionScoped;
		isSessionScoped = newIsSessionScoped;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JEE7Package.CDI_BEAN__IS_SESSION_SCOPED, oldIsSessionScoped, isSessionScoped));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsApplicationScoped() {
		return isApplicationScoped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsApplicationScoped(boolean newIsApplicationScoped) {
		boolean oldIsApplicationScoped = isApplicationScoped;
		isApplicationScoped = newIsApplicationScoped;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JEE7Package.CDI_BEAN__IS_APPLICATION_SCOPED, oldIsApplicationScoped, isApplicationScoped));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsConversationScoped() {
		return isConversationScoped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsConversationScoped(boolean newIsConversationScoped) {
		boolean oldIsConversationScoped = isConversationScoped;
		isConversationScoped = newIsConversationScoped;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JEE7Package.CDI_BEAN__IS_CONVERSATION_SCOPED, oldIsConversationScoped, isConversationScoped));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JEE7Package.CDI_BEAN__IS_REQUEST_SCOPED:
				return isIsRequestScoped();
			case JEE7Package.CDI_BEAN__IS_SESSION_SCOPED:
				return isIsSessionScoped();
			case JEE7Package.CDI_BEAN__IS_APPLICATION_SCOPED:
				return isIsApplicationScoped();
			case JEE7Package.CDI_BEAN__IS_CONVERSATION_SCOPED:
				return isIsConversationScoped();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JEE7Package.CDI_BEAN__IS_REQUEST_SCOPED:
				setIsRequestScoped((Boolean)newValue);
				return;
			case JEE7Package.CDI_BEAN__IS_SESSION_SCOPED:
				setIsSessionScoped((Boolean)newValue);
				return;
			case JEE7Package.CDI_BEAN__IS_APPLICATION_SCOPED:
				setIsApplicationScoped((Boolean)newValue);
				return;
			case JEE7Package.CDI_BEAN__IS_CONVERSATION_SCOPED:
				setIsConversationScoped((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JEE7Package.CDI_BEAN__IS_REQUEST_SCOPED:
				setIsRequestScoped(IS_REQUEST_SCOPED_EDEFAULT);
				return;
			case JEE7Package.CDI_BEAN__IS_SESSION_SCOPED:
				setIsSessionScoped(IS_SESSION_SCOPED_EDEFAULT);
				return;
			case JEE7Package.CDI_BEAN__IS_APPLICATION_SCOPED:
				setIsApplicationScoped(IS_APPLICATION_SCOPED_EDEFAULT);
				return;
			case JEE7Package.CDI_BEAN__IS_CONVERSATION_SCOPED:
				setIsConversationScoped(IS_CONVERSATION_SCOPED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JEE7Package.CDI_BEAN__IS_REQUEST_SCOPED:
				return isRequestScoped != IS_REQUEST_SCOPED_EDEFAULT;
			case JEE7Package.CDI_BEAN__IS_SESSION_SCOPED:
				return isSessionScoped != IS_SESSION_SCOPED_EDEFAULT;
			case JEE7Package.CDI_BEAN__IS_APPLICATION_SCOPED:
				return isApplicationScoped != IS_APPLICATION_SCOPED_EDEFAULT;
			case JEE7Package.CDI_BEAN__IS_CONVERSATION_SCOPED:
				return isConversationScoped != IS_CONVERSATION_SCOPED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isRequestScoped: ");
		result.append(isRequestScoped);
		result.append(", isSessionScoped: ");
		result.append(isSessionScoped);
		result.append(", isApplicationScoped: ");
		result.append(isApplicationScoped);
		result.append(", isConversationScoped: ");
		result.append(isConversationScoped);
		result.append(')');
		return result.toString();
	}

} //CDIBeanImpl
