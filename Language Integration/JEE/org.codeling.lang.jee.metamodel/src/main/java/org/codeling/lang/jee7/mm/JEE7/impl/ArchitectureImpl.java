/**
 */
package org.codeling.lang.jee7.mm.JEE7.impl;

import java.util.Collection;

import org.codeling.lang.jee7.mm.JEE7.Architecture;
import org.codeling.lang.jee7.mm.JEE7.Archive;
import org.codeling.lang.jee7.mm.JEE7.Bean;
import org.codeling.lang.jee7.mm.JEE7.Entity;
import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.Namespace;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Architecture</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.ArchitectureImpl#getBeans <em>Beans</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.ArchitectureImpl#getEntities <em>Entities</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.ArchitectureImpl#getNamespaces <em>Namespaces</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.ArchitectureImpl#getArchives <em>Archives</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArchitectureImpl extends MinimalEObjectImpl.Container implements Architecture {
	/**
	 * The cached value of the '{@link #getBeans() <em>Beans</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBeans()
	 * @generated
	 * @ordered
	 */
	protected EList<Bean> beans;

	/**
	 * The cached value of the '{@link #getEntities() <em>Entities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntities()
	 * @generated
	 * @ordered
	 */
	protected EList<Entity> entities;

	/**
	 * The cached value of the '{@link #getNamespaces() <em>Namespaces</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamespaces()
	 * @generated
	 * @ordered
	 */
	protected EList<Namespace> namespaces;

	/**
	 * The cached value of the '{@link #getArchives() <em>Archives</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArchives()
	 * @generated
	 * @ordered
	 */
	protected EList<Archive> archives;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArchitectureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JEE7Package.Literals.ARCHITECTURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Bean> getBeans() {
		if (beans == null) {
			beans = new EObjectContainmentEList<Bean>(Bean.class, this, JEE7Package.ARCHITECTURE__BEANS);
		}
		return beans;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Entity> getEntities() {
		if (entities == null) {
			entities = new EObjectContainmentEList<Entity>(Entity.class, this, JEE7Package.ARCHITECTURE__ENTITIES);
		}
		return entities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Namespace> getNamespaces() {
		if (namespaces == null) {
			namespaces = new EObjectContainmentEList<Namespace>(Namespace.class, this, JEE7Package.ARCHITECTURE__NAMESPACES);
		}
		return namespaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Archive> getArchives() {
		if (archives == null) {
			archives = new EObjectContainmentEList<Archive>(Archive.class, this, JEE7Package.ARCHITECTURE__ARCHIVES);
		}
		return archives;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JEE7Package.ARCHITECTURE__BEANS:
				return ((InternalEList<?>)getBeans()).basicRemove(otherEnd, msgs);
			case JEE7Package.ARCHITECTURE__ENTITIES:
				return ((InternalEList<?>)getEntities()).basicRemove(otherEnd, msgs);
			case JEE7Package.ARCHITECTURE__NAMESPACES:
				return ((InternalEList<?>)getNamespaces()).basicRemove(otherEnd, msgs);
			case JEE7Package.ARCHITECTURE__ARCHIVES:
				return ((InternalEList<?>)getArchives()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JEE7Package.ARCHITECTURE__BEANS:
				return getBeans();
			case JEE7Package.ARCHITECTURE__ENTITIES:
				return getEntities();
			case JEE7Package.ARCHITECTURE__NAMESPACES:
				return getNamespaces();
			case JEE7Package.ARCHITECTURE__ARCHIVES:
				return getArchives();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JEE7Package.ARCHITECTURE__BEANS:
				getBeans().clear();
				getBeans().addAll((Collection<? extends Bean>)newValue);
				return;
			case JEE7Package.ARCHITECTURE__ENTITIES:
				getEntities().clear();
				getEntities().addAll((Collection<? extends Entity>)newValue);
				return;
			case JEE7Package.ARCHITECTURE__NAMESPACES:
				getNamespaces().clear();
				getNamespaces().addAll((Collection<? extends Namespace>)newValue);
				return;
			case JEE7Package.ARCHITECTURE__ARCHIVES:
				getArchives().clear();
				getArchives().addAll((Collection<? extends Archive>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JEE7Package.ARCHITECTURE__BEANS:
				getBeans().clear();
				return;
			case JEE7Package.ARCHITECTURE__ENTITIES:
				getEntities().clear();
				return;
			case JEE7Package.ARCHITECTURE__NAMESPACES:
				getNamespaces().clear();
				return;
			case JEE7Package.ARCHITECTURE__ARCHIVES:
				getArchives().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JEE7Package.ARCHITECTURE__BEANS:
				return beans != null && !beans.isEmpty();
			case JEE7Package.ARCHITECTURE__ENTITIES:
				return entities != null && !entities.isEmpty();
			case JEE7Package.ARCHITECTURE__NAMESPACES:
				return namespaces != null && !namespaces.isEmpty();
			case JEE7Package.ARCHITECTURE__ARCHIVES:
				return archives != null && !archives.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ArchitectureImpl
