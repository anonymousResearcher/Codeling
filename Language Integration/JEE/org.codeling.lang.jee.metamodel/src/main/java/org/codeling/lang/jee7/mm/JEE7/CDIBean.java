/**
 */
package org.codeling.lang.jee7.mm.JEE7;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CDI Bean</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsRequestScoped <em>Is Request Scoped</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsSessionScoped <em>Is Session Scoped</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsApplicationScoped <em>Is Application Scoped</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsConversationScoped <em>Is Conversation Scoped</em>}</li>
 * </ul>
 *
 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getCDIBean()
 * @model
 * @generated
 */
public interface CDIBean extends Bean {
	/**
	 * Returns the value of the '<em><b>Is Request Scoped</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Request Scoped</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Request Scoped</em>' attribute.
	 * @see #setIsRequestScoped(boolean)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getCDIBean_IsRequestScoped()
	 * @model default="false"
	 * @generated
	 */
	boolean isIsRequestScoped();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsRequestScoped <em>Is Request Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Request Scoped</em>' attribute.
	 * @see #isIsRequestScoped()
	 * @generated
	 */
	void setIsRequestScoped(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Session Scoped</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Session Scoped</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Session Scoped</em>' attribute.
	 * @see #setIsSessionScoped(boolean)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getCDIBean_IsSessionScoped()
	 * @model default="false"
	 * @generated
	 */
	boolean isIsSessionScoped();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsSessionScoped <em>Is Session Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Session Scoped</em>' attribute.
	 * @see #isIsSessionScoped()
	 * @generated
	 */
	void setIsSessionScoped(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Application Scoped</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Application Scoped</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Application Scoped</em>' attribute.
	 * @see #setIsApplicationScoped(boolean)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getCDIBean_IsApplicationScoped()
	 * @model default="false"
	 * @generated
	 */
	boolean isIsApplicationScoped();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsApplicationScoped <em>Is Application Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Application Scoped</em>' attribute.
	 * @see #isIsApplicationScoped()
	 * @generated
	 */
	void setIsApplicationScoped(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Conversation Scoped</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Conversation Scoped</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Conversation Scoped</em>' attribute.
	 * @see #setIsConversationScoped(boolean)
	 * @see org.codeling.lang.jee7.mm.JEE7.JEE7Package#getCDIBean_IsConversationScoped()
	 * @model default="false"
	 * @generated
	 */
	boolean isIsConversationScoped();

	/**
	 * Sets the value of the '{@link org.codeling.lang.jee7.mm.JEE7.CDIBean#isIsConversationScoped <em>Is Conversation Scoped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Conversation Scoped</em>' attribute.
	 * @see #isIsConversationScoped()
	 * @generated
	 */
	void setIsConversationScoped(boolean value);

} // CDIBean
