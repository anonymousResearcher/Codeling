/**
 */
package org.codeling.lang.jee7.mm.JEE7.impl;

import org.codeling.lang.jee7.mm.JEE7.JEE7Package;
import org.codeling.lang.jee7.mm.JEE7.SessionBean;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Session Bean</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.SessionBeanImpl#isIsStateless <em>Is Stateless</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.SessionBeanImpl#isIsStateful <em>Is Stateful</em>}</li>
 *   <li>{@link org.codeling.lang.jee7.mm.JEE7.impl.SessionBeanImpl#isIsSingleton <em>Is Singleton</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SessionBeanImpl extends BeanImpl implements SessionBean {
	/**
	 * The default value of the '{@link #isIsStateless() <em>Is Stateless</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsStateless()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_STATELESS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsStateless() <em>Is Stateless</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsStateless()
	 * @generated
	 * @ordered
	 */
	protected boolean isStateless = IS_STATELESS_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsStateful() <em>Is Stateful</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsStateful()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_STATEFUL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsStateful() <em>Is Stateful</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsStateful()
	 * @generated
	 * @ordered
	 */
	protected boolean isStateful = IS_STATEFUL_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsSingleton() <em>Is Singleton</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsSingleton()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_SINGLETON_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsSingleton() <em>Is Singleton</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsSingleton()
	 * @generated
	 * @ordered
	 */
	protected boolean isSingleton = IS_SINGLETON_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SessionBeanImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JEE7Package.Literals.SESSION_BEAN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsStateless() {
		return isStateless;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsStateless(boolean newIsStateless) {
		boolean oldIsStateless = isStateless;
		isStateless = newIsStateless;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JEE7Package.SESSION_BEAN__IS_STATELESS, oldIsStateless, isStateless));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsStateful() {
		return isStateful;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsStateful(boolean newIsStateful) {
		boolean oldIsStateful = isStateful;
		isStateful = newIsStateful;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JEE7Package.SESSION_BEAN__IS_STATEFUL, oldIsStateful, isStateful));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsSingleton() {
		return isSingleton;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsSingleton(boolean newIsSingleton) {
		boolean oldIsSingleton = isSingleton;
		isSingleton = newIsSingleton;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JEE7Package.SESSION_BEAN__IS_SINGLETON, oldIsSingleton, isSingleton));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JEE7Package.SESSION_BEAN__IS_STATELESS:
				return isIsStateless();
			case JEE7Package.SESSION_BEAN__IS_STATEFUL:
				return isIsStateful();
			case JEE7Package.SESSION_BEAN__IS_SINGLETON:
				return isIsSingleton();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JEE7Package.SESSION_BEAN__IS_STATELESS:
				setIsStateless((Boolean)newValue);
				return;
			case JEE7Package.SESSION_BEAN__IS_STATEFUL:
				setIsStateful((Boolean)newValue);
				return;
			case JEE7Package.SESSION_BEAN__IS_SINGLETON:
				setIsSingleton((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JEE7Package.SESSION_BEAN__IS_STATELESS:
				setIsStateless(IS_STATELESS_EDEFAULT);
				return;
			case JEE7Package.SESSION_BEAN__IS_STATEFUL:
				setIsStateful(IS_STATEFUL_EDEFAULT);
				return;
			case JEE7Package.SESSION_BEAN__IS_SINGLETON:
				setIsSingleton(IS_SINGLETON_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JEE7Package.SESSION_BEAN__IS_STATELESS:
				return isStateless != IS_STATELESS_EDEFAULT;
			case JEE7Package.SESSION_BEAN__IS_STATEFUL:
				return isStateful != IS_STATEFUL_EDEFAULT;
			case JEE7Package.SESSION_BEAN__IS_SINGLETON:
				return isSingleton != IS_SINGLETON_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isStateless: ");
		result.append(isStateless);
		result.append(", isStateful: ");
		result.append(isStateful);
		result.append(", isSingleton: ");
		result.append(isSingleton);
		result.append(')');
		return result.toString();
	}

} //SessionBeanImpl
