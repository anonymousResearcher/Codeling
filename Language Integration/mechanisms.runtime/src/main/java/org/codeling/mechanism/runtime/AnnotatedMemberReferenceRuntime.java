package org.codeling.mechanism.runtime;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.IntegrationMechanismRuntime;
import org.codeling.mechanism.runtime.common.ReferenceMechanismRuntime;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class AnnotatedMemberReferenceRuntime extends ReferenceMechanismRuntime {
	Class<? extends Annotation> referenceAnnotation;

	public AnnotatedMemberReferenceRuntime(TypeMechanismRuntime<?> owningRuntime,
			Class<? extends Annotation> referenceAnnotation,
			Class<? extends IntegrationMechanismRuntime> targetRuntimeClass, boolean instantiateTargets) {
		super(owningRuntime, targetRuntimeClass, instantiateTargets);
		this.referenceAnnotation = referenceAnnotation;
	}

	private List<Field> getReferenceAnnotationFields() {
		LinkedList<Field> fields = new LinkedList<>();
		final Class<?> source = owningTypeRuntime.getInstance().getClass();
		for (final Field f : source.getDeclaredFields())
			if (f.isAnnotationPresent(referenceAnnotation)) {
				fields.add(f);
			}

		return fields;
	}

	protected Class<?>[] getTargetClasses() throws IntegratedModelException {
		List<Field> fields = getReferenceAnnotationFields();
		List<Class<?>> targetClasses = new LinkedList<>();

		for (Field field : fields) {
			Class<?> fieldType = field.getType();
			if (fieldType.isArray())
				fieldType = fieldType.getComponentType();
			boolean isMarkerInterface = fieldType.isInterface();

			// Resolve the type if this is an array (for x..* marker interface
			// targets)
			if (isMarkerInterface) {
				// The field is a marker interface. So we expect an
				// annotation parameter "value" to state the concrete
				// type to instantiate
				final Annotation a = field.getAnnotation(referenceAnnotation);
				String parameterName = "value";
				Method annotationParameter = getAnnotationParameter(a, parameterName);
				Object annotationParameterValue = getAnnotationParameterValue(a, annotationParameter);

				boolean isMany = annotationParameter.getReturnType().isArray();
				if (isMany) {
					Class<?>[] parameterValues = (Class<?>[]) annotationParameterValue;
					for (Class<?> parameterValue : parameterValues)
						targetClasses.add(parameterValue);
				} else
					targetClasses.add((Class<?>) annotationParameterValue);
			} else {
				targetClasses.add(fieldType);
			}
		}
		return targetClasses.toArray(new Class<?>[targetClasses.size()]);
	}

	protected Object getAnnotationParameterValue(final Annotation a, Method annotationParameter)
			throws IntegratedModelException {
		try {
			return annotationParameter.invoke(a, new Object[0]);
		} catch (Exception e) {
			throw new IntegratedModelException(MessageFormat.format(
					"The annotation parameter [value()] of the annotation [{0}] could not be invoked.",
					referenceAnnotation.getName()));
		}
	}

	protected Method getAnnotationParameter(final Annotation a, String parameterName) throws IntegratedModelException {
		Method annotationParameter;
		try {
			annotationParameter = a.annotationType().getDeclaredMethod(parameterName, new Class[0]);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new IntegratedModelException(MessageFormat
					.format("The annotation [{0}] must have the parameter [value].", referenceAnnotation.getName()));
		}
		return annotationParameter;
	}

	public void setTargets(Object[] targets) throws IntegratedModelException {
		super.setTargets(targets);

		// Set the target in the field
		final List<Field> fields = getReferenceAnnotationFields();

		boolean isMarkerInterfaceAndMany = fields.size() == 1 && fields.get(0).getType().isArray();
		if (isMarkerInterfaceAndMany) {
			Field field = fields.get(0);
			Object[] targetArray = (Object[]) asArray(field.getType().getComponentType(), targets);
			set(owningTypeRuntime.getInstance(), field, targetArray);
		} else {
			for (Object target : targets) {
				for (Field field : fields) {
					if (field.getType().isAssignableFrom(target.getClass()))
						set(owningTypeRuntime.getInstance(), field, target);
				}
			}
		}
	}

	private <T> T[] asArray(Class<T> arrayType, Object[] instances) {
		@SuppressWarnings("unchecked")
		final T[] targetArray = (T[]) Array.newInstance(arrayType, instances.length);
		for (int i = 0; i < instances.length; i++)
			Array.set(targetArray, i, instances[i]);
		return targetArray;
	}
}
