package org.codeling.mechanism.runtime;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class ContainmentOperationRuntime<T> extends TypeMechanismRuntime<T> {
	Class<? extends Annotation> referenceAnnotation;
	private final TypeMechanismRuntime<T> owningTypeRuntime;

	public ContainmentOperationRuntime(TypeMechanismRuntime<T> owningTypeRuntime,
			Class<? extends Annotation> referenceAnnotation) {
		this.owningTypeRuntime = owningTypeRuntime;
		this.referenceAnnotation = referenceAnnotation;
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}

	public List<Method> getContainmentOperations() throws IntegratedModelException {
		final Class<?> source = getOwningTypeRuntime().getInstance().getClass();

		final LinkedList<Method> verifiedMethods = new LinkedList<>();
		for (final Method m : source.getDeclaredMethods())
			if (m.isAnnotationPresent(referenceAnnotation)) {
				verifiedMethods.add(m);
			}

		return verifiedMethods;
	}

	public Object invoke(String targetName) throws IntegratedModelException {
		final Object owningObject = getOwningTypeRuntime().getInstance();
		final Class<?> owningClass = owningObject.getClass();

		// Find the method
		Method method;
		try {
			method = owningClass.getDeclaredMethod(targetName);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new IntegratedModelException(
					MessageFormat.format("Could not find the Containment Operation [{0}()] of the source class [{1}].",
							targetName, owningClass.getName()),
					e);
		}

		// Invoke the method
		try {
			return method.invoke(owningObject, new Object[0]);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new IntegratedModelException(MessageFormat.format(
					"Could not invoke the Containment Operation [{0}()] of the source class [{1}].", targetName,
					owningClass.getName()), e);
		}
	}

	public Object invoke(String targetName, Object[] references) throws IntegratedModelException {
		final Object owningObject = getOwningTypeRuntime().getInstance();
		final Class<?> owningClass = owningObject.getClass();

		// Find the parameter types
		final Class<?>[] parameterTypes = new Class<?>[references.length];
		for (int i = 0; i < references.length; i++)
			parameterTypes[i] = references[i].getClass();

		// Find the method
		Method method;
		try {
			method = owningClass.getDeclaredMethod(targetName, parameterTypes);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new IntegratedModelException(
					MessageFormat.format("Could not find the Containment Operation [{0}()] of the source class [{1}].",
							targetName, owningClass.getName()),
					e);
		}

		// Invoke the method
		try {
			return method.invoke(owningObject, references);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new IntegratedModelException(MessageFormat.format(
					"Could not invoke the Containment Operation [{0}()] of the source class [{1}].", targetName,
					owningClass.getName()), e);
		}
	}

	protected Class<?>[] getTargetClasses() throws IntegratedModelException {
		// Does not instantiate its targets.
		return new Class<?>[0];
	}

	public Class<? extends Annotation> getReferenceAnnotation() {
		return referenceAnnotation;
	}

	public TypeMechanismRuntime<T> getOwningTypeRuntime() {
		return owningTypeRuntime;
	}

}
