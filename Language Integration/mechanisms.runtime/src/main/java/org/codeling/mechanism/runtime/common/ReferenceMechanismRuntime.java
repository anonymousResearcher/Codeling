package org.codeling.mechanism.runtime.common;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;

public abstract class ReferenceMechanismRuntime extends AttributeOrReferenceMechanismRuntime {

	private boolean instantiateTargets;
	private Class<? extends IntegrationMechanismRuntime> targetRuntimeClass;
	final LinkedHashMap<Object, IntegrationMechanismRuntime> targetRuntimes = new LinkedHashMap<Object, IntegrationMechanismRuntime>();
	private Object[] targets;

	public ReferenceMechanismRuntime(TypeMechanismRuntime owningTypeRuntime,
			Class<? extends IntegrationMechanismRuntime> targetRuntimeClass, boolean instantiateTargets) {
		super(owningTypeRuntime);
		this.targetRuntimeClass = targetRuntimeClass;
		this.instantiateTargets = instantiateTargets;
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();

		Object[] targets = {};
		if (instantiateTargets) {
			final Class<?>[] targetClasses = getTargetClasses();
			targets = instantiateTargets(targetClasses);
		} else {
			targets = findTargets();
		}
		setTargets(targets);
	}

	protected abstract Class<?>[] getTargetClasses() throws IntegratedModelException;

	private Object[] findTargets() throws IntegratedModelException {
		final LinkedList<Object> targets = new LinkedList<Object>();
		final Set<Object> instances = Runtimes.getInstance().keySet();
		final Class<?>[] targetClasses = getTargetClasses();
		for (final Object instance : instances) {
			for (final Class<?> targetClass : targetClasses) {
				if (instance.getClass() == targetClass) {
					targets.add(instance);
				}
			}
		}
		return targets.toArray();
	}

	protected TypeMechanismRuntime instantiateTypeMechanismRuntime(final Class<?> mechanismClass)
			throws InstantiationException, IllegalAccessException, InvocationTargetException, IntegratedModelException {
		@SuppressWarnings("unchecked")
		final Constructor<TypeMechanismRuntime>[] constructors = (Constructor<TypeMechanismRuntime>[]) targetRuntimeClass
				.getConstructors();
		if (constructors.length != 1)
			throw new IllegalArgumentException("The targetRuntimeClass must have exactly one constructor. The class "
					+ targetRuntimeClass.getName() + " has " + constructors.length + " constructors.");

		boolean accessible = constructors[0].isAccessible();
		if(!accessible) constructors[0].setAccessible(true);
		final TypeMechanismRuntime runtime = constructors[0].newInstance(mechanismClass);
		if(!accessible) constructors[0].setAccessible(accessible);
		runtime.initialize();

		return runtime;
	}

	public Object[] instantiateTargets(Class<?>[] targetClasses) throws IntegratedModelException {
		// Instantiate all target classes and their runtimes
		LinkedList<Object> instances = new LinkedList<>();
		for (final Class<?> targetClass : targetClasses) {
			TypeMechanismRuntime runtime;
			try {
				runtime = instantiateTypeMechanismRuntime(targetClass);
			} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
				throw new IntegratedModelException(MessageFormat.format(
						"Cannot instantiate the type mechsnism runtime for the target class [{0}].",
						targetClass.getName()), e);
			}
			instances.add(runtime.getInstance());
		}

		for (Object instance : instances)
			((TypeMechanismRuntime) Runtimes.getInstance().get(instance)).initializeContainments();
		
		for (Object instance : instances)
			((TypeMechanismRuntime) Runtimes.getInstance().get(instance)).initializeCrossReferences();
		
		return instances.toArray();
	}

	public Object[] getTargets() {
		return targets;
	}

	public void setTargets(Object[] targets) throws IntegratedModelException {
		targetRuntimes.clear();
		this.targets = targets;

		// Set target runtimes
		for (final Object target : targets) {
			final IntegrationMechanismRuntime targetRuntime = Runtimes.getInstance().get(target);
			targetRuntimes.put(target, targetRuntime);
		}
	}

	public IntegrationMechanismRuntime getTargetRuntime(Object target) {
		return targetRuntimes.get(target);
	}

	public IntegrationMechanismRuntime[] getTargetRuntimes() {
		return targetRuntimes.values().toArray(new IntegrationMechanismRuntime[targetRuntimes.size()]);
	}

	public LinkedHashMap<Object, IntegrationMechanismRuntime> getTargetRuntimeMap() {
		return targetRuntimes;
	}
}