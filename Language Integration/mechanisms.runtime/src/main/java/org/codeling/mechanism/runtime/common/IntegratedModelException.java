package org.codeling.mechanism.runtime.common;

public class IntegratedModelException extends Exception {

	private static final long serialVersionUID = -1822567914374521728L;

	public IntegratedModelException(String msg) {
		super(msg);
	}

	public IntegratedModelException(String msg, Throwable e) {
		super(msg, e);
	}
}
