package org.codeling.mechanism.runtime;

import java.text.MessageFormat;

import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.Runtimes;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class StaticInterfaceImplementationRuntime<I, T extends I> extends TypeMechanismRuntime<T> {

	protected final Class<I> staticInterfaceClass;
	protected final Class<T> implementingClass;

	public StaticInterfaceImplementationRuntime(Class<T> implementingClass, Class<I> staticInterfaceClass) {
		super();
		this.implementingClass = implementingClass;
		this.staticInterfaceClass = staticInterfaceClass;
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
		verify();

		try {
			instance = implementingClass.newInstance();
			Runtimes.getInstance().put(instance, this);
		} catch (final Exception e) {
			throw new IntegratedModelException(MessageFormat.format(
					"Could not instantiate an object of the class [{0}], which implements the static interface [{1}]",
					implementingClass.getName(), staticInterfaceClass.getName()), e);
		}
	}

	private void verify() throws IntegratedModelException {
		if (!isInterfaceOf(staticInterfaceClass, implementingClass))
			throw new IntegratedModelException(MessageFormat.format(
					"Tried to use [{0}] in the Static Interface Implementation mechanism , but it does not implement the interface [{1}].",
					implementingClass.getCanonicalName(), staticInterfaceClass.getCanonicalName()));
	}

}
