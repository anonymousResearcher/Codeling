package org.codeling.mechanism.runtime;

import java.text.MessageFormat;

import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.Runtimes;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class MarkerInterfaceRuntime<I, T extends I> extends TypeMechanismRuntime<T> {

	protected final Class<I> markerInterfaceClass;
	protected final Class<T> implementingClass;

	public MarkerInterfaceRuntime(Class<T> implementingClass, Class<I> markerInterfaceClass) {
		super();
		this.implementingClass = implementingClass;
		this.markerInterfaceClass = markerInterfaceClass;
	}

	public void initialize(T instance) throws IntegratedModelException {
		super.initialize();
		verify();

		try {
			this.instance = instance;
			Runtimes.getInstance().put(instance, this);
		} catch (final Exception e) {
			throw new IntegratedModelException(MessageFormat.format(
					"Could not instantiate an object of the class [{0}], which implements the marker interface [{1}]",
					implementingClass.getName(), markerInterfaceClass.getName()), e);
		}
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
		verify();

		try {
			instance = implementingClass.newInstance();
			Runtimes.getInstance().put(instance, this);
		} catch (final Exception e) {
			throw new IntegratedModelException(MessageFormat.format(
					"Could not instantiate an object of the class [{0}], which implements the marker interface [{1}]",
					implementingClass.getName(), markerInterfaceClass.getName()), e);
		}
	}

	private void verify() throws IntegratedModelException {
		if (!isInterfaceOf(markerInterfaceClass, implementingClass))
			throw new IntegratedModelException(
					MessageFormat.format("The class [{0}] must implement the marker interface [{1}].",
							implementingClass.getCanonicalName(), markerInterfaceClass.getCanonicalName()));
	}

}
