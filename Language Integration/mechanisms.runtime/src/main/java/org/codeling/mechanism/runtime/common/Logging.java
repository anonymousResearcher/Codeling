package org.codeling.mechanism.runtime.common;

import java.text.MessageFormat;

public class Logging {
	// ---------- Logging ----------
	public static enum LogLevel {
		NONE, MODEL, MODEL_DEBUG, INFRASTRUCTURE
	}

	public static LogLevel logLevel = LogLevel.INFRASTRUCTURE;

	public static void log(LogLevel level, String message) {
		if (isLoggable(level))
			System.out.println(MessageFormat.format("[{0}] - {1}", level.name(), message));
	}

	private static boolean isLoggable(LogLevel message_level) {
		switch (logLevel) {
		case NONE:
			return false;
		case MODEL:
			return message_level == LogLevel.MODEL;
		case MODEL_DEBUG:
			return message_level == LogLevel.MODEL || message_level == LogLevel.MODEL_DEBUG;
		default: // INFRASTRUCTURE
			return true;
		}
	}
}
