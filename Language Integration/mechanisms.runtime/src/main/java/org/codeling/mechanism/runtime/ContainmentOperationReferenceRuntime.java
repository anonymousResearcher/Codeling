package org.codeling.mechanism.runtime;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.IntegrationMechanismRuntime;
import org.codeling.mechanism.runtime.common.ReferenceMechanismRuntime;
import org.codeling.mechanism.runtime.common.Runtimes;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class ContainmentOperationReferenceRuntime<T> extends ReferenceMechanismRuntime {

	private ContainmentOperationRuntime<T> owningContainmentOperationRuntime;
	private String referenceName;

	public ContainmentOperationReferenceRuntime(ContainmentOperationRuntime<T> owningContainmentOperationRuntime,
			String referenceName, Class<? extends IntegrationMechanismRuntime> targetRuntimeClass,
			boolean instantiateTargets) {
		super(owningContainmentOperationRuntime.getOwningTypeRuntime(), targetRuntimeClass, instantiateTargets);
		this.owningContainmentOperationRuntime = owningContainmentOperationRuntime;
		this.referenceName = referenceName;
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
	}

	@Override
	protected Class<?>[] getTargetClasses() throws IntegratedModelException {
		LinkedList<Class<?>> targetClasses = new LinkedList<>();
		List<Method> methods = owningContainmentOperationRuntime.getContainmentOperations();
		for (Method method : methods) {
			Annotation annotation = method.getAnnotation(owningContainmentOperationRuntime.getReferenceAnnotation());
			Method annotationParameter;
			try {
				annotationParameter = owningContainmentOperationRuntime.getReferenceAnnotation()
						.getDeclaredMethod(referenceName);
				Object parameterValue = annotationParameter.invoke(annotation, new Object[0]);
				targetClasses.add((Class<?>) parameterValue);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				throw new IntegratedModelException(
						MessageFormat.format("Could not get annotation parameter value [{0}] for annotation [{1}]",
								referenceName, annotation.getClass().getName()),
						e);
			}

		}
		return targetClasses.toArray(new Class<?>[targetClasses.size()]);
	}

	/**
	 * Returns the instance of the targeted element by this reference, in the
	 * operation with the given name.
	 * 
	 * @throws IntegratedModelException
	 */
	public TypeMechanismRuntime<?>[] getTargetRuntime(String targetName) throws IntegratedModelException {
		List<Method> methods = owningContainmentOperationRuntime.getContainmentOperations();
		for (Method method : methods) {
			if (method.getName().equals(targetName)) {
				Annotation annotation = method
						.getAnnotation(owningContainmentOperationRuntime.getReferenceAnnotation());
				Method annotationParameter;
				try {
					annotationParameter = owningContainmentOperationRuntime.getReferenceAnnotation()
							.getDeclaredMethod(referenceName);
					Object parameterValue = annotationParameter.invoke(annotation, new Object[0]);
					IntegrationMechanismRuntime actualTarget = Runtimes.getInstance().get((Class<?>) parameterValue);

					// TODO: Here we should also handle arrays.
					return new TypeMechanismRuntime[] { (TypeMechanismRuntime<?>) actualTarget };
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					throw new IntegratedModelException(
							MessageFormat.format("Could not get annotation parameter value [{0}] for annotation [{1}]",
									referenceName, annotation.getClass().getName()),
							e);
				}
			}
		}
		return null;
	}
}
