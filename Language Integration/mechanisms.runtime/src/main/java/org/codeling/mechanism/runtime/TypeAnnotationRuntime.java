package org.codeling.mechanism.runtime;

import java.lang.annotation.Annotation;
import java.text.MessageFormat;

import org.codeling.mechanism.runtime.common.IntegratedModelException;
import org.codeling.mechanism.runtime.common.Runtimes;
import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

public class TypeAnnotationRuntime<T> extends TypeMechanismRuntime<T> {

	private final Class<T> implementingClass;
	private final Class<? extends Annotation> typeAnnotation;

	public TypeAnnotationRuntime(Class<T> implementingClass, Class<? extends Annotation> typeAnnotation) {
		this.implementingClass = implementingClass;
		this.typeAnnotation = typeAnnotation;
	}

	public void initialize(T instance) throws IntegratedModelException {
		super.initialize();
		verifyTypeAnnotationMechanism();

		try {
			this.instance = instance;
			Runtimes.getInstance().put(instance, this);
		} catch (final Exception e) {
			throw new IntegratedModelException(MessageFormat.format(
					"Could not instantiate an object of the class [{0}], which is marked with a type annotation [{1}]",
					implementingClass.getName(), typeAnnotation.getName()), e);
		}
	}

	@Override
	public void initialize() throws IntegratedModelException {
		super.initialize();
		verifyTypeAnnotationMechanism();

		try {
			instance = implementingClass.getConstructor(new Class[0]).newInstance(new Object[0]);
			Runtimes.getInstance().put(instance, this);
		} catch (final Exception e) {
			throw new IntegratedModelException(MessageFormat.format(
					"Could not instantiate an object of the class [{0}], which is marked with a type annotation [{1}]",
					implementingClass.getName(), typeAnnotation.getName()), e);
		}
	}

	private void verifyTypeAnnotationMechanism() throws IntegratedModelException {
		if (!implementingClass.isAnnotationPresent(typeAnnotation))
			throw new IntegratedModelException(MessageFormat.format("The class [{0}] must have the annotation [{1}].",
					implementingClass.getCanonicalName(), typeAnnotation.getCanonicalName()));
	}

}
