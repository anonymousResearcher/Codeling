package org.codeling.mechanism.runtime.common;

public abstract class AttributeOrReferenceMechanismRuntime extends IntegrationMechanismRuntime {

	/**
	 * The runtime of the object that owns the attribute or reference
	 */
	protected final TypeMechanismRuntime owningTypeRuntime;

	/**
	 * @param owningTypeRuntime
	 *            The runtime of the object that owns the attribute or reference
	 */
	public AttributeOrReferenceMechanismRuntime(TypeMechanismRuntime owningTypeRuntime) {
		this.owningTypeRuntime = owningTypeRuntime;
	}

	public TypeMechanismRuntime getOwningTypeRuntime() {
		return owningTypeRuntime;
	}
}
