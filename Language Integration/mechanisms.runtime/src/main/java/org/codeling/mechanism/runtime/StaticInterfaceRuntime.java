package org.codeling.mechanism.runtime;

import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

/**
 * As the Static Interface mechanism employs an interface declaration as
 * representative, the execution semantics can only be implemented in the
 * execution runtime, or in a class that implements the interface using the
 * Static Interface Implementation mechanism.
 */
public class StaticInterfaceRuntime<I> extends TypeMechanismRuntime<I> {

}
