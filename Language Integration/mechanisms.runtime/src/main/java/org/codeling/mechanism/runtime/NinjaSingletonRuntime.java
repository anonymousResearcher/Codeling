package org.codeling.mechanism.runtime;

import org.codeling.mechanism.runtime.common.TypeMechanismRuntime;

/**
 * As the Ninja Singleton mechanism employs no element as representative, but
 * implies the existence of exactly one instance, the execution semantics can
 * only be implemented in the execution runtime.
 */
public class NinjaSingletonRuntime extends TypeMechanismRuntime<Object> {

}
